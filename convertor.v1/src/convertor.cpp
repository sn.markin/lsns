// convertor.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "load_nsm.h"

int main(int argc, char *argv[])
{
	//cout << argv[1] << endl;
	if( argc != 2 ){
		cout << "ERROR!::Please specify the name of nsm file" << endl;
		return 0;
	}
	string str, strs;
	strs = str = argv[1];
	int idx = strs.find(".nsm");
	if (idx != std::string::npos) {
		strs.erase(idx, strs.size() - idx);
		strs = strs + ".nns";
		nsm_model model;
		if (model.load_model(str.c_str())) {
			model.save_model(strs.c_str());
		};
	}
	else {
		cout << "ERROR!::Only nsm files could be converted to nns files." << endl;
		return 0;
	}

	//if( model.load_model("peripheralChemo.nsm")){
	//	model.save_model("new_peripheralChemo.nns");
	//}
	//getchar();
	return 1;
}


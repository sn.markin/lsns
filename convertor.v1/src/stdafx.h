// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#if !defined( __LINUX__ ) /*check if macros defined in the command line*/
	#define __LINUX__
#endif /* __LINUX__ */

#include "targetver.h"

#include <stdio.h>

#include "stdafx.h"

#include "load_nsm.h"

#define GTYPE 0
#define MHTYPE 1
#define ITYPE 2
#define DSTATUS 3
#define PCTG 100

float eds( float rtfz, float out, float in )
{
	return rtfz*log( out/in );
}

hhnpair<float> hack1( hhnpair<float>& e, const string &name, vector<netunit<iondata> > &ions )
{
	if( name[0] == 'N' && name[1] == 'a' ){
		for( size_t i = 0; i < ions.size(); ++i ){
			if( ions[i].Name == "Na" ){
				e = ions[i]().Eds;
			}
		}
	}
	if( name[0] == 'K' ){
		for( size_t i = 0; i < ions.size(); ++i ){
			if( ions[i].Name == "K" ){
				e = ions[i]().Eds;
			}
		}
	}
	if( name[0] == 'C' && name[1] == 'a' ){
		for( size_t i = 0; i < ions.size(); ++i ){
			if( ions[i].Name == "Ca" ){
				e = ions[i]().Eds;
			}
		}
	}
	return e;
}

bool hack2( const string &name, vector<netunit<chandata> > &chans )
{
	if( name[0] == 'C' && name[1] == 'a' ){
		for( size_t i = 0; i < chans.size(); ++i ){
			if( chans[i].Name.find( "Ca" ) != string::npos ){
				return true;
			}
		}
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////
// class simdata
//--- constructors/destructor
//--- public methods
simdata &simdata::operator = ( const simdata &data )
{
	Seed = data.Seed;
	SimStep = data.SimStep;
	SimTime  = data.SimTime;
	UpdatingTime = data.UpdatingTime;
	Freq = data.Freq;
	HistBin = data.HistBin;
	HistNorm = data.HistNorm;
	BeginView = data.BeginView;
	EndView = data.EndView;
	TimeFactor = data.TimeFactor;
	Promt = data.Promt;
	return *this;
};

bool simdata::loadpar( istream &file, const string &parname )
{
	string str;
	if( parname == "Seed"){
		file >> str >> Seed;
		return true;
	} 
	else if( parname == "SimStep"){
		file >> str >> SimStep;
		return true;
	}
	else if( parname == "SimNNStep"){
		file >> str >> SimStep;
		return true;
	}
	else if( parname == "SimTime"){
		file >> str >> SimTime;
		return true;
	}
	else if( parname == "UpdatingTime"){
		file >> str >> UpdatingTime;
		return true;
	}
	else if( parname == "Freq"){
		file >> str >> Freq;
		return true;
	}
	else if( parname == "HistBin"){
		file >> str >> HistBin;
		return true;
	}
	else if( parname == "HistNorm"){
		file >> str >> HistNorm;
		return true;
	}
	else if( parname == "BeginView"){
		file >> str >> BeginView;
		return true;
	}
	else if( parname == "EndView"){
		file >> str >> EndView;
		return true;
	}
	else if( parname == "TimeFactor"){
		file >> str >> TimeFactor;
		return true;
	}
	return false;
}

bool simdata::validate( const string &type )
{
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// class iondata
//--- constructors/destructor
//--- public methods
iondata &iondata::operator = ( const iondata &ion )
{
	In = ion.In;
	In0 = ion.In0;
	Out = ion.Out;
	T = ion.T;
	Eds = ion.Eds;
	Kp = ion.Kp;
	Rpmp = ion.Rpmp;
	Rch = ion.Rch;
	B = ion.B;
	K = ion.K;
	IType = ion.IType;
	PType = ion.PType;
	return *this;
};

bool iondata::loadpar( istream &file, const string &parname )
{
	string str;
	if( parname == "In"){
		file >> str >> In;
		return true;
	}
	else if( parname == "In0"){
		file >> str >> In0;
		In0.Y = 1;
		return true;
	}
	else if( parname == "Rpmp"){
		file >> str >> Rpmp;
		return true;
	}
	else if( parname == "Rch"){
		file >> str >> Rch;
		return true;
	}
	else if( parname == "Out"){
		file >> str >> Out;
		return true;
	}
	else if( parname == "T"){
		file >> str >> T;
		return true;
	}
	else if( parname == "Eds"){
		file >> str >> Eds;
		return true;
	}
	else if( parname == "B"){ // Ca dynamics
		file >> str >> B;
		return true;
	}
	else if( parname == "K"){ // Ca dynamics
		file >> str >> K;
		return true;
	}
	return false;
}

bool iondata::validate( const string &type )
{
	if( type == "Na" ){
		IType = 1;
		PType = 0;
		Z = 1;
		Eds.X = eds( RTF/Z, Out.X, In.X );
	}
	else if( type == "K" ){
		IType = 2;
		PType = 0;
		Z = 1;
		Eds.X = eds( RTF/Z, Out.X, In.X );
	}
	else if( type == "Ca" ){
		IType = 3;
		PType = 3;
		Z = 2;
		Eds.X = eds( RTF/Z, Out.X, In.X );
		Rch = 0.0045f;
		Kp = 0.0f;
	}
	else if( type == "Cl" ){
		IType = 4;
		PType = 0;
		Z = -1;
		Eds.X = eds( RTF/Z, Out.X, In.X );
	}
	else{
		IType = 0;
		PType = 0;
		Z = 1;
		string mess = "unknown ions type:" + type;
		message( mess.c_str(), "Warning" );
		return false;
	}
	return true;
}

bool iondata::save( ostream &file )
{
	file << "IonType = " << IType << endl;
	file << "PumpType = " << PType << endl;	
	file << "Cin = " << In << endl;
	file << "Cout = " << Out << endl;
	file << "Z = " << Z << endl;
	if (In0.Y == 1) file << "InEq = " << In0 << endl; //by TK 7.11.2016
	else file << "InEq = " << In << endl;
	file << "t = " << T << endl;		 // time constant
	file << "Rpmp = " << Rpmp << endl;
	file << "Rch = " << Rch << endl;
	file << "Kp = " << Kp << endl;
	file << "K = " << K << endl;
	file << "B = " << B << endl;

	return true;
}

/////////////////////////////////////////////////////////////////////////////
// class syndata
//--- constructors/destructor
//--- public methods
syndata &syndata::operator = ( const syndata &syn )
{
	Eds = syn.Eds;
	Gmax =syn.Gmax;
	Ts = syn.Ts;
	Vs = syn.Vs;
	return *this;
}

bool syndata::loadpar( istream &file, const string &parname )
{
	string str;
	if( parname == "K" || parname == "Gmax"){
		file >> str >> Gmax;
		return true;
	}
	else if( parname == "Eds"){
		file >> str >> ws;
		getline( file, Eds );
		return true;
	}
	else if( parname == "TimeCostant"){
		file >> str >> Ts;
		return true;
	}
	else if( parname == "Amplitude"){
		file >> str >> Vs;
		return true;
	}
	return false;
}

bool syndata::validate( const string &type )
{
	if( !( type == "Excitatory" || type == "Excitatory2" || type == "Inhibitory I" || type == "Inhibitory II" )){
		return false;
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// class dsyndata
//--- constructors/destructor
//--- public methods
dsyndata &dsyndata::operator = ( const dsyndata &dsyn )
{
	Kdep = dsyn.Kdep;
	Tdep = dsyn.Tdep;
	return *this;
}

bool dsyndata::loadpar( istream &file, const string &parname )
{
	string str;
	if( parname == "Amplitude"){
		file >> str >> Kdep;
		return true;
	}
	else if( parname == "TimeCostant"){
		file >> str >> Tdep;
		return true;
	}
	return false;
}

bool dsyndata::validate( const string &type )
{
	if( !( type == "Excitatory" || type == "Excitatory2" || type == "Inhibitory I" || type == "Inhibitory II" )){
		return false;
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// class dsyndata
//--- constructors/destructor
//--- public methods
ssyndata &ssyndata::operator = ( const ssyndata &ssyn )
{
	Hv = ssyn.Hv;
	Slp = ssyn.Slp;
	return *this;
}

bool ssyndata::loadpar( istream &file, const string &parname )
{
	string str;
	if( parname == "Hv"){
		file >> str >> Hv;
		return true;
	}
	else if( parname == "Slp"){
		file >> str >> Slp;
		return true;
	}
	return false;
}

bool ssyndata::validate( const string &type )
{
	if( !( type == "Excitatory" || type == "Excitatory2" || type == "Inhibitory I" || type == "Inhibitory II" )){
		return false;
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// class chandata
//--- constructors/destructor
static const char *ChannelNames[] = {
	"Na fast",
	"NaP channel (generic)",
	"K",
	"KCa",
	"CaL",
	"Leak",
};

//--- public methods
chandata &chandata::operator = ( const chandata &chdat )
{
	M = chdat.M;
	H = chdat.H;
	Gmax = chdat.Gmax;
	Eds = chdat.Eds;
	Pcl = chdat.Pcl;
	Pna = chdat.Pna;
	Tm = chdat.Tm;
	Th = chdat.Th;
	t0m = chdat.t0m; // by TK 7.11.2016
	t0h = chdat.t0h; // by TK 7.11.2016
	V12m = chdat.V12m;
	V12h = chdat.V12m;
	Slpm = chdat.Slpm;
	Slph = chdat.Slph;
	V12tm = chdat.V12tm;
	V12th = chdat.V12tm;
	Slptm = chdat.Slptm;
	Slpth = chdat.Slpth;
	PowM = chdat.PowM;
	PowH = chdat.PowH;
	Vg = chdat.Vg;
	Family = chdat.Family;
	GType = chdat.GType;
	MHType = chdat.MHType;
	IonType = chdat.IonType;
	Dstatus = chdat.Dstatus;
	return *this;
}

bool chandata::loadpar( istream &file, const string &parname )
{
	string str;
	if( parname == "M" || parname == "Mna" || parname == "Mnap" || parname == "Mk" || parname == "Mcal" || parname == "Mkca" ){
		file >> str >> M;
		return true;
	}
	else if( parname == "H" || parname == "Hna" || parname == "Hnap" || parname == "Hk" ){
		file >> str >> H;
		return true;
	}
	//else if (parname == "E" || parname == "Eleak") {
	else if( parname == "E"  || parname == "Eleak" || parname == "Eds"){ //Added by Taegyo 12.14.2015
		file >> str >> Eds;
		return true;
	}
	else if( parname == "Gmax" ){
		file >> str >> Gmax;
		return true;
	}
	else if( parname == "Tm" || parname ==  "Tkca" ){
		file >> str >> Tm;
		return true;
	}
	else if( parname == "Th" ){
		file >> str >> Th;
		return true;
	}
	//else if (parname == "V12m") {
	else if( parname == "V12m" || parname == "Hvm" ){ //Added by Taegyo 12.14.2015
		file >> str >> V12m;
		return true;
	}
	//else if (parname == "V12h") {
	else if( parname == "V12h" || parname == "Hvh" ){ //Added by Taegyo 12.14.2015
		file >> str >> V12h;
		return true;
	}
	else if( parname == "Slpm" || parname ==  "Km" ){
		file >> str >> Slpm;
		return true;
	}
	else if( parname == "Slph" || parname ==  "Kh" ){
		file >> str >> Slph;
		return true;
	}
	//else if( parname == "V12tm" ){
	else if (parname == "V12tm" || parname == "Hvtm") { //Added by Taegyo 12.14.2015
		file >> str >> V12tm;
		return true;
	}
	//else if (parname == "V12th") {
	else if( parname == "V12th" || parname == "Hvth"){ //Added by Taegyo 12.14.2015
		file >> str >> V12th;
		return true;
	}
	else if( parname == "Slptm" || parname ==  "Ktm" ){
		file >> str >> Slptm;
		return true;
	}
	else if( parname == "Slpth" || parname ==  "Kth" ){
		file >> str >> Slpth;
		return true;
	}
	else if( parname == "PowM" || parname ==  "M_power" ){
		file >> str >> PowM;
		return true;
	}
	else if( parname == "PowH" || parname ==  "H_power" ){
		file >> str >> PowH;
		return true;
	}
	else if( parname == "Pcl" ){
		file >> str >> Pcl;
		return true;
	}
	else if( parname == "Pna" ){
		file >> str >> Pna;
		return true;
	}
	else if( parname == "AdjustableLeak" ){
		file >> str >> Vg;
		return true;
	}
	return false;
}

bool chandata::validate( const string &type )
{
	if( Slptm.X == 0.f ){
		cout << "WARNING! Issue in calculation of time constant for activation: slope is zero" << endl;
		Tm.X = Tm.Y = 0.f;
		Slptm.X = 1.f;
	}
	if( Slpth.X == 0.f ){
		cout << "WARNING! Issue in calculation of time constant for inactivation: slope is zero" << endl;
		Th.X = Th.Y = 0.f;
		Slpth.X = 1.f;
	}
	return true;
}

bool chandata::save( ostream &file, const string &name, int id )
{
	file << "Name = " << name << endl;
	//file << "ID = " << id << endl;
	file << "g = " << Gmax.X << " (" << Gmax.Y*PCTG << ")" << endl;
	file << "E = " << Eds.X << " (" << Eds.Y*PCTG << ")" << endl;
	if( Family == "generic" ){

		file << "mpower = "<< PowM.X << endl;
		file << "hpower = "<< PowH.X << endl;
		file << "M0 = " << M << endl;
		file << "H0 = " << H << endl;
		//file << "t0m = " << 0 << endl; 
		//file << "t0h = " << 0 << endl;
		file << "t0m = " << t0m << endl; //Added on 7.11.2016 by TK
		file << "t0h = " << t0h << endl; //Added on 7.11.2016 by TK
		file << "Vhfm = "<< V12m.X << " (" << V12m.Y*PCTG << ")" << endl;
		file << "km = "<< Slpm.X << " (" << Slpm.Y*PCTG << ")" <<endl;
		file << "tm = "<< Tm.X << " (" << Tm.Y*PCTG << ")" << endl;
		file << "ktm = "<< Slptm.X << " (" << Slptm.Y*PCTG << ")" << endl;

		file << "Vhfh = "<< V12h.X << " (" << V12h.Y*PCTG << ")" << endl;
		file << "kh = "<< Slph.X << " (" << Slph.Y*PCTG << ")" <<endl;
		file << "th = "<< Th.X << " (" << Th.Y*PCTG << ")" << endl;
		file << "kth = "<< Slpth.X << " (" << Slpth.Y*PCTG << ")" << endl;

	}
	else if( Family == "ab-type" ){

		file << "mpower = " << PowM.X << endl;
		file << "hpower = " << PowH.X << endl;
		file << "M0 = " << M << endl;
		file << "H0 = " << H << endl;

		file << "t0 = " << 0 << endl;
		file << "tmax = " << 1 << endl;

		file << "Aa = " << -0.01 << " (0)" << endl;
		file << "Ba = " << -0.44 << " (0)" << endl;
		file << "Ca = " << 44 << " (0)" << endl;
		file << "Da = " << -5 << " (0)" << endl;
		file << "Ea = " << -1 << " (0)" << endl;

		file << "Ab = " << 0 << " (0)" << endl;
		file << "Bb = " << 0.17 << " (0)" << endl;
		file << "Cb = " << 49 << " (0)" << endl;
		file << "Db = " << 40 << " (0)" << endl;
		file << "Eb = " << 0 << " (0)" << endl;

	}
	else if( Family == "KCA" ){

		file << "mpower = "<< PowM.X << endl;
		file << "hpower = " << PowH.X << endl;
		file << "M0 = " << M << endl;
		file << "H0 = " << H << endl;
		file << "A = "<< 0.5 << " (0)" << endl;
		file << "B = "<< 1 << " (0)" << endl;
		file << "L = "<< 2 << " (0)" << endl;
		file << "G = "<< 1 << " (0)" << endl;
		file << "Vhf = " << 0 << " (0)" << endl;
		file << "slp = " << 1 << " (0)" << endl;
		file << "t0 = " << 0 << endl;
		file << "tmax = "<< Tm << endl;

	}
	else{
		return false;
	}
	return true;
}

int chandata::type( const string &name , int opt)
{
	Family = "";
	int nChan = sizeof( ChannelNames )/sizeof( char * );
	for( int i = 0; i < nChan; ++i ){
		if( name == ChannelNames[i] ){
			t0m(0, 0); //Added on 7.11.2016 by TK
			t0h(0, 0); //Added on 7.11.2016 by TK
			switch( i ){
				case 0: // Na fast
					Family = "generic";
					GType = 3;
					MHType = 0; //m & h
					IonType = 1; //Na
					Dstatus = 0; //Off
					PowM( 3, 0 );
					PowH( 1, 0 );
					V12m( -43.8f, 0); //by Taegyo 12.14.2105
					Slpm(-6.f, 0);
					V12h(-67.5f, 0); //by Taegyo 12.14.2105
					Slph( 10.8f, 0 );
					Tm( 0.252f, 0 );
					Th( 8.456f, 0);
					Slptm( 14.f, 0 );
					Slpth( 12.8f, 0 );
					switch (opt) {
					case 0:
						return GType;
					case 1:
						return MHType;
					case 2:
						return IonType;
					case 3:
						return Dstatus;
					}					
				case 1: // NaP channel (generic)
					Family = "generic";
					GType = 3;
					MHType = 0; //m & h
					IonType = 1; //Na
					Dstatus = 0; //Off
					switch (opt) {
					case 0:
						return GType;
					case 1:
						return MHType;
					case 2:
						return IonType;
					case 3:
						return Dstatus;
					}
				case 2: // K 
					Family = "ab-type";
					GType = 7;
					MHType = 1; //m only
					IonType = 2; //K
					Dstatus = 0; //Off
					PowM( 4, 0 );
					PowH( 0, 0);
					Tm( 1, 0 );
					switch (opt) {
					case 0:
						return GType;
					case 1:
						return MHType;
					case 2:
						return IonType;
					case 3:
						return Dstatus;
					}
				case 3: // KCa
					Family = "KCA";
					GType = 9;
					MHType = 1; //m only
					IonType = 2; //K
					Dstatus = 0; //Off
					PowM( 2, 0 );
					PowH( 0, 0);
					//Tm.X *= 1000.f;
					switch (opt) {
					case 0:
						return GType;
					case 1:
						return MHType;
					case 2:
						return IonType;
					case 3:
						return Dstatus;
					}
				case 4: // CaL
					Family = "generic";
					GType = 3;
					MHType = 0; //m & h
					IonType = 3; //Ca
					Dstatus = 1; //Off
					PowM( 1, 0 );
					PowH( 1, 0 );
					H(0.01f, 0); //Added on 7.11.2016 by TK
					t0m(0.5f, 0); //Added on 7.11.2016 by TK
					t0h(18, 0); //Added on 7.11.2016 by TK
					V12m( -27.4f, 0 ); //by Taegyo 12.14.2105
					Slpm( -5.7f, 0 ); //by Taegyo 12.14.2105
					V12h( -52.4f, 0 ); //by Taegyo 12.14.2105
					Slph( 5.2f, 0 );
					Tm( 0, 0 ); // by TK 7.11.2016 
					Th( 0, 0);  // by TK 7.11.2016
					Slptm(1, 0); //Added on 7.11.2016 by TK
					Slpth(1, 0); //Added on 7.11.2016 by TK
					switch (opt) {
					case 0:
						return GType;
					case 1:
						return MHType;
					case 2:
						return IonType;
					case 3:
						return Dstatus;
					}
				case 5: // Leak
					Family = "leak";
					GType = 0;
					MHType = 3; //No Gate
					IonType = 0; //Ca
					Dstatus = 0; //Off
					switch (opt) {
					case 0:
						return GType;
					case 1:
						return MHType;
					case 2:
						return IonType;
					case 3:
						return Dstatus;
					}				
			}
		}
	}
	return -1;
}

/////////////////////////////////////////////////////////////////////////////
// class cmpdata
//--- constructors/destructor
//--- public methods
cmpdata &cmpdata::operator = ( const cmpdata &cdat )
{
	S = cdat.S;
	V = cdat.V;
	Chans = cdat.Chans;
	return *this;
}

bool cmpdata::loadpar( istream &file, const string &parname )
{
	string str;
	if( parname == "S"){
		file >> str >> S;
		return true;
	}
	else if( parname == "V"){
		file >> str >> V;
		return true;
	}
	else if( parname == "<Channel"){
		load_unit( file, Chans, "Channel", "anyname" );
		return true;
	}
	return false;
}

bool cmpdata::validate( const string &type )
{
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// class hhndata
//--- constructors/destructor
//--- public methods
hhndata &hhndata::operator = ( const hhndata &hhn )
{
	C = hhn.C;
	P = hhn.P;
	Ex = hhn.Ex;
	Ex2 = hhn.Ex2;
	InhI = hhn.InhI;
	InhII = hhn.InhII;
	Cmps = hhn.Cmps;
	return *this;
}

bool hhndata::loadpar( istream &file, const string &parname )
{
	string str;
	if( parname == "C"){
		file >> str >> C;
		return true;
	}
	else if( parname == "P"){
		file >> str >> P;
		return true;
	}
	else if( parname == "Ex"){
		file >> str >> Ex;
		return true;
	}
	else if( parname == "Ex2"){
		file >> str >> Ex2;
		return true;
	}
	else if( parname == "InhI"){
		file >> str >> InhI;
		return true;
	}
	else if( parname == "InhII"){
		file >> str >>InhII;
		return true;
	}
	else if( parname == "<Compartment"){
		load_unit( file, Cmps, "Compartment", "Soma|Dendrite" );
		return true;
	}
	return false;
}

bool hhndata::validate( const string &type )
{
	return true;
}


/////////////////////////////////////////////////////////////////////////////
// class popdata
//--- constructors/destructor
//--- public methods
popdata &popdata::operator = ( const popdata &pdat )
{
	Size = pdat.Size;
	Hhn = pdat.Hhn;
	return *this;
}

bool popdata::loadpar( istream &file, const string &parname )
{
	string str;
	if( parname == "Size"){
		file >> str >> Size;
		return true;
	}
	else if( parname == "<Neuron>" ){
		load_unit( file, Hhn, "Neuron" );
		return true;
	}
	return false;
}

bool popdata::validate( const string &type )
{
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// class drivedata
//--- constructors/destructor
//--- public methods
drivedata &drivedata::operator = ( const drivedata &ddat )
{
	Size  = ddat.Size;
	return *this;
}

bool drivedata::loadpar( istream &file, const string &parname )
{
	string str;
	if( parname == "Size"){
		file >> str >> Size;
		return true;
	}
	return false;
}

bool drivedata::validate( const string &type )
{
	Size = 1;	// it's always = 1
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// class outdata
//--- constructors/destructor
//--- public methods
outdata &outdata::operator = ( const outdata &odat )
{
	Size  = odat.Size;
	Tup = odat.Tup;
	Tdown = odat.Tdown;
	Threshold = odat.Threshold;
	return *this;
}

bool outdata::loadpar( istream &file, const string &parname )
{
	string str;
	if( parname == "Size"){
		file >> str >> Size;
		return true;
	}
	else if( parname == "Tup"){
		file >> str >> Tup;
		return true;
	}
	else if( parname == "Tdown"){
		file >> str >> Tdown;
		return true;
	}
	else if( parname == "Threshold"){
		file >> str >> Threshold;
		return true;
	}
	return false;
}

bool outdata::validate( const string &type )
{
	Size = 1;	// it's always = 1
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// class unitdata
//--- constructors/destructor
//--- public methods
unitdata &unitdata::operator = ( const unitdata &udat )
{
	PData = udat.PData;
	DData = udat.DData;
	OData = udat.OData;
	return *this;
}

bool unitdata::loadpar( istream &file, const string &parname )
{
	if( parname == "<Population"){
		load_unit( file, PData, "Population", "anyname" );
		return true;
	}
	else if( parname == "<Drive"){
		load_unit( file, DData, "Drive", "anyname" );
		return true;
	}
	else if( parname == "<Output"){
		load_unit( file, OData, "Output", "anyname" );
		return true;
	}
	return false;
}

bool unitdata::validate( const string &type )
{
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// class trgdata
//--- constructors/destructor
//--- public methods
wdata &wdata::operator = ( const wdata &wdat )
{
	Type = wdat.Type;
	Prob = wdat.Prob;
	Ex = wdat.Ex;
	Ex2 = wdat.Ex2;
	InhI = wdat.InhI;
	InhII = wdat.InhII;
	return *this;
}

bool wdata::loadpar( istream &file, const string &parname )
{
	string str;
	if( parname == "Type"){
		file >> str >> Type;
		return true;
	}
	else if( parname == "Probability"){
		file >> str >> Prob;
		return true;
	}
	else if( parname == "Ex"){
		file >> str >> Ex;
		return true;
	}
	else if( parname == "Ex2"){
		file >> str >> Ex2;
		return true;
	}
	else if( parname == "InhI"){
		file >> str >> InhI;
		return true;
	}
	else if( parname == "InhII"){
		file >> str >> InhII;
		return true;
	}
	return false;
}

bool wdata::validate( const string &type )
{
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// class trgdata
//--- constructors/destructor
//--- public methods
trgdata &trgdata::operator = ( const trgdata &tdat )
{
	WData = tdat.WData;
	return *this;
}

bool trgdata::loadpar( istream &file, const string &parname )
{
	if( parname == "<Connect>"){
		load_unit( file, WData, "Connect" );
		return true;
	}
	return false;
}

bool trgdata::validate( const string &type )
{
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// class srcdata
//--- constructors/destructor
//--- public methods
srcdata &srcdata::operator = ( const srcdata &sdat )
{
	TrgData = sdat.TrgData;
	return *this;
}

bool srcdata::loadpar( istream &file, const string &parname )
{
	if( parname == "<Target"){
		load_unit( file, TrgData, "Target", "anyname" );
		return true;
	}
	return false;
}

bool srcdata::validate( const string &type )
{
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// class condata
//--- constructors/destructor
//--- public methods
condata &condata::operator = ( const condata &cndat )
{
	SrcData = cndat.SrcData;
	return *this;
}

bool condata::loadpar( istream &file, const string &parname )
{
	if( parname == "<Source"){
		load_unit( file, SrcData, "Source", "anyname" );
		return true;
	}
	return false;
}

bool condata::validate( const string &type )
{
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// class ctrdata
//--- public methods
ctrdata &ctrdata::operator = ( const ctrdata &ctdat )
{
	return *this;
}

bool ctrdata::loadpar( istream &file, const string &parname )
{
	return false;
}

bool ctrdata::validate( const string &type )
{
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// class netdata
//--- constructors/destructor
//--- public methods
netdata &netdata::operator = ( const netdata &net )
{
	Threshold = net.Threshold;
	Ions = net.Ions;
	Syns = net.Syns;
	DSyns = net.DSyns;
	SSyns = net.SSyns;
	Connections = net.Connections; //added by Taegyo. 11/18/2015
	Controls = net.Controls; //added by Taegyo
	Units = net.Units;
	return *this;
}

bool netdata::loadpar( istream &file, const string &parname )
{
	string str;
	if( parname == "Threshold"){
		file >> str >> Threshold;
		return true;
	}
	else if( parname == "<Ion"){
		load_unit( file, Ions, "Ion", "Na|K|Ca|Cl" );
		return true;
	}
	else if( parname == "<Synapse" ){
		load_unit( file,Syns, "Synapse", "Excitatory|Excitatory2|Inhibitory I|Inhibitory II" );
		return true;
	}
	else if( parname == "<Depression" ){
		load_unit( file, DSyns, "Depression", "Excitatory|Excitatory2|Inhibitory I|Inhibitory II" );
		return true;
	}
	else if( parname == "<Sigma" ){
		load_unit( file, SSyns, "Sigma", "Excitatory|Excitatory2|Inhibitory I|Inhibitory II" );
		return true;
	}
	else if( parname == "<Units>"){
		load_unit( file, Units, "Units"  );
		return true;
	}
	else if( parname == "<Connections>"){
		load_unit( file, Connections, "Connections"  );
		return true;
	}

	//else if( parname == "<Control"){
	//	load_unit( file, Controls, "Control", "anyname"  );
	//	return true;
	//}

	return false;
}
bool netdata::validate( const string &type )
{
	return true;
}

//--- private methods
void netdata::clear( void )
{
}

/////////////////////////////////////////////////////////////////////////////
// class nsm_model
//--- constructors/destructor
//--- public methods
bool nsm_model::load_model( const char *filename )
{
#ifdef __LINUX__
	remove_char( filename, 0x0D );
#endif
	bool LoadOK = true;
	string str;
	ifstream file( filename );
	while( file >> str ){
		if( str == "Generated" ){
			getline( file, str );
		}
		else if(str == "<SimulateData>" ){
			LoadOK = load_unit( file, SimData, "SimulateData" );
		}
		else if( str == "<Network" ){
			load_unit( file, Network, "Network", "Network" );
		}
/*
		else if( str == "<Views>" ){
//			load_unit( file, Views, "Views" );
		}
		else if( str == "<Records>" ){
//			load_unit( file, Records, "Records" );
		}
		else if( str == "<Biomechanics" ){
//			load_unit( file, Biomech, "Biomechanics", "Cat" );
		}
*/
		else{
			::unknown_string( file, str );
		}
	}
	return LoadOK;
}

int find_type(string name, netdata& net) {
	int np, nd, no, type=-1;
	np = net.Units.Unit.PData.size();
	nd = net.Units.Unit.DData.size();
	no = net.Units.Unit.OData.size();

	for (int n = 0;n < np;n++) {
		if (net.Units.Unit.PData[n].Name == name) {
			type = 0;
			break;
		};
	};
	if (type == -1) {
		for (int n = 0;n < nd;n++) {
			if (net.Units.Unit.DData[n].Name == name) {
				type = 1;
				break;
			};
		};
	};
	if (type == -1) {
		for (int n = 0;n < no;n++) {
			if (net.Units.Unit.OData[n].Name == name) {
				type = 2;
				break;
			};
		};
	};
	return type;
};

vector<vector<int>> make_reIdx4Ch(netdata& net) {

	vector<vector<int>> out;
	vector<int> tmp;
	vector<string> key;
	vector<int> kid, kid2;
	key.push_back("K");
	key.push_back("CaL");
	key.push_back("KCa");
	kid.resize(key.size());
	kid2.resize(key.size());
	
	
	int np, nc, tid; //, tid2; SM
	np = net.Units.Unit.PData.size();

	for (int ip = 0;ip < np;ip++) {
		nc = net.Units.Unit.PData[ip].Unit.Hhn.Unit.Cmps[0].Unit.Chans.size();
		tmp.clear();
		tmp.resize(nc);
		kid = { -1,-1,-1 };
		kid2 = { 0,0,0 };
		for (int ic = 0;ic < nc;ic++) {
			for (size_t ik = 0;ik < key.size();ik++) { // SM
				if (net.Units.Unit.PData[ip].Unit.Hhn.Unit.Cmps[0].Unit.Chans[ic].Name == key[ik]) {
						kid[ik] = ic;
				};
			};
			tmp[ic] = ic;
		};

		kid2 = kid;

		if (kid[2] != -1) {
			if (kid[1] > kid[2] && kid[2] > kid[0]) {
				tid = kid[1];
				kid[1] = kid[2];
				kid[2] = tid;
			}
			else if (kid[0] > kid[2] && kid[2] > kid[1]) {
				tid = kid[0];
				kid[0] = kid[2];
				kid[2] = tid;
			}
			else if (kid[1] > kid[2] && kid[0] > kid[2]) {
				if (kid[1] > kid[0]) {
					tid = kid[2];
					kid[2] = kid[1];
					kid[1] = kid[0];
					kid[0] = tid;
				}
				else {
					tid = kid[2];
					kid[2] = kid[0];
					kid[0] = kid[1];
					kid[1] = tid;
				};

			};
			for (size_t ik = 0;ik < key.size();ik++) { // SM
				tmp[kid2[ik]] = kid[ik];
			};

		};

		out.push_back(tmp);
	};
	
	return out;
};

string rmspaces(string str) {
	size_t id;
	bool getout = 0;
	while (getout == 0) {
		id = str.find(" ");
		if (id != std::string::npos)
			str.erase(id, 1);
		else
			getout = 1;
	};
	return str;
};

void change_naming(netdata& net) {
	size_t fd;
	string key;
	
	int nd = net.Units.Unit.DData.size();
	for (int in = 0;in < nd;in++) {
		net.Units.Unit.DData[in].Name = rmspaces(net.Units.Unit.DData[in].Name);
		key = "[";
		fd = net.Units.Unit.DData[in].Name.find(key);
		if (fd != std::string::npos) {
			net.Units.Unit.DData[in].Name.erase(fd, 1);
			net.Units.Unit.DData[in].Name.insert(fd, "(");
		};
		key = "]";
		fd = net.Units.Unit.DData[in].Name.find(key);
		if (fd != std::string::npos) {
			net.Units.Unit.DData[in].Name.erase(fd, 1);
			net.Units.Unit.DData[in].Name.insert(fd, ")");
		};
	};

	nd = net.Units.Unit.OData.size();
	for (int in = 0;in < nd;in++) {
		net.Units.Unit.OData[in].Name = rmspaces(net.Units.Unit.OData[in].Name);
		key = "[";
		fd = net.Units.Unit.OData[in].Name.find(key);
		if (fd != std::string::npos) {
			net.Units.Unit.OData[in].Name.erase(fd, 1);
			net.Units.Unit.OData[in].Name.insert(fd, "(");
		};
		key = "]";
		fd = net.Units.Unit.OData[in].Name.find(key);
		if (fd != std::string::npos) {
			net.Units.Unit.OData[in].Name.erase(fd, 1);
			net.Units.Unit.OData[in].Name.insert(fd, ")");
		};
	};

	nd = net.Units.Unit.PData.size();
	for (int in = 0;in < nd;in++) {
		net.Units.Unit.PData[in].Name = rmspaces(net.Units.Unit.PData[in].Name);
		key = "[";
		fd = net.Units.Unit.PData[in].Name.find(key);
		if (fd != std::string::npos) {
			net.Units.Unit.PData[in].Name.erase(fd, 1);
			net.Units.Unit.PData[in].Name.insert(fd, "(");
		};
		key = "]";
		fd = net.Units.Unit.PData[in].Name.find(key);
		if (fd != std::string::npos) {
			net.Units.Unit.PData[in].Name.erase(fd, 1);
			net.Units.Unit.PData[in].Name.insert(fd, ")");
		};

		//Channel renaming caused issues due to internal comparison by Sergey
		//int nc = net.Units.Unit.PData[in].Unit.Hhn.Unit.Cmps[0].Unit.Chans.size();
		//for (int ic = 0;ic < nc;ic++) {
		//	net.Units.Unit.PData[in].Unit.Hhn.Unit.Cmps[0].Unit.Chans[ic].Name = rmspaces(net.Units.Unit.PData[in].Unit.Hhn.Unit.Cmps[0].Unit.Chans[ic].Name);
		//};
	};


	int ns = net.Connections[0].Unit.SrcData.size();
	for (int i = 0;i < ns;i++) {
		net.Connections[0].Unit.SrcData[i].Name = rmspaces(net.Connections[0].Unit.SrcData[i].Name);
		key = "[";
		fd = net.Connections[0].Unit.SrcData[i].Name.find(key);
		if (fd != std::string::npos) {
			net.Connections[0].Unit.SrcData[i].Name.erase(fd, 1);
			net.Connections[0].Unit.SrcData[i].Name.insert(fd, "(");
		};
		key = "]";
		fd = net.Connections[0].Unit.SrcData[i].Name.find(key);
		if (fd != std::string::npos) {
			net.Connections[0].Unit.SrcData[i].Name.erase(fd, 1);
			net.Connections[0].Unit.SrcData[i].Name.insert(fd, ")");
		};

		int nt = net.Connections[0].Unit.SrcData[i].Unit.TrgData.size();
		for (int j = 0;j < nt;j++) {
			net.Connections[0].Unit.SrcData[i].Unit.TrgData[j].Name = rmspaces(net.Connections[0].Unit.SrcData[i].Unit.TrgData[j].Name);
			key = "[";
			fd = net.Connections[0].Unit.SrcData[i].Unit.TrgData[j].Name.find(key);
			if (fd != std::string::npos) {
				net.Connections[0].Unit.SrcData[i].Unit.TrgData[j].Name.erase(fd, 1);
				net.Connections[0].Unit.SrcData[i].Unit.TrgData[j].Name.insert(fd, "(");
			};
			key = "]";
			fd = net.Connections[0].Unit.SrcData[i].Unit.TrgData[j].Name.find(key);
			if (fd != std::string::npos) {
				net.Connections[0].Unit.SrcData[i].Unit.TrgData[j].Name.erase(fd, 1);
				net.Connections[0].Unit.SrcData[i].Unit.TrgData[j].Name.insert(fd, ")");
			};
		};
	};

};

bool nsm_model::save_model( const char *filename )
{
	change_naming(Network());
	vector<vector<int> > rid;
	rid = make_reIdx4Ch(Network());

	string tmpstr;
	int nsynps = 4, nnets = 1;
	vector<string> synpnames;
	vector<int> synpiontypes;
	synpnames.push_back("SynpEx1");
	synpnames.push_back("SynpEx2");
	synpnames.push_back("SynpIn1");
	synpnames.push_back("SynpIn2");
	synpiontypes.push_back(0);
	synpiontypes.push_back(0);
	synpiontypes.push_back(0);
	synpiontypes.push_back(0);

	ofstream nns( filename );
	if (nns.is_open()){
		nns << "Neural Network Simulator (NNS), 2015(C)" << endl;
		nns << endl;
		nns << "<VERSION>" << endl;
		nns << "NNS V1.0" << endl;
		nns << "</VERSION>" << endl; // the tag must be closed
		nns << endl;
		nns << "<CTR PARAM>" << endl;
		nns << "Seed = " << SimData().Seed << endl;
		nns << "SimStep = "<< SimData().SimStep << endl;
		nns << "SimDuration = "<< SimData().SimTime << endl;
		nns << "</CTR PARAM>" << endl;
		nns << endl;

		//Synapse type definition
		nns << "<SYNAPSE PARAM>" << endl;
		nns << "N of SynapseType = " << nsynps << endl;
		nns << endl;

		for (int isp = 0; isp < nsynps; isp++) {
			nns << "<SYNP " << isp << ">" << endl;
			nns << "Name = " << synpnames[isp] << endl;
			nns << "IonType = " << synpiontypes[isp] << endl;
			nns << "Gm = " << Network().Syns[isp].Unit.Gmax << " (" << 0 << ")" << endl;
			if (isp == 2) nns << "E = " << -94 << " (" << 0 << ")" << endl;
			else if (isp == 3) nns << "E = " << -75 << " (" << 0 << ")" << endl;
			else nns << "E = " << Network().Syns[isp].Unit.Eds << " (" << 0 << ")" << endl;
			nns << "Tc = " << Network().Syns[isp].Unit.Ts << endl;
			nns << "Amp = " << Network().Syns[isp].Unit.Vs << endl;
			nns << "</SYNP " << isp << ">" << endl;
		};

		nns << "</SYNAPSE PARAM>" << endl;
		nns << endl;

		size_t npops = Network().Units().PData.size();
		nns << "<POPULATIONS>" << endl;					// Populations definition
		nns << "N of Pops = " << npops << endl;
		nns << endl;

		for( size_t i = 0; i < npops; ++i ){
			size_t nICH = Network().Units().PData[i]().Hhn().Cmps[0]().Chans.size();
			size_t nIONS = Network().Ions.size();
			nns << "<POP "<< i << ">" << endl;
			nns << "Name = " << Network().Units().PData[i].Name << endl;
			nns << "ID = " << i << endl;
			nns << "Size = " << Network().Units().PData[i]().Size << endl;
			nns << "Threshold = " << Network().Threshold << endl;
			nns << endl;

			nns << "<NEURON PARAM>" << endl;
			nns << "Cm = " << Network().Units().PData[i]().Hhn().C.X << " (" << Network().Units().PData[i]().Hhn().C.Y*PCTG << ")" << endl; // Cm must be parameter of compartment
			nns << "Vm = " << Network().Units().PData[i]().Hhn().Cmps[0]().V.X << " (" << Network().Units().PData[i]().Hhn().Cmps[0]().V.Y*PCTG << ")" << endl;
			nns << "Area = " << Network().Units().PData[i]().Hhn().Cmps[0]().S.X << endl;
			nns << "Type = " << 0 << endl;					// ????
			nns << endl;

			nns << "<ICHANNELS PARAM>" << endl;
			nns << "nICH = " << nICH << endl;
			nns << "nICHNameList = {";
			for (size_t j = 0; j < nICH - 1; ++j) {
				nns << Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][j]].Name << ", ";
			}
			if (nICH > 0) {
				nns << Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][nICH - 1]].Name;
			}
			nns << "}" << endl;
			nns << "nICHGTypeList = {";
			for( size_t j = 0; j < nICH-1; ++j ){
				string name = Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][j]].Name;
				nns << Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][j]]().type( name, GTYPE ) << ", "; // must be type instead of name
			}
			if( nICH > 0 ){
				string name = Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][nICH - 1]].Name;
				nns << Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][nICH - 1]]().type( name, GTYPE); // must be type instead of name
			}
			nns << "}" << endl;
			nns << "nICHMHTypeList = {";
			for (size_t j = 0; j < nICH - 1; ++j) {
				string name = Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][j]].Name;
				nns << Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][j]]().type(name, MHTYPE) << ", "; // must be type instead of name
			}
			if (nICH > 0) {
				string name = Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][nICH - 1]].Name;
				nns << Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][nICH - 1]]().type(name, MHTYPE); // must be type instead of name
			}
			nns << "}" << endl;
			nns << "nICHITypeList = {";
			for (size_t j = 0; j < nICH - 1; ++j) {
				string name = Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][j]].Name;
				nns << Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][j]]().type(name, ITYPE) << ", "; // must be type instead of name
			}
			if (nICH > 0) {
				string name = Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][nICH - 1]].Name;
				nns << Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][nICH - 1]]().type(name, ITYPE); // must be type instead of name
			}
			nns << "}" << endl;
			nns << "nICHDStatusList = {";
			for (size_t j = 0; j < nICH - 1; ++j) {
				string name = Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][j]].Name;
				nns << Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][j]]().type(name, DSTATUS) << ", "; // must be type instead of name
			}
			if (nICH > 0) {
				string name = Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][nICH - 1]].Name;
				nns << Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][nICH - 1]]().type(name, DSTATUS); // must be type instead of name
			}
			nns << "}" << endl;
			nns << endl;

			for( size_t j = 0; j < nICH; ++j ){
				string name = Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][j]].Name;
				hhnpair<float> eds = Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][j]]().Eds;
				eds = hack1( eds, name, Network().Ions );
				Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][j]]().Eds = eds;
				nns << "<ICH " << j << ">" << endl;
				Network().Units().PData[i]().Hhn().Cmps[0]().Chans[rid[i][j]]().save( nns, name, j );
				nns << "</ICH " << j << ">" << endl;
				nns << endl;
			}
			nns << "</ICHANNELS PARAM>" << endl;
			nns << "</NEURON PARAM>" << endl;
			nns << endl;
			nns << "<IONS PARAM>" << endl;
			size_t n_ions = 0;
			for( size_t j = 0; j < nIONS; ++j ){
				if( hack2( Network().Ions[j].Name, Network().Units().PData[i]().Hhn().Cmps[0]().Chans )){
					n_ions=1;
				}
			}
			nns << "nIONS = " << n_ions << endl;
			nns << endl;

			if (n_ions > 0) {
				for (size_t j = 0; j < nIONS; ++j) {
					if (hack2(Network().Ions[j].Name, Network().Units().PData[i]().Hhn().Cmps[0]().Chans)) {
						/*check if Ca channels are present then print it*/
						//nns << "<ION " << j << ">" << endl;
						nns << "<ION " << 0 << ">" << endl;
						nns << "Name = " << Network().Ions[j].Name << endl;
						nns << "ID = " << j << endl;
						Network().Ions[j]().save(nns);
						//nns << "</ION " << j << ">" << endl;
						nns << "</ION " << 0 << ">" << endl;
						nns << endl;
					}
				};
			};
			nns << "</IONS PARAM>" << endl;
			nns << "</POP "<< i << ">" << endl;
			nns<<endl;
		}
		nns << "</POPULATIONS>" << endl;
		nns<<endl;
		
		//Nonspike units definition
		int nd = Network().Units.Unit.DData.size();
		int no = Network().Units.Unit.OData.size();
		int nnsu = nd + no;

		if (nnsu != 0) {
			nns << "<NSUNITS>" << endl;
			nns << endl;

			if (nd != 0) {
				nns << "<DRIVES>" << endl;
				nns << endl;

				for (int i = 0;i < nd;i++) {
					nns << "[" << Network().Units.Unit.DData[i].Name << "]" << endl;
				};

				nns << "</DRIVES>" << endl;
				nns << endl;
			};

			if (no != 0) {
				nns << "<OUTPUTS>" << endl;
				nns << endl;

				for (int i = 0;i < no;i++) {
					nns << "[" << Network().Units.Unit.OData[i].Name << ";" << Network().Units.Unit.OData[i].Unit.Tup << ";" << Network().Units.Unit.OData[i].Unit.Tdown << ";" << Network().Units.Unit.OData[i].Unit.Threshold << "]" << endl;
				};

				nns << "</OUTPUTS>" << endl;
				nns << endl;
			};

			nns << "</NSUNITS>" << endl;
			nns << endl;
		};

		//All Networks definition
		nns << "<NETWORKS>" << endl;
		nns << endl;
		//nns << "Optimization = " << 1 << endl;
		nns << "N of Nets = " << 1 << endl;
		nns << "N of MNets = " << 0 << endl;
		nns << endl;

		nns << "//Synapse Types - Excitation1:0, Excitation2:1, Inhibition1:2, Inhibition2:3" << endl;
		nns << "//Syntax for Nets: (To select a nonspike unit, use u: to specify nonspike unit before the name)" << endl;
		nns << "//[optimization OnOff;normalization OnOff;synapse type;source popname;target popname; connection prob; weight]" << endl;
		nns << "//[optimization OnOff;normalization OnOff;synapse type;u:source unitname;target popname; connection prob; weight]" << endl;
		nns << "// For All to all connection: " << endl;
		nns << "//[optimization OnOff;normalization OnOff;synapse type;source popname;target popname; weight]&*" << endl;
		nns << "// For All to all connection with a network plasticity rate:" << endl;
		nns << "//[optimization OnOff;normalization OnOff;synapse type;source popname;target popname; weight; prate]&*&p" << endl;
		nns << "// For one to one connection: " << endl;
		nns << "//[optimization OnOff;normalization OnOff;synapse type;source popname;target popname; weight]&n" << endl;
		nns << "// For one to one connection with a network plasticity rate:" << endl;
		nns << "//[optimization OnOff;normalization OnOff;synapse type;source popname;target popname; weight; prate]&n&p" << endl;
		nns << "//Syntax for MNets:" << endl;
		nns << "//[optimization OnOff;normalization OnOff;synapse type;source netname:popname;target netname:popname; connection prob; weight]" << endl;
		nns << endl;

		tmpstr = filename;

		string srcname, trgname;
		int srctype, trgtype;
		int id = tmpstr.find(".nns");
		if (id != std::string::npos) tmpstr.erase(id, 4);
		int np = Network().Units.Unit.PData.size();
		int nc = Network().Connections[0].Unit.SrcData.size();
		int type; //, srcpopflg; SM
		hhnpair<float> wg, prob;

		nns << "<NET " << 0 << ">" << endl;
		nns << "Name = " << tmpstr << endl;
		nns << "OnOff = " << 1 << endl;

		nns << "pnameList = {";
		for (int i = 0;i < np;i++) {
			if (i != np - 1)
				nns << Network().Units.Unit.PData[i].Name << ", ";
			else
				nns << Network().Units.Unit.PData[i].Name << "}" << endl;
		};

		if (nnsu != 0) {
			nns << "unameList = {";
			for (int i = 0;i < nd; i++) {
				if (i != nd - 1)
					nns << Network().Units.Unit.DData[i].Name << ", ";
				else {
					if (no != 0)
						nns << Network().Units.Unit.DData[i].Name << ",";
					else
						nns << Network().Units.Unit.DData[i].Name << "}" << endl;
				};
			};
			for (int i = 0;i < no; i++) {
				if (i != no - 1)
					nns << Network().Units.Unit.OData[i].Name << ", ";
				else
					nns << Network().Units.Unit.OData[i].Name << "}" << endl;
			};
		};
		nns << endl;

		for (int i = 0;i < nc;i++) {
			srcname = Network().Connections[0].Unit.SrcData[i].Name;
			srctype = find_type(srcname, Network());
			if (srctype != 0) {
				srcname = "u:" + srcname;
			};
			for (size_t j = 0;j < Network().Connections[0].Unit.SrcData[i].Unit.TrgData.size(); j++) {

				trgname = Network().Connections[0].Unit.SrcData[i].Unit.TrgData[j].Name;
				trgtype = find_type(trgname, Network());
				if (trgtype != 0) {
					trgname = "u:" + trgname;
				};
				prob(Network().Connections[0].Unit.SrcData[i].Unit.TrgData[j].Unit.WData[0].Unit.Prob, 0);

				if (Network().Connections[0].Unit.SrcData[i].Unit.TrgData[j].Unit.WData[0].Unit.Ex.X != 0) {
					type = 0;
					wg = Network().Connections[0].Unit.SrcData[i].Unit.TrgData[j].Unit.WData[0].Unit.Ex;					
				}else if (Network().Connections[0].Unit.SrcData[i].Unit.TrgData[j].Unit.WData[0].Unit.Ex2.X != 0) {
					type = 1;
					wg = Network().Connections[0].Unit.SrcData[i].Unit.TrgData[j].Unit.WData[0].Unit.Ex2;
				}else if (Network().Connections[0].Unit.SrcData[i].Unit.TrgData[j].Unit.WData[0].Unit.InhI.X != 0) {
					type = 2;
					wg = Network().Connections[0].Unit.SrcData[i].Unit.TrgData[j].Unit.WData[0].Unit.InhI;
				}else if (Network().Connections[0].Unit.SrcData[i].Unit.TrgData[j].Unit.WData[0].Unit.InhII.X != 0) {
					type = 3;
					wg = Network().Connections[0].Unit.SrcData[i].Unit.TrgData[j].Unit.WData[0].Unit.InhII;
				}
				else {
					type = 0;
					wg(0, 0);
				};

				////This is temporary procedure to check if the source is one of populations
				//srcpopflg = 0;
				//for (int k = 0;k < np;k++) {
				//	if (Network().Units.Unit.PData[k].Name == srcname) {
				//		srcpopflg = 1;
				//		break;
				//	};
				//};
				//
				//if (srcpopflg == 1) //If the source for a connection is a population, then print out following
				//nns << "[" << type << ";" << srcname << ";" << trgname << ";" << prob.X << "(" << prob.Y << ")" << ";" << prob.X << "(" << prob.Y << ")" << ";" << wg.X << "(" << wg.Y << ")" << "]" << endl;

				nns << "[1;1;" << type << ";" << srcname << ";" << trgname << ";" << prob.X * 100 << "(" << prob.Y * 100 << ")" << ";" << wg.X << "(" << wg.Y * 100 << ")" << "]" << endl;
			};
		};
		nns << "</NET " << 0 << ">" << endl;
		nns << endl;

		nns << "</NETWORKS>" << endl;
		nns << endl;

		nns << "<IODATA>" << endl;
		nns << endl;
		nns << "//Converting Viewdata in NSM to IODATA in NNS is not available" << endl;
		nns << "//Please newly define IODATA commands before use" << endl;
		nns << endl;
		
		nns << "</IODATA>" << endl;
		nns << endl;

		nns << "//IODATA command Syntax:" << endl;
		nns << "//[Name; IODataTypeNumber; command line]" << endl;
		nns << endl;
		nns << "//IOData Type Number Definitions" << endl;
		nns << "//  0: IONSI Pump current" << endl;
		nns << "//  1: IONSI Channel current" << endl;
		nns << "//  4: IONSE Reversal potential" << endl;
		nns << "//  5: IONSE Concentration of inside ion" << endl;
		nns << "//  6: IONSE Concentration of outside ion" << endl;
		nns << "//  9: CHANG Channel conductance" << endl;
		nns << "// 10: CHANG Channel current" << endl;
		nns << "// 12: CHANMH Channel activation" << endl;
		nns << "// 14: CHANMH Channel inactivation" << endl;
		nns << "// 16: CELLV Membrane potential" << endl;
		nns << "// 18: CELLV Spikes onset" << endl;
		nns << "// 19: CELLV Injected current" << endl;
		nns << endl;

		nns << "//Command line syntax:" << endl;
		nns << "//for poulation selection, p[]" << endl;
		nns << "//for neuron selection, n[]" << endl;
		nns << "//for channel or ion selection, c[]" << endl;
		nns << "//Various selection methods, ALL, [all], [0,2], [0:3], [0,4:5,9] etc." << endl;
		nns << "//for view option, /v" << endl;
		nns << "//for save option, /s" << endl;

		nns << "//ie. [Spikes; 18; ALL /v/s]" << endl;
		nns << "//ie. [Cin; 5; p[all] n[0,2:4,9] c[0] /v/s]" << endl;
		nns << endl;

		nns.close();
	}
	return false;
}

!./learn >dat 2>www

set term pdfcairo size 16,8
set out "tmp.pdf"
set multi lay 2,2

unset key

set xrange [0:]
set yrange [0:]

set title "population activity"
set ylabel "MC    D1    D2    GPe    GPi"
plot "dat" u 2:1:3 matrix w image

#set title "W cortex->striatum"
#set ylabel "w 25->i"
#plot "www" index 0 u 26 w lp title "D1","www" index 1 u 26 w lp title "D2"

set title "Synaptic weights"
set ylabel "W1 W2 WM 25->j"
#plot "www" matrix index 0 w image
plot "www" u 2:1:3 matrix w image


#set title "W2 i->j"
#set ylabel "j"
#plot "www" matrix index 1 w image

set title "Reaching points"
set ylabel "Y"

set grid
plot [-.5:.5][0:1]"qqq" u 3:4 w p pt 7 ps 2 lc 0,"qqq" u 6:7 w p pt 7 ps 1,"qqq" u 1:2:0 w p pt 7 ps .3 lc palette z

set ylabel "Distance"
#plot "<histogram qqq" w boxes

stats "qqq" u 5
set title sprintf("Mean error %g",STATS_mean)

#plot "qqq" u 5 w p
set y2tics
set ytics nomirror
plot [][*:*]"qqq" u ((-atan2($1-$6,$2-$7)/pi*180)) w p,"qqq" u (-atan2($3-$6,$4-$7)/pi*180) w l,"qqq" u 5 axis x1y2 w l

unset multi

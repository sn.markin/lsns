function plot_histf(pop,hist_peaks,Bth,info_opt)

histo_info_on = info_opt;
text_opt = 0;

% p_max = max(hist_peaks(1).amp);
y_max = 100;
txt_off = 5;

t_divider = 1;
t1 = 1;
tn = pop.rec_duration/pop.rec_precision/t_divider;
tcrp = 9; % Starting time to show
tlow = tcrp*1000/pop.rec_precision;

figure; plot(pop.time(t1:tn),pop.hist(t1:tn),'k-','LineWidth',1.)
% plot(time,hist,'k-','LineWidth',1.)
hold on;

if histo_info_on == 1
    i_tmp = find(hist_peaks(1).ps >= t1/(1000/pop.rec_precision) & hist_peaks(1).ps <= tn/(1000/pop.rec_precision)); 
    plot(hist_peaks(1).ps(i_tmp),hist_peaks(1).amp(i_tmp),'r+');
    clear i_tmp;
    i_tmp = find(hist_peaks(2).ps >= t1/(1000/pop.rec_precision) & hist_peaks(2).ps <= tn/(1000/pop.rec_precision));
    plot(hist_peaks(2).ps(i_tmp),hist_peaks(2).amp(i_tmp),'b+');

    plot([pop.time(tlow) pop.time(tn)],[Bth Bth],'-.','color',[0.543 0 0],'LineWidth',0.5);
end

% plot(hist_peaks(1).ps,hist_peaks(1).amp,'r+');
% plot(hist_peaks(2).ps,hist_peaks(2).amp,'b+');
xlabel('Time (S)');
title(['Histogram of ' pop.name ' with Peaks Info.']);
ylim([0 y_max]);
xlim([pop.time(tlow) pop.time(tn)]);

if text_opt == 1
    text(1,y_max-txt_off,['Bursts Freq = ' num2str(round(hist_peaks(1).freq,2)) 'Hz'],'color','red');
    text(1,y_max-txt_off-3,['Bursts Avg. Amp = ' num2str(round(hist_peaks(1).avg_amp,1))],'color','red');
    text(1,y_max-txt_off-6,['Burstlets Freq = ' num2str(round(hist_peaks(2).freq,2)) 'Hz'],'color','blue');
    text(1,y_max-txt_off-9,['Burstlets Avg. Amp = ' num2str(round(hist_peaks(2).avg_amp,1))],'color','blue');
end

% clear p_max y_max txt_off;

end
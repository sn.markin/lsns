
ymin = -90; ymax = 50; 
ymin2 = 0; ymax2 = 6;

MarkerSize = 5;
MarkerColor = [0 0 0.5];

for i=1:npop
    if raster_on == 1
        if pops(i).type == 0 
            %plot_rasterf(pops(i));
            plot_rasterf2(pops(i),0,MarkerColor,MarkerSize);
            title(['Pop ' num2str(i) ': ' pops(i).name]);
        end
    end

    if hist_on == 1
        if pops(i).type == 0
            figure;
            plot(pops(i).time,pops(i).hist,'b-','LineWidth',.5);
            title(['Pop ' num2str(i) ': ' pops(i).name]);
            ylim([0 100]);
        end
    end
end
if wave_on == 1
    for i=1:npop
        if subplot_on == 0
            ndata = length(pops(i).data);
            if (idt(1) <= ndata)
                for j=1:pops(i).data(idt(1)).nneu
                    figure;
                    plot(time(ws:we),pops(i).data(idt(1)).waves(j,ws:we));
                    if pops(i).type == 0 
                        title(['Pop ' num2str(i) ': ' pops(i).name ', Neuron ' num2str(pops(i).data(idt(1)).neuIDs(j))]);
                        ylim([ymin,ymax]);
                    else
                        title(['NSU ' num2str(i) ': ' pops(i).name]);
                        ylim([ymin2,ymax2]);
                    end
                    xlabel('Time (ms)');
                    ylabel('Votage (uV)');
                    if spikesinfo_on == 1
                        if pops(i).type == 0
                            hold on;
                            ct=0;
    %                         for k=1:size(pops(i).data(idt(2)).tspikes,2)
                                tmpspikes = pops(i).data(idt(2)).tspikes{1,j};
                                if ~isempty(tmpspikes)                              
                                    for nsp = 1:size(tmpspikes)
                                        if (tmpspikes(nsp) >= time(ws))&&(tmpspikes(nsp) <= time(we))
                                            plot(tmpspikes(nsp),pops(i).threshold,'+r');
                                            ct = ct+1;
                                        end
                                    end
                                end
    %                         end
                            y = ylim;
                            text(txt_x_off,y(2)-txt_y_off,['N of spikes = ' num2str(ct)],'color','r');
                        end
                    end
                    if cin_on == 1
                        figure;
                        plot(time(ws:we),pops(i).data(idt(2)).waves(j,ws:we));
                        title(['Pop ' num2str(i) ': ' pops(i).name ', Neuron ' num2str(j)]);
                        xlabel('Time (ms)');
                        ylabel('Votage (uV)');
                    end
                end
            end
        else           
            ndata = length(pops(i).data);
            if (idt(1) <= ndata)
                figure;
                n = 1; m = pops(i).data(idt(1)).nneu;
                for j=1:pops(i).data(idt(1)).nneu
                    subplot(m,n,j);
                    hold on;
                    plot(time(ws:we),pops(i).data(idt(1)).waves(j,ws:we));
                    if pops(i).type == 0
                        title(['Pop ' num2str(i) ': ' pops(i).name ', Neuron ' num2str(pops(i).data(idt(1)).neuIDs(j))]);
                        ylim([ymin,ymax]);
                    else
                         title(['NSU ' num2str(i) ': ' pops(i).name]);
                        ylim([ymin2,ymax2]);
                    end
                    if j == m
                        xlabel('Time (ms)');
                    end

                    if spikesinfo_on == 1
                        if pops(i).type == 0
                            hold on;
                            ct=0;
    %                         for k=1:size(pops(i).data(idt(2)).tspikes,2)
                                tmpspikes = pops(i).data(idt(2)).tspikes{1,j};
                                if ~isempty(tmpspikes)                              
                                    for nsp = 1:length(tmpspikes)
                                        if (tmpspikes(nsp) >= time(ws))&&(tmpspikes(nsp) <= time(we))
                                            subplot(m,n,j), plot(tmpspikes(nsp),pops(i).threshold,'+r');
                                            ct = ct+1;
                                        end
                                    end
                                end
    %                         end
                            y = ylim;
                            text(txt_x_off,y(2)-txt_y_off,['N of spikes = ' num2str(ct)],'color','r');
                        end
                    end

                    if cin_on == 1
                        figure;
                        subplot(m,n,j), plot(time(ws:we),pops(i).data(idt(2)).waves(j,ws:we));
                        title(['Pop ' num2str(i) ': ' pops(i).name ', Neuron ' num2str(j)]);
                        xlabel('Time (ms)');
                        ylabel('Votage (uV)');
                    end
                    hold off;
                end
            end
        end        
    end
end

clear i j y ct n m
function plot_rasterf(pop,channels)
%This function is to plot raster of neuron spikes
%Taegyo Kim, Ph.D. Drexel University College of Medicine
%12.23.2015

[n,m] = size(pop.tspk);

if channels == 0   
    figure;
    for i=1:m
        spk_i = find(pop.tspk(:,i) ~= 0);
        if ~isempty(spk_i)
            plot(pop.time(spk_i),pop.tspk(spk_i,i)*i,'k.','MarkerSize',0.5);
            hold on;
        end
    end
    
else
    figure;
    nchs = length(channels);
    for i=1:nchs
        spk_i = find(pop.tspk(:,channels(i)) ~= 0);
        if ~isempty(spk_i)
            plot(pop.time(spk_i),pop.tspk(spk_i,channels(i))*channels(i),'k.','MarkerSize',0.5);
            hold on;
        end
    end
    
end
xlim([0 pop.time(end)]);
ylim([0 pop.size]);
xlabel('Time (s)');
ylabel('Neuron ID');

% tn = pop.rec_duration/pop.rec_precision/2;

end
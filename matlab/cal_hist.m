function hist = cal_hist(spikes,bin,scale_factor)

Nsum = sum(spikes,2);
Tn = length(Nsum);
hist = zeros(Tn,1);
if sum(Nsum) > 0
    for j = 1:bin:Tn-bin+1
        Bsum = Nsum(j:j+bin-1); 
        hist(j:j+bin-1) = sum(Bsum,1);
    end;    
    hist = hist*scale_factor; %10 - scale factor = 1000ms/bin,ms
end
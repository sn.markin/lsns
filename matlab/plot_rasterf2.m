function plot_rasterf2(pop,channels,MKcolor,MKsize)
%This function is to plot raster of neuron spikes with options
%Taegyo Kim, Ph.D. Drexel University College of Medicine
%12.23.2015

if nargin == 1
    channels = 0;
    MKcolor = [0 0 0.8];
    MKsize = 0.5;
elseif nargin == 2
    MKcolor = [0 0 0.8];
    MKsize = 0.5;
elseif nargin == 3
    MKsize = 0.5;
end
    

[n,m] = size(pop.tspk);

if channels == 0   
    figure;
    for i=1:m
        spk_i = find(pop.tspk(:,i) ~= 0);
        if ~isempty(spk_i)
            plot(pop.time(spk_i),pop.tspk(spk_i,i)*i,'.','color',MKcolor,'MarkerSize',MKsize);
            hold on;
        end
    end
    
else
    figure;
    nchs = length(channels);
    for i=1:nchs
        spk_i = find(pop.tspk(:,channels(i)) ~= 0);
        if ~isempty(spk_i)
            plot(pop.time(spk_i),pop.tspk(spk_i,channels(i))*channels(i),'.','color',MKcolor,'MarkerSize',MKsize);
            hold on;
        end
    end
    
end
xlim([0 pop.time(end)]);
ylim([0 pop.size]);
xlabel('Time (s)');
ylabel('Neuron ID');

% tn = pop.rec_duration/pop.rec_precision/2;

end
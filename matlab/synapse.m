clear all;

dT = 0.1;    %msec
Tsym = 1000; %msec
Tsyn = 10;   %msec
Asyn = 0.1;  %
MaxSteps = Tsym/dT;

t = 1:MaxSteps;
SynG = zeros( MaxSteps, 1 );

Fspike = 40; %Hz
SpikeDelay = ( 1000/Fspike )/dT;

figure;
hold on

for i = 1:6
    Fspike = Fspike+10;
    SpikeDelay = ( 1000/Fspike )/dT;
    SynG( 1 ) = 0.1;
    t_spike = SpikeDelay;
    for i = 1:MaxSteps-1
        delta = 0;
        if t_spike <= t(i)
            delta = 1;
            t_spike = t_spike+SpikeDelay;
        end
        SynG(i+1) = SynG(i)*exp( -dT/Tsyn )+Asyn*delta;
    end
    plot( t, SynG );
end

hold off    
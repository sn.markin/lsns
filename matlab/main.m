clear
clc

%% File name setting

pathname = '..\'; 
filename = 'BG_model-v35-45.dat';

pfilename = [pathname filename];


%% Parameter setting

fdtype = [16; 18];
% fdtype = [16; 5];

%cropping window
ws=1; %start index
we=1000; %end step

plot_on = 1;
subplot_on = 1;

spikesinfo_on = 0; %Overlapping spikes info on waves

raster_on = 1;
hist_on = 0;
wave_on = 0;

cin_on = 0;

txt_y_off = 2;
txt_x_off = 5;

%% Import data

pops = im_nnsdata(pfilename,1);

%% Processing
npop = length(pops);

nstep = pops(1).rec_duration/pops(1).rec_precision;
we = nstep - (32+1);
time =0;
for i=1:nstep
   time(i) = (i-1)*pops(1).rec_precision;
end

ndt = length(pops(1).data);
nfdt = length(fdtype);

for i=1:nfdt
    for j=1:ndt
       if pops(1).data(j).type == fdtype(i)
           idt(i) = j;
       end
    end
end

%% Plotting
if plot_on == 1

    main_plots;

end




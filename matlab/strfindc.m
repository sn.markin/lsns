function idx = strfindc(cdata,key)

tempc = strfind(cdata,key);
empty_idx = cellfun(@isempty,tempc);
tempc(empty_idx) = {0};
idx = find([tempc{:}] == 1);

end
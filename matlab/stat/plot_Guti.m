
all_RPEs = 0;

figure;
errorplot([1:param.tr],dw.d_avg(:,7)',dw.d_std(:,7)',color(6,:),0.2);
xlim([0 param.tr]);
ylim([-0.1 0.5]);
xlabel('Trials');
ylabel('Errors (m)');

% figure;
% errorplot([1:param.tr],dw.d_avg(:,11)',dw.d_std(:,11)',color(6,:),0.2);
% xlim([0 param.tr]);
% % ylim([-0.1 0.5]);
% xlabel('Trials');
% ylabel('RPE');

if all_RPEs == 1
    for i=1:fn
        figure;
        plot(dw.dt(11).d(i,:));
        xlabel('Trials');
        ylabel('RPE');
    end
end

clear all_RPEs i;
#include "cal_funcs.h"

//////////////////////////////////////////////////////////////////////
// Definition of functions to calculate all paramaters for I channels

double cal_Tm(double Vc, const vector<strCom_GenPara>& compara,int tp, int id) {

	double Tm, tm_max, Vhfm, ktm;
	if (compara[tp].tm_max.size() == 1)
		tm_max = compara[tp].tm_max[0];
	else tm_max = compara[tp].tm_max[id];
	if (compara[tp].Vhfm.size() == 1)
		Vhfm = compara[tp].Vhfm[0];
	else Vhfm = compara[tp].Vhfm[id];
	if (compara[tp].ktm.size() == 1)
		ktm = compara[tp].ktm[0];
	else ktm = compara[tp].ktm[id];

	Tm = tm_max / cosh((Vc-Vhfm) / ktm);

	return Tm;
};

double cal_m_inf(double Vc, const vector<strCom_GenPara>& compara,int tp,int id) {
	double m_inf, Vhfm, km;
	if (compara[tp].Vhfm.size() == 1)
		Vhfm = compara[tp].Vhfm[0];
	else Vhfm = compara[tp].Vhfm[id];
	if (compara[tp].km.size() == 1)
		km = compara[tp].km[0];
	else km = compara[tp].km[id];
	m_inf = 1./(1. + exp(-(Vc-Vhfm)/ km));
	return m_inf;
};

double cal_M(double m_inf, double Mpre, double ODEstep, double Tm) {

	return (m_inf - Mpre)*ODEstep/Tm;
};

double cal_Th(double Vc,const vector<strCom_GenPara>& compara,int tp,int id) {
	double Th, th_max, Vhfh, kth;
	if (compara[tp].th_max.size() == 1)
		th_max = compara[tp].th_max[0];
	else th_max = compara[tp].th_max[id];
	if (compara[tp].Vhfm.size() == 1)
		Vhfh = compara[tp].Vhfh[0];
	else Vhfh = compara[tp].Vhfh[id];
	if (compara[tp].ktm.size() == 1)
		kth = compara[tp].kth[0];
	else kth = compara[tp].kth[id];
	Th = th_max / cosh((Vc-Vhfh)/ kth);
	return Th;
};

double cal_h_inf(double Vc,const vector<strCom_GenPara>& compara,int tp,int id) {
	double h_inf, Vhfh, kh;
	if (compara[tp].Vhfh.size() == 1)
		Vhfh = compara[tp].Vhfh[0];
	else Vhfh = compara[tp].Vhfh[id];
	if (compara[tp].kh.size() == 1)
		kh = compara[tp].kh[0];
	else kh = compara[tp].kh[id];
	h_inf = 1./(1. + exp(-(Vc-Vhfh)/ kh));
	return h_inf;
};

double cal_H(double h_inf, double Hpre, double ODEstep, double Th) {
	return (h_inf - Hpre)*ODEstep/Th;;
};

double cal_I(double g_max, double M, double H, double Vc, double E,const vector<strCom_Gen1Para>& com1Para,int tp) {

	return g_max*pow(M,com1Para[tp].mpower)*pow(H,com1Para[tp].hpower)*(Vc-E);;
};
double cal_Im(double g_max, double M, double Vc, double E,const vector<strCom_Gen1Para>& com1Para,int tp) {

	return g_max*pow(M,com1Para[tp].mpower)*(Vc-E);;
};
double cal_Ih(double g_max, double H, double Vc, double E,const vector<strCom_Gen1Para>& com1Para,int tp) {

	return g_max*pow(H,com1Para[tp].hpower)*(Vc-E);
};
double cal_Ing(double g, double Vc, double E) {

	return g*(Vc-E);
};

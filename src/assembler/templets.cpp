#include "templets.h"


void load_tmpl(vector<neural_population>& pops,string name, int type) {
	
	size_t np = pops.size();
	int pid =-1;
	for (size_t i=0;i<np;i++) {
		if (pops[i].name == name){
			pid = (int)i;
			break;
		};
	};
	if (pid != -1){
		load_tmplc(pops[pid], type);
	}else {
		cout<<"ERROR::Population ("<<name<<"): Not found in populations"<<endl;
	};
};

void load_tmpl(neural_population& pop, int type) {
	load_tmplc(pop, type);
};

void load_tmplc(neural_population& pop, int type) {
	string tname;
	int i=0;

	create_ich(pop, "SynpEx1", 13, 1, 0);
	create_ich(pop, "SynpEx2", 13, 1, 0);
	create_ich(pop, "SynpIn1", 13, 1, 0);
	create_ich(pop, "SynpIn2", 13, 1, 0);

	i = 0;
	pop.paraset.ichs.ng_para[i].g.val = 1;
	pop.paraset.ichs.ng_para[i].g.var = 0;
	pop.paraset.ichs.ng_para[i].E.val = 0;
	pop.paraset.ichs.ng_para[i].E.var = 0;
	pop.paraset.ichs.ng_para[i].Tc = 5;
	pop.paraset.ichs.ng_para[i].Amp = 1;

	i = 1;
	pop.paraset.ichs.ng_para[i].g.val = 1;
	pop.paraset.ichs.ng_para[i].g.var = 0;
	pop.paraset.ichs.ng_para[i].E.val = 0;
	pop.paraset.ichs.ng_para[i].E.var = 0;
	pop.paraset.ichs.ng_para[i].Tc = 5;
	pop.paraset.ichs.ng_para[i].Amp = 1;

	i = 2;
	pop.paraset.ichs.ng_para[i].g.val = 1;
	pop.paraset.ichs.ng_para[i].g.var = 0;
	pop.paraset.ichs.ng_para[i].E.val = -94;
	pop.paraset.ichs.ng_para[i].E.var = 0;
	pop.paraset.ichs.ng_para[i].Tc = 15;
	pop.paraset.ichs.ng_para[i].Amp = 1;

	i = 3;
	pop.paraset.ichs.ng_para[i].g.val = 1;
	pop.paraset.ichs.ng_para[i].g.var = 0;
	pop.paraset.ichs.ng_para[i].E.val = -75;
	pop.paraset.ichs.ng_para[i].E.var = 0;
	pop.paraset.ichs.ng_para[i].Tc = 15;
	pop.paraset.ichs.ng_para[i].Amp = 1;

	i = 0;
	switch (type) {
	case 0: //Naf(G), NaP(G), K(G), Leak, SynpE (Rybak 2004)

		//====Channel Settings===================
		create_ich(pop, "Naf", 3, 0, 1);
		create_ich(pop, "NaP", 3, 0, 1);
		create_ich(pop, "K", 3, 1, 2);
		create_ich(pop, "Leak", 0, 3, 0);
		//create_ich(pop, "Synp", 13, 3);

		//iNaf parameters setting
		pop.paraset.ichs.g_para[i].g.val = 150.; //nS
		pop.paraset.ichs.g_para[i].E.val = 60.;
		pop.paraset.ichs.g_para[i].mpower = 3;
		pop.paraset.ichs.g_para[i].hpower = 1;
		pop.paraset.ichs.g_para[i].M0 = 0.02;
		pop.paraset.ichs.g_para[i].H0 = 0.3;
		pop.paraset.ichs.g_para[i].t0m = 0.;
		pop.paraset.ichs.g_para[i].t0h = 0.;
		pop.paraset.ichs.g_para[i].tm.val = 0.9; //ms
		pop.paraset.ichs.g_para[i].th.val = 35.2; //ms

		pop.paraset.ichs.g_para[i].Vsm.val = -47.1; //mV
		pop.paraset.ichs.g_para[i].Vhfm.val = -47.1; //mV
		pop.paraset.ichs.g_para[i].Vhfm2.val = -47.1; //mV
		pop.paraset.ichs.g_para[i].km.val = -6.0; //mV ** This value should be minus(-)		
		pop.paraset.ichs.g_para[i].ktm.val = 14.0; //mV
		pop.paraset.ichs.g_para[i].ktm2.val = 14.0; //mV
		pop.paraset.ichs.g_para[i].Vsh.val = -67.5; //mV
		pop.paraset.ichs.g_para[i].Vhfh.val = -67.5; //mV
		pop.paraset.ichs.g_para[i].Vhfh2.val = -67.5; //mV
		pop.paraset.ichs.g_para[i].kh.val = 10.8; //mV		
		pop.paraset.ichs.g_para[i].kth.val = 12.8; //mV
		pop.paraset.ichs.g_para[i].kth2.val = 12.8; //mV

														 //iNaP parameters setting
		i = 1;
		pop.paraset.ichs.g_para[i].g.val = 4.; //nS
		pop.paraset.ichs.g_para[i].g.var = 10; //percentage (%)
		pop.paraset.ichs.g_para[i].E.val = 60;
		pop.paraset.ichs.g_para[i].mpower = 1;
		pop.paraset.ichs.g_para[i].hpower = 1;
		pop.paraset.ichs.g_para[i].M0 = 0.02;
		pop.paraset.ichs.g_para[i].H0 = 0.3;
		pop.paraset.ichs.g_para[i].t0m = 0.;
		pop.paraset.ichs.g_para[i].t0h = 0.;
		pop.paraset.ichs.g_para[i].tm.val = 0.9; //ms
		pop.paraset.ichs.g_para[i].th.val = 20000; //ms
		pop.paraset.ichs.g_para[i].Vsm.val = -47.1; //mV
		pop.paraset.ichs.g_para[i].Vhfm.val = -47.1; //mV
		pop.paraset.ichs.g_para[i].Vhfm2.val = -47.1; //mV
		pop.paraset.ichs.g_para[i].km.val = -3.1; //mV		
		pop.paraset.ichs.g_para[i].ktm.val = 6.2; //mV
		pop.paraset.ichs.g_para[i].ktm2.val = 6.2; //mV
		pop.paraset.ichs.g_para[i].Vsh.val = -57.0; //mV
		pop.paraset.ichs.g_para[i].Vhfh.val = -57.0; //mV
		pop.paraset.ichs.g_para[i].Vhfh2.val = -57.0; //mV
		pop.paraset.ichs.g_para[i].kh.val = 3.0; //mV		
		pop.paraset.ichs.g_para[i].kth.val = 6.0; //mV
		pop.paraset.ichs.g_para[i].kth2.val = 6.0; //mV

														//K parameters
		i = 2;
		pop.paraset.ichs.g_para[i].g.val = 50.0; //nS
		pop.paraset.ichs.g_para[i].g.var = 10; //percentage (%)
		pop.paraset.ichs.g_para[i].E.val = -94;
		pop.paraset.ichs.g_para[i].mpower = 4;
		pop.paraset.ichs.g_para[i].hpower = 0;
		pop.paraset.ichs.g_para[i].M0 = 0.02;
		pop.paraset.ichs.g_para[i].H0 = 1;
		pop.paraset.ichs.g_para[i].t0m = 0.;
		pop.paraset.ichs.g_para[i].t0h = 0.;
		pop.paraset.ichs.g_para[i].tm.val = 4.0; //ms
		pop.paraset.ichs.g_para[i].Vsm.val = -44.5; //mV
		pop.paraset.ichs.g_para[i].Vhfm.val = -44.5; //mV
		pop.paraset.ichs.g_para[i].Vhfm2.val = -44.5; //mV
		pop.paraset.ichs.g_para[i].km.val = -5.0; //mV	
		pop.paraset.ichs.g_para[i].ktm.val = 10.0; //mV
		pop.paraset.ichs.g_para[i].ktm2.val = 10.0; //mV

														 //Leak parameters
		i = 0;
		pop.paraset.ichs.ng_para[i].g.val = 2.0; //nS
		pop.paraset.ichs.ng_para[i].E.val = -76;
		pop.paraset.ichs.ng_para[i].E.var = 0;


		//======== ion settings=========================
		create_ion(pop,"Na",1);
		//create_ion(pop,"Ca",3);

		i=0; //Na ion
		pop.paraset.ions.ion_para[i].Cin = 15;
		pop.paraset.ions.ion_para[i].Cout = 120;
		pop.paraset.ions.ion_para[i].Z = Z_NA;
		pop.paraset.ions.ion_para[i].pumpType = 0;
		pop.paraset.ions.ion_para[i].InEq = 15;
		pop.paraset.ions.ion_para[i].t = 500;
		pop.paraset.ions.ion_para[i].Rpmp = 1;
		pop.paraset.ions.ion_para[i].Rch = 1;
		pop.paraset.ions.ion_para[i].Kp = 15;

		//i=1; //Ca ion
		//pop.paraset.ions.ion_para[i].Cin = 140;
		//pop.paraset.ions.ion_para[i].Cout = 4;
		//pop.paraset.ions.ion_para[i].Z = Z_CA;
		//pop.paraset.ions.ion_para[i].pumpType = 0;
		//pop.paraset.ions.ion_para[i].InEq = 15;
		//pop.paraset.ions.ion_para[i].t = 500;
		//pop.paraset.ions.ion_para[i].Rpmp = 0.0045;
		//pop.paraset.ions.ion_para[i].Rch = 1;
		//pop.paraset.ions.ion_para[i].K = 0.001;
		//pop.paraset.ions.ion_para[i].B = 0.03;

		//======== neuron settings ======================
		pop.paraset.neu_para.Cm.val = 36.2; // pF
		pop.paraset.neu_para.Vm.val = -60; //mV

		//pop.paraset.net_para.E.val = 0; //mV
		//pop.paraset.net_para.g.val = 0.1; //nS
		//pop.paraset.net_para.t.val = 5; //ms
		
		//========= control settings =====================
		//pop.paraset.ctr_para.seed = newseed();   // SM uncomment?
		//pop.paraset.ctr_para.simduration = 1000; // SM uncomment?
		//pop.paraset.ctr_para.simstep = 0.05;     // SM uncomment? 
		//pop.paraset.ctr_para.threshold = -35;
		pop.threshold = -35;

		break;
	case 1: //Naf(G), NaP(G), K(ab), Leak, SynpE (for preI, Smith 2007)

		//====Channel Settings===================
		create_ich(pop, "Naf", 3, 0, 1);
		create_ich(pop, "NaP", 3, 0, 1);
		create_ich(pop, "K", 7, 1, 2);
		create_ich(pop, "Leak", 0, 3, 0);
		//create_ich(pop, "Synp", 13, 3);

		//iNaf parameters setting
		pop.paraset.ichs.g_para[i].g.val = 170.; //nS
		pop.paraset.ichs.g_para[i].E.val = 55.1884;
		pop.paraset.ichs.g_para[i].mpower = 3;
		pop.paraset.ichs.g_para[i].hpower = 1;
		pop.paraset.ichs.g_para[i].M0 = 0.02;
		pop.paraset.ichs.g_para[i].H0 = 0.3;
		pop.paraset.ichs.g_para[i].t0m = 0.;
		pop.paraset.ichs.g_para[i].t0h = 0.;
		pop.paraset.ichs.g_para[i].tm.val = 0.252; //ms
		pop.paraset.ichs.g_para[i].th.val = 8.456; //ms
		pop.paraset.ichs.g_para[i].Vsm.val = -43.8; //mV
		pop.paraset.ichs.g_para[i].Vhfm.val = -43.8; //mV
		pop.paraset.ichs.g_para[i].Vhfm2.val = -43.8; //mV
		pop.paraset.ichs.g_para[i].km.val = -6.0; //mV ** This value should be minus(-)		
		pop.paraset.ichs.g_para[i].ktm.val = 14.0; //mV
		pop.paraset.ichs.g_para[i].ktm2.val = 14.0; //mV
		pop.paraset.ichs.g_para[i].Vsh.val = -67.5; //mV
		pop.paraset.ichs.g_para[i].Vhfh.val = -67.5; //mV
		pop.paraset.ichs.g_para[i].Vhfh2.val = -67.5; //mV
		pop.paraset.ichs.g_para[i].kh.val = 10.8; //mV		
		pop.paraset.ichs.g_para[i].kth.val = 12.8; //mV
		pop.paraset.ichs.g_para[i].kth2.val = 12.8; //mV

		//iNaP parameters setting
		i = 1;
		pop.paraset.ichs.g_para[i].g.val = 5.; //nS
		//pop.paraset.ichs.g_para[i].g.var = 10; //percentage (%)
		pop.paraset.ichs.g_para[i].E.val = 55.1884;
		pop.paraset.ichs.g_para[i].mpower = 1;
		pop.paraset.ichs.g_para[i].hpower = 1;
		pop.paraset.ichs.g_para[i].M0 = 0.02;
		pop.paraset.ichs.g_para[i].H0 = 0.3;
		pop.paraset.ichs.g_para[i].t0m = 0.;
		pop.paraset.ichs.g_para[i].t0h = 0.;
		pop.paraset.ichs.g_para[i].tm.val = 1.; //ms
		pop.paraset.ichs.g_para[i].th.val = 5000; //ms
		pop.paraset.ichs.g_para[i].Vsm.val = -47.1; //mV
		pop.paraset.ichs.g_para[i].Vhfm.val = -47.1; //mV
		pop.paraset.ichs.g_para[i].Vhfm2.val = -47.1; //mV
		pop.paraset.ichs.g_para[i].km.val = -3.1; //mV	
		pop.paraset.ichs.g_para[i].ktm.val = 6.2; //mV
		pop.paraset.ichs.g_para[i].ktm2.val = 6.2; //mV
		pop.paraset.ichs.g_para[i].Vsh.val = -60.0; //mV
		pop.paraset.ichs.g_para[i].Vhfh.val = -60.0; //mV
		pop.paraset.ichs.g_para[i].Vhfh2.val = -60.0; //mV
		pop.paraset.ichs.g_para[i].kh.val = 9.0; //mV		
		pop.paraset.ichs.g_para[i].kth.val = 9.0; //mV
		pop.paraset.ichs.g_para[i].kth2.val = 9.0; //mV

		//K parameters (alpha beta)
		i = 0;
		pop.paraset.ichs.ab_para[i].g.val = 180.0; //nS
		//pop.paraset.ichs.ab_para[i].g.var = 0; //percentage (%)
		pop.paraset.ichs.ab_para[i].E.val = -94.358937;
		pop.paraset.ichs.ab_para[i].mpower = 4;
		pop.paraset.ichs.ab_para[i].hpower = 0;
		pop.paraset.ichs.ab_para[i].M0 = 0.03;
		pop.paraset.ichs.ab_para[i].H0 = 1.;
		pop.paraset.ichs.ab_para[i].t0m = 0.;
		pop.paraset.ichs.ab_para[i].tmaxm = 1.;
		pop.paraset.ichs.ab_para[i].t0h = 0.;
		pop.paraset.ichs.ab_para[i].tmaxh = 1.;
		pop.paraset.ichs.ab_para[i].MAa.val = -0.01; //mV
		pop.paraset.ichs.ab_para[i].MBa.val = -0.44; //mV
		pop.paraset.ichs.ab_para[i].MCa.val = 44.; //ms
		pop.paraset.ichs.ab_para[i].MDa.val = -5.; //mV
		pop.paraset.ichs.ab_para[i].MEa.val = -1.; //mV
		pop.paraset.ichs.ab_para[i].MAb.val = 0.; //mV
		pop.paraset.ichs.ab_para[i].MBb.val = 0.17; //mV
		pop.paraset.ichs.ab_para[i].MCb.val = 49.; //ms
		pop.paraset.ichs.ab_para[i].MDb.val = 40.; //mV
		pop.paraset.ichs.ab_para[i].MEb.val = 0; //mV
		pop.paraset.ichs.ab_para[i].HAa.val = -0.01; //mV
		pop.paraset.ichs.ab_para[i].HBa.val = -0.44; //mV
		pop.paraset.ichs.ab_para[i].HCa.val = 44.; //ms
		pop.paraset.ichs.ab_para[i].HDa.val = -5.; //mV
		pop.paraset.ichs.ab_para[i].HEa.val = -1.; //mV
		pop.paraset.ichs.ab_para[i].HAb.val = 0.; //mV
		pop.paraset.ichs.ab_para[i].HBb.val = 0.17; //mV
		pop.paraset.ichs.ab_para[i].HCb.val = 49.; //ms
		pop.paraset.ichs.ab_para[i].HDb.val = 40.; //mV
		pop.paraset.ichs.ab_para[i].HEb.val = 0; //mV

		//Leak parameters
		i = 0;
		pop.paraset.ichs.ng_para[i].g.val = 2.5; //nS
		pop.paraset.ichs.ng_para[i].E.val = -68;
		pop.paraset.ichs.ng_para[i].E.var = 2;


		//======== ion settings=========================
		create_ion(pop, "Na", 1);
		//			create_ion(pop,"Ca",3);

		i = 0; //Na ion
		pop.paraset.ions.ion_para[i].Cin = 15;
		pop.paraset.ions.ion_para[i].Cout = 145;
		pop.paraset.ions.ion_para[i].Z = Z_NA;
		pop.paraset.ions.ion_para[i].pumpType = 0;
		pop.paraset.ions.ion_para[i].InEq = 15;
		pop.paraset.ions.ion_para[i].t = 500;
		pop.paraset.ions.ion_para[i].Rpmp = 1;
		pop.paraset.ions.ion_para[i].Rch = 1;
		pop.paraset.ions.ion_para[i].Kp = 15;


		//i=1; //Ca ion
		//pop.paraset.ions.ion_para[i].Cin = 140;
		//pop.paraset.ions.ion_para[i].Cout = 4;
		//pop.paraset.ions.ion_para[i].Z = Z_CA;
		//pop.paraset.ions.ion_para[i].pumpType = 0;
		//pop.paraset.ions.ion_para[i].InEq = 0;
		//pop.paraset.ions.ion_para[i].t = 500;
		//pop.paraset.ions.ion_para[i].Rpmp = 0.0045;
		//pop.paraset.ions.ion_para[i].Rch = 1;
		//pop.paraset.ions.ion_para[i].K = 0.001;
		//pop.paraset.ions.ion_para[i].B = 0.03;

		//======== neuron settings ======================
		pop.paraset.neu_para.Cm.val = 36.2; // pF
		pop.paraset.neu_para.Vm.val = -60; //mV

		//pop.paraset.net_para.E.val = 0; //mV
		//pop.paraset.net_para.g.val = 0.1; //nS
		//pop.paraset.net_para.t.val = 5; //ms

		//========= control settings =====================
		//pop.paraset.ctr_para.seed = newseed();   // SM uncomment?
		//pop.paraset.ctr_para.simduration = 1000; // SM uncomment?
		//pop.paraset.ctr_para.simstep = 0.05;     // SM uncomment?
		//pop.paraset.ctr_para.threshold = -35;    
		pop.threshold = -35;

		break;
	case 2: //Naf(G), K(ab),CaL(G),KCa(ZG), Leak, SynpE (for others, Smith 2007)
		
		//====Channel Settings===================
		create_ich(pop, "Naf", 3, 0, 1);
		create_ich(pop, "K", 7, 1, 2);
		create_ich(pop, "CaL", 3, 0, 3);
		create_ich(pop, "KCa", 9, 1, 3);
		create_ich(pop, "Leak", 0, 3, 0);
		//create_ich(pop, "Synp", 13, 3);

		//iNaf (Genetic) parameters setting
		pop.paraset.ichs.g_para[i].g.val = 400.; //nS
		pop.paraset.ichs.g_para[i].E.val = 55.1884;
		pop.paraset.ichs.g_para[i].mpower = 3;
		pop.paraset.ichs.g_para[i].hpower = 1;
		pop.paraset.ichs.g_para[i].M0 = 0.02;
		pop.paraset.ichs.g_para[i].H0 = 0.3;
		pop.paraset.ichs.g_para[i].t0m = 0.;
		pop.paraset.ichs.g_para[i].t0h = 0.;
		pop.paraset.ichs.g_para[i].tm.val = 0.252; //ms
		pop.paraset.ichs.g_para[i].th.val = 8.456; //ms
		pop.paraset.ichs.g_para[i].Vsm.val = -43.8; //mV
		pop.paraset.ichs.g_para[i].Vhfm.val = -43.8; //mV
		pop.paraset.ichs.g_para[i].Vhfm2.val = -43.8; //mV
		pop.paraset.ichs.g_para[i].km.val = -6.0; //mV ** This value should be minus(-)		
		pop.paraset.ichs.g_para[i].ktm.val = 14.0; //mV
		pop.paraset.ichs.g_para[i].ktm2.val = 14.0; //mV
		pop.paraset.ichs.g_para[i].Vsh.val = -67.5; //mV
		pop.paraset.ichs.g_para[i].Vhfh.val = -67.5; //mV
		pop.paraset.ichs.g_para[i].Vhfh2.val = -67.5; //mV
		pop.paraset.ichs.g_para[i].kh.val = 10.8; //mV	
		pop.paraset.ichs.g_para[i].kth.val = 12.8; //mV
		pop.paraset.ichs.g_para[i].kth2.val = 12.8; //mV

 	    //K parameters (alpha beta)
		i = 0;
		pop.paraset.ichs.ab_para[i].g.val = 250.0; //nS
		//pop.paraset.ichs.ab_para[i].g.var = 0; //percentage (%)
		pop.paraset.ichs.ab_para[i].E.val = -94.358937;
		pop.paraset.ichs.ab_para[i].mpower = 4;
		pop.paraset.ichs.ab_para[i].hpower = 0;
		pop.paraset.ichs.ab_para[i].M0 = 0.03;
		pop.paraset.ichs.ab_para[i].H0 = 1.;
		pop.paraset.ichs.ab_para[i].t0m = 0.;
		pop.paraset.ichs.ab_para[i].tmaxm = 1.;
		pop.paraset.ichs.ab_para[i].t0h = 0.;
		pop.paraset.ichs.ab_para[i].tmaxh = 1.;
		pop.paraset.ichs.ab_para[i].MAa.val = -0.01; //mV
		pop.paraset.ichs.ab_para[i].MBa.val = -0.44; //mV
		pop.paraset.ichs.ab_para[i].MCa.val = 44.; //ms
		pop.paraset.ichs.ab_para[i].MDa.val = -5.; //mV
		pop.paraset.ichs.ab_para[i].MEa.val = -1.; //mV
		pop.paraset.ichs.ab_para[i].MAb.val = 0.; //mV
		pop.paraset.ichs.ab_para[i].MBb.val = 0.17; //mV
		pop.paraset.ichs.ab_para[i].MCb.val = 49.; //ms
		pop.paraset.ichs.ab_para[i].MDb.val = 40.; //mV
		pop.paraset.ichs.ab_para[i].MEb.val = 0; //mV

		pop.paraset.ichs.ab_para[i].HAa.val = -0.01; //mV
		pop.paraset.ichs.ab_para[i].HBa.val = -0.44; //mV
		pop.paraset.ichs.ab_para[i].HCa.val = 44.; //ms
		pop.paraset.ichs.ab_para[i].HDa.val = -5.; //mV
		pop.paraset.ichs.ab_para[i].HEa.val = -1.; //mV
		pop.paraset.ichs.ab_para[i].HAb.val = 0.; //mV
		pop.paraset.ichs.ab_para[i].HBb.val = 0.17; //mV
		pop.paraset.ichs.ab_para[i].HCb.val = 49.; //ms
		pop.paraset.ichs.ab_para[i].HDb.val = 40.; //mV
		pop.paraset.ichs.ab_para[i].HEb.val = 0; //mV
		//CaL parameters (Generic)
		i = 1;
		pop.paraset.ichs.g_para[i].g.val = 0.05; //nS
		pop.paraset.ichs.g_para[i].E.val = 149.815405;
		pop.paraset.ichs.g_para[i].mpower = 1;
		pop.paraset.ichs.g_para[i].hpower = 1;
		pop.paraset.ichs.g_para[i].M0 = 0.03;
		pop.paraset.ichs.g_para[i].H0 = 0.2;
		pop.paraset.ichs.g_para[i].t0m = 0.5;
		pop.paraset.ichs.g_para[i].t0h = 18;
		pop.paraset.ichs.g_para[i].tm.val = 0; //ms
		pop.paraset.ichs.g_para[i].th.val = 0; //ms
		pop.paraset.ichs.g_para[i].Vsm.val = -27.4; //mV
		pop.paraset.ichs.g_para[i].Vhfm.val = -27.4; //mV
		pop.paraset.ichs.g_para[i].Vhfm2.val = -27.4; //mV
		pop.paraset.ichs.g_para[i].km.val = -5.7; //mV ** This value should be minus(-)		
		pop.paraset.ichs.g_para[i].ktm.val = 1; //mV
		pop.paraset.ichs.g_para[i].ktm2.val = 1; //mV
		pop.paraset.ichs.g_para[i].Vsh.val = -52.4; //mV
		pop.paraset.ichs.g_para[i].Vhfh.val = -52.4; //mV
		pop.paraset.ichs.g_para[i].Vhfh2.val = -52.4; //mV
		pop.paraset.ichs.g_para[i].kh.val = 5.2; //mV		
		pop.paraset.ichs.g_para[i].kth.val = 1; //mV
		pop.paraset.ichs.g_para[i].kth2.val = 1; //mV

		//KCa parameters (Z Generic)
		i = 0;
		pop.paraset.ichs.z_para[i].g.val = 3.0; //nS
		pop.paraset.ichs.z_para[i].E.val = 149.815405;
		pop.paraset.ichs.z_para[i].mpower = 2;
		pop.paraset.ichs.z_para[i].hpower = 0;
		pop.paraset.ichs.z_para[i].M0 = 0.03;
		pop.paraset.ichs.z_para[i].H0 = 1.;
		pop.paraset.ichs.z_para[i].A.val = 0.5; //0.5 * 1e8
		pop.paraset.ichs.z_para[i].B.val = 1;
		pop.paraset.ichs.z_para[i].L.val = 2;
		pop.paraset.ichs.z_para[i].G.val = 1;
		pop.paraset.ichs.z_para[i].Vhf.val = 0;
		pop.paraset.ichs.z_para[i].slp.val = 1;
		pop.paraset.ichs.z_para[i].t0 = 0;
		pop.paraset.ichs.z_para[i].tmax = 1; //1*400

		//Leak parameters
		i = 0;
		pop.paraset.ichs.ng_para[i].g.val = 2.5; //nS
		pop.paraset.ichs.ng_para[i].E.val = -60;
		pop.paraset.ichs.ng_para[i].E.var = 0;


		//======== ion settings=========================
		create_ion(pop, "Na", 1);
		create_ion(pop,"Ca",3);

		i = 0; //Na ion
		pop.paraset.ions.ion_para[i].Cin = 15;
		pop.paraset.ions.ion_para[i].Cout = 120;
		pop.paraset.ions.ion_para[i].Z = Z_NA;
		pop.paraset.ions.ion_para[i].pumpType = 0;
		pop.paraset.ions.ion_para[i].InEq = 15;
		pop.paraset.ions.ion_para[i].t = 500;
		pop.paraset.ions.ion_para[i].Rpmp = 1;
		pop.paraset.ions.ion_para[i].Rch = 1;
		pop.paraset.ions.ion_para[i].Kp = 15;


		i=1; //Ca ion
		pop.paraset.ions.ion_para[i].Cin = 5e-5;
		pop.paraset.ions.ion_para[i].Cout = 4;
		pop.paraset.ions.ion_para[i].Z = Z_CA;
		pop.paraset.ions.ion_para[i].pumpType = 3;
		pop.paraset.ions.ion_para[i].InEq = 5e-5;
		pop.paraset.ions.ion_para[i].t = 500;
		pop.paraset.ions.ion_para[i].Rpmp = 1;
		pop.paraset.ions.ion_para[i].Rch = 0.0045;
		pop.paraset.ions.ion_para[i].K = 0.001;
		pop.paraset.ions.ion_para[i].B = 0.03;

		//======== neuron settings ======================
		pop.paraset.neu_para.Cm.val = 36.2; // pF
		pop.paraset.neu_para.Vm.val = -60; //mV

		//pop.paraset.net_para.E.val = 0; //mV
		//pop.paraset.net_para.g.val = 0.1; //nS
		//pop.paraset.net_para.t.val = 5; //ms

		//========= control settings =====================
		//pop.paraset.ctr_para.seed = newseed();   // SM uncomment?
		//pop.paraset.ctr_para.simduration = 1000; // SM uncomment?
		//pop.paraset.ctr_para.simstep = 0.05;     // SM uncomment? 
		pop.threshold = -35;

		break;
	};
};

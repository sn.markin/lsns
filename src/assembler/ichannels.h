
#ifndef __ICHANNELS_H
#define __ICHANNELS_H

#include "myheader.h"
#include "parameters.h"

struct strCom_Gen1Para {
	string name;
	int ID;
	int mpower;
	int hpower;
};

//struct strCom_GenPara {
//	string name;
//	int ID;
//	double *pg_max;	//Maximum Conductance
//	double *ptm_max;
//	double *pth_max;
//	double *pVhfm;	//Activation half voltage
//	double *pVhfh;	//Inactivation half voltage
//	double *pkm;		//Slope factor for activation
//	double *pkh;		//Slope factor for inactivation
//	double *pktm;		//Slope factor for actiavation time constant
//	double *pkth;		//Slope factor for inactiavation time constant
//	double *pE;		//Reverse Potential or rest potential
//};

struct strCom_GenPara {
	string name;
	int ID;
	//int sizel[10];
	vector<double> g_max;	//Maximum Conductance
	vector<double> tm_max;
	vector<double> th_max;
	vector<double> Vhfm;	//Activation half voltage
	vector<double> Vhfh;	//Inactivation half voltage
	vector<double> km;		//Slope factor for activation
	vector<double> kh;		//Slope factor for inactivation
	vector<double> ktm;		//Slope factor for actiavation time constant
	vector<double> kth;		//Slope factor for inactiavation time constant
	vector<double> E;		//Reverse Potential or rest potential
};

struct strGeneral_IChannel {
	string name;
	int ID;
	double I;		//Current
	double G;		//Conductance	
	double M;		//Activation of voltage-gated channel
	double H;		//Inactivation of voltage-gated channel
	double m_inf;	//steady-state activation
	double h_inf;	//steady-state inactivation
	double Tm;		//Voltage dependent time constant for activation	
	double Th;		//Voltage dependent time constant for inactivation
	double Vc;		//Current Voltage
	
};

struct strAct_IChannel {
	string name;
	int ID;
	double I;		//Current
	double G;		//Conductance
	double M;		//Activation of voltage-gated channel
	double m_inf;	//steady-state activation
	double Tm;		//Voltage dependent time constant for activation	
	double Vc;		//Current Voltage

};

struct strInAct_IChannel {
	string name;
	int ID;
	double I;		//Current
	double G;		//Conductance
	double H;		//Activation of voltage-gated channel
	double h_inf;	//steady-state activation
	double Th;		//Voltage dependent time constant for activation	
	double Vc;		//Current Voltage

};

struct strNoGate_IChannel {
	string name;
	int ID;
	double I;		//Current
	double G;		//Conductance
	double Vc;		//Current Voltage
};

class General_IChannel{
public:
	General_IChannel();
	// General_IChannel(char*,double,double,double);
	virtual ~General_IChannel();
	General_IChannel& operator= (const General_IChannel& channel);
public:
	int init_ICh(strGeneral_IChannel&);
	void copy_ICh(strGeneral_IChannel&);	 
public:
	string name;
	int ID;
	double I;		//Current
	double G;		//Total Conductance
	double M;		//Activation of voltage-gated channel
	double H;		//Inactivation of voltage-gated channel
	double m_inf;	//steady-state activation
	double h_inf;	//steady-state inactivation
	double Tm;		//Voltage dependent time constant for activation	
	double Th;		//Voltage dependent time constant for inactivation
	double Vc;		//Current Voltage

public:
	void cal_all(double,const vector<strCom_Gen1Para>&,const vector<strCom_GenPara>&,int,int);
	void cal_all(double,double,const vector<strCom_Gen1Para>&,const vector<strCom_GenPara>&,int,int);
	
};

class Act_IChannel{
public:
	Act_IChannel();
	// Act_IChannel(char*,int,double,double,double);
	virtual ~Act_IChannel();
public:
	int init_ICh(strAct_IChannel&);
	int init_ICh(strGeneral_IChannel&);
	void copy_ICh(strAct_IChannel&);
public:
	string name;
	int ID;
	double I;		//Current
	double G;		//Total Conductance
	double M;		//Activation of voltage-gated channel
	double m_inf;	//steady-state activation
	double Tm;		//Voltage dependent time constant for activation
	double Vc;		//Current Voltage
	void cal_all(double,const vector<strCom_Gen1Para>&,const vector<strCom_GenPara>&,int,int);
	void cal_all(double,double,const vector<strCom_Gen1Para>&,const vector<strCom_GenPara>&,int,int);
};

class InAct_IChannel{
	public:
		InAct_IChannel();
	// InAct_IChannel(char*,int,double,double,double);
	virtual ~InAct_IChannel();
public:	
	int init_ICh(strInAct_IChannel&);
	int init_ICh(strGeneral_IChannel&);
	void copy_ICh(strInAct_IChannel&);
public:
	string name;
	int ID;
	double I;		//Current
	double G;		//Total Conductance
	double H;		//Activation of voltage-gated channel
	double h_inf;	//steady-state activation
	double Th;
	double Vc;		//Current Voltage
	void cal_all(double,const vector<strCom_Gen1Para>&,const vector<strCom_GenPara>&,int,int);
	void cal_all(double,double,const vector<strCom_Gen1Para>&,const vector<strCom_GenPara>&,int,int);

};

class NoGate_IChannel{
public:
	NoGate_IChannel();
	//NoGate_IChannel(char*,double,double,double);
	virtual ~NoGate_IChannel();
public:
	int init_ICh(strNoGate_IChannel&);
	int init_ICh(strGeneral_IChannel&);
	void copy_ICh(strNoGate_IChannel&);
public:
	string name;
	int ID;
	double I;		//Current
	double G;		//Conductance
	double g_max;	//Maximum Conductance
	double Vc;		//Current Voltage
	void cal_all(const vector<strCom_GenPara>&,int,int);
	void cal_all(double,const vector<strCom_GenPara>&,int,int);
};

class Naf_IChannel: public General_IChannel {
public:
	Naf_IChannel(){}
	// Naf_IChannel(char*,double,double,double);
	virtual ~Naf_IChannel() {}
	//mpower = 3; hpower = 1;
};

class NaP_IChannel: public General_IChannel {
public:
	NaP_IChannel(){}
	// NaP_IChannel(char*,double,double,double);
	virtual ~NaP_IChannel() {}
	//mpower = 1; hpower = 1;
};

class K_IChannel: public Act_IChannel {
public:
	K_IChannel(){}
	// Naf_IChannel(char*,double,double,double);
	virtual ~K_IChannel() {}
	//mpower = 4;
};


#endif
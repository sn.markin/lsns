#ifndef _PARAMETERS_H
#define _PARAMETERS_H

#include "myheader.h"
#include "customfuncs.h"

struct param {
	double val;
	double var;
};

//Definition of channel parameter classes
class ich_parameters {
public:
	ich_parameters(){ init_para();};
	ich_parameters(string,int);
	//virtual ~ich_parameters();
public:
	string name;
	int ID;
	int MHtype;	//0: MH, 1:M, 2:H, 3:NoGate
	int Gtype; //types defined by gates.h, 0 ~ 13
	int Itype; //Ion types
	int Dstatus; //Ion Dynamic On/Off
	param g;
	param E;
	int mpower;
	int hpower;

	double M0;
	double H0;

	param Vsm; //mV, steady state half voltage
	param Vhfm; //mV, half voltage
	param Vhfm2; //mV, half voltage for modified generic
	param km; //mV, slope
	//param km2; //mV, slope for modified generic
	param tm; //ms, max time constant
	param ktm; //mV, slope factor for time constant
	param ktm2; //mV, slope factor for time constant 2

	double t0m;
	double t0h;

	param Vsh; //mV, steady state half voltage
	param Vhfh; //mV
	param Vhfh2; //mV, half voltage for modified generic
	param kh; //mV
	//param kh2; //mV, slope for modified generic
	param th; //ms
	param kth; //mV
	param kth2; //mV, slope factor for time constant 2

	void init_para();
};

class ich_ng_parameters {
public:
	ich_ng_parameters(){init_para();};
	ich_ng_parameters(string,int);
	//virtual ~ich_ng_parameters();
public:
	string name;
	int ID;
	int MHtype;	//0: MH, 1:M, 2:H, 3:NoGate
	int Gtype;
	int Itype;
	int Dstatus;
	param g;
	param E;
	double Tc;
	double Amp;

	void init_para();
};

class synp_parameters { // added by TK
public:
	synp_parameters() { init_para(); };
	synp_parameters(string);
public:
	string name;
	int IonType;
	param g;
	param E;
	double Tc;
	double Amp;

	void init_para();
};

class ich_ab_parameters {
public:
	ich_ab_parameters() { init_para(); };
	ich_ab_parameters(string, int);
	//virtual ~ich_parameters();
public:
	string name;
	int ID;
	int MHtype;	//0: MH, 1:M, 2:H, 3:NoGate
	int Gtype;
	int Itype;
	int Dstatus;
	param g;
	param E;
	int mpower;
	int hpower;

	double M0;
	double H0;

	double t0m;
	double t0h;
	double tmaxm;
	double tmaxh;

	//Alpha parameters
	param MAa;
	param MBa;
	param MCa;
	param MDa;
	param MEa;

	//Beta parameters
	param MAb;
	param MBb;
	param MCb;
	param MDb;
	param MEb;

	//Alpha parameters
	param HAa;
	param HBa;
	param HCa;
	param HDa;
	param HEa;

	//Beta parameters
	param HAb;
	param HBb;
	param HCb;
	param HDb;
	param HEb;

	void init_para();
};

class ich_z_parameters {
public:
	ich_z_parameters() { init_para(); };
	ich_z_parameters(string, int);
	//virtual ~ich_parameters();
public:
	string name;
	int ID;
	int MHtype;	//0: MH, 1:M, 2:H, 3:NoGate
	int Gtype;
	int Itype;
	int Dstatus;
	param g;
	param E;
	int mpower;
	int hpower;

	double M0;
	double H0;

	param A;
	param B;
	param L;
	param G;
	param Vhf;

	param slp;
	double t0;
	double tmax;

	void init_para();
};

//ion parameter class
class ion_parameters {
public:
	ion_parameters(){init_para();};
	ion_parameters(string,int);
	//virtual ~ions_parameters();
public:
	string name;
	int ID;
	int ionType;
	int pumpType;
	double Cin;
	double Cout;
	double E;
	double R;
	double F; // C/M
	double T; //K
	double Z;
	double InEq; //Equilibrium point inside of cell
	double t; //Time constant for ion dynamics
	double Rpmp; //Ratio of pump (or apump)
	double Rch; //Ratio of channel
	double Kp; //Only for Na ion
	double K; //Coefficient only for Ca ion
	double B; //Coefficient only for Ca ion
	void init_para();
};
//definition of neuron parameter class
class neu_parameters {
public:
	neu_parameters(){ init_para(); };
	//virtual ~neu_parameters();
public:
	int neuT; //neuron type: soma, dendrite
	param Cm; // pF
	param Vm; //mV
	double area;
	//double pkgmax;
	
	void init_para();
};
class contr_parameters {
public:
	contr_parameters(){init_para();};
	//virtual ~contr_parameters();
public:
	double threshold;
	unsigned int seed;
	double simduration;
	double simstep;
	bool srand_status;
	//RNG rng;

	//vector<synp_parameters> syn_para;

	void init_para();
};
//class netw_parameters {        //TK
//public:
//	netw_parameters(){};
//	netw_parameters(string){};
//	//virtual ~netw_parameters();
//public:
//	string name;
//	int type;
//	//param E; //mV
//	//param g; //nS
//	//param t; //ms
//	//param w;
//
//
//};

class ichs_set {
//This class is to make a set to include all types of I channels
public:
	ichs_set(){init();};
	ichs_set& operator= (const ichs_set&);
public:
	vector<ich_parameters> g_para;
	vector<ich_ab_parameters> ab_para;
	vector<ich_z_parameters> z_para;
	vector<ich_ng_parameters> ng_para;
	
	int nIChs;	//the number of chennel including leak and synapse
	int nIonTs; //the number of ion types including leak and synapse, this could be smaller than nIChs
	
	vector<string> IChNList; //Name List
	vector<int> IChDSList; //Ion Dynamics Status List
	vector<int> IChGTList; //Gate Type List
	vector<int> IChMHTList; //MH Type List. 0:MH, 1:M, 2:H, 3:NG
	vector<vector<int>> IChIdxList; //IChIdxList[x]: one of g_para, ab_para, z_para, ng_para
									//IChIdxList[x][y]: index of the vector defined by x
	vector<int> IChIonTList;
	vector<int> ICh2IonTmap;
	vector<int> IonTList;
	//vector<string> IonTNList;

	void update();
	void init();
};

class ions_set {
public:
	ions_set(){init();};
	ions_set& operator= (const ions_set&);
public:
	bool sw_ion_dynam;
	vector<ion_parameters> ion_para;
	int nIons;
	//vector<int> IonTList;
	//vector<string> IonNList;
	void update();
	void init();
};

class parameter_set {
public:
	parameter_set(){init();};
	parameter_set& operator= (const parameter_set&);
public:
	
	neu_parameters neu_para;
	//netw_parameters net_para;     TK
	//contr_parameters ctr_para;    TK

	ions_set ions;
	ichs_set ichs;

	void init();

};


#endif
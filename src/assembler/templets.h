#ifndef _TEMPLETS_H
#define _TEMPLETS_H

#include "myheader_all.h"

#define Z_NA	1
#define Z_K		1
#define Z_CL	-1
#define Z_CA	2
#define Z_MG	2

void load_tmpl(vector<neural_population>&,string,int);
void load_tmpl(neural_population&,int);
void load_tmplc(neural_population&, int); //This is the core of loading
#endif
#include "nprojects.h"
#include "commands.h"

void nns_project::reset() {
	pops.clear();
	tnet.nets.clear();
	tnet.mnets.clear();
	iodata.clear();

};

void create_param(vector<parameter_set>& tAllpara) {
	parameter_set tmp;
	//tmp = new parameter_set;   TK
	tAllpara.push_back(tmp);
};

void create_proj(vector<nns_project>& proj, string name) {
	nns_project tmp;
	//tmp = new nns_project;   TK
	tmp.name = name;
	proj.push_back(tmp);
};

void rename_proj(vector<nns_project>& pros, string oldname, string newname) {
	int pid = find_id(pros, oldname);

	if (pid != -1) {
		int tid = find_id(pros, newname);
		if (tid == -1) {
			pros[pid].name = newname;
			cout << "Project (" << oldname << "): renamed as (" << newname << ")" << endl;
		}
		else {
			cout << "ERROR::Project (" << newname << "): already exists in the project list" << endl;
		};
	}
	else {
		cout << "ERROR::Project (" << oldname << "): Not found in the project list" << endl;
	};
};

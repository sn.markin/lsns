#include "customfuncs.h"

RNG Rng(( unsigned long)time( 0 )); // SM
//Uniform distribution random number array
/* To control rand() with a seed, use srand(seed value).
N: the number of elements in the target array
DECI: decimal point
AVG: mean value
PERCENT: percentage(%) for variance from the AVG
*/
double *unirdarray(int n, int deci, double avg, int percent) {
	double maxv = avg + avg*((double)percent/100);
	double minv = avg - avg*((double)percent/100);
	unsigned int tmp = 1;
	tmp = (unsigned int)pow(10.,deci);
	double *rarray = new double [n];
	for (int i=0;i<n;i++) {
		
		rarray[i] = minv + (double(rand()%tmp) / tmp)*(maxv-minv);
	};
	return rarray;
	//delete [] rarray;
};

//Normal distribution (Gauss) random number array
/* Create RNG first outside this function and feed it to this.
To control RNG with a seed, use RNG.seed(seed value).
N: the number of elements in the target array
SIG: standard deviation sigma (This is not actual standard deviation value,
rather it is a value of sigma for normalized standard deviation. Within 4 sigma,
data fall in 99.7% range.)
AVG: mean value
PERCENT: percentage(%) for variance from the AVG
*/
double *normrdarray(RNG& rng, int n, double sig, double avg, double percent) {
		
	double var;
	double *ndrdarr = new double [n];
	int yflg = 0;
	double x, y, r, d, nd, max_t, min_t;
	double maxv = double(rng.max());

	var = avg*(percent/100.); //Variance

	//Box Muller method for probability density function
	for (int i=0;i<n;i++) {
		if (yflg == 0)
		{
			r = 0;
			while (r==0 || r>=1) {
				x = 2.*double(rng())/maxv - 1;
				y = 2.*double(rng())/maxv - 1;
				r = x*x + y*y;
			};
			d = sqrt(-2.*log(r)/r);
			nd = x*d;
			yflg = 1;
		}
		else
		{
			nd = y*d;
			yflg = 0;
		};
		ndrdarr[i] = nd*sig;
	};

	max_t = *max_element(ndrdarr,ndrdarr+n);
	min_t = *min_element(ndrdarr,ndrdarr+n);

	//Normalizing
	for (int i=0;i<n;i++) {
		if (ndrdarr[i] >= 0)
			ndrdarr[i] = var*ndrdarr[i]/(max_t-min_t) + avg;
		else
			ndrdarr[i] = var*ndrdarr[i]/(max_t-min_t) + avg;
	};
	return ndrdarr;
	//delete [] ndrdarr;
};

double normrd( RNG &rng, double mean, double var )   // SM
{
static	int yflg = 0;
static	double nd[2] = {0.};
	if( yflg == 0 ){
		double r = 0.;
		double x = 0., y = 0.;
		double maxv = double( rng.max() );
		while( r == 0. || r >= 1 ){
			x = 2.*rng()/maxv-1.;
			y = 2.*rng()/maxv-1.;
			r = x*x+y*y;
		}
		double d = sqrt( -2.*log( r )/r );
		nd[1] = x*d;
		nd[0] = y*d;
		yflg = 1;
	}
	else{
		yflg = 0;
	}
	return  mean*( 1.+var*nd[yflg] );
}

double unird( RNG& rng, double mean, double var ) // SM
{
	double ampl = double( rng.max()-rng.min() );
	return mean*( 1.+var*( 2.*rng()/ampl-1. ));
}

double unird( RNG& rng, double var ) // SM
{
	double ampl = double( rng.max()-rng.min() );
	return var*rng()/ampl;
}

vector<double> normrdvec(RNG& rng, int n, double sig, double avg, double percent) {
		
	double var;
	vector<double> ndrdvec;
	ndrdvec.resize(n);
	int yflg = 0;
	double x, y, r, d, nd, max_t, min_t;
	double maxv = double(rng.max());

	var = avg*(percent/100.); //Variance

	//Box Muller method for probability density function
	for (int i=0;i<n;i++) {
		if (yflg == 0)
		{
			r = 0;
			while (r==0 || r>=1) {
				x = 2.*double(rng())/maxv - 1;
				y = 2.*double(rng())/maxv - 1;
				r = x*x + y*y;
			};
			d = sqrt(-2.*log(r)/r);
			nd = x*d;
			yflg = 1;
		}
		else
		{
			nd = y*d;
			yflg = 0;
		};
		ndrdvec[i] = nd*sig;
	};

	max_t = *max_element(ndrdvec.begin(),ndrdvec.end());
	min_t = *min_element(ndrdvec.begin(),ndrdvec.end());

	//Normalizing
	for (int i=0;i<n;i++) {
		if (ndrdvec[i] >= 0)
			ndrdvec[i] = var*ndrdvec[i]/(max_t-min_t) + avg;
		else
			ndrdvec[i] = var*ndrdvec[i]/(max_t-min_t) + avg;
	};
	return ndrdvec;
};

//Random integer array
/* To control rand() with a seed, use srand(seed value).
n: the number of elements in the target array
maxn: maximum number of integers. The integer range is 0 ~ maxn-1
*/
int *rdiarray(int n, int maxn) {
	int *iarray = new int [n];
	for (int i=0;i<n;i++) {
		iarray[i] = rand() % maxn;
	};
	return iarray;
	//delete [] iarray;
};


//Random integer permutation array (without repeatation) 
/* To control rand() with a seed, use srand(seed value).
n: the number of elements in the target array
maxn: maximum number of integers. The integer range is 0 ~ maxn-1
*/
int *rdipermarray(int n, int maxn) {
	int *iarray = new int [n];
	int tmp, c, doneflg;

	for (int i=0;i<n;i++) {
		if (i==0) 
			iarray[i] = rand() % maxn;
		else{
			doneflg = 0;
			while (doneflg == 0) {
				tmp =  rand() % maxn;
				c=0;
				for (int j=0;j<i;j++) {
					if (iarray[j] == tmp) {
						c++;
						break;
					};
				};
				if (c==0) {
					iarray[i] = tmp;
					doneflg = 1;
				};
			};
		};
	};
	return iarray;
	//delete [] iarray;
};

double round1(double x) {
	return floor(x+0.5);
};

//rounding decimal
double roundd(double x, int dec) {
	return floor(x*pow(10,double(dec))+0.5)/pow(10,double(dec));
};

//rounding decimal
double roundd(double* px, int dec) {
	return floor((*px)*pow(10,double(dec))+0.5)/pow(10,double(dec));
};

//flooring decimal
double floord(double x, int dec) {
	return floor(x*pow(10,double(dec)))/pow(10,double(dec));
};

//flooring decimal
double floord(double* px, int dec) {
	return floor((*px)*pow(10,double(dec)))/pow(10,double(dec));
};

//ceiling decimal
double ceild(double x, int dec) {
	return ceil(x*pow(10,double(dec)))/pow(10,double(dec));
};

//ceiling decimal
double ceild(double* px, int dec) {
	return ceil((*px)*pow(10,double(dec)))/pow(10,double(dec));
};


unsigned int newseed() {
	unsigned int outseed;
	outseed = (unsigned int)time(NULL);
	return outseed;
	//delete &outseed;
};

unsigned int newseed(unsigned int seed) {
	unsigned int outseed;
	int getout = 0;
	while (getout == 0) {
		outseed = (unsigned int)time(NULL);
		if (outseed != seed)
		{
			getout = 1;
		};
	}

	return outseed;
	//delete &outseed;
};

//Making histogram
map<double,int> hist(double *arr, int n, double bin) {
	int dec;
	double tmp;
	//int n = sizeof(*arr);
	map<double,int> histo;
	map<double,int>::iterator it;
	double *tarr = new double [n];
	for (int i=0;i<n;++i){
		tarr[i] = arr[i];
	};
	
	if (bin >= 1)
		dec = 1;
	else if (bin >= 0.1 || bin < 1)
		dec = 2;
	else if (bin >= 0.01 || bin < 0.1)
		dec = 3;
	else if (bin >= 0.001 || bin < 0.01)
		dec = 4;

	sort(tarr,tarr+n);
	int j=0, flg=0;
	for (int i=0;i<n;i++) {
		if (i==0) {
			histo[floord(tarr[i],dec-1)]=++j;
			it = histo.begin();
		}
		else {
			tmp = floord(tarr[i],dec);
			if (tmp >= it->first && tmp < (it->first + bin))
				histo[it->first] = ++j;
			else {
				flg = 0;
				while(flg==0) {
					j=0;
					histo[it->first + bin] = j;
					++it;
					if (tmp >= it->first && tmp < it->first+bin) {
						histo[it->first] = ++j;
						flg=1;
					};
				};

			};
		};
		
	};

	delete [] tarr;
	return histo;
};

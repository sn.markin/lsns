#ifndef _COMMANDS_H
#define _COMMANDS_H

#include "myheader.h"
//#include "myheader_all.h"
#include "nprojects.h"
//#include "networks.h"
#include "netmap.h"

#define CONNT_PP 0
#define CONNT_UP 1
#define CONNT_PU 2
#define CONNT_UU 3

using namespace engine;

struct unitpset {
	int *ppop;
	int *ppopion;
	int *ppopich;
	int *pnsu;
	int *pnsuich;
};

extern int find_id(vector<neural_population>&, string);
extern int find_id(vector<nns_project>&, string);
extern int find_id(vector<nns_net>&, string);
extern int find_id(vector<nns_mnet>&, string);

extern size_t find_UOffSet(string, size_t, nns_project&);
extern size_t find_UOffSet(string, nns_project&);
extern size_t find_SOffSet(string, size_t, size_t, nns_project&);
extern size_t find_SOffSet(string, size_t, nns_project&);

extern void create_iodata(string, int, string, nns_project&);
extern void delete_iodata(nns_project&);

extern void init_cuparam(netpar&, nns_project&);
extern void init_synapse(netpar&, nns_project&, mapsyn&);
extern void copy_mem2cu(netpar&, vector<neural_population>&, nns_nonspikeunit&, vector<nns_iodata>&, contr_parameters&);
extern void copy_mem2cu(netpar&, nns_project&);
extern void copy_map2mem(netpar&, mapsyn&);
extern int make_i4LUT(int, int, int);

extern void prep_sim(nns_project&, netpar&);
extern void run_sim(nns_project&, netpar&);
extern void clear_sim(nns_project&, netpar&);

extern void post_proc(nns_project&);

//void sim(vector<neural_population>&);

extern void edit();
extern void add();

extern void show(vector<neural_population>&, string);
extern void show(vector<neural_population>&, int);

extern void make_testnns(nns_project&);

extern size_t get_size(string, nns_project&);			// SM

#endif
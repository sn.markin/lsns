#ifndef __CAL_FUNCS_H
#define __CAL_FUNCS_H

#include "myheader.h"
#include "ichannels.h"

extern double cal_Tm(double,const vector<strCom_GenPara>&,int,int);
extern double cal_m_inf(double,const vector<strCom_GenPara>&,int,int);
extern double cal_M(double, double, double, double);

extern double cal_Th(double,const vector<strCom_GenPara>&,int,int);
extern double cal_h_inf(double,const vector<strCom_GenPara>&,int,int);
extern double cal_H(double, double, double, double);

extern double cal_I(double, double, double, double, double,const vector<strCom_Gen1Para>&,int);
extern double cal_Im(double, double, double, double,const vector<strCom_Gen1Para>&,int);
extern double cal_Ih(double, double, double, double,const vector<strCom_Gen1Para>&,int);
extern double cal_Ing(double, double, double);

#endif

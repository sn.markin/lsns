#ifndef __NPOPULATIONS_H
#define __NPOPULATIONS_H

#include "myheader.h"
#include "parameters.h"

class neural_population {
public:
	neural_population(){init();};
	neural_population(string n,int a,unsigned int b):name(n),ID(a),nneurons(b){};
	virtual ~neural_population();
	neural_population& operator= (const neural_population&);
public:
	string name;
	int ID;
	unsigned int nneurons;		//number of neurons in a population
	double threshold;

	parameter_set paraset;		//a set of pointers to all parameters and vectors

	void set_paraset(const parameter_set&);
	void init();
	
};

void create_pop(vector<neural_population>&, string, int);
void delete_pop(vector<neural_population>&, string);
void delete_pop(vector<neural_population>&, int);
void copy_pop(vector<neural_population>&, string);
void copy_pop(vector<neural_population>&, string, string);
void rename_pop(vector<neural_population>&, string, string);

void create_ich(vector<neural_population>&, int, string, int, int, int);
void create_ich(vector<neural_population>&, int, string, int, int, int, int);
void create_ich(neural_population&, string, int, int, int);
void create_ich(neural_population&, string, int, int, int, int);
void create_ich(vector<parameter_set>&, int, string, int, int, int);
void create_ich(vector<parameter_set>&, int, string, int, int, int, int);
void create_ichc(parameter_set&, string, int, int, int, int);
void create_ichc(ichs_set&, string, int, int, int, int);

void delete_ich(vector<neural_population>&, int, string);
void delete_ich(vector<neural_population>&, string);
void delete_ich(vector<parameter_set>&, int, string);
void delete_ich(vector<parameter_set>&, string);
int delete_ichc(parameter_set&, string);
int delete_ichc(ichs_set&, string);

void create_ion(vector<neural_population>&, int, string, int);
void create_ion(neural_population&, string, int);
void create_ion(vector<parameter_set>&, int, string, int);
void create_ion(parameter_set&, string, int);

void delete_ion(vector<neural_population>&, int, string);
void delete_ion(vector<neural_population>&, string);
void delete_ion(vector<parameter_set>&, int, string);
void delete_ion(vector<parameter_set>&, string);
void delete_ion(parameter_set&, string);


#endif
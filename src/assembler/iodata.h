#ifndef _IODATA_H
#define _IODATA_H

#include "myheader.h"
#include "parameters.h"
#include "npopulations.h"
#include "nnonspikeunits.h"
//#include "nprojects.h"

//Data Type definitions

#define IOD_IONSI_P 0		//IONSI.X PUMP CURRENT
#define IOD_IONSI_CH 1		//IONSI.Y CHANNEL CURRENT


#define IOD_IONSE_E 4		//IONSE.X REVERSAL POTENTIAL
#define IOD_IONSE_CI 5		//IONSE.Y CONCENTRATION OF INSIDE ION
#define IOD_IONSE_CO 6		//IONSE.Z CONCENTRATION OF OUTSIDE ION


#define IOD_CHANG_G 9		//CHANG.Y CHANNEL CONDUCTANCE
#define IOD_CHANG_I 10			//CHANG.Z CHANNEL CURRENT

#define IOD_CHANMH_AC 12	//CHANMH.X CHANNEL ACTIVATION
#define IOD_CHANMH_IAC 14	//CHANMH.Z CHANNEL INACTIVATION

#define IOD_CELLV_V 16		//CELLV.X MEMBRANE POTENTIAL
#define IOD_CELLV_SP 18	//CELLV.Z SPIKES ONSET
#define IOD_CELLV_IJ 19	//CELLV.W INJECTED CURRENT


class nns_iodata {
public:
	nns_iodata(){init();};
	nns_iodata(const nns_iodata& iod);	// modify by SM 01/05.2016
	nns_iodata& operator= (const nns_iodata&);
public:
	bool view;
	bool save;
	string name;
	string cmdline;
	int ID;

	int type;
	//vector<int> popList;
	//vector<vector<int>> neuList;
	//vector<vector<vector<int>>> chList;
	int npop; //number of selected populations
	int ntneu; //number of total neurons in selected pops
	int nnsu; //number of selected nsu
	vector<int> nneu;
	vector<vector<int>> popList;
	vector<vector<vector<int>>> neuList;
	vector<vector<int>> nsuList;
	int ich;
	float *data;

	void clear();
	void meminit(int);
private:
	void init();	// modify by SM 01/05.2016

};

extern void create_iodata(string name, int type, string strline, vector<nns_iodata>& iodata, vector<neural_population>& pops, nns_nonspikeunit& nsu, contr_parameters& ctr_para);
extern void delete_iodata(vector<nns_iodata>& iodata);
extern int parse_cmdline(nns_iodata*, string, vector<neural_population>&, nns_nonspikeunit&);


#endif
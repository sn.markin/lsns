#include "commands.h"
#include <string.h>
#include "iodata.h"
#include "datafiles.h"
#include "templets.h"

#include <iostream>  // SM
#include <fstream>   // SM

#define MAX_COMMA 20
#define ZGAco 1e8
#define ZGTMco 400

int find_id(vector<nns_project>& pros, string name) {
	size_t np = pros.size();
	int pid = -1;
	for (size_t i = 0;i<np;i++) {
		if (pros[i].name == name) {
			pid = (int)i;
			break;
		};
	};
	return pid;
};

int find_id(vector<neural_population>& pops, string name) {
	size_t np = pops.size();
	int pid = -1;
	for (size_t i = 0;i<np;i++) {
		if (pops[i].name == name) {
			pid = (int)i;
			break;
		};
	};
	return pid;
};

int find_id(nns_nonspikeunit& nsu, string name) {
	size_t nu = nsu.units.size();
	int uid = -1;
	for (size_t i = 0;i < nu;i++) {
		if (nsu.units[i].name == name) {
			uid = (int)i;
			break;
		};
	};
	return uid;
};

int find_id(vector<nns_net>& nets, string name) {
	size_t np = nets.size();
	int pid = -1;
	for (size_t i = 0;i<np;i++) {
		if (nets[i].name == name) {
			pid = (int)i;
			break;
		};
	};
	return pid;
};

int find_id(vector<nns_mnet>& mnets, string name) {
	size_t np = mnets.size();
	int pid = -1;
	for (size_t i = 0;i<np;i++) {
		if (mnets[i].name == name) {
			pid = (int)i;
			break;
		};
	};
	return pid;
};

void create_iodata(string a, int b, string c, nns_project& pro) {
	create_iodata(a, b, c, pro.iodata, pro.pops, pro.nsu, pro.ctr_para);
};

void delete_iodata(nns_project& pro) {
	delete_iodata(pro.iodata);
};

void show(vector<neural_population>& pops,int ID) {
	if (ID >=0 && ID < (int)pops.size())
	{
		cout<<"The population ID ["<<ID<<"] Information"<<endl;
		cout<<"1. Name: "<<pops[ID].name<<endl;
		cout<<"2. Size: "<<pops[ID].nneurons<<endl;
		if (pops[ID].paraset.ichs.nIChs <= 0 )
			cout<<"3. I Channels [0]: Not created yet"<<endl;
		else {
			cout<<"3. I Channels ["<<pops[ID].paraset.ichs.nIChs<<"]: ";
			for (int i=0;i<pops[ID].paraset.ichs.nIChs;i++) {
				if (i != pops[ID].paraset.ichs.nIChs -1)
					cout<<pops[ID].paraset.ichs.IChNList[i]<<", ";
				else
					cout<<pops[ID].paraset.ichs.IChNList[i]<<endl;
			};
		};
		
	}else {
		cout<<"ERROR::Population ID = "<<ID<<": Not exist"<<endl;
	};
};

void show(vector<neural_population>& pops,string name) {
	int ID = -1;
	for (unsigned int i=0;i<pops.size();i++) {
		if (pops[i].name == name){
			ID = i; break;};
	};
	if (ID >=0)
	{
		cout<<"The population ID ["<<ID<<"] Information"<<endl;
		cout<<"1. Name: "<<pops[ID].name<<endl;
		cout<<"2. Size: "<<pops[ID].nneurons<<endl;
		if (pops[ID].paraset.ichs.nIChs <= 0)
			cout<<"3. I Channels [0]: Not created yet"<<endl;
		else {
			cout<<"3. I Channels ["<<pops[ID].paraset.ichs.nIChs<<"]: ";
			for (int i=0;i<pops[ID].paraset.ichs.nIChs;i++) {
				if (i != pops[ID].paraset.ichs.nIChs -1)
					cout<<pops[ID].paraset.ichs.IChNList[i]<<", ";
				else
					cout<<pops[ID].paraset.ichs.IChNList[i]<<endl;
			};
		};
		
	}else {
		cout<<"ERROR::The population ("<<name<<"): Not exist"<<endl;
	};
};

void init_cuparam(netpar& cupar,nns_project& pro) {

	cupar.Threshold = (float)pro.pops[0].threshold;
	cupar.Step = (float)pro.ctr_para.simstep;
	cupar.NetDim.MaxIons = 0;
	//npop * nneurons * nchs;
	cupar.NetDim.MaxChan = 0;
	cupar.NetDim.MaxCells = 0;
	for (int i=0;i<(int)pro.pops.size();i++) {
		cupar.NetDim.MaxChan = cupar.NetDim.MaxChan + pro.pops[i].nneurons * pro.pops[i].paraset.ichs.nIChs;
		cupar.NetDim.MaxCells = cupar.NetDim.MaxCells + pro.pops[i].nneurons;
		
		////Counting ions with duplication since Naf and NaP are both same Na ions
		//int tc=0, ttc=0;
		//string strtmp;
		//size_t found;
		//for (int ii = 0;ii<pro.pops[i].paraset.ichs.nIonTs;ii++) {
			cupar.NetDim.MaxIons = cupar.NetDim.MaxIons + pro.pops[i].nneurons * pro.pops[i].paraset.ichs.nIonTs;
		//};

	};

	cupar.NetDim.MaxUnits = (int)pro.nsu.units.size();

	for (int i = 0;i < cupar.NetDim.MaxUnits;i++) {
			cupar.NetDim.MaxChan = cupar.NetDim.MaxChan + pro.nsu.units[i].ichs.nIChs;
			//cupar.NetDim.MaxCells++;
			cupar.NetDim.MaxIons = cupar.NetDim.MaxIons + pro.nsu.units[i].ichs.nIonTs;
	};
	
	int cv=0,tmpvp=0;
	cupar.NetDim.MaxViewPars = 0;
// SM	cupar.NetDim.nViewTs = (int)pro.iodata.size(); // TK
	for (size_t iio = 0;iio < pro.iodata.size();iio++) {
		//cupar.MaxViewPars = cupar.MaxViewPars + pro.iodata[iio].ntneu;
		//cupar.NetDim.MaxViewPars = cupar.NetDim.MaxViewPars + (int)ceil((pro.iodata[iio].ntneu + pro.iodata[iio].nnsu)/4.)*4; //10.20.2015
		cupar.NetDim.MaxViewPars = cupar.NetDim.MaxViewPars + (int)ceil((pro.iodata[iio].ntneu + pro.iodata[iio].nnsu) / 4.) * 4; //2.16.2016
	};

};

syntype makesyntype(net_pconn& conn, netpar& cupar, nns_project pro) {
	syntype st;
	switch (conn.stype) {
	case UTYPE_POP:
		st.Type = LSNS_PULSE_SYN;
		break;
	case UTYPE_NSU:
		st.Type = LSNS_SUM_SYN;
		break;
	};
	switch (conn.ttype) {
	case UTYPE_POP:
		if (st.Type == LSNS_PULSE_SYN) {
			st.Par = conn.syntype;
			st.Gmax = (float)pro.pops[conn.tid].paraset.ichs.ng_para[st.Par].g.val;
		}
		else if (st.Type == LSNS_SUM_SYN) {
			st.Par = conn.syntype + 4;
			st.Gmax = (float)pro.pops[conn.tid].paraset.ichs.ng_para[st.Par].g.val;
		};
		
		break;
	case UTYPE_NSU:
		if (st.Type == LSNS_PULSE_SYN) {
			st.Par = conn.syntype;
			st.Gmax = (float)pro.nsu.units[conn.tid].ichs.ng_para[st.Par].g.val;
		}
		else if (st.Type == LSNS_SUM_SYN) {
			st.Par = conn.syntype + 4;
			st.Gmax = (float)pro.nsu.units[conn.tid].ichs.ng_para[st.Par].g.val;
		};
		break;
	};
	//_synType(cupar.Synapses[st.Par]) = st.Type; //By Sergey 12.9.2015
	return st;
};

syntrg makesyntrg(net_pconn& conn, int itn, unitpset& ps, nns_project pro) {
	syntrg stg;
	int offset = 4;

	switch (conn.ttype) {
	case UTYPE_POP:
		stg.Vm = ps.ppop[conn.tid] + itn;
		if (conn.stype == UTYPE_POP) {			
			stg.Syn = ps.ppopich[conn.tid] + itn*pro.pops[conn.tid].paraset.ichs.nIChs + conn.syntype;
			stg.Eds = ps.ppopion[conn.tid] + itn*pro.pops[conn.tid].paraset.ichs.nIonTs + conn.syntype;
		}
		else if (conn.stype == UTYPE_NSU) {
			stg.Syn = ps.ppopich[conn.tid] + itn*pro.pops[conn.tid].paraset.ichs.nIChs + conn.syntype + offset;
			stg.Eds = ps.ppopion[conn.tid] + itn*pro.pops[conn.tid].paraset.ichs.nIonTs + conn.syntype + offset;
		};
		break;
	case UTYPE_NSU:
		stg.Vm = ps.pnsu[conn.tid];
		if (conn.stype == UTYPE_POP) {	
			stg.Syn = ps.pnsuich[conn.tid] + conn.syntype;
			stg.Eds = 0;
		}
		else if (conn.stype == UTYPE_NSU) {
			stg.Syn = ps.pnsuich[conn.tid] + conn.syntype + offset;
			stg.Eds = 0 + offset;
		};
		break;
	};

	return stg;
};

void getnpopnsu(net_pconn& conn, int& nsrc, int& ntrg, nns_project pro) {
	switch (conn.stype) {
	case UTYPE_POP:
		nsrc = pro.pops[conn.sid].nneurons;
		break;
	case UTYPE_NSU:
		nsrc = 1;
		break;
	};

	switch (conn.ttype) {
	case UTYPE_POP:
		ntrg = pro.pops[conn.tid].nneurons;
		break;
	case UTYPE_NSU:
		ntrg = 1;
		break;
	};
};

void getnmsrcntrg(vector<int> ipclist, int inet, vector<pair<int,int>>& minnsrc, vector<int>& nmsrc, int& ntrg, nns_project pro) {
	vector<int> tmpnmsrc;
	pair<int, int> tmppair;
	for (size_t i = 0; i < ipclist.size(); i++) {
		net_pconn& conn = pro.tnet.nets[inet].pconn[ipclist[i]];
		switch (conn.stype) {
		case UTYPE_POP:
			nmsrc.push_back(pro.pops[conn.sid].nneurons);
			break;
		case UTYPE_NSU:
			nmsrc.push_back(1);
			break;
		};

		switch (conn.ttype) {
		case UTYPE_POP:
			ntrg = pro.pops[conn.tid].nneurons;
			break;
		case UTYPE_NSU:
			ntrg = 1;
			break;
		};
	};
	tmppair.first = find_min(nmsrc);
	tmppair.second = find_imin(nmsrc);
	minnsrc.push_back(tmppair);
	tmpnmsrc = nmsrc;
	size_t nm = nmsrc.size();
	for (size_t i = 1; i < nm-1; i++) {		
		tmpnmsrc.erase(tmpnmsrc.begin() + tmppair.second);
		
		tmppair.first = find_min(tmpnmsrc);
		tmppair.second = find_imin(tmpnmsrc);
		minnsrc.push_back(tmppair);
	};
};



void makeconmap(vector<int> ipclist, int inet, int nth, unitpset& ps, wsum::srcmap& conv, nns_project& pro, int reverse) {
	wsum::con con;

	for (int ips = 0; ips < (int)ipclist.size();ips++) {
		net_pconn& conn = pro.tnet.nets[inet].pconn[ipclist[ips]];
		switch (conn.opt) {
		case CONN_REG:

			switch (conn.stype) {
			case UTYPE_POP:
				for (int isn = 0; isn < (int)pro.pops[conn.sid].nneurons;isn++) {
					//Source filtering should be included later for probability connection 12/4/2015
					if (conn.spmtx[isn] == 1) {
						if (conn.w.var != 0) {
							if (conn.norm_OnOff == true) con.second = (float)conn.wmtx[isn] / pro.pops[conn.sid].nneurons;
							else con.second = (float)conn.wmtx[isn];
						}
						else {
							if (conn.norm_OnOff == true) con.second = (float)conn.w.val / pro.pops[conn.sid].nneurons;
							else con.second = (float)conn.w.val;
						};
						con.first = ps.ppop[conn.sid] + isn;
						conv.push_back(con);
					};
				};
				break;
			case UTYPE_NSU:
				if (conn.ttype == UTYPE_POP) {
					if (conn.w.var != 0) {
						if (conn.counter != conn.nwmtx) {
							con.second = (float)conn.wmtx[conn.counter];
							conn.counter++;
							//pro.tnet.nets[inet].pconn[ipclist[ips]].counter++;
						}
						else conn.counter = 0; //pro.tnet.nets[inet].pconn[ipclist[ips]].counter = 0;
					}
					else con.second = (float)conn.w.val;
				}
				else if (conn.ttype == UTYPE_NSU) con.second = (float)conn.w.val;

				con.first = ps.pnsu[conn.sid];
				conv.push_back(con);
				break;
			};

			break;
		case CONN_ALL: case CONN_ALLP:

			switch (conn.stype) {
			case UTYPE_POP:
				for (int in = 0; in < (int)pro.pops[conn.sid].nneurons;in++) {
					if (conn.norm_OnOff == true) con.second = (float)conn.w.val / pro.pops[conn.sid].nneurons;
					else con.second = (float)conn.w.val;
					con.first = ps.ppop[conn.sid] + in;
					conv.push_back(con);
				};
				break;
			case UTYPE_NSU:
				if (conn.ttype == UTYPE_POP) {
					if (conn.w.var != 0) {
						if (conn.counter != conn.nwmtx) {
							con.second = (float)conn.wmtx[conn.counter];
							conn.counter++;
							//pro.tnet.nets[inet].pconn[ipclist[ips]].counter++;
						}
						else conn.counter = 0;//pro.tnet.nets[inet].pconn[ipclist[ips]].counter = 0;
					}
					else con.second = (float)conn.w.val;
				}
				else if (conn.ttype == UTYPE_NSU) con.second = (float)conn.w.val;

				con.first = ps.pnsu[conn.sid];
				conv.push_back(con);
				break;
			};
			break;
		case CONN_ONE: case CONN_ONEP:
			switch (conn.stype) {
			case UTYPE_POP:
				if (reverse == 0) {
						if (conn.norm_OnOff == true) con.second = (float)conn.w.val / pro.pops[conn.sid].nneurons;
						else con.second = (float)conn.w.val;
						con.first = ps.ppop[conn.sid] + nth;
						conv.push_back(con);
				}
				else {
						if (conn.norm_OnOff == true) con.second = (float)conn.w.val / pro.pops[conn.tid].nneurons;
						else con.second = (float)conn.w.val;
						con.first = ps.ppop[conn.sid] + nth;
						conv.push_back(con);
				};
				
				break;
			case UTYPE_NSU:
				if (conn.ttype == UTYPE_POP) {
					if (conn.w.var != 0) {
						if (conn.counter != conn.nwmtx) {
							con.second = (float)conn.wmtx[conn.counter];
							conn.counter++;
							//pro.tnet.nets[inet].pconn[ipclist[ips]].counter++;
						}
						else conn.counter = 0;//pro.tnet.nets[inet].pconn[ipclist[ips]].counter = 0;
					}
					else con.second = (float)conn.w.val;
				}
				else if (conn.ttype == UTYPE_NSU) con.second = (float)conn.w.val;

				con.first = ps.pnsu[conn.sid];
				conv.push_back(con);
				break;
			};
			break;
		};
	};
};

void init_synapse(netpar& cupar, nns_project& pro, mapsyn& mps) {

	//int shareTrg = 1;

	syntype sytp;
	syntrg sytrg;
	wsum::con con; //connection variables <position(uint32), weight(float)>
	wsum::srcmap conv; //Connection vector

	int *ppop = new int[pro.pops.size()];
	int *ppopion = new int[pro.pops.size()];
	int *ppopich = new int[pro.pops.size()];
	int *pnsu = new int[pro.nsu.units.size()];
	int *pnsuich = new int[pro.nsu.units.size()];
	size_t nnet = pro.tnet.nets.size();
	int ctrg, ct, cttp, ctsyntp, cstp;
	vector<vector<pair<int,int>>> tmslist;
	vector<pair<int,int>> mslist;
	vector<int> tmsstplist;
	vector<int> ipclist;
	vector<int> tpctypelist;
	vector<vector<int>> tipclist;
	//vector<vector<int>> tmsstplist;
	vector<pair<int, int>> pairvec;
	vector<int> tlut;
	vector<int> tsw;
	pair<int, int> tmppair;
	vector<int> tmpipc;
	vector<int> tmpipc2;
	
	int tmpsyntp;
	int nsynps = 4;
	int npcells = 0, npichs = 0;

	unitpset ps;

	ps.ppop = ppop;
	ps.ppopich = ppopich;
	ps.ppopion = ppopion;
	ps.pnsu = pnsu;
	ps.pnsuich = pnsuich;

	//for (int isp = 0; isp < nsynps; isp++) {
	//	_synA(cupar.Synapses[isp]) = (float) pro.pops[0].paraset.ichs.ng_para[isp].Amp;
	//	_synTmax(cupar.Synapses[isp]) = (float) pro.pops[0].paraset.ichs.ng_para[isp].Tc;
	//	_synDt(cupar.Synapses[isp]) = 1.f;
	//	_synEdt(cupar.Synapses[isp]) = exp(-cupar.Step / _synTmax(cupar.Synapses[isp]));
	//};

	//for (int isp = 4; isp < nsynps+4; isp++) {
	//	_synA(cupar.Synapses[isp]) = 1.;// This value is what the nsm package has. 12.22.2015
	//	_synTmax(cupar.Synapses[isp]) = (float)pro.pops[0].paraset.ichs.ng_para[isp].Tc;
	//	_synDt(cupar.Synapses[isp]) = 1.f;
	//	_synEdt(cupar.Synapses[isp]) = 0.f;
	//};

	for (int isp = 0; isp < nsynps; isp++) {
		_synA(cupar.Synapses[isp]) = (float)pro.syn_para[isp].Amp;
		_synTmax(cupar.Synapses[isp]) = (float)pro.syn_para[isp].Tc;
		_synDt(cupar.Synapses[isp]) = 1.f;
		_synEdt(cupar.Synapses[isp]) = exp(-cupar.Step / _synTmax(cupar.Synapses[isp]));
	};

	for (int isp = 4; isp < nsynps + 4; isp++) {
		_synA(cupar.Synapses[isp]) = 1.;// This value is what the nsm package has. 12.22.2015
		_synTmax(cupar.Synapses[isp]) = (float)pro.syn_para[isp].Tc;
		_synDt(cupar.Synapses[isp]) = 1.f;
		_synEdt(cupar.Synapses[isp]) = 0.f;
	};

	for (int ip = 0;ip < (int)pro.pops.size();ip++) {
		if (ip == 0) {
			ppop[ip] = 0;
			ppopion[ip] = 0;
			ppopich[ip] = 0;
		}
		else {
			ppop[ip] = ps.ppop[ip - 1] + pro.pops[ip - 1].nneurons;
			ppopich[ip] = ppopich[ip - 1] + pro.pops[ip - 1].nneurons*pro.pops[ip - 1].paraset.ichs.nIChs;
			ppopion[ip] = ppopion[ip - 1] + pro.pops[ip - 1].nneurons*pro.pops[ip - 1].paraset.ichs.nIonTs;
		};
		npcells = npcells + pro.pops[ip].nneurons;
		npichs = npichs + pro.pops[ip].nneurons*pro.pops[ip].paraset.ichs.nIChs;
	};

	// added by SM 01/22/16
	#if defined ( __CUDA__ )
		npcells = kdim3<cudametric>(npcells);
	#else
		npcells = kdim3<x86metric>(npcells);
	#endif /* __CUDA__ */

	//int nich_drv = 1, nich_out = 4;
	for (int iu = 0;iu < (int)pro.nsu.units.size();iu++) {
		if (iu == 0) {
			pnsu[iu] = npcells;
			pnsuich[iu] = npichs;
		}
		else {
			pnsu[iu] = pnsu[iu - 1] + 1;
			pnsuich[iu] = pnsuich[iu - 1] + pro.nsu.units[iu - 1].ichs.nIChs;
		};
	};


	//Finding multi-sources for each target in nets
	ct = 0;
	for (size_t inet = 0;inet < nnet;inet++) {
		int syntpoff = 4;
		for (int ipc = 0;ipc < (int)pro.tnet.nets[inet].pconn.size();ipc++) {
			ctrg = pro.tnet.nets[inet].pconn[ipc].tid;
			cttp = pro.tnet.nets[inet].pconn[ipc].ttype;
			cstp = pro.tnet.nets[inet].pconn[ipc].stype;
			ctsyntp = pro.tnet.nets[inet].pconn[ipc].syntype;
			if (cttp == 0 && cstp == 0) tpctypelist.push_back(0);
			else if (cttp == 0 && cstp == 1) tpctypelist.push_back(1);
			else if (cttp == 1 && cstp == 0) tpctypelist.push_back(2);
			else if (cttp == 1 && cstp == 1) tpctypelist.push_back(3);
			int fd = 0;
			//downward id check
			for (int ipcd = 0;ipcd < ipc;ipcd++) {
				if (ctrg == pro.tnet.nets[inet].pconn[ipcd].tid && cttp == pro.tnet.nets[inet].pconn[ipc].ttype) {
					if (ctsyntp == pro.tnet.nets[inet].pconn[ipcd].syntype) {
						tmpsyntp = ctsyntp;
						if (cttp != cstp) {
							tmpsyntp = tmpsyntp + syntpoff;
						}
						else {
							if (cttp == 1) tmpsyntp = tmpsyntp + syntpoff;
						};
						if (tmsstplist[ipcd] == tmpsyntp) {
							fd = 1;
							tsw.push_back(0);
							tlut.push_back(-1);
						};
					};
				};
			};
			if (fd == 0) {
				tlut.push_back(ct);
				tsw.push_back(1);
				tmppair.first = pro.tnet.nets[inet].pconn[ipc].stype;
				tmppair.second = pro.tnet.nets[inet].pconn[ipc].sid;
				tmpsyntp = pro.tnet.nets[inet].pconn[ipc].syntype;
				if (cttp != tmppair.first) {
					tmpsyntp = tmpsyntp + syntpoff;
				}
				else {
					if (cttp == 1) tmpsyntp = tmpsyntp + syntpoff;
				};
				mslist.push_back(tmppair);
				tmsstplist.push_back(tmpsyntp);
				ipclist.push_back(ipc);
				//Upperward id check
				for (int ipcu = ipc + 1;ipcu < (int)pro.tnet.nets[inet].pconn.size();ipcu++) {
					if (ctrg == pro.tnet.nets[inet].pconn[ipcu].tid && cttp == pro.tnet.nets[inet].pconn[ipcu].ttype) {
						if (ctsyntp == pro.tnet.nets[inet].pconn[ipcu].syntype) {
							tmppair.first = pro.tnet.nets[inet].pconn[ipcu].stype;
							tmppair.second = pro.tnet.nets[inet].pconn[ipcu].sid;
							tmpsyntp = pro.tnet.nets[inet].pconn[ipcu].syntype;
							if (pro.tnet.nets[inet].pconn[ipcu].ttype != tmppair.first) {
								tmpsyntp = tmpsyntp + syntpoff;
							}
							else {
								if (pro.tnet.nets[inet].pconn[ipcu].ttype == 1) tmpsyntp = tmpsyntp + syntpoff;
							};
							if (tmsstplist[tmsstplist.size() - 1] == tmpsyntp) {
								ipclist.push_back(ipcu);
								mslist.push_back(tmppair);
							};
						};
					}
				};
				tipclist.push_back(ipclist);
				ipclist.clear();
				tmslist.push_back(mslist);
				mslist.clear();
				ct++;
			}
			else {
				tmsstplist.push_back(-1);
			};
		};

		//tmsstplist.clear();
		tmslist.clear();

	//adding synapses with shared targets for nets
	//for (int inet = 0;inet < nnet;inet++) {
		for (int ipc = 0;ipc < (int)pro.tnet.nets[inet].pconn.size();ipc++) {
			switch (pro.tnet.nets[inet].pconn[ipc].opt) {
			case CONN_REG:
					if (tsw[ipc] == 1) {
						sytp = makesyntype(pro.tnet.nets[inet].pconn[ipc], cupar, pro);
						//conv.clear();
						for (size_t im = 0;im < tipclist[tlut[ipc]].size();im++) {
							pro.tnet.nets[inet].make_mtx(tipclist[tlut[ipc]][im], pro.ctr_para);
						};
						
						//makeconmap(tipclist[tlut[ipc]], (int)inet, 0, ps, conv, pro, 0);

						switch (pro.tnet.nets[inet].pconn[ipc].ttype) {
						case UTYPE_POP:
							for (int itn = 0; itn < (int)pro.pops[pro.tnet.nets[inet].pconn[ipc].tid].nneurons;itn++) {
								conv.clear();
								makeconmap(tipclist[tlut[ipc]], (int)inet, 0, ps, conv, pro, 0);
								if (pro.tnet.nets[inet].pconn[ipc].tpmtx[itn] == 1) {
									sytrg = makesyntrg(pro.tnet.nets[inet].pconn[ipc], itn, ps, pro);
									mps.add_synapse(sytp, sytrg, conv, pro.tnet.nets[inet].pconn[ipc].optmz_OnOff);
								};
							};
							break;
						case UTYPE_NSU:
							conv.clear();
							makeconmap(tipclist[tlut[ipc]], (int)inet, 0, ps, conv, pro, 0);
							sytrg = makesyntrg(pro.tnet.nets[inet].pconn[ipc], 0, ps, pro);
							mps.add_synapse(sytp, sytrg, conv, pro.tnet.nets[inet].pconn[ipc].optmz_OnOff);
							break;
						};

						for (size_t im = 0;im < tipclist[tlut[ipc]].size();im++) {
							pro.tnet.nets[inet].del_mtx(tipclist[tlut[ipc]][im]);
						};
					};

				break;
			case CONN_ALL: case CONN_ALLP:
				if (tsw[ipc] == 1) {
					sytp = makesyntype(pro.tnet.nets[inet].pconn[ipc], cupar, pro);
					//conv.clear();
					//tmpipc.clear();
					//tmpipc.push_back(ipc);

					for (size_t im = 0;im < tipclist[tlut[ipc]].size();im++) {
						pro.tnet.nets[inet].make_mtx(tipclist[tlut[ipc]][im], pro.ctr_para);
					};//Added on 4.7.2016

					//makeconmap(tipclist[tlut[ipc]], (int)inet, 0, ps, conv, pro, 0);

					switch (pro.tnet.nets[inet].pconn[ipc].ttype) {
					case UTYPE_POP:
						for (int itn = 0; itn < (int)pro.pops[pro.tnet.nets[inet].pconn[ipc].tid].nneurons;itn++) {
							conv.clear();
							makeconmap(tipclist[tlut[ipc]], (int)inet, 0, ps, conv, pro, 0);
							sytrg = makesyntrg(pro.tnet.nets[inet].pconn[ipc], itn, ps, pro);
							mps.add_synapse(sytp, sytrg, conv, pro.tnet.nets[inet].pconn[ipc].optmz_OnOff);
						};
						break;
					case UTYPE_NSU:
						conv.clear();
						makeconmap(tipclist[tlut[ipc]], (int)inet, 0, ps, conv, pro, 0);
						sytrg = makesyntrg(pro.tnet.nets[inet].pconn[ipc], 0, ps, pro);
						mps.add_synapse(sytp, sytrg, conv, pro.tnet.nets[inet].pconn[ipc].optmz_OnOff);
						break;
					};

					for (size_t im = 0;im < tipclist[tlut[ipc]].size();im++) {
						pro.tnet.nets[inet].del_mtx(tipclist[tlut[ipc]][im]);
					};//Added on 4.7.2016
				};
				break;
			case CONN_ONE: case CONN_ONEP:
				if (tsw[ipc] == 1) {
					int nsrc, nsrc0, ntrg, nipc;
					vector<int> nmsrc;
					vector<pair<int, int>> minnsrc;
					tmpipc.clear();
					tmpipc = tipclist[tlut[ipc]];
					nipc = (int)tmpipc.size();
					getnmsrcntrg(tmpipc, (int)inet, minnsrc, nmsrc, ntrg, pro);

					sytp = makesyntype(pro.tnet.nets[inet].pconn[ipc], cupar, pro);

					for (size_t imin = 0; imin < minnsrc.size(); imin++) {
						nsrc = minnsrc[imin].first;
						if (imin == 0) {
							if (nsrc <= ntrg) {
								if (tpctypelist[ipc] == CONNT_PP) {
									for (int in = 0; in < nsrc;in++) {
										conv.clear();
										makeconmap(tmpipc, (int)inet, in, ps, conv, pro, 0);
										sytrg = makesyntrg(pro.tnet.nets[inet].pconn[tmpipc[0]], in, ps, pro);
										mps.add_synapse(sytp, sytrg, conv, pro.tnet.nets[inet].pconn[tmpipc[0]].optmz_OnOff);
									};
								}
								else { //stype == NSU
									conv.clear();
									makeconmap(tmpipc, (int)inet, 0, ps, conv, pro, 0);
									sytrg = makesyntrg(pro.tnet.nets[inet].pconn[tmpipc[0]], 0, ps, pro);
									mps.add_synapse(sytp, sytrg, conv, pro.tnet.nets[inet].pconn[tmpipc[0]].optmz_OnOff);
								};
							}
							else { //nsrc > ntrg

								if (tpctypelist[ipc] == CONNT_PP) {
									for (int in = 0; in < ntrg;in++) {
										conv.clear();
										makeconmap(tmpipc, (int)inet, in, ps, conv, pro, 1);
										sytrg = makesyntrg(pro.tnet.nets[inet].pconn[tmpipc[0]], in, ps, pro);
										mps.add_synapse(sytp, sytrg, conv, pro.tnet.nets[inet].pconn[tmpipc[0]].optmz_OnOff);
									};
								}
								else { //ttype == nsu
									conv.clear();
									makeconmap(tmpipc, (int)inet, 0, ps, conv, pro, 1);
									sytrg = makesyntrg(pro.tnet.nets[inet].pconn[tmpipc[0]], 0, ps, pro);
									mps.add_synapse(sytp, sytrg, conv, pro.tnet.nets[inet].pconn[tmpipc[0]].optmz_OnOff);
								};
							};
						}
						else if (imin > 0 && imin < minnsrc.size() - 1) {
							nsrc0 = minnsrc[imin - 1].first;
							vector<int> tmpnmsrc;
							tmpipc2.clear();
							tmpipc2 = tmpipc;
							tmpnmsrc = nmsrc;
							for (size_t i = 0; i < imin; i++) {
								tmpipc2.erase(tmpipc2.begin() + minnsrc[i].second);
								tmpnmsrc.erase(tmpnmsrc.begin() + minnsrc[i].second);
							};
							size_t nm = tmpnmsrc.size();
							for (size_t i = 0; i < nm; i++) {
								if (tmpnmsrc[i] <= nsrc0) {
									tmpnmsrc.erase(tmpnmsrc.begin() + i);
									tmpipc2.erase(tmpipc2.begin() + i);
									nm = tmpnmsrc.size();
									i = 0;
								}
							};

							if (tmpipc2.size() > 0) {
								if (nsrc <= ntrg) {
									if (tpctypelist[ipc] == CONNT_PP) {
										for (int in = nsrc0; in < nsrc;in++) {
											conv.clear();
											makeconmap(tmpipc2, (int)inet, in, ps, conv, pro, 0);
											sytrg = makesyntrg(pro.tnet.nets[inet].pconn[ipc], in, ps, pro);
											mps.add_synapse(sytp, sytrg, conv, pro.tnet.nets[inet].pconn[ipc].optmz_OnOff);
										};
									}
									else { //stype == NSU
										conv.clear();
										makeconmap(tmpipc2, (int)inet, 0, ps, conv, pro, 0);
										sytrg = makesyntrg(pro.tnet.nets[inet].pconn[ipc], 0, ps, pro);
										mps.add_synapse(sytp, sytrg, conv, pro.tnet.nets[inet].pconn[ipc].optmz_OnOff);
									};
								};
								// if nsrc > ntrg, there is nothing to do
							};
						}
						else if (imin == minnsrc.size() - 1) { //The last one
							nsrc0 = minnsrc[imin - 1].first;
							vector<int> tmpnmsrc;
							tmpipc2.clear();
							tmpipc2 = tmpipc;
							tmpnmsrc = nmsrc;
							for (size_t i = 0; i < imin; i++) {
								tmpipc2.erase(tmpipc2.begin() + minnsrc[i].second);
								tmpnmsrc.erase(tmpnmsrc.begin() + minnsrc[i].second);
							};
							size_t nm = tmpnmsrc.size();
							for (size_t i = 0; i < nm; i++) {
								if (tmpnmsrc[i] <= nsrc0) {
									tmpnmsrc.erase(tmpnmsrc.begin() + i);
									tmpipc2.erase(tmpipc2.begin() + i);
									nm = tmpnmsrc.size();
									i = 0;
								};
							};

							if (tmpipc2.size() > 0) {
								if (nsrc <= ntrg) {
									if (tpctypelist[ipc] == CONNT_PP) {
										for (int in = nsrc0; in < nsrc;in++) {
											conv.clear();
											makeconmap(tmpipc2, (int)inet, in, ps, conv, pro, 0);
											sytrg = makesyntrg(pro.tnet.nets[inet].pconn[ipc], in, ps, pro);
											mps.add_synapse(sytp, sytrg, conv, pro.tnet.nets[inet].pconn[ipc].optmz_OnOff);
										};
									}
									else { //stype == NSU
										conv.clear();
										makeconmap(tmpipc2, (int)inet, 0, ps, conv, pro, 0);
										sytrg = makesyntrg(pro.tnet.nets[inet].pconn[ipc], 0, ps, pro);
										mps.add_synapse(sytp, sytrg, conv, pro.tnet.nets[inet].pconn[ipc].optmz_OnOff);
									};
								};
							};

							nsrc0 = nsrc;
							nm = tmpnmsrc.size();
							for (size_t i = 0; i < nm-1; i++) {
								if (tmpnmsrc[i] <= nsrc0) {
									tmpnmsrc.erase(tmpnmsrc.begin() + i);
									tmpipc2.erase(tmpipc2.begin() + i);
									nm = tmpnmsrc.size();
									i = 0;
								};
							};

							if (tmpipc2.size() > 0) {
								if (nsrc <= ntrg) {
									if (tpctypelist[ipc] == CONNT_PP) {
										for (int in = nsrc0; in < tmpnmsrc[0];in++) {
											conv.clear();
											makeconmap(tmpipc2, (int)inet, in, ps, conv, pro, 0);
											sytrg = makesyntrg(pro.tnet.nets[inet].pconn[ipc], in, ps, pro);
											mps.add_synapse(sytp, sytrg, conv, pro.tnet.nets[inet].pconn[ipc].optmz_OnOff);
										};
									}
									else { //stype == NSU
										conv.clear();
										makeconmap(tmpipc2, (int)inet, 0, ps, conv, pro, 0);
										sytrg = makesyntrg(pro.tnet.nets[inet].pconn[ipc], 0, ps, pro);
										mps.add_synapse(sytp, sytrg, conv, pro.tnet.nets[inet].pconn[ipc].optmz_OnOff);
									};
								};
							};


						}; // if imin

					}; //for imin

				};
				break;
			}; //switch(opt)
		}; //for ipc
	}; //for inet

	cupar.NetDim.MaxWS = mps.size4_src();
	int nout = 0;
	for (size_t i = 0;i < pro.nsu.units.size();i++) {
		if (pro.nsu.units[i].type == NSU_OUTPUT) nout++;
	};
	cupar.NetDim.MaxWsyn = (cupar.NetDim.MaxCells + nout) * 8;

	delete[] ppop;
	delete[] ppopion;
	delete[] ppopich;
	delete[] pnsu;
	delete[] pnsuich;
};

void copy_map2mem(netpar& cupar, mapsyn& mps) {
	mps.map2mem(cupar.Data, cupar.Synapses);
};


void copy_mem2cu(netpar& cupar, vector<neural_population>& pops, nns_nonspikeunit& nsu, vector<nns_iodata>& iodata, contr_parameters& ctr_para) {

	bool sw_ion_dynam;
	
	if (ctr_para.srand_status != 1) {
		srand(ctr_para.seed);
		ctr_para.srand_status = 1;
	};
//	RNG Rng; // SM

	vector<vector<double>> dist_g;
	vector<vector<double>> dist_e;
	vector<double> tmp_v;
	vector<int> i_dist_g;
	vector<int> i_dist_e;

	//int nsynps = 4; //the number of synaptic channels
	int nsynps = 8; //the number of synaptic channels (4 spikes units + 4 nonspikes units) 12.21.2015

	//gc:gate counter, cc:channel counter, nc:neuron counter, ncc:neuron*channel counter
	//itc: ion type counter, ioc:ion counter, nic:neuron*ion counter
	int gc=0,cc=0, cc2=0, nc=0, ncc=0, ncc2=0, itc=0, ioc=0, nic=0, nic2=0; 

	for (int ip=0;ip<(int)pops.size();ip++) {
		
		dist_g.clear();
		i_dist_g.clear();
		dist_e.clear();
		i_dist_e.clear();
		//Making distributed values with variance
		for (int ic = 0;ic < pops[ip].paraset.ichs.nIChs;ic++) {

			double val, var;
			switch (pops[ip].paraset.ichs.IChIdxList[ic][0]) {
			case 0:
				val = pops[ip].paraset.ichs.g_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].g.val;
				var = pops[ip].paraset.ichs.g_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].g.var;
				if (var > 0) {
					tmp_v.clear();
					tmp_v = normrdvec(Rng, pops[ip].nneurons, 2, val, var);
					dist_g.push_back(tmp_v);
					i_dist_g.push_back((int)dist_g.size()-1);
				}
				else {
					i_dist_g.push_back(-1);
				};
				val = pops[ip].paraset.ichs.g_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].E.val;
				var = pops[ip].paraset.ichs.g_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].E.var;
				if (var > 0) {
					tmp_v.clear();
					tmp_v = normrdvec(Rng, pops[ip].nneurons, 2, val, var);
					dist_e.push_back(tmp_v);
					i_dist_e.push_back((int)dist_e.size() - 1);
				}
				else {
					i_dist_e.push_back(-1);
				};
				break;
			case 1:
				val = pops[ip].paraset.ichs.ab_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].g.val;
				var = pops[ip].paraset.ichs.ab_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].g.var;
				if (var > 0) {
					tmp_v.clear();
					tmp_v = normrdvec(Rng, pops[ip].nneurons, 2, val, var);
					dist_g.push_back(tmp_v);
					i_dist_g.push_back((int)dist_g.size() - 1);
				}
				else {
					i_dist_g.push_back(-1);
				};
				val = pops[ip].paraset.ichs.ab_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].E.val;
				var = pops[ip].paraset.ichs.ab_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].E.var;
				if (var > 0) {
					tmp_v.clear();
					tmp_v = normrdvec(Rng, pops[ip].nneurons, 2, val, var);
					dist_e.push_back(tmp_v);
					i_dist_e.push_back((int)dist_e.size() - 1);
				}
				else {
					i_dist_e.push_back(-1);
				};
				break;
			case 2:
				val = pops[ip].paraset.ichs.z_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].g.val;
				var = pops[ip].paraset.ichs.z_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].g.var;
				if (var > 0) {
					tmp_v.clear();
					tmp_v = normrdvec(Rng, pops[ip].nneurons, 2, val, var);
					dist_g.push_back(tmp_v);
					i_dist_g.push_back((int)dist_g.size() - 1);
				}
				else {
					i_dist_g.push_back(-1);
				};
				val = pops[ip].paraset.ichs.z_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].E.val;
				var = pops[ip].paraset.ichs.z_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].E.var;
				if (var > 0) {
					tmp_v.clear();
					tmp_v = normrdvec(Rng, pops[ip].nneurons, 2, val, var);
					dist_e.push_back(tmp_v);
					i_dist_e.push_back((int)dist_e.size() - 1);
				}
				else {
					i_dist_e.push_back(-1);
				};
				break;
			case 3:
				val = pops[ip].paraset.ichs.ng_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].g.val;
				var = pops[ip].paraset.ichs.ng_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].g.var;
				if (var > 0) {
					tmp_v.clear();
					tmp_v = normrdvec(Rng, pops[ip].nneurons, 2, val, var);
					dist_g.push_back(tmp_v);
					i_dist_g.push_back((int)dist_g.size() - 1);
				}
				else {
					i_dist_g.push_back(-1);
				};
				val = pops[ip].paraset.ichs.ng_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].E.val;
				var = pops[ip].paraset.ichs.ng_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].E.var;
				if (var > 0) {
					tmp_v.clear();
					tmp_v = normrdvec(Rng, pops[ip].nneurons, 2, val, var);
					dist_e.push_back(tmp_v);
					i_dist_e.push_back((int)dist_e.size() - 1);
				}
				else {
					i_dist_e.push_back(-1);
				};
				break;
			};
		};
		
		//sw_ion_dynam = pops[ip].paraset.ions.sw_ion_dynam;
		pops[ip].paraset.ions.sw_ion_dynam = 1;
		sw_ion_dynam = 1;

		//int sgc=gc; //gc = 0; cc =0; itc=0; ioc=0;
		vector<int> tg1m, tg1h, tg2m, tg2h, tab1m, tab1h, tab2m, tab2h, tz;
		for (size_t in=0;in<pops[ip].nneurons;in++) {
			
			//Cell data copy
//			_cell_v(cupar.Data->Cells.CellV[nc]) = float(pops[ip].paraset.neu_para.Vm.val); //TODO: SM add randomization of V 
			_cell_v(cupar.Data->Cells.CellV[nc]) = float(normrd( Rng, float( pops[ip].paraset.neu_para.Vm.val ), float( pops[ip].paraset.neu_para.Vm.var/100. ))); //TODO: SM add randomization of V 
			_cell_spike(cupar.Data->Cells.CellV[nc]) = 0.f; //SM
			_cell_c(cupar.Data->Cells.CellV[nc]) = float(pops[ip].paraset.neu_para.Cm.val);
			_cell_area(cupar.Data->Cells.CellV[nc]) = float(pops[ip].paraset.neu_para.area); //Taegyo

			string strtmp;
			
			int nint4vw = int(MAX_CHAN_PER_PUMP / 4 + 1);
			int4vw *tmpNa = new int4vw [nint4vw];
			tmpNa[0].pDat[0] = 0;
			int4vw *tmpK = new int4vw[nint4vw];
			tmpK[0].pDat[0] = 0;		
			int4vw *tmpCl = new int4vw[nint4vw];
			tmpCl[0].pDat[0] = 0;
			int4vw *tmpMg = new int4vw[nint4vw];
			tmpMg[0].pDat[0] = 0;
			int4vw *tmpCa = new int4vw[nint4vw];
			tmpCa[0].pDat[0] = 0;
			int4vw *tmpNoIon = new int4vw [nint4vw];
			tmpNoIon[0].pDat[0] = 0;

			//Channel data copy
			int tg1c=0, tg2c=0, tg3c=0, t4c=0, tgc=0, ticNa=0, ticK=0, ticCl=0, ticMg=0, ticCa=0;
			int tab1c = 0, tab2c = 0, tab3c = 0, tz1c = 0, tz2c = 0, tz3c = 0, lsync = 0;
			bool skipflg = 0, kcaflg=0;
			int lgmhc=0, lgmc=0, lghc=0, labmhc=0, labhc=0, labmc=0, lzmhc=0, lzmc=0, lzhc=0;
			int idx_x;
			for (int ic=0;ic<pops[ip].paraset.ichs.nIChs;ic++) {
				switch (pops[ip].paraset.ichs.IChIonTList[ic]) {
				case 1://Na
						tmpNa[0].pDat[0]++;
						if (ticNa < 3) tmpNa[0].pDat[ticNa + 1] = ncc;
						else tmpNa[int((ticNa + 1) / 4)].pDat[ticNa + 1 - int((ticNa + 1) / 4) * 4] = ncc;
						ticNa++;
						break;
				case 2://K
						tmpK[0].pDat[0]++;
						if (ticK < 3) tmpK[0].pDat[ticK + 1] = ncc;
						else tmpK[int((ticK + 1) / 4)].pDat[ticK + 1 - int((ticK + 1) / 4) * 4] = ncc;
						ticK++;
						break;
				case 3://Ca
						tmpCa[0].pDat[0]++;
						if (ticCa < 3) tmpCa[0].pDat[ticCa + 1] = ncc;
						else tmpCa[int((ticCa + 1) / 4)].pDat[ticCa + 1 - int((ticCa + 1) / 4) * 4] = ncc;
						ticCa++;
						break;
				case 4://Cl
						tmpCl[0].pDat[0]++;
						if (ticCl < 3) tmpCl[0].pDat[ticCl + 1] = ncc;
						else tmpCl[int((ticCl + 1) / 4)].pDat[ticCl + 1 - int((ticCl + 1) / 4) * 4] = ncc;
						ticCl++;
						break;
				case 5://Mg
						tmpMg[0].pDat[0]++;
						if (ticMg < 3) tmpMg[0].pDat[ticMg + 1] = ncc;
						else tmpMg[int((ticMg + 1) / 4)].pDat[ticMg + 1 - int((ticMg + 1) / 4) * 4] = ncc;
						ticMg++;
						break;
				};
				switch (pops[ip].paraset.ichs.IChGTList[ic]) {
				case 8: case 9: case 10: case 11: //Z genetic and Z ab
					kcaflg = 1;
					break;
				default:
					kcaflg = 0;
					break;
				};

				if (ic == 0) {
					skipflg = 0;
				}
				else {
					for (int ti = 0;ti < ic;ti++) {
						if (pops[ip].paraset.ichs.ICh2IonTmap[ic] == pops[ip].paraset.ichs.ICh2IonTmap[ti]) {
							skipflg = 1;
							idx_x = ti;
							break;
						}
						else {
							skipflg = 0;
						};
					};
				};

				if (skipflg == 0) {
					//This IonsE setting for ion dynamics not in use. for MaxIons
					double val = 0., var = 0.;
					_ions_eds(cupar.Data->Channels.IonsE[nic]) = 0.f;
					switch (pops[ip].paraset.ichs.IChIdxList[ic][0]) {
					case 0:
						val = pops[ip].paraset.ichs.g_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].E.val;
						var = pops[ip].paraset.ichs.g_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].E.var;
						_ions_eds(cupar.Data->Channels.IonsE[nic]) = (var > 0.f)? float(dist_e[i_dist_e[ic]][in]): float(val);
						_chan_lute(cupar.Data->Channels.ChanLUT[ncc]) = nic; //revers potential LUT for each channel
						nic++;
						break;
					case 1:
						val = pops[ip].paraset.ichs.ab_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].E.val;
						var = pops[ip].paraset.ichs.ab_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].E.var;
						_ions_eds(cupar.Data->Channels.IonsE[nic]) = (var > 0.f)? float(dist_e[i_dist_e[ic]][in]): float(val);
						_chan_lute(cupar.Data->Channels.ChanLUT[ncc]) = nic; //revers potential LUT for each channel
						nic++;
						break;
					case 2:
						val = pops[ip].paraset.ichs.z_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].E.val;
						var = pops[ip].paraset.ichs.z_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].E.var;
						_ions_eds(cupar.Data->Channels.IonsE[nic]) = (var > 0.f)? float(dist_e[i_dist_e[ic]][in]): float(val);
						_chan_lute(cupar.Data->Channels.ChanLUT[ncc]) = nic; //revers potential LUT for each channel
						nic++;
						break;
					case 3:
						val = pops[ip].paraset.ichs.ng_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].E.val;
						var = pops[ip].paraset.ichs.ng_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].E.var;
						_ions_eds(cupar.Data->Channels.IonsE[nic]) = (var > 0.f)? float(dist_e[i_dist_e[ic]][in]): float(val);
						_chan_lute(cupar.Data->Channels.ChanLUT[ncc]) = nic; //revers potential LUT for each channel
						nic++;
						break;
					};

				}
				else { //If skipflg == 1
					_chan_lute(cupar.Data->Channels.ChanLUT[ncc]) = nic - ic + idx_x;
				};

				// initialize chanG structure (modified by SM 07/15/2016)
				double val = 0., var = 0.;
				switch (pops[ip].paraset.ichs.IChIdxList[ic][0]) {
				case 0:
					val = pops[ip].paraset.ichs.g_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].g.val;
					var = pops[ip].paraset.ichs.g_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].g.var;
					break;
				case 1:
					val = pops[ip].paraset.ichs.ab_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].g.val;
					var = pops[ip].paraset.ichs.ab_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].g.var;
					break;
				case 2:
					val = pops[ip].paraset.ichs.z_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].g.val;
					var = pops[ip].paraset.ichs.z_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].g.var;
					break;
				case 3:
					val = pops[ip].paraset.ichs.ng_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].g.val;
					var = pops[ip].paraset.ichs.ng_para[pops[ip].paraset.ichs.IChIdxList[ic][1]].g.var;
					break;
				};
				_chan_g(cupar.Data->Channels.ChanG[ncc]) = 0.f;
				_chan_ge(cupar.Data->Channels.ChanG[ncc]) = 0.f;
				_chan_i(cupar.Data->Channels.ChanG[ncc]) = 0.f;
				_chan_gmax(cupar.Data->Channels.ChanG[ncc]) = ( var > 0. )? float(dist_g[i_dist_g[ic]][in]): float(val);
				
				if (ic >= nsynps) map2lut(cupar.Data->Cells.VChanGLUT, nc, ncc, MAX_CHAN_PER_CELL); //11.13.2015 this is critical
				_chan_lutv(cupar.Data->Channels.ChanLUT[ncc]) = nc; //Cell membrane potential LUT		
				
				switch (pops[ip].paraset.ichs.IChGTList[ic]) {
				case 2: case 3: case 4: case 5: //General type gates
					switch (pops[ip].paraset.ichs.IChMHTList[ic]) {
					case 0: //MH
						if (in == 0) {
							_ggatev12(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg1c].Vsm.val);
							_ggateslp(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg1c].km.val);
							_ggateslpt(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg1c].ktm.val);
							_ggatetmax(cupar.Gates[gc]) = (_ggateslpt(cupar.Gates[gc]) != 0.f ) ? 
											float(pops[ip].paraset.ichs.g_para[tg1c].tm.val):
											0.f;
							_ggatev12t(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg1c].Vhfm.val);
							_ggatet0(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg1c].t0m);

							_ggateslpt2(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg1c].ktm2.val);
							_ggatev12t2(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg1c].Vhfm2.val);
							tg1m.push_back(gc);
							gc++;
							_ggatev12(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg1c].Vsh.val);
							_ggateslp(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg1c].kh.val);
							_ggateslpt(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg1c].kth.val);
							_ggatetmax(cupar.Gates[gc]) = (_ggateslpt(cupar.Gates[gc]) != 0.f ) ? 
											float(pops[ip].paraset.ichs.g_para[tg1c].th.val):
											0.f;
							_ggatev12t(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg1c].Vhfh.val);
							_ggatet0(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg1c].t0h);
							_ggateslpt2(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg1c].kth2.val);
							_ggatev12t2(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg1c].Vhfh2.val);
							tg1h.push_back(gc);
							gc++;
						};

						_gate_typem(cupar.Data->Channels.ChanType[ncc]) = pops[ip].paraset.ichs.IChGTList[ic];
						_gate_parm(cupar.Data->Channels.ChanType[ncc]) = tg1m[lgmhc];
						_gate_m(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.g_para[tg1c].M0);
						_gate_powm(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.g_para[tg1c].mpower);

						_gate_typeh(cupar.Data->Channels.ChanType[ncc]) = pops[ip].paraset.ichs.IChGTList[ic];
						_gate_parh(cupar.Data->Channels.ChanType[ncc]) = tg1h[lgmhc];
						_gate_h(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.g_para[tg1c].H0);
						_gate_powh(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.g_para[tg1c].hpower);

						tg1c++;
						lgmhc++;
						break;
					case 1: //M
						tg2c = tg1c;
						if (in == 0) {
							_ggatev12(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg2c].Vsm.val);
							_ggateslp(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg2c].km.val);
							_ggateslpt(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg2c].ktm.val);
							_ggatetmax(cupar.Gates[gc]) = (_ggateslpt(cupar.Gates[gc]) != 0.f ) ? 
											float(pops[ip].paraset.ichs.g_para[tg2c].tm.val):
											0.f;
							_ggatev12t(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg2c].Vhfm.val);
							_ggatet0(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg1c].t0m);
							_ggateslpt2(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg2c].ktm2.val);
							_ggatev12t2(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg2c].Vhfm2.val);
							tg2m.push_back(gc); // This is the index to point the shared Gate variables
							gc++;
						};
							_gate_typem(cupar.Data->Channels.ChanType[ncc]) = pops[ip].paraset.ichs.IChGTList[ic];
							_gate_parm(cupar.Data->Channels.ChanType[ncc]) = tg2m[lgmc];
							_gate_m(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.g_para[tg2c].M0);
							_gate_powm(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.g_para[tg2c].mpower);

							_gate_typeh(cupar.Data->Channels.ChanType[ncc]) = LSNS_NOGATE;
							_gate_parh(cupar.Data->Channels.ChanType[ncc]) = LSNS_MAX_GATEPARS;
							_gate_h(cupar.Data->Channels.ChanMH[ncc]) = 1.;
							_gate_powh(cupar.Data->Channels.ChanMH[ncc]) = 0.;
						
							tg1c++;
							lgmc++;

						break;
					case 2: //H
						tg3c = tg1c;
						if (in == 0) {
							_ggatev12(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg3c].Vsh.val);
							_ggateslp(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg3c].kh.val);
							_ggateslpt(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg3c].kth.val);
							_ggatetmax(cupar.Gates[gc]) = (_ggateslpt(cupar.Gates[gc]) != 0.f ) ? 
											float(pops[ip].paraset.ichs.g_para[tg3c].th.val):
											0.f;
							_ggatev12t(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg3c].Vhfh.val);
							_ggatet0(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg1c].t0h);
							_ggateslpt2(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg3c].kth2.val);
							_ggatev12t2(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.g_para[tg3c].Vhfh2.val);
							tg2h.push_back(gc);
							gc++;
						};

						_gate_typem(cupar.Data->Channels.ChanType[ncc]) = LSNS_NOGATE;
						_gate_parm(cupar.Data->Channels.ChanType[ncc]) = LSNS_MAX_GATEPARS;
						_gate_m(cupar.Data->Channels.ChanMH[ncc]) = 1.;
						_gate_powm(cupar.Data->Channels.ChanMH[ncc]) = 0.;

						_gate_typeh(cupar.Data->Channels.ChanType[ncc]) = pops[ip].paraset.ichs.IChGTList[ic];
						_gate_parh(cupar.Data->Channels.ChanType[ncc]) = tg2h[lghc];
						_gate_h(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.g_para[tg3c].H0);
						_gate_powh(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.g_para[tg3c].hpower);

						tg1c++;
						lghc++;
						break;
					
					};
					break;
				case 6: case 7: // alpha beta type gates
					switch (pops[ip].paraset.ichs.IChMHTList[ic]) {
					case 0: //MH
						if (in == 0) {
							_abgatet0(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].t0m);
							_abgatetmax(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].tmaxm);
							_abgateAa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].MAa.val);
							_abgateBa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].MBa.val);
							_abgateCa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].MCa.val);
							_abgateDa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].MDa.val);
							_abgateEa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].MEa.val);
							_abgateAb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].MAb.val);
							_abgateBb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].MBb.val);
							_abgateCb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].MCb.val);
							_abgateDb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].MDb.val);
							_abgateEb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].MEb.val);

							// This is the index to point the shared Gate variables
							tab1m.push_back(gc);
							gc++;
							_abgatet0(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].t0h);
							_abgatetmax(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].tmaxh);
							_abgateAa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].HAa.val);
							_abgateBa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].HBa.val);
							_abgateCa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].HCa.val);
							_abgateDa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].HDa.val);
							_abgateEa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].HEa.val);
							_abgateAb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].HAb.val);
							_abgateBb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].HBb.val);
							_abgateCb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].HCb.val);
							_abgateDb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].HDb.val);
							_abgateEb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].HEb.val);
							tab1h.push_back(gc);
							gc++;

						}

						_gate_typem(cupar.Data->Channels.ChanType[ncc]) = pops[ip].paraset.ichs.IChGTList[ic];
						_gate_parm(cupar.Data->Channels.ChanType[ncc]) = tab1m[labmhc];
						_gate_m(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].M0);
						_gate_powm(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].mpower);

						_gate_typeh(cupar.Data->Channels.ChanType[ncc]) = pops[ip].paraset.ichs.IChGTList[ic];
						_gate_parh(cupar.Data->Channels.ChanType[ncc]) = tab1h[labmhc];
						_gate_h(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].H0);
						_gate_powh(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.ab_para[tab1c].hpower);

						tab1c++;
						labmhc++;
						break;
					case 1: //M
						tab2c = tab1c;
						if (in == 0) {
							_abgatet0(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab2c].t0m);
							_abgatetmax(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab2c].tmaxm);
							_abgateAa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab2c].MAa.val);
							_abgateBa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab2c].MBa.val);
							_abgateCa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab2c].MCa.val);
							_abgateDa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab2c].MDa.val);
							_abgateEa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab2c].MEa.val);
							_abgateAb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab2c].MAb.val);
							_abgateBb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab2c].MBb.val);
							_abgateCb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab2c].MCb.val);
							_abgateDb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab2c].MDb.val);
							_abgateEb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab2c].MEb.val);

							// This is the index to point the shared Gate variables
							tab2m.push_back(gc);
							gc++;
						};

						_gate_typem(cupar.Data->Channels.ChanType[ncc]) = pops[ip].paraset.ichs.IChGTList[ic];
						_gate_parm(cupar.Data->Channels.ChanType[ncc]) = tab2m[labmc];
						_gate_m(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.ab_para[tab2c].M0);
						_gate_powm(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.ab_para[tab2c].mpower);

						_gate_typeh(cupar.Data->Channels.ChanType[ncc]) = LSNS_NOGATE;
						_gate_parh(cupar.Data->Channels.ChanType[ncc]) = LSNS_MAX_GATEPARS;
						_gate_h(cupar.Data->Channels.ChanMH[ncc]) = 1.;
						_gate_powh(cupar.Data->Channels.ChanMH[ncc]) = 0.;

						tab1c++;
						labmc++;
						break;
					case 2: //H
						tab3c = tab1c;
						if (in == 0) {
							_abgatet0(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab3c].t0h);
							_abgatetmax(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab3c].tmaxh);
							_abgateAa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab3c].HAa.val);
							_abgateBa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab3c].HBa.val);
							_abgateCa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab3c].HCa.val);
							_abgateDa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab3c].HDa.val);
							_abgateEa(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab3c].HEa.val);
							_abgateAb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab3c].HAb.val);
							_abgateBb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab3c].HBb.val);
							_abgateCb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab3c].HCb.val);
							_abgateDb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab3c].HDb.val);
							_abgateEb(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.ab_para[tab3c].HEb.val);

							tab2h.push_back(gc);
							gc++;
						};

						_gate_typem(cupar.Data->Channels.ChanType[ncc]) = LSNS_NOGATE;
						_gate_parm(cupar.Data->Channels.ChanType[ncc]) = LSNS_MAX_GATEPARS;
						_gate_m(cupar.Data->Channels.ChanMH[ncc]) = 1.;
						_gate_powm(cupar.Data->Channels.ChanMH[ncc]) = 0.;

						_gate_typeh(cupar.Data->Channels.ChanType[ncc]) = pops[ip].paraset.ichs.IChGTList[ic];
						_gate_parh(cupar.Data->Channels.ChanType[ncc]) = tab2h[labhc];
						_gate_h(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.ab_para[tab3c].H0);
						_gate_powh(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.ab_para[tab3c].hpower);

						tab1c++;
						labhc++;
						break;
					};
					break;
				case 8: case 9: case 10: case 11: // z type gates
					switch (pops[ip].paraset.ichs.IChMHTList[ic]) {
					case 0:
						if (in == 0) {
							_zgateA(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz1c].A.val)*float(ZGAco);
							_zgateB(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz1c].B.val);
							_zgateL(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz1c].L.val);
							_zgateG(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz1c].G.val);
							_zgatev12(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz1c].Vhf.val);
							_zgateslp(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz1c].slp.val);
							_zgatet0(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz1c].t0);
							_zgatetmax(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz1c].tmax)*float(ZGTMco);

							tz.push_back(gc);
							gc++;
						};

						_gate_typem(cupar.Data->Channels.ChanType[ncc]) = pops[ip].paraset.ichs.IChGTList[ic];
						_gate_parm(cupar.Data->Channels.ChanType[ncc]) = tz[lzmhc];
						_gate_m(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.z_para[tz1c].M0);
						_gate_powm(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.z_para[tz1c].mpower);

						_gate_typeh(cupar.Data->Channels.ChanType[ncc]) = pops[ip].paraset.ichs.IChGTList[ic];
						_gate_parh(cupar.Data->Channels.ChanType[ncc]) = tz[lzmhc];
						_gate_h(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.z_para[tz1c].H0);
						_gate_powh(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.z_para[tz1c].hpower);

						tz1c++;
						lzmhc++;
						break;
					case 1:
						tz2c = tz1c;
						if (in == 0) {
							_zgateA(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz2c].A.val)*float(ZGAco);
							_zgateB(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz2c].B.val);
							_zgateL(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz2c].L.val);
							_zgateG(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz2c].G.val);
							_zgatev12(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz2c].Vhf.val);
							_zgateslp(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz2c].slp.val);
							_zgatet0(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz2c].t0);
							_zgatetmax(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz2c].tmax)*float(ZGTMco);
							tz.push_back(gc);
							gc++;
						};

						_gate_typem(cupar.Data->Channels.ChanType[ncc]) = pops[ip].paraset.ichs.IChGTList[ic];
						_gate_parm(cupar.Data->Channels.ChanType[ncc]) = tz[lzmc];
						_gate_m(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.z_para[tz2c].M0);
						_gate_powm(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.z_para[tz2c].mpower);

						_gate_typeh(cupar.Data->Channels.ChanType[ncc]) = LSNS_NOGATE;
						_gate_parh(cupar.Data->Channels.ChanType[ncc]) = LSNS_MAX_GATEPARS;
						_gate_h(cupar.Data->Channels.ChanMH[ncc]) = 1.;
						_gate_powh(cupar.Data->Channels.ChanMH[ncc]) = 0.;

						tz1c++;
						lzmc++;
						break;
					case 2:
						tz3c = tz1c;
						if (in == 0) {
							_zgateA(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz3c].A.val)*float(ZGAco);
							_zgateB(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz3c].B.val);
							_zgateL(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz3c].L.val);
							_zgateG(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz3c].G.val);
							_zgatev12(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz3c].Vhf.val);
							_zgateslp(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz3c].slp.val);
							_zgatet0(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz3c].t0);
							_zgatetmax(cupar.Gates[gc]) = float(pops[ip].paraset.ichs.z_para[tz3c].tmax)*float(ZGTMco);

							tz.push_back(gc);
							gc++;
						};

						_gate_typem(cupar.Data->Channels.ChanType[ncc]) = LSNS_NOGATE; 
						_gate_parm(cupar.Data->Channels.ChanType[ncc]) = LSNS_MAX_GATEPARS;
						_gate_m(cupar.Data->Channels.ChanMH[ncc]) = 1.;
						_gate_powm(cupar.Data->Channels.ChanMH[ncc]) = 0.;

						_gate_typeh(cupar.Data->Channels.ChanType[ncc]) = pops[ip].paraset.ichs.IChGTList[ic];
						_gate_parh(cupar.Data->Channels.ChanType[ncc]) = tz[lzhc];
						_gate_h(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.z_para[tz3c].H0);
						_gate_powh(cupar.Data->Channels.ChanMH[ncc]) = float(pops[ip].paraset.ichs.z_para[tz3c].hpower);

						tz1c++;
						lzhc++;
						break;
					};
					break;
				case 0: case 12:					
					_gate_typem(cupar.Data->Channels.ChanType[ncc]) = LSNS_NOGATE;
					_gate_typeh(cupar.Data->Channels.ChanType[ncc]) = LSNS_NOGATE;
					_gate_parm(cupar.Data->Channels.ChanType[ncc]) = LSNS_MAX_GATEPARS;
					_gate_parh(cupar.Data->Channels.ChanType[ncc]) = LSNS_MAX_GATEPARS;
					_gate_m(cupar.Data->Channels.ChanMH[ncc]) = 1.;
					_gate_powm(cupar.Data->Channels.ChanMH[ncc]) = 0.;
					_gate_h(cupar.Data->Channels.ChanMH[ncc]) = 1.;
					_gate_powh(cupar.Data->Channels.ChanMH[ncc]) = 0.;

					break;
				case 13:
					if (in == 0 && ip == 0) {
						//Initialization for Synapse variables
					};
					_gate_typem(cupar.Data->Channels.ChanType[ncc]) = LSNS_SYNAPSE;
					_gate_typeh(cupar.Data->Channels.ChanType[ncc]) = LSNS_NOGATE;
					if (lsync < 4) {
						if (pops[ip].paraset.ichs.IChNList[ic] == "SynpEx1")
							_gate_parm(cupar.Data->Channels.ChanType[ncc]) = 0;
						else if (pops[ip].paraset.ichs.IChNList[ic] == "SynpEx2")
							_gate_parm(cupar.Data->Channels.ChanType[ncc]) = 1;
						else if (pops[ip].paraset.ichs.IChNList[ic] == "SynpIn1")
							_gate_parm(cupar.Data->Channels.ChanType[ncc]) = 2;
						else if (pops[ip].paraset.ichs.IChNList[ic] == "SynpIn2")
							_gate_parm(cupar.Data->Channels.ChanType[ncc]) = 3;
					}
					else {
						if (pops[ip].paraset.ichs.IChNList[ic] == "SynpEx1")
							_gate_parm(cupar.Data->Channels.ChanType[ncc]) = 4;
						else if (pops[ip].paraset.ichs.IChNList[ic] == "SynpEx2")
							_gate_parm(cupar.Data->Channels.ChanType[ncc]) = 5;
						else if (pops[ip].paraset.ichs.IChNList[ic] == "SynpIn1")
							_gate_parm(cupar.Data->Channels.ChanType[ncc]) = 6;
						else if (pops[ip].paraset.ichs.IChNList[ic] == "SynpIn2")
							_gate_parm(cupar.Data->Channels.ChanType[ncc]) = 7;
					};
					_gate_parh(cupar.Data->Channels.ChanType[ncc]) = LSNS_MAX_GATEPARS;
					_gate_m(cupar.Data->Channels.ChanMH[ncc]) = 0.;
					_gate_powm(cupar.Data->Channels.ChanMH[ncc]) = 1.;
					_gate_h(cupar.Data->Channels.ChanMH[ncc]) = 1.;
					_gate_powh(cupar.Data->Channels.ChanMH[ncc]) = 0.;

					lsync++;
					if (lsync == 8) lsync = 0;

					break;
				};
						
				ncc++;
			}; //for channel ic

//ION Data setting ===========================================================================================

			//Ion data copy
			if (sw_ion_dynam == 1) {

				string xstr;
				int sw_value = 0;
				int iit, iif, idx_t, idx_ca;
				bool flg[6] = {0,0,0,0,0,0}; //NoIon, Na, K, Ca, Cl, Mg
				int tna, tca;
				//int tk, cl, mg;
				
				for (int ic=0;ic<pops[ip].paraset.ichs.nIChs;ic++) {
				
					//Check if the current channel is on the ions list for ion dynamics
					//if it is, set sw_value to ionType defined in ion_para
					//if it is not, set sw_value to 0 to define this channel as non-specific ions
					idx_t = -1, iif=0;
					for (int ii = 0;ii<pops[ip].paraset.ions.nIons;ii++) {
						if (pops[ip].paraset.ichs.IChIonTList[ic] == pops[ip].paraset.ions.ion_para[ii].ionType) {
							iif = sw_value = pops[ip].paraset.ions.ion_para[ii].ionType;
							iit = ii;
							idx_t = pops[ip].paraset.ichs.ICh2IonTmap[ic];
							break;
						}
						else {
							sw_value = 0;
							iif = 0;
						};

					};
					skipflg = 0;
					for (int ti = 0;ti < ic;ti++) {
						if (pops[ip].paraset.ichs.ICh2IonTmap[ic] == pops[ip].paraset.ichs.ICh2IonTmap[ti]) {
							skipflg = 1;
							if (idx_t == -1) idx_t = ti; //SM mistype fixed (was idx_t = -1)
							break;
						}
						else {
							skipflg = 0;
						};
					};
					switch (pops[ip].paraset.ichs.IChGTList[ic]) {
					case 8: case 9: case 10: case 11: //Z genetic and Z ab
						kcaflg = 1;
						break;
					default:
						kcaflg = 0;
						break;
					};

					if ((flg[iif] == 0 || sw_value == 0 ) && skipflg == 0) {

						_chan_lutv(cupar.Data->Ions.IonsLUT[nic2]) = nc;

						switch(sw_value) {
						case 0: //Non-specific ions

							_ions_typeeds(cupar.Data->Ions.IonsType[nic2]) = LSNS_NOSPEC_EDS;
							_ions_typepump(cupar.Data->Ions.IonsType[nic2]) = LSNS_NO_DYN;

							memcpy(&(cupar.Data->Ions.IChanGLUT[nic2*int(MAX_CHAN_PER_PUMP/4+1)]),&(tmpNoIon->Dat),sizeof(uint4)*int(MAX_CHAN_PER_PUMP/4+1));
					
							break;
						case 1: //Na ions

							memcpy(&(cupar.Data->Ions.IChanGLUT[nic2*int(MAX_CHAN_PER_PUMP/4+1)]),&(tmpNa->Dat),sizeof(uint4)*int(MAX_CHAN_PER_PUMP/4+1));

							if (in == 0) {
								_pump_in0(cupar.Ions[itc]) = float(pops[ip].paraset.ions.ion_para[iit].InEq);
								_pump_rpump(cupar.Ions[itc]) = float(pops[ip].paraset.ions.ion_para[iit].Rpmp);
								_pump_tau(cupar.Ions[itc]) = float(pops[ip].paraset.ions.ion_para[iit].t);
								_pump_apump(cupar.Ions[itc]) = float(pops[ip].paraset.ions.ion_para[iit].Rch);
								_napump_kp3(cupar.Ions[itc]) = float(pops[ip].paraset.ions.ion_para[iit].Kp);
								_napump_in03(cupar.Ions[itc]) = float(pow(pops[ip].paraset.ions.ion_para[iit].InEq,3.) / (pow(pops[ip].paraset.ions.ion_para[iit].InEq,3.)+pow(pops[ip].paraset.ions.ion_para[iit].Kp,3)));
								tna = itc;
								itc++;
							};

							_ions_typeeds(cupar.Data->Ions.IonsType[nic2]) = LSNS_NA_IONS;
							if (pops[ip].paraset.ichs.IChDSList[ic] == 1)
								_ions_typepump(cupar.Data->Ions.IonsType[nic2]) = pops[ip].paraset.ions.ion_para[iit].pumpType;
							else
								_ions_typepump(cupar.Data->Ions.IonsType[nic2]) = LSNS_NO_DYN;

							_ions_parpump(cupar.Data->Ions.IonsType[nic2]) = tna; // This is the index to point the shared Ions variables

							_ions_in(cupar.Data->Ions.IonsE[nic2]) = float(pops[ip].paraset.ions.ion_para[iit].Cin);
							_ions_out(cupar.Data->Ions.IonsE[nic2]) = float(pops[ip].paraset.ions.ion_para[iit].Cout);
							_ions_rtfz(cupar.Data->Ions.IonsE[nic2]) = float(pops[ip].paraset.ions.ion_para[iit].R*pops[ip].paraset.ions.ion_para[iit].T/(pops[ip].paraset.ions.ion_para[iit].F*pops[ip].paraset.ions.ion_para[iit].Z));

							flg[iif] = 1;
							break;
						case 2: //K ions
							//memcpy(&(cupar.Data->Ions.ChanGLUT[nic2*int(MAX_CHAN_PER_PUMP / 4 + 1)]), &(tmpK->Dat), sizeof(uint4)*int(MAX_CHAN_PER_PUMP / 4 + 1));
							//flg[iif] = 1;
							break;
						case 3: //Ca ions
							
							memcpy(&(cupar.Data->Ions.IChanGLUT[nic2*int(MAX_CHAN_PER_PUMP/4+1)]),&(tmpCa->Dat),sizeof(uint4)*int(MAX_CHAN_PER_PUMP/4+1));

							if (in==0) {
								_pump_in0(cupar.Ions[itc]) = float(pops[ip].paraset.ions.ion_para[iit].InEq);
								_pump_rpump(cupar.Ions[itc]) = float(pops[ip].paraset.ions.ion_para[iit].Rpmp);
								_pump_tau(cupar.Ions[itc]) = float(pops[ip].paraset.ions.ion_para[iit].t);
								_pump_apump(cupar.Ions[itc]) = float(pops[ip].paraset.ions.ion_para[iit].Rch);

								_pump_k(cupar.Ions[itc]) = float(pops[ip].paraset.ions.ion_para[iit].K);
								_pump_b(cupar.Ions[itc]) = float(pops[ip].paraset.ions.ion_para[iit].B);
								tca = itc;
								itc++;
							};

							_ions_typeeds(cupar.Data->Ions.IonsType[nic2]) = LSNS_CA_IONS;
							if (pops[ip].paraset.ichs.IChDSList[ic] == 1)
								_ions_typepump(cupar.Data->Ions.IonsType[nic2]) = pops[ip].paraset.ions.ion_para[iit].pumpType;
							else
								_ions_typepump(cupar.Data->Ions.IonsType[nic2]) = LSNS_NO_DYN;

							_ions_parpump(cupar.Data->Ions.IonsType[nic2]) = tca;
							_ions_in(cupar.Data->Ions.IonsE[nic2]) = float(pops[ip].paraset.ions.ion_para[iit].Cin);
							_ions_out(cupar.Data->Ions.IonsE[nic2]) = float(pops[ip].paraset.ions.ion_para[iit].Cout);
							_ions_rtfz(cupar.Data->Ions.IonsE[nic2]) = float(pops[ip].paraset.ions.ion_para[iit].R*pops[ip].paraset.ions.ion_para[iit].T/(pops[ip].paraset.ions.ion_para[iit].F*pops[ip].paraset.ions.ion_para[iit].Z));						
							idx_ca = nic2;
							flg[iif] = 1;
							break;
						case 4: //Cl ions
								//memcpy(&(cupar.Data->Ions.ChanGLUT[nic2*int(MAX_CHAN_PER_PUMP / 4 + 1)]), &(tmpCl->Dat), sizeof(uint4)*int(MAX_CHAN_PER_PUMP / 4 + 1));
								//flg[iif] = 1;
							break;
						case 5: //Mg ions
								//memcpy(&(cupar.Data->Ions.ChanGLUT[nic2*int(MAX_CHAN_PER_PUMP / 4 + 1)]), &(tmpMg->Dat), sizeof(uint4)*int(MAX_CHAN_PER_PUMP / 4 + 1));
								//flg[iif] = 1;
							break;
						};
						
						if (pops[ip].paraset.ichs.IChGTList[ic] != LSNS_SYNAPSE) {
							_chan_lutm(cupar.Data->Channels.ChanLUT[ncc2]) = nic2; //LUT for Activation Cin
							_chan_luth(cupar.Data->Channels.ChanLUT[ncc2]) = nic2; //LUT for Inctivation Cin
						}else {
							_chan_lutm(cupar.Data->Channels.ChanLUT[ncc2]) = 0; //LUT for Activation Cin
							_chan_luth(cupar.Data->Channels.ChanLUT[ncc2]) = 0; //LUT for Inctivation Cin
						}
						nic2++;

					} //if flg & sw_value
					else { 
						if (kcaflg == 0) {
							//If ion setting was skipped, instead put the index of the ion data
							_chan_lutm(cupar.Data->Channels.ChanLUT[ncc2]) = nic2 - ic + idx_t;
							_chan_luth(cupar.Data->Channels.ChanLUT[ncc2]) = nic2 - ic + idx_t;
						}
						else {// This is only for KCa setting
							_chan_lutm(cupar.Data->Channels.ChanLUT[ncc2]) = idx_ca;
							_chan_luth(cupar.Data->Channels.ChanLUT[ncc2]) = idx_ca;
						}
					};
					ncc2++;
				}; //for channel ic

			}; //if sw_ion_dynam == 0

			delete[] tmpNa;
			delete[] tmpK;
			delete[] tmpCa;
			delete[] tmpCl;
			delete[] tmpMg;
			delete[] tmpNoIon;		

			nc++;

		}; //for neuron in
		
	}; // for population ip

//Nonspike Units setting ======================================================================

	int ucc=0;
	for (int iu = 0;iu < (int)nsu.units.size();iu++) {
		
		_unit_tup(cupar.Data->Units.UnitV[iu]) = (float)nsu.units[iu].Tup;
		_unit_tdw(cupar.Data->Units.UnitV[iu]) = (float)nsu.units[iu].Tdown;
		_unit_thr(cupar.Data->Units.UnitV[iu]) = (float)nsu.units[iu].Threshold;

		switch (nsu.units[iu].type) {
		case NSU_DRIVE:
			_chan_gmax(cupar.Data->Units.ChanG[ncc + ucc]) = 1;
			_chan_g(cupar.Data->Units.ChanG[ncc + ucc]) = 1.;
			_chan_ge(cupar.Data->Units.ChanG[ncc + ucc]) = 0.;
			_chan_i(cupar.Data->Units.ChanG[ncc + ucc]) = 0.;

			map2lut(cupar.Data->Units.UChanGLUT, iu, ncc + ucc, MAX_CHAN_PER_CELL);	// SM

			_gate_typem(cupar.Data->Channels.ChanType[ncc + ucc]) = LSNS_NOGATE;
			_gate_typeh(cupar.Data->Channels.ChanType[ncc + ucc]) = LSNS_NOGATE;
			_gate_parm(cupar.Data->Channels.ChanType[ncc + ucc]) = LSNS_MAX_GATEPARS;
			_gate_parh(cupar.Data->Channels.ChanType[ncc + ucc]) = LSNS_MAX_GATEPARS;
			_gate_m(cupar.Data->Channels.ChanMH[ncc + ucc]) = 1.;
			_gate_powm(cupar.Data->Channels.ChanMH[ncc + ucc]) = 0.;
			_gate_h(cupar.Data->Channels.ChanMH[ncc + ucc]) = 1.;
			_gate_powh(cupar.Data->Channels.ChanMH[ncc + ucc]) = 0.;

			ucc++;
			break;
		case NSU_OUTPUT:
			for (int ic = 0;ic < nsynps;ic++, ucc++) {
				_chan_gmax(cupar.Data->Units.ChanG[ncc + ucc]) = 1;
				_chan_g(cupar.Data->Units.ChanG[ncc + ucc]) = 0.;
				_chan_ge(cupar.Data->Units.ChanG[ncc + ucc]) = 0.;
				_chan_i(cupar.Data->Units.ChanG[ncc + ucc]) = 0.;

				map2lut(cupar.Data->Units.UChanGLUT, iu, ncc + ucc, MAX_CHAN_PER_CELL);	// SM

				_gate_typem(cupar.Data->Channels.ChanType[ncc + ucc]) = LSNS_SYNAPSE;
				_gate_typeh(cupar.Data->Channels.ChanType[ncc + ucc]) = LSNS_NOGATE;
				if (ic < nsynps - 4) {
					if (nsu.units[iu].ichs.IChNList[ic] == "SynpEx1")
						_gate_parm(cupar.Data->Channels.ChanType[ncc + ucc]) = 0;
					else if (nsu.units[iu].ichs.IChNList[ic] == "SynpEx2")
						_gate_parm(cupar.Data->Channels.ChanType[ncc + ucc]) = 1;
					else if (nsu.units[iu].ichs.IChNList[ic] == "SynpIn1")
						_gate_parm(cupar.Data->Channels.ChanType[ncc + ucc]) = 2;
					else if (nsu.units[iu].ichs.IChNList[ic] == "SynpIn2")
						_gate_parm(cupar.Data->Channels.ChanType[ncc + ucc]) = 3;
				}
				else {//Added on 12.21.2015
					if (nsu.units[iu].ichs.IChNList[ic] == "SynpEx1")
						_gate_parm(cupar.Data->Channels.ChanType[ncc + ucc]) = 4;
					else if (nsu.units[iu].ichs.IChNList[ic] == "SynpEx2")
						_gate_parm(cupar.Data->Channels.ChanType[ncc + ucc]) = 5;
					else if (nsu.units[iu].ichs.IChNList[ic] == "SynpIn1")
						_gate_parm(cupar.Data->Channels.ChanType[ncc + ucc]) = 6;
					else if (nsu.units[iu].ichs.IChNList[ic] == "SynpIn2")
						_gate_parm(cupar.Data->Channels.ChanType[ncc + ucc]) = 7;
				};
				_gate_parh(cupar.Data->Channels.ChanType[ncc + ucc]) = LSNS_MAX_GATEPARS;
				_gate_m(cupar.Data->Channels.ChanMH[ncc + ucc]) = 0.;
				_gate_powm(cupar.Data->Channels.ChanMH[ncc + ucc]) = 1.;
				_gate_h(cupar.Data->Channels.ChanMH[ncc + ucc]) = 1.;
				_gate_powh(cupar.Data->Channels.ChanMH[ncc + ucc]) = 0.;
			};
			break;
		};
	
	}; //for unit



//IOData setting ==============================================================================
	//Index for type sections in the global memory structure
	int iIonsI, iIonsE, iChanG, iChanMH, iCellV, iUnitV;
	iIonsI = 0;
	iIonsE = cupar.NetDim.MaxIons;
	iChanG = iIonsE + cupar.NetDim.MaxIons;
	iChanMH = iChanG + cupar.NetDim.MaxChan;
	iCellV = iChanMH + cupar.NetDim.MaxChan;
	iUnitV = iCellV + cupar.NetDim.MaxCells;

	//Setting globalviewdataLUT
	int iT=0, iTpre=0, iOff=0, in=0, cpt=0, cut=0, iPP=0;
	vector<int> neuTemp;
	vector<int> nsuList;
	vector<vector<int>> neuList;
	//int * popList;
	vector<int> popList;

	for (int iv=0;iv<(int)iodata.size();iv++) {
		in=0;
		//Making full lists for indexing
		cpt=0, cut=0;
		//popList = new int [iodata[iv].npop];
		popList.resize(iodata[iv].npop);
		for (int j=0;j<(int)iodata[iv].popList.size();j++) {
			if (iodata[iv].popList[j][1] != -1) {
				for (int jt=0;jt<iodata[iv].popList[j][1]-iodata[iv].popList[j][0]+1;jt++) {
					popList[cpt] = iodata[iv].popList[j][0] + jt;
					cpt++;
				};
			}else {
				popList[cpt] = iodata[iv].popList[j][0];
				cpt++;
			};
		};
		for (int jp=0;jp<iodata[iv].npop;jp++) {
			neuTemp.clear();
			for (int jn=0;jn<(int)iodata[iv].neuList[jp].size();jn++) {
				if (iodata[iv].neuList[jp][jn][1] != -1) {
					for (int jnn=0;jnn<iodata[iv].neuList[jp][jn][1]-iodata[iv].neuList[jp][jn][0]+1;jnn++) {
						neuTemp.push_back(iodata[iv].neuList[jp][jn][0]+jnn);
					};
				}else {
					neuTemp.push_back(iodata[iv].neuList[jp][jn][0]);
				};
			};
			neuList.push_back(neuTemp);
		};

		if (iodata[iv].type == 16) {
		//if (iodata[iv].type == 25) {
			if (iodata[iv].nnsu != 0) {
				nsuList.resize(iodata[iv].nnsu);
				for (int j = 0;j < (int)iodata[iv].nsuList.size();j++) {
					if (iodata[iv].nsuList[j][1] != -1) {
						for (int jt = 0;jt < iodata[iv].nsuList[j][1] - iodata[iv].nsuList[j][0] + 1;jt++) {
							nsuList[cut] = iodata[iv].nsuList[j][0] + jt;
							cut++;
						};
					}
					else {
						nsuList[cut] = iodata[iv].nsuList[j][0];
						cut++;
					};
				};
			};
		};


		if (iodata[iv].type < 8) {
			if (iodata[iv].type < 4) { //IONSI
				iT = iTpre, iPP = 0;
				for (int jp = 0;jp<iodata[iv].npop;jp++) {
					for (int jn = 0;jn<iodata[iv].nneu[jp];jn++) {
						iOff = in/4;
						if (in == iodata[iv].ntneu-1) {
							switch(in%4) {
							case 0:
								cupar.Data->IOData.ViewLUT[iT+iOff].x = make_i4LUT(iodata[iv].type,iIonsI, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIonTs + pops[popList[jp]].paraset.ichs.ICh2IonTmap[iodata[iv].ich]);
								cupar.Data->IOData.ViewLUT[iT+iOff].y = -1;
								cupar.Data->IOData.ViewLUT[iT+iOff].z = -1;
								cupar.Data->IOData.ViewLUT[iT+iOff].w = -1;
								break;
							case 1:
								cupar.Data->IOData.ViewLUT[iT+iOff].y = make_i4LUT(iodata[iv].type,iIonsI, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIonTs + pops[popList[jp]].paraset.ichs.ICh2IonTmap[iodata[iv].ich]);
								cupar.Data->IOData.ViewLUT[iT+iOff].z = -1;
								cupar.Data->IOData.ViewLUT[iT+iOff].w = -1;
								break;
							case 2:
								cupar.Data->IOData.ViewLUT[iT+iOff].z = make_i4LUT(iodata[iv].type,iIonsI, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIonTs + pops[popList[jp]].paraset.ichs.ICh2IonTmap[iodata[iv].ich]);
								cupar.Data->IOData.ViewLUT[iT+iOff].w = -1;
								break;
							case 3:
								cupar.Data->IOData.ViewLUT[iT+iOff].w = make_i4LUT(iodata[iv].type,iIonsI, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIonTs + pops[popList[jp]].paraset.ichs.ICh2IonTmap[iodata[iv].ich]);
								break;
							};
						}else{
							switch(in%4) {
							case 0:
								cupar.Data->IOData.ViewLUT[iT+iOff].x = make_i4LUT(iodata[iv].type,iIonsI, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIonTs + pops[popList[jp]].paraset.ichs.ICh2IonTmap[iodata[iv].ich]);
								break;
							case 1:
								cupar.Data->IOData.ViewLUT[iT+iOff].y = make_i4LUT(iodata[iv].type,iIonsI, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIonTs + pops[popList[jp]].paraset.ichs.ICh2IonTmap[iodata[iv].ich]);
								break;
							case 2:
								cupar.Data->IOData.ViewLUT[iT+iOff].z = make_i4LUT(iodata[iv].type,iIonsI, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIonTs + pops[popList[jp]].paraset.ichs.ICh2IonTmap[iodata[iv].ich]);
								break;
							case 3:
								cupar.Data->IOData.ViewLUT[iT+iOff].w = make_i4LUT(iodata[iv].type,iIonsI, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIonTs + pops[popList[jp]].paraset.ichs.ICh2IonTmap[iodata[iv].ich]);
								break;
							};
						};
						in++;
					};
					//iPP = iPP + pops[jp].nneurons*pops[popList[jp]].paraset.ichs.nIonTs;
					iPP = iPP + pops[popList[jp]].nneurons*pops[popList[jp]].paraset.ichs.nIonTs;
				};
			}else { //For IONSE
				iT = iTpre, iPP = 0;
				for (int jp = 0;jp<iodata[iv].npop;jp++) {
					for (int jn = 0;jn<iodata[iv].nneu[jp];jn++) {
						iOff = in/4;
						if (in == iodata[iv].ntneu-1) {
							switch(in%4) {
							case 0:
								cupar.Data->IOData.ViewLUT[iT+iOff].x = make_i4LUT(iodata[iv].type,iIonsE, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIonTs + pops[popList[jp]].paraset.ichs.ICh2IonTmap[iodata[iv].ich]);
								cupar.Data->IOData.ViewLUT[iT+iOff].y = -1;
								cupar.Data->IOData.ViewLUT[iT+iOff].z = -1;
								cupar.Data->IOData.ViewLUT[iT+iOff].w = -1;
								break;
							case 1:
								cupar.Data->IOData.ViewLUT[iT+iOff].y = make_i4LUT(iodata[iv].type,iIonsE, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIonTs + pops[popList[jp]].paraset.ichs.ICh2IonTmap[iodata[iv].ich]);
								cupar.Data->IOData.ViewLUT[iT+iOff].z = -1;
								cupar.Data->IOData.ViewLUT[iT+iOff].w = -1;
								break;
							case 2:
								cupar.Data->IOData.ViewLUT[iT+iOff].z = make_i4LUT(iodata[iv].type,iIonsE, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIonTs + pops[popList[jp]].paraset.ichs.ICh2IonTmap[iodata[iv].ich]);
								cupar.Data->IOData.ViewLUT[iT+iOff].w = -1;
								break;
							case 3:
								cupar.Data->IOData.ViewLUT[iT+iOff].w = make_i4LUT(iodata[iv].type,iIonsE, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIonTs + pops[popList[jp]].paraset.ichs.ICh2IonTmap[iodata[iv].ich]);
								break;
							};
						}else{
							switch(in%4) {
							case 0:
								cupar.Data->IOData.ViewLUT[iT+iOff].x = make_i4LUT(iodata[iv].type,iIonsE, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIonTs + pops[popList[jp]].paraset.ichs.ICh2IonTmap[iodata[iv].ich]);
								break;
							case 1:
								cupar.Data->IOData.ViewLUT[iT+iOff].y = make_i4LUT(iodata[iv].type,iIonsE, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIonTs + pops[popList[jp]].paraset.ichs.ICh2IonTmap[iodata[iv].ich]);
								break;
							case 2:
								cupar.Data->IOData.ViewLUT[iT+iOff].z = make_i4LUT(iodata[iv].type,iIonsE, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIonTs + pops[popList[jp]].paraset.ichs.ICh2IonTmap[iodata[iv].ich]);
								break;
							case 3:
								cupar.Data->IOData.ViewLUT[iT+iOff].w = make_i4LUT(iodata[iv].type,iIonsE,iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIonTs + pops[popList[jp]].paraset.ichs.ICh2IonTmap[iodata[iv].ich]);
								break;
							};
						};
						in++;
					};
					//iPP = iPP + pops[jp].nneurons*pops[popList[jp]].paraset.ichs.nIonTs;
					iPP = iPP + pops[popList[jp]].nneurons*pops[popList[jp]].paraset.ichs.nIonTs;
				};
			};

			iTpre = iT + (int)ceil(iodata[iv].ntneu / 4.);

		}else if(iodata[iv].type >= 8 && iodata[iv].type < 16) {
			if (iodata[iv].type < 12) {//CHANG
				iT = iTpre, iPP = 0;
				for (int jp=0;jp<iodata[iv].npop;jp++) {
					for (int jn=0;jn<iodata[iv].nneu[jp];jn++) {					
						iOff = in/4;
						if (in == iodata[iv].ntneu -1) {
							switch(in%4) {
							case 0:
								cupar.Data->IOData.ViewLUT[iT+iOff].x = make_i4LUT(iodata[iv].type,iChanG, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIChs + iodata[iv].ich);
								cupar.Data->IOData.ViewLUT[iT+iOff].y = -1;
								cupar.Data->IOData.ViewLUT[iT+iOff].z = -1;
								cupar.Data->IOData.ViewLUT[iT+iOff].w = -1;
								break;
							case 1:
								cupar.Data->IOData.ViewLUT[iT+iOff].y = make_i4LUT(iodata[iv].type,iChanG, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIChs + iodata[iv].ich);
								cupar.Data->IOData.ViewLUT[iT+iOff].z = -1;
								cupar.Data->IOData.ViewLUT[iT+iOff].w = -1;
								break;
							case 2:
								cupar.Data->IOData.ViewLUT[iT+iOff].z = make_i4LUT(iodata[iv].type,iChanG, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIChs + iodata[iv].ich);
								cupar.Data->IOData.ViewLUT[iT+iOff].w = -1;
								break;
							case 3:
								cupar.Data->IOData.ViewLUT[iT+iOff].w = make_i4LUT(iodata[iv].type,iChanG, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIChs + iodata[iv].ich);
								break;
							};
						}else{
							switch(in%4) {
							case 0:
								cupar.Data->IOData.ViewLUT[iT+iOff].x = make_i4LUT(iodata[iv].type,iChanG, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIChs + iodata[iv].ich);
								break;
							case 1:
								cupar.Data->IOData.ViewLUT[iT+iOff].y = make_i4LUT(iodata[iv].type,iChanG, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIChs + iodata[iv].ich);
								break;
							case 2:
								cupar.Data->IOData.ViewLUT[iT+iOff].z = make_i4LUT(iodata[iv].type,iChanG, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIChs + iodata[iv].ich);
								break;
							case 3:
								cupar.Data->IOData.ViewLUT[iT+iOff].w = make_i4LUT(iodata[iv].type,iChanG, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIChs + iodata[iv].ich);
								break;
							};
						};
						in++;
					};
					//iPP = iPP + pops[jp].nneurons*pops[popList[jp]].paraset.ichs.nIChs;
					iPP = iPP + pops[popList[jp]].nneurons*pops[popList[jp]].paraset.ichs.nIChs;
				};
			}else{ //For CHANMH
				iT = iTpre, iPP = 0;
				for (int jp=0;jp<iodata[iv].npop;jp++) {
					for (int jn=0;jn<iodata[iv].nneu[jp];jn++) {
						iOff = in/4;
						if (in == iodata[iv].ntneu-1) {
							switch(in%4) {
							case 0:
								cupar.Data->IOData.ViewLUT[iT+iOff].x = make_i4LUT(iodata[iv].type,iChanMH, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIChs + iodata[iv].ich);
								cupar.Data->IOData.ViewLUT[iT+iOff].y = -1;
								cupar.Data->IOData.ViewLUT[iT+iOff].z = -1;
								cupar.Data->IOData.ViewLUT[iT+iOff].w = -1;
								break;
							case 1:
								cupar.Data->IOData.ViewLUT[iT+iOff].y = make_i4LUT(iodata[iv].type,iChanMH, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIChs + iodata[iv].ich);
								cupar.Data->IOData.ViewLUT[iT+iOff].z = -1;
								cupar.Data->IOData.ViewLUT[iT+iOff].w = -1;
								break;
							case 2:
								cupar.Data->IOData.ViewLUT[iT+iOff].z = make_i4LUT(iodata[iv].type,iChanMH, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIChs + iodata[iv].ich);
								cupar.Data->IOData.ViewLUT[iT+iOff].w = -1;
								break;
							case 3:
								cupar.Data->IOData.ViewLUT[iT+iOff].w = make_i4LUT(iodata[iv].type,iChanMH, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIChs + iodata[iv].ich);
								break;
							};
						}else{
							switch(in%4) {
							case 0:
								cupar.Data->IOData.ViewLUT[iT+iOff].x = make_i4LUT(iodata[iv].type,iChanMH, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIChs + iodata[iv].ich);
								break;
							case 1:
								cupar.Data->IOData.ViewLUT[iT+iOff].y = make_i4LUT(iodata[iv].type,iChanMH, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIChs + iodata[iv].ich);
								break;
							case 2:
								cupar.Data->IOData.ViewLUT[iT+iOff].z = make_i4LUT(iodata[iv].type,iChanMH, iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIChs + iodata[iv].ich);
								break;
							case 3:
								cupar.Data->IOData.ViewLUT[iT+iOff].w = make_i4LUT(iodata[iv].type,iChanMH,iPP + neuList[jp][jn]*pops[popList[jp]].paraset.ichs.nIChs + iodata[iv].ich);
								break;
							};
						};
						in++;
					};
					//iPP = iPP + pops[jp].nneurons*pops[popList[jp]].paraset.ichs.nIChs;
					iPP = iPP + pops[popList[jp]].nneurons*pops[popList[jp]].paraset.ichs.nIChs;
				};
			};

			iTpre = iT + (int)ceil(iodata[iv].ntneu / 4.);

		}else if(iodata[iv].type >= 16 && iodata[iv].type < 20) { //For CELLV
			int totaln;
			if (iodata[iv].type == 16) {
				totaln = iodata[iv].ntneu + iodata[iv].nnsu;
			}
			else {
				totaln = iodata[iv].ntneu;
			};
			iT = iTpre; iPP = 0;
			for (int jp=0;jp<iodata[iv].npop;jp++) {
				//for (int jn=0;jn<iodata[iv].nneu[popList[jp]];jn++) {
				for (int jn = 0;jn<iodata[iv].nneu[jp];jn++) {
					iOff = in/4;
					if (in == totaln-1) {
					//if (in == iodata[iv].ntneu) {
						//at the end of for loop, check if data are not filled in all x y z w positions
						//if not, insert -1 as "Non-index for memory access" in not filled positions
						switch(in%4) {
						case 0:
							cupar.Data->IOData.ViewLUT[iT+iOff].x = make_i4LUT(iodata[iv].type,iCellV, iPP + neuList[jp][jn]);
							cupar.Data->IOData.ViewLUT[iT+iOff].y = -1;
							cupar.Data->IOData.ViewLUT[iT+iOff].z = -1;
							cupar.Data->IOData.ViewLUT[iT+iOff].w = -1;
							break;
						case 1:
							cupar.Data->IOData.ViewLUT[iT+iOff].y = make_i4LUT(iodata[iv].type,iCellV, iPP + neuList[jp][jn]);
							cupar.Data->IOData.ViewLUT[iT+iOff].z = -1;
							cupar.Data->IOData.ViewLUT[iT+iOff].w = -1;
							break;
						case 2:
							cupar.Data->IOData.ViewLUT[iT+iOff].z = make_i4LUT(iodata[iv].type,iCellV, iPP + neuList[jp][jn]);
							cupar.Data->IOData.ViewLUT[iT+iOff].w = -1;
							break;
						case 3:
							cupar.Data->IOData.ViewLUT[iT+iOff].w = make_i4LUT(iodata[iv].type,iCellV, iPP + neuList[jp][jn]);
							break;
						};
					}else {
						switch(in%4) {
						case 0:
							cupar.Data->IOData.ViewLUT[iT+iOff].x = make_i4LUT(iodata[iv].type,iCellV, iPP + neuList[jp][jn]);
							break;
						case 1:
							cupar.Data->IOData.ViewLUT[iT+iOff].y = make_i4LUT(iodata[iv].type,iCellV, iPP + neuList[jp][jn]);
							break;
						case 2:
							cupar.Data->IOData.ViewLUT[iT+iOff].z = make_i4LUT(iodata[iv].type,iCellV, iPP + neuList[jp][jn]);
							break;
						case 3:
							cupar.Data->IOData.ViewLUT[iT+iOff].w = make_i4LUT(iodata[iv].type,iCellV, iPP + neuList[jp][jn]);
							break;
						};
					};
					in++;
				};
				//iPP = iPP + pops[jp].nneurons;
				iPP = iPP + pops[popList[jp]].nneurons;
			};

			//iTpre = iT + (int)ceil(iodata[iv].ntneu / 4.);

			if (iodata[iv].type == 16) {
			//if (iodata[iv].type == 25) {
				//NSU data
				int iun = in;
				//iT = iTpre;
				for (int ju = 0;ju < iodata[iv].nnsu;ju++) {
					iOff = iun / 4;
					if (iun == totaln - 1) {
						//if (ju == iodata[iv].nnsu) {
						//at the end of for loop, check if data are not filled in all x y z w positions
						//if not, insert -1 as "Non-index for memory access" in not filled positions
						switch (iun % 4) {
						case 0:
							cupar.Data->IOData.ViewLUT[iT + iOff].x = make_i4LUT(iodata[iv].type, iUnitV, nsuList[ju]);
							cupar.Data->IOData.ViewLUT[iT + iOff].y = -1;
							cupar.Data->IOData.ViewLUT[iT + iOff].z = -1;
							cupar.Data->IOData.ViewLUT[iT + iOff].w = -1;
							break;
						case 1:
							cupar.Data->IOData.ViewLUT[iT + iOff].y = make_i4LUT(iodata[iv].type, iUnitV, nsuList[ju]);
							cupar.Data->IOData.ViewLUT[iT + iOff].z = -1;
							cupar.Data->IOData.ViewLUT[iT + iOff].w = -1;
							break;
						case 2:
							cupar.Data->IOData.ViewLUT[iT + iOff].z = make_i4LUT(iodata[iv].type, iUnitV, nsuList[ju]);
							cupar.Data->IOData.ViewLUT[iT + iOff].w = -1;
							break;
						case 3:
							cupar.Data->IOData.ViewLUT[iT + iOff].w = make_i4LUT(iodata[iv].type, iUnitV, nsuList[ju]);
							break;
						};
					}
					else {
						switch (iun % 4) {
						case 0:
							cupar.Data->IOData.ViewLUT[iT + iOff].x = make_i4LUT(iodata[iv].type, iUnitV, nsuList[ju]);
							break;
						case 1:
							cupar.Data->IOData.ViewLUT[iT + iOff].y = make_i4LUT(iodata[iv].type, iUnitV, nsuList[ju]);
							break;
						case 2:
							cupar.Data->IOData.ViewLUT[iT + iOff].z = make_i4LUT(iodata[iv].type, iUnitV, nsuList[ju]);
							break;
						case 3:
							cupar.Data->IOData.ViewLUT[iT + iOff].w = make_i4LUT(iodata[iv].type, iUnitV, nsuList[ju]);
							break;
						};
					};
					iun++;
				}; //For nsu
				//iTpre = iT + (int)ceil(iodata[iv].nnsu / 4.);
			};
			//int tempp = (int)ceil(totaln / 4.);
			iTpre = iT + (int)ceil(totaln / 4.);

		}; //For CELLV

		neuList.clear();
		popList.clear();
		nsuList.clear(); //added on 12.16.2015

	}; // For iv

};

int make_i4LUT (int ViewT, int iTSec, int iTOff) {

	int idx = (iTSec + iTOff) & 0x3FFFFFFF;
	int i4 = ViewT%4;
	int i4shft = (i4) << 30;
	int iout = i4shft | idx;

	return iout;

};

void prep_sim(nns_project& pro, netpar& cupar) {

	mapsyn mps;
	//1. Initializeing cupar and synapse
	init_cuparam( cupar, pro );
	init_synapse( cupar, pro, mps );
	//2. Memory allocation with the netpar
	lsns_alloc(cupar);
	//3. Copying parameters from population and network into the global memoery for cuda processing
	copy_mem2cu( cupar, pro.pops, pro.nsu, pro.iodata, pro.ctr_para );
	copy_map2mem( cupar, mps );
	//4. Mapping to device(GPU)
	lsns_map2dev(cupar);
};

void make_testnns(nns_project& pro) {

	//create_pop(pro.pops, "other", 1);
	//load_tmpl(pro.pops, "other", 1);

	//create_pop(pro.pops, "popA", 50);
	//load_tmpl(pro.pops, "popA", 0);
	//create_pop(pro.pops, "popB", 50);
	//load_tmpl(pro.pops, "popB", 0);
	//create_ich(pro.pops[0], "Naf", 3, 0);
	//create_ich(pro.pops[0], "K", 3, 1);
	//create_ich(pro.pops[0], "Leak", 0,3);
	//create_ich(pro.pops[0], "Leak", 4);

	create_pop(pro.pops,"preI",100);
	load_tmpl(pro.pops,"preI",0);
	//
	copy_pop(pro.pops,"preI");
	rename_pop(pro.pops,"preI_c0","postI");
	////copy_pop(pops,"preI","postI");
	////copy_pop(pops,"preI");
	////copy_pop(pops,"preI");
	////

	vector<string> ptmp;
	vector<string> utmp;
	param sprb; param tprb; param w;
	int syntype = 0;

	ptmp.push_back("preI");
	ptmp.push_back("postI");

	make_net("cpg4rr",ptmp, utmp,pro.pops, pro.nsu, pro.tnet.nets);
	
	sprb.val = 20; sprb.var = 10;
	tprb.val = 20; tprb.var = 10;
	w.val = 1; w.var = 10;

	//conn_pops("cpg4rr", syntype,CONN_REG, "preI","preI",UTYPE_POP,UTYPE_POP, sprb, tprb,w,pro.tnet.nets);

	sprb.val = 20; sprb.var = 10;
	tprb.val = 30; tprb.var = 20;

	//conn_pops("cpg4rr",syntype, CONN_REG,"preI","postI", UTYPE_POP, UTYPE_POP, sprb, tprb,w,pro.tnet.nets);

	copy_net("cpg4rr",pro.pops,pro.nsu,pro.tnet.nets);
	rename_net("cpg4rr_c0","cpg4rr_ul",pro.tnet.nets);
	copy_net("cpg4rr",pro.pops, pro.nsu, pro.tnet.nets);
	rename_net("cpg4rr_c0","cpg4rr_dr",pro.tnet.nets);
	copy_net("cpg4rr",pro.pops, pro.nsu, pro.tnet.nets);
	rename_net("cpg4rr_c0","cpg4rr_dl",pro.tnet.nets);
	rename_net("cpg4rr","cpg4rr_ur",pro.tnet.nets);

	ptmp.clear();

	ptmp.push_back("cpg4rr_ur");
	ptmp.push_back("cpg4rr_ul");

	make_mnet("cpg4rr_up",ptmp,pro.pops,pro.tnet.nets,pro.tnet.mnets);
	conn_nets("cpg4rr_up",syntype,"cpg4rr_ur","cpg4rr_ul", "postI", "preI_c0", sprb, tprb,w,pro.tnet.nets,pro.tnet.mnets);
	conn_nets("cpg4rr_up",syntype,"cpg4rr_ul","cpg4rr_ur", "postI_c0","preI", sprb, tprb,w,pro.tnet.nets,pro.tnet.mnets);

	//make_mnet("cpg4rr_down",tmp,pops,nets,mnets);

	copy_mnet("cpg4rr_up",pro.pops,pro.nsu,pro.tnet.nets,pro.tnet.mnets);
	rename_mnet("cpg4rr_up_c0","cpg4rr_down",pro.tnet.mnets);

	create_iodata("Spikes",IOD_CELLV_SP,"ALL /v/s",pro);
	create_iodata("Vm", IOD_CELLV_V, "ALL /v/s", pro);
	//create_iodata("Vm",IOD_CELLV_V,"p[all] n[0:5] /v/s",pro);

	stringstream tmpsstrm;
	tmpsstrm <<pro.name<<".nns";
	save_nns(pro,tmpsstrm.str());
};



void post_proc(nns_project& pro) {

	for (int i=0;i<(int)pro.iodata.size();i++) {
		//changing Spikes data structure
		if (pro.iodata[i].type == 18) {

			int itn = pro.iodata[i].ntneu;
			int nstep = int( pro.ctr_para.simduration/pro.ctr_para.simstep);
			vector<float> tmpdv;
			vector<vector<float>> tmpdv2;
			float tmpf;

			for (int in=0;in<itn;in++) {
				for (int is=0;is<nstep;is++) {
					tmpf = pro.iodata[i].data[is*itn + in];
					if (tmpf==1.f) {
						tmpdv.push_back(is*(float)pro.ctr_para.simstep);
					};
				};
				tmpdv2.push_back(tmpdv);
				tmpdv.clear();
			};
			size_t iN = 0;
			for (size_t j=0;j<tmpdv2.size();j++) {
				pro.iodata[i].data[iN] = (float)tmpdv2[j].size(); //10.22.2015
				for (size_t jj = 0;jj < tmpdv2[j].size();jj++) {
					pro.iodata[i].data[iN + jj + 1] = tmpdv2[j][jj];
				};
				iN = iN + tmpdv2[j].size() + 1;
			};
			cout<<"Post Processing for data type (18:spikes): rearranged"<<endl; 
		};

		//changing Vm data structure
		if (pro.iodata[i].type == 16) {
			int itn = pro.iodata[i].ntneu + pro.iodata[i].nnsu;
			int nstep = int( pro.ctr_para.simduration/pro.ctr_para.simstep);
			vector<float> tmpdv;
			vector<vector<float>> tmpdv2;
			for (int in = 0;in<itn;in++) {
				for (int is = 0;is<nstep;is++) {
					tmpdv.push_back(pro.iodata[i].data[is*itn + in]);
				};
				tmpdv2.push_back(tmpdv);
				tmpdv.clear();
			};
			for (int j = 0;j<(int)tmpdv2.size();j++) {
				for (int jj = 0;jj<(int)tmpdv2[j].size();jj++) {
					pro.iodata[i].data[j*tmpdv2[j].size() + jj] = tmpdv2[j][jj];
				};
			};
			cout << "Post Processing for data type (16:membrane potential): rearranged" << endl;

		};
		if ((pro.iodata[i].type != 18)&&(pro.iodata[i].type != 16)) {
			int itn = pro.iodata[i].ntneu;
			int nstep = int( pro.ctr_para.simduration /pro.ctr_para.simstep);
			vector<float> tmpdv;
			vector<vector<float>> tmpdv2;
			for (int in = 0;in<itn;in++) {
				for (int is = 0;is<nstep;is++) {
					tmpdv.push_back(pro.iodata[i].data[is*itn + in]);
				};
				tmpdv2.push_back(tmpdv);
				tmpdv.clear();
			};
			for (int j = 0;j<(int)tmpdv2.size();j++) {
				for (int jj = 0;jj<(int)tmpdv2[j].size();jj++) {
					pro.iodata[i].data[j*tmpdv2[j].size() + jj] = tmpdv2[j][jj];
				};
			};
			cout << "Post Processing for data type ("<< pro.iodata[i].type <<"): rearranged" << endl;

		};
	};
};

size_t find_UOffSet(string pname, size_t nnum, nns_project& pro) {
	size_t OffSet;
	int pid = find_id(pro.pops, pname);
	if (pid == -1) {
//		cout << "ERROR::POP [" << pname << "]: Not in the population list" << endl;
		return -1;
	};
	if (nnum >= pro.pops[pid].nneurons) {
//		cout << "ERROR::Neuron ["<< nnum <<"]: Not in the range of POP [" << pname << "]" << endl;
		return -2;
	};
	size_t count = 0;

	for (size_t i=0;i < (size_t)pid; i++) {
		count = count + pro.pops[i].nneurons;
	};
/*
	// added by SM 08/17/17
	#if defined ( __CUDA__ )
		count = kdim3<cudametric>(count);
	#else
		count = kdim3<x86metric>(count);
	#endif // __CUDA__
*/
	OffSet = size_t(count + nnum);
	return OffSet;
};

size_t find_UOffSet(string uname, nns_project& pro) {
	size_t OffSet;
	int uid = find_id(pro.nsu, uname);
	if (uid == -1) {
//		cout << "ERROR::NSU [" << uname << "]: Not in the nonspike unit list" << endl;
		return -1;
	};
	size_t count = 0;
	for (size_t i = 0;i < pro.pops.size();i++) {
		count = count + pro.pops[i].nneurons;
	};

	// added by SM 08/17/17
	#if defined ( __CUDA__ )
		count = kdim3<cudametric>(count);
	#else
		count = kdim3<x86metric>(count);
	#endif // __CUDA__

	OffSet = size_t(count + uid);
	return OffSet;
};

size_t find_SOffSet(string pname, size_t nnum, size_t synType, nns_project& pro) {
	size_t OffSet;
	int pid = find_id(pro.pops, pname);
	if (pid == -1) {
//		cout << "ERROR::POP [" << pname << "]: Not in the population list" << endl;
		return -1;
	};
	if (nnum >= pro.pops[pid].nneurons) {
//		cout << "ERROR::Neuron [" << nnum << "]: Out of range of POP [" << pname << "]" << endl;
		return -1;
	};
	size_t count = 0;

	for (size_t i=0;i < (size_t)pid; i++) {
		count = count + pro.pops[i].nneurons*pro.pops[i].paraset.ichs.nIChs;
	};

	OffSet = size_t(count + nnum*pro.pops[pid].paraset.ichs.nIChs + synType);

	return OffSet;
};

size_t find_SOffSet(string uname, size_t synType, nns_project& pro) {
	size_t OffSet;
	size_t nsynp = 4;
	
	int uid = find_id(pro.nsu, uname);
	if (uid == -1) {
//		cout << "ERROR::NSU [" << uname << "]: Not in the nonspike unit list" << endl;
		return -1;
	};
	if (uid < pro.nsu.ndrives) {
//		cout << "ERROR::NSU Drive [" << uname << "]: Cannot have a synapse" << endl;
		return -1;
	};

	size_t count = 0;

	for (size_t i=0;i < pro.pops.size(); i++) {
		count = count + pro.pops[i].nneurons*pro.pops[i].paraset.ichs.nIChs;
	};
	//Total neurons in all pops + total drives + partial ouputs*8 + 4 + current synapse type;
	OffSet = size_t(count + pro.nsu.ndrives + (uid - pro.nsu.ndrives)*nsynp*2 + nsynp + synType);

	return OffSet;
};

size_t get_size( string name, nns_project &pro)
{
	if( find_id(pro.nsu, name) == -1){
		int id = find_id(pro.pops, name );
		if( id >= 0 ){
			return pro.pops[id].nneurons;
		}
	}
	else{
		return 1;
	}
	return -1;
}

#include "parameters.h"

ich_parameters::ich_parameters(string n,int id) {
	name = n;
	ID = id;
	init_para();
};

ich_ng_parameters::ich_ng_parameters(string n,int id) {
	name = n;
	ID = id;
	init_para();
};

ich_ab_parameters::ich_ab_parameters(string n, int id) {
	name = n;
	ID = id;
	init_para();
};

ich_z_parameters::ich_z_parameters(string n, int id) {
	name = n;
	ID = id;
	init_para();
};

ion_parameters::ion_parameters(string n,int id) {
	name = n;
	ID = id;
	init_para();
};

synp_parameters::synp_parameters(string n) {       // TK added
	name = n;
	init_para();
};

void ich_parameters::init_para() {
	MHtype = 0;
	Itype = 0;
	g.val=0;
	E.val=0;
	mpower=0;
	hpower=0;
	M0=0.02;
	H0=0.3;
	t0m = 0.;
	t0h = 0.;

	Vsm.val = 0;
	Vhfm.val=0;
	Vhfm2.val = 0;
	km.val=0;
	//km2.val = 0;
	tm.val=0;
	ktm.val=0;
	ktm2.val = 0;

	Vsh.val = 0;
	Vhfh.val=0;
	Vhfh2.val = 0;
	kh.val=0;
	//kh2.val = 0;
	th.val=0;
	kth.val=0;
	kth2.val = 0;


	//Variance in percentage
	g.var=0;
	E.var=0;

	Vsm.val = 0;
	Vhfm.var=0;
	Vhfm2.var = 0;
	km.var=0;
	//km2.var = 0;
	tm.var=0;
	ktm.var=0;
	ktm2.var = 0;

	Vsh.val = 0;
	Vhfh.var=0;
	Vhfh2.var = 0;
	kh.var=0;
	//kh2.var = 0;
	th.var=0;
	kth.var=0;
	kth2.var = 0;
};

void ich_ng_parameters::init_para() {
	MHtype = 3;
	Itype = 0;
	Tc = 0;
	Amp = 0;
	g.val=0;
	E.val=0;

	g.var=0;
	E.var=0;

};

void ich_ab_parameters::init_para() {
	MHtype = 0;
	Itype = 0;
	g.val = 0;
	E.val = 0;
	mpower = 0;
	hpower = 0;
	M0 = 0.02;
	H0 = 0.3;
	t0m = 0.;
	t0h = 0.;
	tmaxm = 1;
	tmaxh = 1;

	MAa.val = 0;
	MBa.val = 0;
	MCa.val = 0;
	MDa.val = 0;
	MEa.val = 0;

	MAb.val = 0;
	MBb.val = 0;
	MCb.val = 0;
	MDb.val = 0;
	MEb.val = 0;

	HAa.val = 0;
	HBa.val = 0;
	HCa.val = 0;
	HDa.val = 0;
	HEa.val = 0;

	HAb.val = 0;
	HBb.val = 0;
	HCb.val = 0;
	HDb.val = 0;
	HEb.val = 0;

	//Variance in percentage
	g.var = 0;
	E.var = 0;

	MAa.var = 0;
	MBa.var = 0;
	MCa.var = 0;
	MDa.var = 0;
	MEa.var = 0;

	MAb.var = 0;
	MBb.var = 0;
	MCb.var = 0;
	MDb.var = 0;
	MEb.var = 0;

	HAa.var = 0;
	HBa.var = 0;
	HCa.var = 0;
	HDa.var = 0;
	HEa.var = 0;

	HAb.var = 0;
	HBb.var = 0;
	HCb.var = 0;
	HDb.var = 0;
	HEb.var = 0;
};

void ich_z_parameters::init_para() {
	MHtype = 0;
	Itype = 0;
	g.val = 0;
	E.val = 0;
	mpower = 0;
	hpower = 0;
	M0 = 0.02;
	H0 = 0.3;
	t0 = 0.;
	tmax = 1;

	A.val = 0;
	B.val = 0;
	L.val = 0;
	G.val = 0;
	Vhf.val = 0;
	slp.val = 0;
	t0 = 0;
	tmax = 0;

	//Variance in percentage
	g.var = 0;
	E.var = 0;

	A.var = 0;
	B.var = 0;
	L.var = 0;
	G.var = 0;
	Vhf.var = 0;
	slp.var = 0;
};

void ion_parameters::init_para() {
	ionType = 0;
	pumpType = 0;
	Cin = 0.;
	Cout = 0.;
	E = 0;
	R = 8.3143*1000;
	F = 9.648*10000;
	T = 308;
	Z = 0;
	InEq = 0;
	t = 500;
	Rpmp = 1;
	Rch = 1;
	Kp = 0;
	K = 0;
	B = 0;
};

void neu_parameters::init_para() {
	neuT = 0;
	area = 0.0025;

	Cm.val = 36.2;
	Vm.val = -60;

	Cm.var = 0;
	Vm.var = 0;
};

void contr_parameters::init_para() {
	simduration = 1000;
	simstep = 0.05;
	srand_status = 0;
};

void synp_parameters::init_para() {       // added TK
	IonType = -1;
	g.val = 0;
	g.var = 0;
	E.val = 0;
	E.var = 0;
	Tc = 0;
	Amp = 0;
};

parameter_set& parameter_set::operator= (const parameter_set& paraset){
	
	neu_para = paraset.neu_para;
	//net_para = paraset.net_para;      TK
	//ctr_para = paraset.ctr_para;      TK

	ions = paraset.ions;
	ichs = paraset.ichs;
	return *this;
};

ichs_set& ichs_set::operator= (const ichs_set& ichset){
	g_para = ichset.g_para;
	ab_para = ichset.ab_para;
	z_para = ichset.z_para;	
	ng_para = ichset.ng_para;

	nIChs = ichset.nIChs;
	nIonTs = ichset.nIonTs;

	IChNList = ichset.IChNList;
	IChDSList = ichset.IChDSList;
	IChGTList = ichset.IChGTList;
	IChMHTList = ichset.IChMHTList;
	IChIdxList = ichset.IChIdxList;
	IChIonTList = ichset.IChIonTList;
	ICh2IonTmap = ichset.ICh2IonTmap;
	IonTList = ichset.IonTList;

	return *this;
};

ions_set& ions_set::operator= (const ions_set& ionset){

	ion_para = ionset.ion_para;
	nIons = ionset.nIons;
	sw_ion_dynam = ionset.sw_ion_dynam;
	//IonTList = ionset.IonTList;
	//IonNList = ionset.IonNList;

	return *this;
};

void ichs_set::init() {
	g_para.clear();
	ab_para.clear();
	z_para.clear();
	ng_para.clear();
	
	update();
};

void ions_set::init() {
	sw_ion_dynam = 0;
	ion_para.clear();
	update();
};

void ichs_set::update(){
	bool reorder = 0; //0: as it is, 1:reordering
	vector<int> tmp;
	tmp.push_back(0);
	tmp.push_back(0);
	int nG = (int)g_para.size();
	int nZ = (int)z_para.size();
	int nAB = (int)ab_para.size();
	int nNG = (int)ng_para.size();
	nIChs = nG + nAB + nZ + nNG;
	int cg=0,cab=0,cz=0,cn=0;
	IChGTList.clear();
	IChNList.clear();
	IChMHTList.clear();
	IChIonTList.clear();
	IChIdxList.clear();
	IonTList.clear();
	ICh2IonTmap.clear();	
	IChDSList.clear();

	
	if (reorder == 1) {
		//This is for reordering channels
		bool setflg = 0;
		for (int i = 0;i < nIChs;i++)
		{
			if (nG != 0 && cg < nG) {
				IChNList.push_back(g_para.at(cg).name);
				IChGTList.push_back(g_para.at(cg).Gtype);
				IChMHTList.push_back(g_para.at(cg).MHtype);
				IChIonTList.push_back(g_para.at(cg).Itype);
				IChDSList.push_back(g_para.at(cg).Dstatus);
				tmp[0] = 0, tmp[1] = cg;
				IChIdxList.push_back(tmp); cg++;
				setflg = 1;
			};
			if (nAB != 0 && cab < nAB && setflg == 0) {
				IChNList.push_back(ab_para.at(cab).name);
				IChGTList.push_back(ab_para.at(cab).Gtype);
				IChMHTList.push_back(ab_para.at(cab).MHtype);
				IChIonTList.push_back(ab_para.at(cab).Itype);
				IChDSList.push_back(ab_para.at(cab).Dstatus);
				tmp[0] = 1, tmp[1] = cab;
				IChIdxList.push_back(tmp); cab++;
				setflg = 1;
			};
			if (nZ != 0 && cz < nZ && setflg == 0) {
				IChNList.push_back(z_para.at(cz).name);
				IChGTList.push_back(z_para.at(cz).Gtype);
				IChMHTList.push_back(z_para.at(cz).MHtype);
				IChIonTList.push_back(z_para.at(cz).Itype);
				IChDSList.push_back(z_para.at(cz).Dstatus);
				tmp[0] = 2, tmp[1] = cz;
				IChIdxList.push_back(tmp); cz++;
				setflg = 1;
			};
			if (nNG != 0 && cn < nNG && setflg == 0) {
				IChNList.push_back(ng_para.at(cn).name);
				IChGTList.push_back(ng_para.at(cn).Gtype);
				IChMHTList.push_back(ng_para.at(cn).MHtype);
				IChIonTList.push_back(ng_para.at(cn).Itype);
				IChDSList.push_back(ng_para.at(cn).Dstatus);
				tmp[0] = 3, tmp[1] = cn;
				IChIdxList.push_back(tmp); cn++;
				setflg = 1;
			};
			setflg = 0;
		};
	}
	else {

		for (int i = 0;i<nIChs;i++)
		{
			if (nG != 0 && cg<nG) {
				if (g_para.at(cg).ID == i)
				{
					IChNList.push_back(g_para.at(cg).name);
					IChGTList.push_back(g_para.at(cg).Gtype);
					IChMHTList.push_back(g_para.at(cg).MHtype);
					IChIonTList.push_back(g_para.at(cg).Itype);
					IChDSList.push_back(g_para.at(cg).Dstatus);
					tmp[0] = 0, tmp[1] = cg;
					IChIdxList.push_back(tmp); cg++;
				};
			};
			if (nAB != 0 && cab<nAB) {
				if (ab_para.at(cab).ID == i)
				{
					IChNList.push_back(ab_para.at(cab).name);
					IChGTList.push_back(ab_para.at(cab).Gtype);
					IChMHTList.push_back(ab_para.at(cab).MHtype);
					IChIonTList.push_back(ab_para.at(cab).Itype);
					IChDSList.push_back(ab_para.at(cab).Dstatus);
					tmp[0] = 1, tmp[1] = cab;
					IChIdxList.push_back(tmp); cab++;
				};
			};
			if (nZ != 0 && cz<nZ) {
				if (z_para.at(cz).ID == i)
				{
					IChNList.push_back(z_para.at(cz).name);
					IChGTList.push_back(z_para.at(cz).Gtype);
					IChMHTList.push_back(z_para.at(cz).MHtype);
					IChIonTList.push_back(z_para.at(cz).Itype);
					IChDSList.push_back(z_para.at(cz).Dstatus);
					tmp[0] = 2, tmp[1] = cz;
					IChIdxList.push_back(tmp); cz++;
				};
			};
			if (nNG != 0 && cn<nNG) {
				if (ng_para.at(cn).ID == i)
				{
					IChNList.push_back(ng_para.at(cn).name);
					IChGTList.push_back(ng_para.at(cn).Gtype);
					IChMHTList.push_back(ng_para.at(cn).MHtype);
					IChIonTList.push_back(ng_para.at(cn).Itype);
					IChDSList.push_back(ng_para.at(cn).Dstatus);
					tmp[0] = 3, tmp[1] = cn;
					IChIdxList.push_back(tmp); cn++;
				};
			};
		};
	};

	////10.2.2015 added...for cuda engine processing
	//ICh2IonTmap.clear();
	//IonTNList.clear();
	//size_t fd;
	//int cna=0,ix=-1;
	//string xstr;
	//for (int i=0;i<IChNList.size();i++) {
	//	xstr = "Na";
	//	fd = IChNList[i].find(xstr);
	//	if (fd != std::string::npos) {
	//		if (cna == 0) {
	//			IonTNList.push_back(xstr);
	//			cna++;
	//			ix = IonTNList.size() - 1;
	//			ICh2IonTmap.push_back(ix);
	//		}else {
	//			ICh2IonTmap.push_back(ix);
	//		};
	//	}else {
	//		IonTNList.push_back(IChNList[i]);
	//		ICh2IonTmap.push_back( IonTNList.size() - 1);
	//	};

	//};

	//10.2.2015 added...for cuda engine processing
	//10.26.2015 modified
	//ICh2IonTmap.clear();
	//IonTNList.clear();
	//size_t fd;
	//int cna = 0, cca = 0, ix = -1;
	//string tstr, xstr, xstrl;
	//for (int i = 0;i<IChNList.size();i++) {		
	//	tstr = IChNList[i];
	//	transform(tstr.begin(), tstr.end(), tstr.begin(), ::tolower); //converting all chars to lowercase
	//	xstr = xstrl = "Na";
	//	transform(xstrl.begin(), xstrl.end(), xstrl.begin(), ::tolower);
	//	fd = tstr.find(xstrl);
	//	if (fd != std::string::npos) {
	//		if (cna == 0) {
	//			IonTNList.push_back(xstr);
	//			cna++;
	//			ix = IonTNList.size() - 1;
	//			ICh2IonTmap.push_back(ix);
	//		}
	//		else {
	//			ICh2IonTmap.push_back(ix);
	//		};
	//	}
	//	else {
	//		xstr = xstrl = "Ca";
	//		transform(xstrl.begin(), xstrl.end(), xstrl.begin(), ::tolower);
	//		fd = tstr.find(xstrl);
	//		if (fd != std::string::npos) {
	//			if (cca == 0) {
	//				IonTNList.push_back(xstr);
	//				cca++;
	//				ix = IonTNList.size() - 1;
	//				ICh2IonTmap.push_back(ix);
	//			}
	//			else {
	//				ICh2IonTmap.push_back(ix);
	//			};
	//		}
	//		else {
	//			IonTNList.push_back(IChNList[i]);
	//			ICh2IonTmap.push_back(IonTNList.size() - 1);
	//		};
	//		
	//	};
	//
	//};

	//nIonTs = IonTNList.size();

	//10.29.2015 added because the structure has changed
	int tmpc = 0, tmpidx;
	for (int ic = 0;ic < (int)IChIonTList.size();ic++) {
		if (ic == 0) {
			IonTList.push_back(IChIonTList[ic]);
			ICh2IonTmap.push_back(0);
		}
		else {
			tmpc = 0;
			for (int j = 0;j < (int)IonTList.size();j++) {
				//if (IonTList[j] == IChIonTList[ic]) {
				if (IonTList[j] == IChIonTList[ic] && IChIonTList[ic] != 0) { //Ion type should not be non specific ion type and in the list
					tmpc = 1;
					tmpidx = j;
					break;
				};
			};
			if (tmpc == 0) {
				IonTList.push_back(IChIonTList[ic]);
				ICh2IonTmap.push_back((int)IonTList.size()-1);
			}
			else {
				ICh2IonTmap.push_back(tmpidx);
			};
		};

	};

	nIonTs = (int)IonTList.size();
	
};

void ions_set::update() {
	nIons = (int)ion_para.size();
};


void parameter_set::init(){
	neu_para.init_para();
	ions.init();
	ichs.init();
};

#include "nnonspikeunits.h"
#include "npopulations.h"


nns_nonspikeunit& nns_nonspikeunit::operator= (const nns_nonspikeunit& nsu) {

	units = nsu.units;

	return *this;
};

void nns_nonspikeunit::update() {
	size_t nu = units.size();
	int cd = 0, co = 0;
	for (size_t i = 0;i < nu;i++) {
		if (units[i].type == NSU_DRIVE) cd++;
		else if (units[i].type == NSU_OUTPUT) co++;
	};
	ndrives = cd;
	noutputs = co;
};

void nns_nonspikeunit::reset() {
	units.clear();
};

void create_nsuc(nns_nonspikeunit& nsu, string name, int type) {
	neural_unit unit;
//	int idx;
	vector<int> idv, idv2;
	string tmpstr;

	unit.type = type;
	if (nsu.units.empty()) {
		unit.name = name;
		nsu.units.push_back(unit);
	}
	else {
		idv = find_ids(nsu.units, name);
		if (idv.size() == 0) {
			unit.name = name;			
		}
		else {
			tmpstr = name + "_c";
			idv2 = find_ids(nsu.units, tmpstr);
			if (idv2.size() == 0) {
				unit.name = tmpstr + to_string(( long long )0);
			}
			else {
				unit.name = tmpstr + to_string(( long long )idv2.size());
			};
		};
		nsu.units.push_back(unit);			
	};
	size_t nunit = nsu.units.size()-1;
	if (type == NSU_DRIVE)
		create_ichc(nsu.units[nunit].ichs, "NSUSynp", 0, 3, 0, 0);
	else if (type == NSU_OUTPUT) {
		create_ichc(nsu.units[nunit].ichs, "SynpEx1", 13, 1, 0, 0);
		create_ichc(nsu.units[nunit].ichs, "SynpEx2", 13, 1, 0, 0);
		create_ichc(nsu.units[nunit].ichs, "SynpIn1", 13, 1, 0, 0);
		create_ichc(nsu.units[nunit].ichs, "SynpIn2", 13, 1, 0, 0);

		//Added 12.21.2015
		create_ichc(nsu.units[nunit].ichs, "SynpEx1", 13, 1, 0, 0);
		create_ichc(nsu.units[nunit].ichs, "SynpEx2", 13, 1, 0, 0);
		create_ichc(nsu.units[nunit].ichs, "SynpIn1", 13, 1, 0, 0);
		create_ichc(nsu.units[nunit].ichs, "SynpIn2", 13, 1, 0, 0);
	};
	nsu.update();
};

void copy_nsuc(nns_nonspikeunit& nsu, string name) {
	size_t nunit = nsu.units.size();
	int did = find_id(nsu.units, name);
	if (did != -1) {
		create_nsuc(nsu, name, NSU_DRIVE);
		nsu.units[nunit] = nsu.units[did];
	}
	else {
		cout << "ERROR::NSUNIT [" << name << "]: Not found in the nsu list" << endl;
	};
};

void rename_nsuc(nns_nonspikeunit& nsu, string oldname, string newname) {
	int idx;

	idx = find_id(nsu.units, newname);
	if (idx != -1) {
		cout << "ERROR::NSUNIT [" << newname << "]: already exists" << endl;
		return;
	};
	idx = find_id(nsu.units, oldname);
	if (idx != -1) {
		nsu.units[idx].name = newname;
		cout << "NSUNIT [" << oldname << "]: renamed as " << "[" << newname << "]" << endl;
	}
	else {
		cout << "ERROR::NSUNIT [" << oldname << "]: not found" << endl;
	};
};

void delete_nsuc(nns_nonspikeunit& nsu, string name) {
	int idx;

	idx = find_id(nsu.units, name);
	if (idx != -1) {
		nsu.units.erase(nsu.units.begin() + idx);
		cout << "NSUNIT [" << name << "]: deleted" << endl;
	}
	else {
		cout << "ERROR::NSUNIT [" << name << "]: not found" << endl;
	};

};

//int find_id(vector<neural_drive>& drives, string name) {
//	int nu = drives.size(), uid = -1;
//	for (int i = 0;i < nu;i++) {
//		if (drives[i].name == name) {
//			uid = i;
//			break;
//		};
//	};
//	return uid;
//};
//
//int find_id(vector<neural_output>& outs, string name) {
//	int nu = outs.size(), uid = -1;
//	for (int i = 0;i < nu;i++) {
//		if (outs[i].name == name) {
//			uid = i;
//			break;
//		};
//	};
//	return uid;
//};

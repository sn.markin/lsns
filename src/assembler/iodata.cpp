#include "iodata.h"
#include "datafiles.h"
#include "config.h"

nns_iodata::nns_iodata(const nns_iodata& iod) // modify by SM 01/05.2016
{
	view = iod.view;
	save = iod.save;
	name = iod.name;
	cmdline = iod.cmdline;
	ID = iod.ID;

	type = iod.type;
	npop = iod.npop;
	ntneu = iod.ntneu;
	nnsu = iod.nnsu;
	nneu = iod.nneu;
	popList = iod.popList;
	neuList = iod.neuList;
	nsuList = iod.nsuList;
	ich = iod.ich;
	data = iod.data;
}

nns_iodata& nns_iodata::operator= (const nns_iodata& adata) {
	type = adata.type;
	npop = adata.npop;
	ntneu = adata.ntneu;
	nneu = adata.nneu;
	popList = adata.popList;
	neuList = adata.neuList;
	nsuList = adata.nsuList;
	ich = adata.ich;
	data = adata.data;

	return *this;
};

void nns_iodata::init() {
	name = "";
	cmdline = "";
	view = 0;
	save = 0;
	type = -1;
	ich = -1;
	ID = -1;
	ntneu = 0;
	npop = 0;
	nnsu = 0;
	data = NULL;	// modify by SM 01/05.2016
};

void nns_iodata::clear() {
	view = 0;
	save = 0;
	type = -1;
	popList.clear();
	neuList.clear();
	nsuList.clear();
	nneu.clear();
	
	npop = 0;
	ntneu = 0;
	ich = -1;
#if !defined( __DATA_OUT__ )
	if( data ){	// modify by SM 01/05.2016
		delete[] data;
		data = NULL;
	}
#endif /*__DATA_OUT__*/
};

void nns_iodata::meminit(int steps) {
	int a = 0;
	//int s1=0;
	//int sn,en;
	//int cp=0,cn=0,cc=0;

	//if (type >= 0 && type < 8) {
	//	data = new float[ntneu*steps];

	//}else if (type >= 8 && type < 16) {
	//	data = new float[ntneu*steps];

	//}else if (type >= 16 && type < 20) {
	//	data = new float[ntneu*steps];

	//};
#if !defined( __DATA_OUT__ )
	if (type == 16) {
		data = new float[(ntneu + nnsu)*steps];
	}
	else {
		data = new float[ntneu*steps];
	};
#endif /*__DATA_OUT__*/
};

bool checkrng(vector<neural_population>& pops, int in) {

	if (in < 0 || in >= (int)pops.size()) {
		cout << "ERROR::IOData Setting: pop number (" << in << ") is out of range [0:" << pops.size() << "]" << endl;
		return 0;
	};

	return 1;
};

bool checkrng(neural_population& pop, int in) {

	if (in < 0 || in >= (int)pop.nneurons) {
		cout << "ERROR::IOData Setting: neu number (" << in << ") is out of range [0:" << pop.nneurons << "]" << endl;
		return 0;
	};
	return 1;
};

bool checkrng(nns_nonspikeunit& nsu, int in) {

	if (in < 0 || in >= (int)nsu.units.size()) {
		cout << "ERROR::IOData Setting: nsu number (" << in << ") is out of range [0:" << nsu.units.size()-1 << "]" << endl;
		return 0;
	};
	return 1;
};

void create_iodata(string name, int type, string strline, vector<nns_iodata>& iodata, vector<neural_population>& pops, nns_nonspikeunit& nsu, contr_parameters& ctr_para) {
	if (type < 0 || type > 19) {
		cout << "ERROR::IOData Type (" << type << "): Out of type range 0 - 19" << endl;
		return;
	};
	int res;
	nns_iodata iod;// = new nns_iodata;
	iod.name = name;
	iod.ID = (int)iodata.size() + 1;
	iod.type = type;

	res = parse_cmdline(&iod, strline, pops, nsu);

	if (res != 1) {
		cout << "ERROR::IOData (" << strline << "): Wrong command line" << endl;
		return;
	};
	iod.cmdline = strline;
	iodata.push_back(iod);
	iodata[iodata.size()-1].meminit(int(ctr_para.simduration / ctr_para.simstep));
	cout << "IOData " << name << "(Type " << type << "): Added to the iodata set" << endl;
};

void delete_iodata(vector<nns_iodata>& iodata) {

	for (int i = 0;i<(int)iodata.size();i++) {
		iodata[i].clear(); //for deleting memory by new command
	};
	iodata.clear();
};

int parse_cmdline(nns_iodata* iod, string cmd, vector<neural_population>& pops, nns_nonspikeunit& nsu) {

	//Ex: p[1:5,7] n[1:50,53,60:70] c[1] /v /s

	bool chkres;
	string key, tmpstr, tmpstr2;
	int idf[3] = { -1,-1,-1 }; //front [ positions
	int idr[3] = { -1,-1,-1 }; //rear ] positions
	int ids[2] = { -1,-1 }; // slash / positions
	//int idcm[MAX_COMMA] = {-1};
	vector<int> idcm;
	int idcl = -1;
	int cfbrack = 0, crbrack = 0, cslash = 0, ccomma = 0, ccolon = 0, cc = 0, cnn = 0;
	int pos = 0, nn = 0, sn = 0, en = 0;
	bool allflg = 0, allpop = 0, allneu = 0, allch = 0;
	vector<int> pb;
	vector<vector<int>> pb2;
	vector<vector<int>> pb3;
	pb.push_back(-1);
	pb.push_back(-1);
	pb2.push_back(pb);


	size_t fd;

	cmd = rmspaces(cmd); //Remove all spaces in the command string line

	key = "[";
	for (int i = 0;i<3;i++) {
		fd = cmd.find(key, pos);
		if (fd != std::string::npos) {
			cfbrack++;
			idf[i] = (int)fd;
			pos = (int)fd + 1;
		};
	};
	pos = 0;
	key = "]";
	for (int i = 0;i<3;i++) {
		fd = cmd.find(key, pos);
		if (fd != std::string::npos) {
			crbrack++;
			idr[i] = (int)fd;
			pos = (int)fd + 1;
		};
	};
	pos = 0;
	key = "/";
	for (int i = 0;i<2;i++) {
		fd = cmd.find(key, pos);
		if (fd != std::string::npos) {
			cslash++;
			ids[i] = (int)fd;
			pos = (int)fd + 1;
		};
	};

	if (cfbrack == 0 || cfbrack == 1) {
		key = "ALL";
		fd = cmd.find(key);
		if (fd != std::string::npos)
			allflg = 1;
		else
			allflg = 0;
		if (cfbrack == 0 && allflg == 0) {
			cout << "ERROR::IOData : requires more numbers" << endl;
			return 0;
		};
	};

	//Type 0~7
	if (iod->type < 8) {
		int cn = 0;
		if (allflg == 1) {
			if (pops.size() != 1) {
				iod->popList.push_back(pb);
				iod->popList[0][0] = 0;
				iod->popList[0][1] = (int)pops.size() - 1;
				iod->npop = (int)pops.size();
			}
			else {
				iod->popList.push_back(pb);
				iod->popList[0][0] = 0;
				iod->popList[0][1] = -1;
				iod->npop = 1;
			};
			for (int ip = 0;ip<(int)pops.size();ip++) {
				if (pops[ip].nneurons != 1) {
					iod->neuList.push_back(iod->popList);
					iod->neuList[ip][0][0] = 0;
					iod->neuList[ip][0][1] = pops[ip].nneurons - 1;
					iod->nneu.push_back(pops[ip].nneurons);
				}
				else {
					iod->neuList.push_back(iod->popList);
					iod->neuList[ip][0][0] = 0;
					iod->neuList[ip][0][1] = -1;
					iod->nneu.push_back(1);
				};
			};

			cc = 0;
			if (idf[cc] == -1) {
				cout << "ERROR::IOData Type " << iod->type << ": requires the number for a current channel (c[x])" << endl;
				return 0;
			}
			else {
				if (cmd.at(idf[cc] - 1) == 'c') {
					tmpstr = cmd.substr(idf[cc] + 1, idr[cc] - idf[cc] - 1);
					iod->ich = stoi(tmpstr);
				}
				else {
					cout << "ERROR::IOData Type " << iod->type << ": requires the number for a current channel (c[x])" << endl;
					return 0;
				};
			};
		}
		else {
			//==============Make population list================================
			cc = 0; //Population List
			if (cmd.at(idf[cc] - 1) == 'p') {
				tmpstr = cmd.substr(idf[cc] + 1, idr[cc] - idf[cc] - 1);
				key = "all";
				fd = tmpstr.find(key);
				if (fd != std::string::npos) {
					iod->popList.push_back(pb);
					iod->popList[0][0] = 0;
					iod->popList[0][1] = (int)pops.size() - 1;
					iod->npop = (int)pops.size();
				}
				else {
					//Chechk if there is a comma. If there is, count the number
					nn = 0; ccomma = 0;
					for (int i = 0;i < (int)tmpstr.length();i++) {
						if (tmpstr.at(i) == ',') {
							//idcm[ccomma] = i;
							idcm.push_back(i);
							ccomma++;
						};
					};
					nn = ccomma + 1;
					cnn = 0;
					for (int id = 0;id < nn;id++) {

						if (nn == 1) { //No comma
							key = ":";
							fd = tmpstr.find(key);
							if (fd != std::string::npos) { //Checking if there is a colon
								sn = stoi(tmpstr.substr(0, fd));
								chkres = checkrng(pops, sn);
								if (chkres == 0) return 0;
								en = stoi(tmpstr.substr(fd + 1, tmpstr.length() - fd - 1));
								chkres = checkrng(pops, en);
								if (chkres == 0) return 0;
								iod->popList.push_back(pb);
								iod->popList[cnn][0] = sn;
								iod->popList[cnn][1] = en;
								cnn++;
							}
							else { //case for that there is no colon. pop[][0] = value, pop[][1] = -1
								iod->popList.push_back(pb);
								sn = stoi(tmpstr);
								chkres = checkrng(pops, sn);
								if (chkres == 0) return 0;
								iod->popList[cnn][0] = sn;
								iod->popList[cnn][1] = -1;
								cnn++;
							};
						}
						else { //With commas

							if (id == 0) {
								tmpstr2 = tmpstr.substr(0, idcm[id]);
							}
							else if (id == nn - 1) {
								tmpstr2 = tmpstr.substr(idcm[id - 1] + 1, tmpstr.length() - idcm[id - 1] - 1);
							}
							else {
								tmpstr2 = tmpstr.substr(idcm[id - 1] + 1, idcm[id] - idcm[id - 1] - 1);
							};

							key = ":";
							fd = tmpstr2.find(key);
							if (fd != std::string::npos) {
								sn = stoi(tmpstr2.substr(0, fd));
								chkres = checkrng(pops, sn);
								if (chkres == 0) return 0;
								en = stoi(tmpstr2.substr(fd + 1, tmpstr2.length() - fd - 1));
								chkres = checkrng(pops, en);
								if (chkres == 0) return 0;
								iod->popList.push_back(pb);
								iod->popList[cnn][0] = sn;
								iod->popList[cnn][1] = en;
								cnn++;
							}
							else {
								iod->popList.push_back(pb);
								sn = stoi(tmpstr2);
								chkres = checkrng(pops, sn);
								if (chkres == 0) return 0;
								iod->popList[cnn][0] = sn;
								iod->popList[cnn][1] = -1;
								cnn++;
							};


						};

					};
					iod->npop = cnn - 1;
				};
			}
			else {
				cout << "ERROR::IOData Type " << iod->type << " : requires a population list (p[x])" << endl;
				return 0;
			};

			//==============Make Neuron list================================
			cc = 1; //Neuron List
			if (cmd.at(idf[cc] - 1) == 'n') {
				//Find the number of pop
				int cp = 0;
				for (int icol = 0;icol < (int)iod->popList.size();icol++) {
					if (iod->popList[icol][1] == -1) {
						cp++;
					}
					else {
						cp = cp + iod->popList[icol][1] - iod->popList[icol][0] + 1;
					};
				};

				tmpstr = cmd.substr(idf[cc] + 1, idr[cc] - idf[cc] - 1);
				key = "all";
				fd = tmpstr.find(key);
				if (fd != std::string::npos) {
					for (int ip = 0;ip < cp;ip++) {
						iod->neuList.push_back(pb2);
						iod->neuList[ip][0][0] = 0;
						iod->neuList[ip][0][1] = pops[ip].nneurons - 1;
						iod->nneu.push_back(pops[ip].nneurons);
					};
				}
				else {
					nn = 0; ccomma = 0; idcm.clear();
					for (int i = 0;i < (int)tmpstr.length();i++) {
						if (tmpstr.at(i) == ',') {
							//idcm[ccomma] = i;
							idcm.push_back(i);
							ccomma++;
						};
					};
					nn = ccomma + 1;
					cnn = 0;
					for (int id = 0;id < nn;id++) {

						if (nn == 1) { //No comma
							key = ":";
							fd = tmpstr.find(key);
							if (fd != std::string::npos) {
								sn = stoi(tmpstr.substr(0, fd));
								en = stoi(tmpstr.substr(fd + 1, tmpstr.length() - fd - 1));
								for (int ip = 0;ip < cp;ip++) {
									chkres = checkrng(pops[ip], sn);
									if (chkres == 0) return 0;
									chkres = checkrng(pops[ip], en);
									if (chkres == 0) return 0;
									iod->neuList.push_back(pb2);
									iod->neuList[ip][0][0] = sn;
									iod->neuList[ip][0][1] = en;
									iod->nneu.push_back(en - sn + 1);
								};
							}
							else {
								for (int ip = 0;ip < cp;ip++) {
									sn = stoi(tmpstr);
									chkres = checkrng(pops[ip], sn);
									if (chkres == 0) return 0;
									iod->neuList.push_back(pb2);
									iod->neuList[ip][0][0] = sn;
									iod->neuList[ip][0][1] = -1;
									iod->nneu.push_back(1);
								};
							};

						}
						else { //With commas

							if (id == 0) {

								//Multidimensional vector initiation for different size based on neuron list input
								pb3.clear();
								for (int j = 0;j < nn;j++) {
									pb3.push_back(pb);
								};
								for (int ip = 0;ip < cp;ip++) {
									iod->neuList.push_back(pb3);
								};

								tmpstr2 = tmpstr.substr(0, idcm[id]);
							}
							else if (id == nn - 1) {
								tmpstr2 = tmpstr.substr(idcm[id - 1] + 1, tmpstr.length() - idcm[id - 1] - 1);
							}
							else {
								tmpstr2 = tmpstr.substr(idcm[id - 1] + 1, idcm[id] - idcm[id - 1] - 1);
							};

							key = ":";
							fd = tmpstr2.find(key);
							for (int ip = 0;ip < cp;ip++) {
								if (fd != std::string::npos) {
									sn = stoi(tmpstr2.substr(0, fd));
									chkres = checkrng(pops[ip], sn);
									if (chkres == 0) return 0;
									en = stoi(tmpstr2.substr(fd + 1, tmpstr2.length() - fd - 1));
									chkres = checkrng(pops[ip], en);
									if (chkres == 0) return 0;
									//iod->neuList.push_back(pb3);
									iod->neuList[ip][id][0] = sn;
									iod->neuList[ip][id][1] = en;
									iod->nneu.push_back(en - sn + 1); //12.23.2015
									cnn = cnn + en - sn + 1;
								}
								else {
									sn = stoi(tmpstr2);
									chkres = checkrng(pops[ip], sn);
									if (chkres == 0) return 0;
									//iod->neuList.push_back(pb3);
									iod->neuList[ip][id][0] = sn;
									iod->neuList[ip][id][1] = -1;
									iod->nneu.push_back(1); //12.23.2015
									cnn++;
								};
								//iod->nneu.push_back(cnn);
							};

						};

					};

				};
			}
			else {
				cout << "ERROR::IOData Type " << iod->type << " : requires a neuron list (n[x])" << endl;
				return 0;
			};

			//==============Make channel selection================================
			cc = 2;
			if (cmd.at(idf[cc] - 1) == 'c') {
				tmpstr = cmd.substr(idf[cc] + 1, idr[cc] - idf[cc] - 1);
				iod->ich = stoi(tmpstr);
			}
			else {
				cout << "ERROR::IOData Type " << iod->type << ": requires the number for a current channel (c[x])" << endl;
				return 0;
			};
		};

	}
	else if (iod->type < 16) {
		int cn = 0;
		if (allflg == 1) {
			if (pops.size() != 1) {
				iod->popList.push_back(pb);
				iod->popList[0][0] = 0;
				iod->popList[0][1] = (int)pops.size() - 1;
				iod->npop = (int)pops.size();
			}
			else {
				iod->popList.push_back(pb);
				iod->popList[0][0] = 0;
				iod->popList[0][1] = -1;
				iod->npop = 1;
			};
			for (int ip = 0;ip < (int)pops.size();ip++) {
				if (pops[ip].nneurons != 1) {
					iod->neuList.push_back(iod->popList);
					iod->neuList[ip][0][0] = 0;
					iod->neuList[ip][0][1] = pops[ip].nneurons - 1;
					iod->nneu.push_back(pops[ip].nneurons);
				}
				else {
					iod->neuList.push_back(iod->popList);
					iod->neuList[ip][0][0] = 0;
					iod->neuList[ip][0][1] = -1;
					iod->nneu.push_back(1);
				};
			};

			cc = 0;
			if (idf[cc] == -1) {
				cout << "ERROR::IOData Type " << iod->type << ": requires the number for a current channel (c[x])" << endl;
				return 0;
			}
			else {
				if (cmd.at(idf[cc] - 1) == 'c') {
					tmpstr = cmd.substr(idf[cc] + 1, idr[cc] - idf[cc] - 1);
					iod->ich = stoi(tmpstr);
				}
				else {
					cout << "ERROR::IOData Type " << iod->type << ": requires the number for a current channel (c[x])" << endl;
					return 0;
				};
			};
		}
		else {
			//==============Make population list================================
			cc = 0; //Population List
			if (cmd.at(idf[cc] - 1) == 'p') {
				tmpstr = cmd.substr(idf[cc] + 1, idr[cc] - idf[cc] - 1);
				key = "all";
				fd = tmpstr.find(key);
				if (fd != std::string::npos) {
					iod->popList.push_back(pb);
					iod->popList[0][0] = 0;
					iod->popList[0][1] = (int)pops.size() - 1;
					iod->npop = (int)pops.size();
				}
				else {
					//Chechk if there is a comma. If there is, count the number
					nn = 0; ccomma = 0;
					for (int i = 0;i < (int)tmpstr.length();i++) {
						if (tmpstr.at(i) == ',') {
							//idcm[ccomma] = i;
							idcm.push_back(i);
							ccomma++;
						};
					};
					nn = ccomma + 1;
					cnn = 0;
					for (int id = 0;id < nn;id++) {

						if (nn == 1) { //No comma
							key = ":";
							fd = tmpstr.find(key);
							if (fd != std::string::npos) { //Checking if there is a colon
								sn = stoi(tmpstr.substr(0, fd));
								chkres = checkrng(pops, sn);
								if (chkres == 0) return 0;
								en = stoi(tmpstr.substr(fd + 1, tmpstr.length() - fd - 1));
								chkres = checkrng(pops, en);
								if (chkres == 0) return 0;
								iod->popList.push_back(pb);
								iod->popList[cnn][0] = sn;
								iod->popList[cnn][1] = en;
								cnn = cnn + en - sn + 1;
							}
							else { //case for that there is no colon. pop[][0] = value, pop[][1] = -1
								sn = stoi(tmpstr);
								chkres = checkrng(pops, sn);
								if (chkres == 0) return 0;
								iod->popList.push_back(pb);
								iod->popList[cnn][0] = sn;
								iod->popList[cnn][1] = -1;
								cnn++;
							};
						}
						else { //With commas

							if (id == 0) {
								tmpstr2 = tmpstr.substr(0, idcm[id]);
							}
							else if (id == nn - 1) {
								tmpstr2 = tmpstr.substr(idcm[id - 1] + 1, tmpstr.length() - idcm[id - 1] - 1);
							}
							else {
								tmpstr2 = tmpstr.substr(idcm[id - 1] + 1, idcm[id] - idcm[id - 1] - 1);
							};

							key = ":";
							fd = tmpstr2.find(key);
							if (fd != std::string::npos) {
								sn = stoi(tmpstr2.substr(0, fd));
								chkres = checkrng(pops, sn);
								if (chkres == 0) return 0;
								en = stoi(tmpstr2.substr(fd + 1, tmpstr2.length() - fd - 1));
								chkres = checkrng(pops, en);
								if (chkres == 0) return 0;
								iod->popList.push_back(pb);
								iod->popList[cnn][0] = sn;
								iod->popList[cnn][1] = en;
								cnn = cnn + en - sn + 1;
							}
							else {
								sn = stoi(tmpstr2);
								iod->popList.push_back(pb);
								chkres = checkrng(pops, sn);
								if (chkres == 0) return 0;
								iod->popList[cnn][0] = sn;
								iod->popList[cnn][1] = -1;
								cnn++;
							};


						};

					};

					iod->npop = cnn;

				};
			}
			else {
				cout << "ERROR::IOData Type " << iod->type << " : requires a population list (p[x])" << endl;
				return 0;
			};

			//==============Make Neuron list================================
			cc = 1; //Neuron List
			if (cmd.at(idf[cc] - 1) == 'n') {
				//Find the number of pop
				int cp = 0;
				for (int icol = 0;icol < (int)iod->popList.size();icol++) {
					if (iod->popList[icol][1] == -1) {
						cp++;
					}
					else {
						cp = cp + iod->popList[icol][1] - iod->popList[icol][0] + 1;
					};
				};

				tmpstr = cmd.substr(idf[cc] + 1, idr[cc] - idf[cc] - 1);
				key = "all";
				fd = tmpstr.find(key);
				if (fd != std::string::npos) {
					for (int ip = 0;ip < cp;ip++) {
						iod->neuList.push_back(pb2);
						iod->neuList[ip][0][0] = 0;
						iod->neuList[ip][0][1] = pops[ip].nneurons - 1;
						iod->nneu.push_back(pops[ip].nneurons);
					};
				}
				else {
					nn = 0; ccomma = 0; idcm.clear();
					for (int i = 0;i < (int)tmpstr.length();i++) {
						if (tmpstr.at(i) == ',') {
							//idcm[ccomma] = i;
							idcm.push_back(i);
							ccomma++;
						};
					};
					nn = ccomma + 1;
					cnn = 0;
					for (int id = 0;id < nn;id++) {

						if (nn == 1) { //No comma
							key = ":";
							fd = tmpstr.find(key);
							if (fd != std::string::npos) {
								sn = stoi(tmpstr.substr(0, fd));
								en = stoi(tmpstr.substr(fd + 1, tmpstr.length() - fd - 1));
								for (int ip = 0;ip < cp;ip++) {
									chkres = checkrng(pops[ip], sn);
									if (chkres == 0) return 0;
									chkres = checkrng(pops[ip], en);
									if (chkres == 0) return 0;
									iod->neuList.push_back(pb2);
									iod->neuList[ip][0][0] = sn;
									iod->neuList[ip][0][1] = en;
									iod->nneu.push_back(en - sn + 1);
									//cnn = en - sn + 1;
								};
							}
							else {
								for (int ip = 0;ip < cp;ip++) {
									sn = stoi(tmpstr);
									chkres = checkrng(pops[ip], sn);
									if (chkres == 0) return 0;
									iod->neuList.push_back(pb2);
									iod->neuList[ip][0][0] = sn;
									iod->neuList[ip][0][1] = -1;
									iod->nneu.push_back(1);
									//cnn++;
								};
							};
						}
						else { //With commas

							if (id == 0) {

								//Multidimensional vector initiation for different size based on neuron list input
								pb3.clear();
								for (int j = 0;j < nn;j++) {
									pb3.push_back(pb);
								};
								for (int ip = 0;ip < cp;ip++) {
									iod->neuList.push_back(pb3);
								};

								tmpstr2 = tmpstr.substr(0, idcm[id]);
							}
							else if (id == nn - 1) {
								tmpstr2 = tmpstr.substr(idcm[id - 1] + 1, tmpstr.length() - idcm[id - 1] - 1);
							}
							else {
								tmpstr2 = tmpstr.substr(idcm[id - 1] + 1, idcm[id] - idcm[id - 1] - 1);
							};

							key = ":";
							fd = tmpstr2.find(key);
							for (int ip = 0;ip < cp;ip++) {
								if (fd != std::string::npos) {
									sn = stoi(tmpstr2.substr(0, fd));
									chkres = checkrng(pops[ip], sn);
									if (chkres == 0) return 0;
									en = stoi(tmpstr2.substr(fd + 1, tmpstr2.length() - fd - 1));
									chkres = checkrng(pops[ip], en);
									if (chkres == 0) return 0;
									//iod->neuList.push_back(pb3);
									iod->neuList[ip][id][0] = sn;
									iod->neuList[ip][id][1] = en;
									iod->nneu.push_back(en - sn + 1); //uncommented on 2.5.2016
									//cnn = cnn + en - sn + 1; //commented on 2.5.2016
								}
								else {
									sn = stoi(tmpstr2);
									chkres = checkrng(pops[ip], sn);
									if (chkres == 0) return 0;
									//iod->neuList.push_back(pb3);
									iod->neuList[ip][id][0] = sn;
									iod->neuList[ip][id][1] = -1;
									iod->nneu.push_back(1); //uncommented on 2.5.2016
									//cnn++; //commented on 2.5.2016
								};
								//iod->nneu.push_back(cnn);
							};


						};

					};
					//iod->nneu.push_back(cnn); //commented on 2.5.2016
				};
			}
			else {
				cout << "ERROR::IOData Type " << iod->type << " : requires a neuron list (n[x])" << endl;
				return 0;
			};

			//==============Make channel selection================================
			cc = 2;
			if (cmd.at(idf[cc] - 1) == 'c') {
				tmpstr = cmd.substr(idf[cc] + 1, idr[cc] - idf[cc] - 1);
				iod->ich = stoi(tmpstr);
			}
			else {
				cout << "ERROR::IOData Type " << iod->type << ": requires the number for a current channel (c[x])" << endl;
				return 0;
			};
		};
	}
	else if (iod->type <20) {
		if (allflg == 1) { //This all is for pops, neurons, channels
			if (pops.size() != 1) {
				iod->popList.push_back(pb);
				iod->popList[0][0] = 0;
				iod->popList[0][1] = (int)pops.size() - 1;
				iod->npop = (int)pops.size();
			}
			else {
				iod->popList.push_back(pb);
				iod->popList[0][0] = 0;
				iod->popList[0][1] = -1;
				iod->npop = 1;
			};
			for (int ip = 0;ip<(int)pops.size();ip++) {
				if (pops[ip].nneurons != 1) {
					iod->neuList.push_back(iod->popList);
					iod->neuList[ip][0][0] = 0;
					iod->neuList[ip][0][1] = pops[ip].nneurons - 1;
					iod->nneu.push_back(pops[ip].nneurons);
				}
				else {
					iod->neuList.push_back(iod->popList);
					iod->neuList[ip][0][0] = 0;
					iod->neuList[ip][0][1] = -1;
					iod->nneu.push_back(1);
				};
			};

			if (iod->type == 16) {
				int cu = (int)nsu.units.size();
				if (nsu.units.size() != 1) {
					iod->nsuList.push_back(pb);
					iod->nsuList[0][0] = 0;
					iod->nsuList[0][1] = cu - 1;
					iod->nnsu = cu;
				}
				else {
					iod->nsuList.push_back(pb);
					iod->nsuList[0][0] = 0;
					iod->nsuList[0][1] = -1;
					iod->nnsu = 1;
				};
			};
		}
		else { //If allflg != 1

			   //==============Make population list================================
			cc = 0; //Population List
			if (cmd.at(idf[cc] - 1) == 'p') {
				tmpstr = cmd.substr(idf[cc] + 1, idr[cc] - idf[cc] - 1);
				key = "all";
				fd = tmpstr.find(key);
				if (fd != std::string::npos) {
					iod->popList.push_back(pb);
					iod->popList[0][0] = 0;
					iod->popList[0][1] = (int)pops.size() - 1;
					iod->npop = (int)pops.size();
				}
				else {
					//Chechk if there is a comma. If there is, count the number
					nn = 0; ccomma = 0;
					for (int i = 0;i<(int)tmpstr.length();i++) {
						if (tmpstr.at(i) == ',') {
							//idcm[ccomma] = i;
							idcm.push_back(i);
							ccomma++;
						};
					};
					nn = ccomma + 1;
					cnn = 0;
					for (int id = 0;id<nn;id++) {

						if (nn == 1) { //No comma
							key = ":";
							fd = tmpstr.find(key);
							if (fd != std::string::npos) { //Checking if there is a colon
								sn = stoi(tmpstr.substr(0, fd));
								chkres = checkrng(pops, sn);
								if (chkres == 0) return 0;
								en = stoi(tmpstr.substr(fd + 1, tmpstr.length() - fd - 1));
								chkres = checkrng(pops, en);
								if (chkres == 0) return 0;
								iod->popList.push_back(pb);
								iod->popList[cnn][0] = sn;
								iod->popList[cnn][1] = en;
								cnn = cnn + en - sn + 1;
							}
							else { //case for that there is no colon. pop[][0] = value, pop[][1] = -1
								sn = stoi(tmpstr);
								chkres = checkrng(pops, sn);
								if (chkres == 0) return 0;
								iod->popList.push_back(pb);
								iod->popList[cnn][0] = sn;
								iod->popList[cnn][1] = -1;
								cnn++;
							};
						}
						else { //With commas

							if (id == 0) {
								tmpstr2 = tmpstr.substr(0, idcm[id]);
							}
							else if (id == nn - 1) {
								tmpstr2 = tmpstr.substr(idcm[id - 1] + 1, tmpstr.length() - idcm[id - 1] - 1);
							}
							else {
								tmpstr2 = tmpstr.substr(idcm[id - 1] + 1, idcm[id] - idcm[id - 1] - 1);
							};

							key = ":";
							fd = tmpstr2.find(key);
							if (fd != std::string::npos) {
								sn = stoi(tmpstr2.substr(0, fd));
								chkres = checkrng(pops, sn);
								if (chkres == 0) return 0;
								en = stoi(tmpstr2.substr(fd + 1, tmpstr2.length() - fd - 1));
								chkres = checkrng(pops, en);
								if (chkres == 0) return 0;
								iod->popList.push_back(pb);
								iod->popList[cnn][0] = sn;
								iod->popList[cnn][1] = en;
								cnn = cnn + en - sn + 1;
							}
							else {
								sn = stoi(tmpstr2);
								chkres = checkrng(pops, sn);
								if (chkres == 0) return 0;
								iod->popList.push_back(pb);
								iod->popList[cnn][0] = sn;
								iod->popList[cnn][1] = -1;
								cnn++;
							};


						};

					};
					iod->npop = cnn;
				};
			};

			//==============Make Neuron list================================
			cc = 1; //Neuron List
			if (cmd.at(idf[cc] - 1) == 'n') {
				//Find the number of pop
				int cp = 0;
				for (int icol = 0;icol<(int)iod->popList.size();icol++) {
					if (iod->popList[icol][1] == -1) {
						cp++;
					}
					else {
						cp = cp + iod->popList[icol][1] - iod->popList[icol][0] + 1;
					};
				};

				tmpstr = cmd.substr(idf[cc] + 1, idr[cc] - idf[cc] - 1);
				key = "all";
				fd = tmpstr.find(key);
				if (fd != std::string::npos) {
					for (int ip = 0;ip<cp;ip++) {
						iod->neuList.push_back(pb2);
						iod->neuList[ip][0][0] = 0;
						iod->neuList[ip][0][1] = pops[ip].nneurons - 1;
						iod->nneu.push_back(pops[ip].nneurons);
					};
				}
				else {
					nn = 0; ccomma = 0; idcm.clear();
					for (int i = 0;i<(int)tmpstr.length();i++) {
						if (tmpstr.at(i) == ',') {
							//idcm[ccomma] = i;
							idcm.push_back(i);
							ccomma++;
						};
					};
					nn = ccomma + 1;
					cnn = 0;
					for (int id = 0;id<nn;id++) {

						if (nn == 1) { //No comma
							key = ":";
							fd = tmpstr.find(key);
							if (fd != std::string::npos) {
								sn = stoi(tmpstr.substr(0, fd));
								en = stoi(tmpstr.substr(fd + 1, tmpstr.length() - fd - 1));
								for (int ip = 0;ip<cp;ip++) {
									chkres = checkrng(pops[ip], sn);
									if (chkres == 0) return 0;
									chkres = checkrng(pops[ip], en);
									if (chkres == 0) return 0;
									iod->neuList.push_back(pb2);
									iod->neuList[ip][0][0] = sn;
									iod->neuList[ip][0][1] = en;
									iod->nneu.push_back(en - sn + 1); //uncommented on 2.5.2016
									//cnn = en - sn + 1; //commented on 2.5.2016
								};
							}
							else {
								for (int ip = 0;ip<cp;ip++) {
									sn = stoi(tmpstr);
									chkres = checkrng(pops[ip], sn);
									if (chkres == 0) return 0;
									iod->neuList.push_back(pb2);
									iod->neuList[ip][0][0] = sn;
									iod->neuList[ip][0][1] = -1;
									iod->nneu.push_back(1); //uncommented on 2.5.2016
									//cnn++; //commented on 2.5.2016
								};
							};
						}
						else { //With commas

							if (id == 0) {

								//Multidimensional vector initiation for different size based on neuron list input
								pb3.clear();
								for (int j = 0;j<nn;j++) {
									pb3.push_back(pb);
								};
								for (int ip = 0;ip<cp;ip++) {
									iod->neuList.push_back(pb3);
								};

								tmpstr2 = tmpstr.substr(0, idcm[id]);
							}
							else if (id == nn - 1) {
								tmpstr2 = tmpstr.substr(idcm[id - 1] + 1, tmpstr.length() - idcm[id - 1] - 1);
							}
							else {
								tmpstr2 = tmpstr.substr(idcm[id - 1] + 1, idcm[id] - idcm[id - 1] - 1);
							};

							key = ":";
							fd = tmpstr2.find(key);
							for (int ip = 0;ip<cp;ip++) {
								if (fd != std::string::npos) {
									sn = stoi(tmpstr2.substr(0, fd));
									chkres = checkrng(pops[ip], sn);
									if (chkres == 0) return 0;
									en = stoi(tmpstr2.substr(fd + 1, tmpstr2.length() - fd - 1));
									chkres = checkrng(pops[ip], en);
									if (chkres == 0) return 0;
									//iod->neuList.push_back(pb3);
									iod->neuList[ip][id][0] = sn;
									iod->neuList[ip][id][1] = en;
									iod->nneu.push_back(en - sn + 1); //uncommented on 2.5.2016
									//cnn = cnn + en - sn + 1; //commented on 2.5.2016
								}
								else {
									sn = stoi(tmpstr2);
									chkres = checkrng(pops[ip], sn);
									if (chkres == 0) return 0;
									//iod->neuList.push_back(pb3);
									iod->neuList[ip][id][0] = sn;
									iod->neuList[ip][id][1] = -1;
									iod->nneu.push_back(1); //uncommented on 2.5.2016
									//cnn++; //commented on 2.5.2016
								};
								//iod->nneu.push_back(cnn);
							};


						};

					};
					//iod->nneu.push_back(cnn); //commented on 2.5.2016
				};
			};

			if (iod->type == 16) {
				//Make NSU List================================================
				cc = 2; //NSU List
				if (idf[cc] != -1) {
					if (cmd.at(idf[cc] - 1) == 'u') {
						//Find the number of nsu
						int cu = (int)nsu.units.size();

						tmpstr = cmd.substr(idf[cc] + 1, idr[cc] - idf[cc] - 1);
						key = "all";
						fd = tmpstr.find(key);
						if (fd != std::string::npos) {
							iod->nsuList.push_back(pb);
							iod->nsuList[0][0] = 0;
							iod->nsuList[0][1] = cu - 1;
							cnn = cu;
						}
						else { //Not All
							nn = 0; ccomma = 0; idcm.clear();
							for (int i = 0;i < (int)tmpstr.length();i++) {
								if (tmpstr.at(i) == ',') {
									//idcm[ccomma] = i;
									idcm.push_back(i);
									ccomma++;
								};
							};
							nn = ccomma + 1;
							cnn = 0;
							for (int id = 0;id < nn;id++) {

								if (nn == 1) { //No comma
									key = ":";
									fd = tmpstr.find(key);
									if (fd != std::string::npos) {
										sn = stoi(tmpstr.substr(0, fd));
										en = stoi(tmpstr.substr(fd + 1, tmpstr.length() - fd - 1));
										chkres = checkrng(nsu, sn);
										if (chkres == 0) return 0;
										chkres = checkrng(nsu, en);
										if (chkres == 0) return 0;
										iod->nsuList.push_back(pb);
										iod->nsuList[0][0] = sn;
										iod->nsuList[0][1] = en;
										cnn = cnn + en - sn + 1;
									}
									else {
										sn = stoi(tmpstr);
										chkres = checkrng(nsu, sn);
										if (chkres == 0) return 0;
										iod->nsuList.push_back(pb);
										iod->nsuList[0][0] = sn;
										iod->nsuList[0][1] = -1;
										cnn++;
									};
								}
								else { //With commas

									if (id == 0) {

										//Multidimensional vector initiation for different size based on neuron list input
										for (int j = 0;j < nn;j++) {
											iod->nsuList.push_back(pb);
										};
										tmpstr2 = tmpstr.substr(0, idcm[id]);
									}
									else if (id == nn - 1) {
										tmpstr2 = tmpstr.substr(idcm[id - 1] + 1, tmpstr.length() - idcm[id - 1] - 1);
									}
									else {
										tmpstr2 = tmpstr.substr(idcm[id - 1] + 1, idcm[id] - idcm[id - 1] - 1);
									};

									key = ":";
									fd = tmpstr2.find(key);
									if (fd != std::string::npos) {
										sn = stoi(tmpstr2.substr(0, fd));
										chkres = checkrng(nsu, sn);
										if (chkres == 0) return 0;
										en = stoi(tmpstr2.substr(fd + 1, tmpstr2.length() - fd - 1));
										chkres = checkrng(nsu, en);
										if (chkres == 0) return 0;
										iod->nsuList[id][0] = sn;
										iod->nsuList[id][1] = en;
										cnn = cnn + en - sn + 1;
									}
									else {
										sn = stoi(tmpstr2);
										chkres = checkrng(nsu, sn);
										if (chkres == 0) return 0;
										iod->nsuList[id][0] = sn;
										iod->nsuList[id][1] = -1;
										cnn++;
									};

								};

							};

						};
					};
					iod->nnsu = cnn;
				};
			};
			//Make NSU List end =======================================

		}; 

	};

	for (int i = 0;i<(int)iod->nneu.size();i++) {
		iod->ntneu = iod->ntneu + iod->nneu[i];
	};

	//View and Save options with slash
	if (cslash > 0) {
		for (int is = 0;is<cslash;is++) {
			if (cmd.at(ids[is] + 1) == 'v') {
				iod->view = 1;
				cout << "IOData Option (/" << cmd.at(ids[is] + 1) << "): Turned on" << endl;
			}
			else if (cmd.at(ids[is] + 1) == 's') {
				iod->save = 1;
				cout << "IOData Option (/" << cmd.at(ids[is] + 1) << "): Turned on" << endl;
			}
			else {
				cout << "ERROR::IOData Option (/" << cmd.at(ids[is] + 1) << "): Wrong character" << endl;
			};
		};
	};

	return 1;
};

#ifndef _NNONSPIKEUNITS_H
#define _NNONSPIKEUNITS_H

#include "myheader.h"
#include "parameters.h"

#define NSU_DRIVE 0
#define NSU_OUTPUT 1

struct neural_unit {
	neural_unit(void) : size(1), Tup(0), Tdown(0), Threshold(0) {}; // TK
	string name;
	int type;
	int size;

	double Tup;
	double Tdown;
	double Threshold;

	ichs_set ichs;
};

class nns_nonspikeunit {    // TK
public:
	nns_nonspikeunit(void) : ndrives(0), noutputs(0) {};
	nns_nonspikeunit& operator= (const nns_nonspikeunit&);
public:
	int ndrives;
	int noutputs;

	vector<neural_unit> units;

	void update();
	void reset();
};

extern void create_nsuc(nns_nonspikeunit&, string, int);
extern void rename_nsuc(nns_nonspikeunit&, string, string);
extern void delete_nsuc(nns_nonspikeunit&, string);
extern void copy_nsuc(nns_nonspikeunit&, string);

template <class T> int find_id(vector<T>& units, string name) {
	int uid = -1;
	for (int i = 0;i < int(units.size());i++) {        // SM
		if (units[i].name == name) {
			uid = i;
			break;
		};
	};
	return uid;
};

template <class T> vector<int> find_ids(vector<T>& units, string name) {
	vector<int> uids;
	for (int i = 0;i < int(units.size());i++) {       // SM
		size_t idx = units[i].name.find(name);
		if (idx != std::string::npos) {
			uids.push_back(i);
			break;
		};
	};
	return uids;
};

//int find_id(vector<neural_drive>&, string);
//int find_id(vector<neural_output>&, string);
//vector<int> find_ids(vector<neural_output>&, string);

#endif

#include "npopulations.h"
#include "customfuncs.h"
#include "commands.h"

neural_population::~neural_population() {

};

neural_population& neural_population::operator= (const neural_population& pop){
	nneurons = pop.nneurons;
	threshold = pop.threshold;
	paraset = pop.paraset;

	return *this;
};



void neural_population::set_paraset(const parameter_set& allpara) {
	//paraset.ctr_para = allpara.ctr_para;   TK
	//paraset.net_para = allpara.net_para;   TK
	paraset.neu_para = allpara.neu_para;

	paraset.ions = allpara.ions;	
	paraset.ichs = allpara.ichs;
	
};

void neural_population::init() {
	paraset.init();
};

void create_pop(vector<neural_population>& pops, string name, int size) {
	if (pops.size() == 0) {
		neural_population *pop;
		pop = new neural_population;
		pop->name = name;
		pop->ID = (int)pops.size() + 1;
		pop->nneurons = size;
		pops.push_back(*pop);
	}
	else {
		size_t sid = (int)name.find("_c");
		if (sid != std::string::npos) {
			name = name.substr(0, sid);
		};
		size_t c = 0;
		for (size_t i = 0;i<pops.size();i++) {
			size_t sid = pops[i].name.find(name);
			if (sid != std::string::npos) {
				size_t sid2 = pops[i].name.find("_c");
				if ((pops[i].name == name) || (sid2 != std::string::npos))
					c++;
			};
		};
		if (c != 0) {
			stringstream stmp;
			stmp << name << "_c" << (c - 1);
			name = stmp.str();
		};
		neural_population *pop;
		pop = new neural_population;
		pop->name = name;
		pop->ID = (int)pops.size() + 1;
		pop->nneurons = size;
		pops.push_back(*pop);
	};
};

void delete_pop(vector<neural_population>& pops, string name) {
	int idx = -1;
	for (unsigned int i = 0;i<pops.size();i++)
	{
		if (pops[i].name == name)
		{
			idx = i; break;
		};
	};
	if (idx != -1)
		pops.erase(pops.begin() + idx);
	else
		cout << "ERROR::Population (" << name << "): Not found" << endl;

};

void delete_pop(vector<neural_population>& pops, unsigned int no) {
	if (no <= pops.size())
		pops.erase(pops.begin() + no);
	else
		cout << "ERROR::Population ID (" << no << "): Not found" << endl;
};

void copy_pop(vector<neural_population>& pops, string name) {
	size_t np = pops.size();
	int pid = find_id(pops, name);
	if (pid != -1) {
		create_pop(pops, name, pops[pid].nneurons);
		pops[np] = pops[pid];
	}
	else {
		cout << "ERROR::Population (" << name << "): Not found in the population list" << endl;
	}
};

void copy_pop(vector<neural_population>& pops, string name, string new_name) {
	size_t np = pops.size();
	int pid = find_id(pops, name);
	if (pid != -1) {
		create_pop(pops, new_name, pops[pid].nneurons);
		pops[np] = pops[pid];
	}
	else {
		cout << "ERROR::Population (" << name << "): Not found in the population list" << endl;
	}
};

void rename_pop(vector<neural_population>& pops, string oldname, string newname) {
	int pid = find_id(pops, oldname);
	if (pid != -1) {
		int tid = find_id(pops, newname);;
		if (tid == -1) {
			pops[pid].name = newname;
			cout << "Population (" << oldname << "): renamed as (" << newname << ")" << endl;
		}
		else {
			cout << "ERROR::Population (" << newname << "): already exists in the population list" << endl;
		};
	}
	else {
		cout << "ERROR::Population (" << oldname << "): Not found in the population list" << endl;
	};
};

void create_ich(vector<neural_population>& pops, int ID, string name, int Gtype, int MHtype, int Itype) {
	create_ichc(pops[ID].paraset, name, Gtype, MHtype, Itype, 0);
};

void create_ich(vector<neural_population>& pops, int ID, string name, int Gtype, int MHtype, int Itype, int Dstatus) {
	create_ichc(pops[ID].paraset, name, Gtype, MHtype, Itype, Dstatus);
};

void create_ich(neural_population& pop, string name, int Gtype, int MHtype, int Itype) {
	create_ichc(pop.paraset, name, Gtype, MHtype, Itype, 0);
};

void create_ich(neural_population& pop, string name, int Gtype, int MHtype, int Itype, int Dstatus) {
	create_ichc(pop.paraset, name, Gtype, MHtype, Itype, Dstatus);
};

void create_ich(vector<parameter_set>& paraset, int ID, string name, int Gtype, int MHtype, int Itype) {
	create_ichc(paraset[ID], name, Gtype, MHtype, Itype, 0);
};

void create_ich(vector<parameter_set>& paraset, int ID, string name, int Gtype, int MHtype, int Itype, int Dstatus) {
	create_ichc(paraset[ID], name, Gtype, MHtype, Itype, Dstatus);
};

void create_ichc(parameter_set& paraset, string name, int Gtype, int MHtype, int Itype, int Dstatus) {
	int size = int(paraset.ichs.g_para.size() + paraset.ichs.ab_para.size() + paraset.ichs.z_para.size() + paraset.ichs.ng_para.size());
	switch (Gtype)
	{
	case 2: case 3: case 4: case 5:
		ich_parameters *ich;
		ich = new ich_parameters;
		ich->name = name;
		ich->ID = size;
		ich->Gtype = Gtype;
		ich->MHtype = MHtype;
		ich->Itype = Itype;
		ich->Dstatus = Dstatus;
		paraset.ichs.g_para.push_back(*ich);
		break;
	case 6: case 7:
		ich_ab_parameters *ichab;
		ichab = new ich_ab_parameters;
		ichab->name = name;
		ichab->ID = size;
		ichab->Gtype = Gtype;
		ichab->MHtype = MHtype;
		ichab->Itype = Itype;
		ichab->Dstatus = Dstatus;
		paraset.ichs.ab_para.push_back(*ichab);
		break;
	case 8: case 9: case 10: case 11:
		ich_z_parameters *ichz;
		ichz = new ich_z_parameters;
		ichz->name = name;
		ichz->ID = size;
		ichz->Gtype = Gtype;
		ichz->MHtype = MHtype;
		ichz->Itype = Itype;
		ichz->Dstatus = Dstatus;
		paraset.ichs.z_para.push_back(*ichz);
		break;
	case 0: case 12: case 13:
		ich_ng_parameters *ichng;
		ichng = new ich_ng_parameters;
		ichng->name = name;
		ichng->ID = size;
		ichng->Gtype = Gtype;
		ichng->MHtype = MHtype;
		ichng->Itype = Itype;
		ichng->Dstatus = Dstatus;
		paraset.ichs.ng_para.push_back(*ichng);
		break;
	};
	paraset.ichs.update();
	//paraset.ichs.IChIonTList.push_back(Itype);
	//paraset.ichs.IChDSList.push_back(Dstatus);
};

void create_ichc(ichs_set& ichs, string name, int Gtype, int MHtype, int Itype, int Dstatus) {
	int size = int(ichs.g_para.size() + ichs.ab_para.size() + ichs.z_para.size() + ichs.ng_para.size());
	switch (Gtype)
	{
	case 2: case 3: case 4: case 5:
		ich_parameters *ich;
		ich = new ich_parameters;
		ich->name = name;
		ich->ID = size;
		ich->Gtype = Gtype;
		ich->MHtype = MHtype;
		ich->Itype = Itype;
		ich->Dstatus = Dstatus;
		ichs.g_para.push_back(*ich);
		break;
	case 6: case 7:
		ich_ab_parameters *ichab;
		ichab = new ich_ab_parameters;
		ichab->name = name;
		ichab->ID = size;
		ichab->Gtype = Gtype;
		ichab->MHtype = MHtype;
		ichab->Itype = Itype;
		ichab->Dstatus = Dstatus;
		ichs.ab_para.push_back(*ichab);
		break;
	case 8: case 9: case 10: case 11:
		ich_z_parameters *ichz;
		ichz = new ich_z_parameters;
		ichz->name = name;
		ichz->ID = size;
		ichz->Gtype = Gtype;
		ichz->MHtype = MHtype;
		ichz->Itype = Itype;
		ichz->Dstatus = Dstatus;
		ichs.z_para.push_back(*ichz);
		break;
	case 0: case 12: case 13:
		ich_ng_parameters *ichng;
		ichng = new ich_ng_parameters;
		ichng->name = name;
		ichng->ID = size;
		ichng->Gtype = Gtype;
		ichng->MHtype = MHtype;
		ichng->Itype = Itype;
		ichng->Dstatus = Dstatus;
		ichs.ng_para.push_back(*ichng);
		break;
	};
	ichs.update();
	//paraset.ichs.IChIonTList.push_back(Itype);
	//paraset.ichs.IChDSList.push_back(Dstatus);
};

void delete_ich(vector<neural_population>& pops, int ID, string name) {
	delete_ichc(pops[ID].paraset, name);
};

void delete_ich(vector<neural_population>& pops, string name) {
	int res;
	for (int ID = 0;ID < (int)pops.size();ID++)
	{
		res = delete_ichc(pops[ID].paraset, name);
		if (res == 1) break;
	};
};

void delete_ich(vector<parameter_set>& paraset, int ID, string name) {
	delete_ichc(paraset[ID], name);
};

void delete_ich(vector<parameter_set>& paraset, string name) {
	int res;
	for (int ID = 0;ID < (int)paraset.size();ID++)
	{
		res = delete_ichc(paraset[ID], name);
		if (res == 1) break;
	};
};

int delete_ichc(parameter_set& paraset, string name) {

	int cg = 0, cab = 0, cz = 0, cn = 0, brk_flg = 0;
	for (int i = 0;i<paraset.ichs.nIChs;i++)
	{
		switch (paraset.ichs.IChGTList[i])
		{
		case 2: case 3: case 4: case 5:
			if (paraset.ichs.g_para[cg].name == name) {
				paraset.ichs.g_para.erase(paraset.ichs.g_para.begin() + cg);
				brk_flg = 1;
			}
			else
				cg++;
			break;
		case 6: case 7:
			if (paraset.ichs.ab_para[cab].name == name) {
				paraset.ichs.ab_para.erase(paraset.ichs.ab_para.begin() + cab);
				brk_flg = 1;
			}
			else
				cab++;
			break;
		case 8: case 9: case 10: case 11:
			if (paraset.ichs.z_para[cz].name == name) {
				paraset.ichs.z_para.erase(paraset.ichs.z_para.begin() + cz);
				brk_flg = 1;
			}
			else
				cz++;
			break;
		case 0: case 12: case 13:
			if (paraset.ichs.ng_para[cn].name == name) {
				paraset.ichs.ng_para.erase(paraset.ichs.ng_para.begin() + cn);
				brk_flg = 1;
			}
			else
				cn++;
			break;
		}

		if (brk_flg == 1) break;
	};
	if (brk_flg == 1) {
		paraset.ichs.update();
		return brk_flg;
	}
	else {
		cout << "ERROR:: I channel (" << name << "): Not found" << endl;
		return brk_flg;
	};

};

void create_ion(vector<neural_population>& pops, int ID, string name, int type) {
	int size = int(pops[ID].paraset.ions.ion_para.size() - 1);

	ion_parameters *ion;
	ion = new ion_parameters;
	ion->name = name;
	ion->ID = size + 1;
	ion->ionType = type;
	pops[ID].paraset.ions.ion_para.push_back(*ion);


	pops[ID].paraset.ions.update();
};

void create_ion(neural_population& pop, string name, int type) {
	int size = int(pop.paraset.ions.ion_para.size() - 1);

	ion_parameters *ion;
	ion = new ion_parameters;
	ion->name = name;
	ion->ID = size + 1;
	ion->ionType = type;
	pop.paraset.ions.ion_para.push_back(*ion);


	pop.paraset.ions.update();
};

void create_ion(vector<parameter_set>& paraset, int ID, string name, int type) {
	int size = int(paraset[ID].ions.ion_para.size() - 1);

	ion_parameters *ion;
	ion = new ion_parameters;
	ion->name = name;
	ion->ID = size + 1;
	ion->ionType = type;
	paraset[ID].ions.ion_para.push_back(*ion);

	paraset[ID].ions.update();
};

void create_ion(parameter_set& paraset, string name, int type) {
	int size = int(paraset.ions.ion_para.size() - 1);

	ion_parameters *ion;
	ion = new ion_parameters;
	ion->name = name;
	ion->ID = size + 1;
	ion->ionType = type;
	paraset.ions.ion_para.push_back(*ion);

	paraset.ions.update();
};

void delete_ion(vector<neural_population>& pops, int ID, string name) {
	int c = 0, brk_flg = 0;
	for (int i = 0;i<pops[ID].paraset.ions.nIons;i++)
	{

		if (pops[ID].paraset.ions.ion_para[c].name == name) {
			pops[ID].paraset.ions.ion_para.erase(pops[ID].paraset.ions.ion_para.begin() + c);
			brk_flg = 1; break;
		}
		else	c++;
	};
	if (brk_flg == 1)
		pops[ID].paraset.ions.update();
	else
		cout << "ERROR:: Ion (" << name << "): Not found" << endl;
};

void delete_ion(vector<neural_population>& pops, string name) {
	for (unsigned int ID = 0;ID<pops.size();ID++)
	{
		int c = 0, brk_flg = 0;
		for (int i = 0;i<pops[ID].paraset.ions.nIons;i++)
		{

			if (pops[ID].paraset.ions.ion_para[c].name == name) {
				pops[ID].paraset.ions.ion_para.erase(pops[ID].paraset.ions.ion_para.begin() + c);
				brk_flg = 1; break;
			}
			else	c++;
		};
		if (brk_flg == 1)
			pops[ID].paraset.ions.update();
		else
			cout << "ERROR:: Ion (" << name << "): Not found" << endl;
	};
};

void delete_ion(vector<parameter_set>& paraset, int ID, string name) {
	int c = 0, brk_flg = 0;
	for (int i = 0;i<paraset[ID].ions.nIons;i++)
	{

		if (paraset[ID].ions.ion_para[c].name == name) {
			paraset[ID].ions.ion_para.erase(paraset[ID].ions.ion_para.begin() + c);
			brk_flg = 1; break;
		}
		else	c++;
	};
	if (brk_flg == 1)
		paraset[ID].ions.update();
	else
		cout << "ERROR:: Ion (" << name << "): Not found" << endl;
};

void delete_ion(vector<parameter_set>& paraset, string name) {
	for (unsigned int ID = 0;ID<paraset.size();ID++)
	{
		int c = 0, brk_flg = 0;
		for (int i = 0;i<paraset[ID].ions.nIons;i++)
		{

			if (paraset[ID].ions.ion_para[c].name == name) {
				paraset[ID].ions.ion_para.erase(paraset[ID].ions.ion_para.begin() + c);
				brk_flg = 1; break;
			}
			else	c++;
		};
		if (brk_flg == 1)
			paraset[ID].ions.update();
		else
			cout << "ERROR:: Ion (" << name << "): Not found" << endl;
	};
};

void delete_ion(parameter_set& paraset, string name) {

	int c = 0, brk_flg = 0;
	for (int i = 0;i<paraset.ions.nIons;i++)
	{

		if (paraset.ions.ion_para[c].name == name) {
			paraset.ions.ion_para.erase(paraset.ions.ion_para.begin() + c);
			brk_flg = 1; break;
		}
		else	c++;
	};
	if (brk_flg == 1)
		paraset.ions.update();
	else
		cout << "ERROR:: Ion (" << name << "): Not found" << endl;

};

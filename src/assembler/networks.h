#ifndef _NETWORKS_H
#define _NETWORKS_H

#include "myheader.h"
//#include "parameters.h"
#include "npopulations.h"
#include "nnonspikeunits.h"

#define CONN_REG  0
#define CONN_ALL  1
#define CONN_ONE  2
#define CONN_ALLP 3
#define CONN_ONEP 4

#define UTYPE_POP 0
#define UTYPE_NSU 1

struct net_pconn {

	int syntype; //0:Ex1, 1:Ex2, 2:In1, 3:In2
	int opt;
	bool optmz_OnOff;
	bool norm_OnOff;

	int stype; //Source type 0:pop, 1:drive
	int ttype;//Source type 0:pop, 1:output

	int sid; // Source population absolute id	
	int tid; // Target population absolute id
	param sprob; // Source probability of connection in percentage. variance in percentage
	param tprob; // Target probability of connection in percentage. variance in percentage
	param w; //Weight value and variance in percentage

	int prate; //Plasticity rate

	int *spmtx; //Source probability of connection matrix
	int *tpmtx; //Target probability of connection matrix
	double *wmtx; //Target weight of connection matrix

	int nwmtx;
	int counter;

};

struct net_nconn {

	int syntype; //Ex1, Ex2, In1, In2
	int opt;
	bool optmz_OnOff;
	bool norm_OnOff;

	int snet; // Source network absolute id
	int tnet; // Target network absolute id

	int stype; //Source type 0:pop, 1:unit
	int ttype;//Source type 0:pop, 1:unit

	int sid; // Source population absolute id in the source network
	int tid; // Target population absolute id in the target network
	param sprob; // Source probability of connection in percentage. variance in percentage
	param tprob; // Target probability of connection in percentage. variance in percentage
	param w; //Weight value and variance in percentage

	int *spmtx; //Source probability of connection matrix
	int *tpmtx; //Target probability of connection matrix
	double *wmtx; //weight of connection matrix

	int nwmtx;
	int counter;

};

class nns_net {
public:
	nns_net(){};
	nns_net& operator= (const nns_net&);
public:
	string name;
	bool OnOff;

	int npList;
	int nuList;
	int nList;
	vector<string> pnameList; //Absolute population name list
	vector<int> pidList; //Absolute population id list
	vector<string> unameList;
	vector<int> uidList; //actual id

	vector<neural_population> *ppops;
	vector<net_pconn> pconn;

	void init(vector<neural_population>*);
	void make_net(string,vector<string>&,vector<int>&, vector<string>&, vector<int>&,vector<neural_population>*);
	void conn_pops(int,int,param,int,int,param,param,int,int,bool,bool);
	void conn_pops(int,int, int, int, int, int,param, int,bool,bool);
	void make_mtx(int id,contr_parameters&);
	void del_mtx(int id);
	void del_conn(int,int);
	void update();
};

class nns_mnet {
public:
	nns_mnet(){};
	nns_mnet& operator= (const nns_mnet&);
public:
	string name;
	bool OnOff;
	
	int nList;
	vector<string> nnameList; //Absolute net name list
	vector<int> nidList; //Absolute net id list

	vector<nns_net> *pnets;
	vector<neural_population> *ppops;
	vector<net_nconn> nconn;

	void init(vector<neural_population>*,vector<nns_net>*);
	void make_mnet(string,vector<string>&,vector<neural_population>*,vector<nns_net>*);
	void conn_nets(int,int,param,int,int,param,param,int);
	void del_conn(int,int);
	void update();
};

class nns_tnet {
public:
	nns_tnet(void) : OnOff(false) {};
	string name;
	bool OnOff;

	vector<nns_net> nets;
	vector<nns_mnet> mnets;

	void reset();

};

//void make_net(string, vector<string>&, vector<neural_population>&, vector<nns_net>&);
void make_net(string, vector<string>&, vector<string>&, vector<neural_population>&, nns_nonspikeunit&, vector<nns_net>&);
//void conn_pops(string, int, string, string, param, param, param, vector<nns_net>&);
void conn_pops(string, int, int, string, string, int, int, param, param, param, bool, bool, nns_nonspikeunit&, vector<nns_net>&);
void conn_pops(string, int, int, string, string, int, int, param, int, bool, bool, nns_nonspikeunit&, vector<nns_net>&);
void copy_net(string, vector<neural_population>&, nns_nonspikeunit&, vector<nns_net>&);
void rename_net(string, string, vector<nns_net>&);
void delete_net(string, vector<nns_net>&);

void make_mnet(string, vector<string>&, vector<neural_population>&, vector<nns_net>&, vector<nns_mnet>&);
//void make_mnet(string, vector<string>&, vector<string>&, vector<neural_population>&, vector<nns_net>&, vector<nns_mnet>&);
void conn_nets(string, int, string, string, string, string, param, param, param, vector<nns_net>&, vector<nns_mnet>&);
void copy_mnet(string, vector<neural_population>&, nns_nonspikeunit&, vector<nns_net>&, vector<nns_mnet>&);
void rename_mnet(string, string, vector<nns_mnet>&);
void delete_mnet(string, vector<nns_mnet>&);

#endif
#ifndef __CUSTOMFUNCS_H
#define __CUSTOMFUNCS_H

#include "myheader.h"

typedef mt19937 RNG; //This is for normrdarray

extern RNG Rng;			/*SM*/
extern double normrd( RNG& rng, double mean, double var ); /*SM*/
extern double unird( RNG& rng, double mean, double var ); /*SM*/
extern double unird( RNG& rng, double var ); /*SM*/

extern double *unirdarray(int n, int deci, double ref, int percent);
extern double *normrdarray(RNG& rng, int n, double sig, double avg, double percent);
extern vector<double> normrdvec(RNG& rng, int n, double sig, double avg, double percent);

extern int *rdiarray(int, int);
extern int *rdipermarray(int, int);

extern double round1(double x);
extern double roundd(double x, int dec);
extern double roundd(double* x, int dec);
extern double floord(double x, int dec);
extern double floord(double* x, int dec);
extern double ceild(double x, int dec);
extern double ceild(double* x, int dec);

extern unsigned int newseed();
extern unsigned int newseed(unsigned int);

extern map<double,int> hist(double *arr, int n, double bin);

template <class T> T find_max(vector<T> in) {
	T maxv;
	maxv = in[0];
	for (size_t i = 1; i < in.size(); i++) {
		maxv = (maxv >= in[i]) ? maxv : in[i];
	};
	return maxv;
};

template <class T> int find_imax(vector<T> in) {
	T maxv;
	int id = 0;
	maxv = in[0];
	for (size_t i = 1; i < in.size(); i++) {
		maxv = (maxv >= in[i]) ? maxv : in[i];
		id = (maxv >= in[i]) ? id : i;
	};
	return id;
};

template <class T> T find_min(vector<T> in) {
	T minv;
	minv = in[0];
	for (size_t i = 1; i < in.size(); i++) {
		minv = (minv <= in[i]) ? minv : in[i];
	};
	return minv;
};

template <class T> int find_imin(vector<T> in) {
	T minv;
	size_t id = 0;
	minv = in[0];
	for (size_t i = 1; i < in.size(); i++) {
		id = (minv <= in[i]) ? id : i;
		minv = (minv <= in[i]) ? minv : in[i];	
	};
	return (int)id;
};

#endif
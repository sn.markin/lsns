#include "bgnet.h"

#define MAX_TRIAL	100

void store_logs( int trial, bgnet &net )
{
	string logname;
	ofstream d12_log;
	ofstream m1_log;
	logname = "spn_learn"+std::to_string( trial+1 )+".log";
	d12_log.open( logname, std::ofstream::out );
	net.storelog_D12( trial+1, d12_log );
	d12_log.close();
	logname = "m1_learn"+std::to_string( trial+1 )+".log";
	m1_log.open( logname, std::ofstream::out );
	net.storelog_M1( trial+1, m1_log );
	m1_log.close();
}

int main( int argc, char *argv[] )
{
	bgnet BGnet;
	if( argc != 2 ){
		return -1;
	}
	string expath = argv[1], nnspath, datapath;
	size_t fd = expath.find("\\\\");
	if (fd != std::string::npos) {
		expath.erase(fd, expath.size() - fd);
	};
	expath = "";
#if defined( __WINDOWS__ )
	nnspath = expath + "nns\\";
	datapath = expath + "data\\";
#else
	nnspath = "";
	datapath = "";
#endif
	nnspath += string( argv[1] );
	ofstream dat_log( "dat", std::ofstream::out );
	ofstream www_log( "www", std::ofstream::out );
	ofstream qqq_log( "qqq", std::ofstream::out );
	size_t n_sessions = 1;

	if( BGnet.load_sim( nnspath )){
		BGnet.init();
//		BGnet.run_sim();
//		BGnet.save_data( "res", "dat" );
////////////////////////////////////////////////////////////////////////////////
// Uncomment this part to reproduce multi-simulations experiment
//

		for( int session = 0; session < n_sessions; ++session ){
			string ns = std::to_string( session+1 );
			string move_name = string( "move" )+ns+string( ".log" );
			string pos_name = string( "pos" )+ns+string( ".txt" );
			ofstream move_log( move_name, std::ofstream::out );
			ofstream pos_log( pos_name, std::ofstream::out );
			cout << endl << "session: " << session+1 << endl << "==================================================="  << endl;

			BGnet.reinit();
			BGnet.run_transition( 10000 );
			for( int trial = 0; trial < MAX_TRIAL; ++trial ){
				cout << endl << "trial: " << trial+1 << endl;
				BGnet.reset();
				BGnet.expl_th( 0.057f, -0.033f );
				BGnet.expl_d1( 0.01f, -0.005f );
				BGnet.expl_d2( 0.01f, -0.005f );
				BGnet.run_transition( 2000 );
				BGnet.run_sim();
				BGnet.import_data();
				if( trial < 50 ){
					BGnet.biomech( 40 ); // 40
				}
				else if( trial < 100 ){
					BGnet.biomech( 10 ); // 10
				}
				else{
					BGnet.biomech( 40 ); // 40
				}
				BGnet.learning();
				BGnet.export_data();
//				BGnet.save_data( std::to_string( trial+1 ), "dat" );
//				store_logs( trial, BGnet );
				BGnet.storelog( trial+1, move_log );
				BGnet.storelog1( trial+1, pos_log );
				BGnet.storelog_f( trial+1, dat_log );  // dat
				BGnet.storelog_w( trial+1, www_log );  // www
				BGnet.storelog_xy( trial+1, qqq_log ); // qqq
			}
		}

		BGnet.close_sim();
	}
}

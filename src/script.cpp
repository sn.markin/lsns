﻿#include "script.h"
#include "commands.h"

///////////////////////////////////////////////////////////////////////////////
// class parmap
//--- methods
bool parmap::create( nns_project &prj, const char *unit_name, const char *par_name )
{
	std::vector<pos> cells_;
	int upos_ = ( int )find_UOffSet( unit_name, prj );
	if( upos_ >= 0 ){	// check if non-spiking unit(s) is a source
		cells_.push_back( pos( upos_ ));
	}
	else{			// check if spiking unit(s) is a source
		upos_ = ( int )find_UOffSet( unit_name, 0, prj );
		if( upos_ < 0 ){
			return false;
		}
		size_t ssz_  = get_size( unit_name, prj );
		for( size_t i = 0; i < ssz_; ++i ){
			cells_.push_back( pos( upos_+i ));
		}
	}
	PMap.clear();
	for( size_t i = 0; i < cells_.size(); ++i ){
		PMap.insert( std::make_pair( cells_[i], 0.f ));
	}
	if( std::string( par_name ) == "Spike" ){
		BaseName = "CellV";
		Bank = _cell_pos_spike;
		return true;
	}
	else if( std::string( par_name ) == "V" ){
		BaseName = "CellV";
		Bank = _cell_pos_v;
		return true;
	}
	else if( std::string( par_name ) == "SpikeStat" ){
		BaseName = "CellStat";
		Bank = _cell_pos_spike;
		return true;
	}
	else if( std::string( par_name ) == "VStat" ){
		BaseName = "CellStat";
		Bank = _cell_pos_v;
		return true;
	}
	return false;
}

//--- overrided methods
bool parmap::copy_data( netdat *data, bool direction )
{
	if( direction && DMap.reset() ){
		for( pit it = PMap.begin(); it != PMap.end(); ++it ){
			pos epos = make_epos32( Bank, it->first );
			if( epos != -1 ){
				DMap.push_back( it->second, epos );
			}
		}
	}
	else if( !direction && PMap.size() == DMap.size() ){
		size_t i = 0;
		for( pit it = PMap.begin(); it != PMap.end(); ++it, ++i ){
			it->second = DMap[i];
		}
		return true;
	}
	return false;
}

bool parmap::set_base( const netdat *data )
{
	float4 *base_ = NULL;
	if( BaseName == "CellV"  && data ){
		base_ = data->DevMap->Cells.CellV;
	}
	else if( BaseName == "CellStat" && data ){
		base_ = data->DevMap->Cells.CellStat;
	}
	if( base_ != NULL ){
		DMap.set_base( base_ );
		return true;
	}
	return false;
}

size_t parmap::validate( const netdat *data )
{
	float4 *base_ = NULL;
	if( BaseName == "CellV"  && data ){
		base_ = data->Cells.CellV;
	}
	else if( BaseName == "CellStat" && data ){
		base_ = data->Cells.CellStat;
	}
	if( Bank != -1 && base_ != NULL ){
		for( pit it = PMap.begin(); it != PMap.end();  ){
			pos epos = make_epos32( Bank, it->first );
			if( epos != -1 ){
				it->second = get_data( epos, base_ ); ++it;
			}
			else{
				PMap.erase( it++ );		// remove non-existing element from PMap
			}
		}
	}
	else{
		PMap.clear();
	}
	return PMap.size();
}
///////////////////////////////////////////////////////////////////////////////
// class wmap
bool wmap::create( nns_project &prj, const char *trg_name, const char *src_name, const char *syn_name )
{
	std::string sname_ = syn_name;
	std::vector<pos> srcs_;
	std::vector<pos> syns_;
	int stype = -1;
	if( sname_ == "SynpEx1"){
		stype = 0;
	}
	else if( sname_ == "SynpEx2"){
		stype = 1;
	}
	else if( sname_ == "SynpIn1"){
		stype = 2;
	}
	else if (sname_ == "SynpIn2"){
		stype = 3;
	}
	else{
		return false;
	}
	int spos_ = ( int )find_UOffSet( src_name, prj );
	if( spos_ >= 0 ){	// non-spiking unit is a source
		stype += 4;	// 4 synapses (spiking units) + 4 sysnapses (non-spiking units)
		srcs_.push_back( pos( spos_ ));
	}
	else{			// check if spiking unit is a source
		spos_ = ( int )find_UOffSet( src_name, 0, prj );
		if( spos_ < 0 ){
			return false;
		}
		size_t ssz_  = get_size( src_name, prj );
		for( size_t i = 0; i < ssz_; ++i ){
			srcs_.push_back( pos( spos_+i ));
		}
	}
	int tpos_ = ( int )find_UOffSet( trg_name, prj );
	if( tpos_ >= 0 ){	// non-spiking unit is a target
		syns_.push_back( pos( find_SOffSet( trg_name, stype, prj )));
	}
	else{
		tpos_ = ( int )find_UOffSet( trg_name, 0, prj );
		if( tpos_ < 0 ){
			return false;
		}
		size_t ssz_  = get_size( trg_name, prj );
		for( size_t i = 0; i < ssz_; ++i ){
			syns_.push_back( pos( find_SOffSet( trg_name, i, stype, prj )));
		}
	}
	// establish the map of all possible connections between target and source units
	WMap.clear();
	for( size_t i = 0; i < syns_.size(); ++i ){
		for( size_t j = 0; j < srcs_.size(); ++j ){
			con s_ = std::make_pair( srcs_[j], 0.f );
			WMap.insert( std::make_pair( syns_[i], s_ ));
		}
	}
	BaseName = "Wsum.Wall";
	return true;
}
//--- overrided methods
bool wmap::copy_data( netdat *data, bool direction )
{
	if( direction && DMap.reset() ){
		for( wsit it = WMap.begin(); it != WMap.end(); ++it ){
			pos syn_pos = it->first;
			pos src_pos = it->second.first;
			pos epos = get_wpos( syn_pos, src_pos, data );
			if( epos != -1 ){
				DMap.push_back( it->second.second, epos );
			}
		}
		return true;
	}
	else if( !direction && WMap.size() == DMap.size() ){
		size_t i = 0;
		for( wsit it = WMap.begin(); it != WMap.end(); ++it, ++i ){
			it->second.second = DMap[i];
		}
		return true;
	}
	return false;
}

bool wmap::set_base( const netdat *data )
{
	DMap.set_base( data->DevMap->Wsum.Wall );
	return true;
}

size_t wmap::validate( const netdat *data )
{
	typedef std::set<pos> eposmap_;
	eposmap_ emap;
	if( data ){
		for( wsit it = WMap.begin(); it != WMap.end(); ){
			float dat_ = 0.f;
			pos syn_pos = it->first;
			pos src_pos = it->second.first;
			pos epos = get_wpos( syn_pos, src_pos, data );
			if( epos != -1 ){
				if( emap.find( epos ) != emap.end()){	// check if current 'epos' has a duplicate in eposmap
					WMap.erase( it++ );		// remove duplicate element from WMap
				}
				else{
					emap.insert( epos );		// add unique epos to eposmap
					it->second.second = get_data( epos, data->Wsum.Wall ); ++it;
				}
			}
			else{
				WMap.erase( it++ );			// remove non-existing element from WMap
			}
		}
	}
	else{
		WMap.clear();
	}
	return WMap.size();
}

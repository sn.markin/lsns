#pragma once

#ifndef __UTIL_H
#define __UTIL_H

#include "config.h"
#include <algorithm>
#include <functional>
#include <string>

template <class T>
void hash_combine(std::size_t &seed, const T &v)
{
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}

class fparser{
#if defined( __WINDOWS__ ) /*check if macros defined in the command line*/
	struct separator{
		bool operator()( char ch ) const{
			return ch == '\\' || ch == '/';
		}
	};
#elif defined( __LINUX__ ) /*check if macros defined in the command line*/
	struct separator{
		bool operator()( char ch ) const{
			return ch == '/';
		}
	};
#endif /* __LINUX__ */
	public: // constructors/destructor
		fparser( void ) : Pathname( "" ), Filename( "" ), Basename( "" ), Extname( "" )
		{
		};
		fparser( const std::string &fullname ) : Pathname( fullname ), Filename( "" ), Basename( "" ), Extname( "" )
		{
			parse( fullname );
		};
		~fparser(){ clear(); };
	public: // constructors/destructor
		void clear( void )
		{
			Extname.clear();
			Basename.clear();
			Filename.clear();
			Pathname.clear();
		};
		void parse( const std::string &fullname )
		{
			clear();
			std::string::const_iterator pivot = std::find_if( fullname.rbegin(), fullname.rend(), separator() ).base();
			if( pivot != fullname.end() ){
				Pathname = std::string( fullname.begin(), std::find_if( fullname.rbegin(), fullname.rend(), separator() ).base() );
				Filename = std::string( pivot, fullname.end() );
				parsebasename( Filename );
			}
		};
	private:
		void parsebasename( const std::string &filename )
		{
		    std::string::const_iterator pivot = std::find( filename.rbegin(), filename.rend(), '.' ).base();
		    Basename = filename;
		    if( pivot != filename.end() ){
			    Basename = std::string( filename.begin(), pivot-1 );
			    Extname = std::string( pivot, filename.end() );
		    }
		};
	public:
		std::string Extname;
		std::string Basename;
		std::string Filename;
		std::string Pathname;
};

#define X87FLAGBITS			6
#define DAZ_BIT				6
#define FTZ_BIT				15
#define DENORMAL_EXCEPTION_MASK		8
#define UNDERFLOW_EXCEPTION_MASK	11
#define OVERFLOW_EXCEPTION_MASK		10

extern void mxcsr_on( int bit_num );
extern void mxcsr_off( int bit_num );
extern void clear_flags( void );
extern void make_denormal( void );
extern bool remove_char( const char *filename, char ch );

#endif /*__UTIL_H*/

#pragma once

#include "engine.h"
#include "reach.h"
#include "script.h"
#include "util.h"

#define ACTIVE_CUE		10
#define ACTIVE_PRG		25
#define ACTIVE_TRG		25

// short-term memory for reward predicion
// weight limits
#define W_D1MIN 0.03f
#define W_D2MIN 0.03f
#define W_D1MAX 0.49f
#define W_D2MAX 0.49f
#define W_M1MIN 0.01f
#define W_M1MAX 0.03f
// learning rate for d1
#define LAM1p 0.08f
#define LAM1n 0.06f
// learning rate for d2
#define LAM2p 0.002f
#define LAM2n 0.001f
// decay rate for d1 and d2
#define GAM1 0.25f
#define GAM2 0.05f
// learning rate for m1
#define LAM3 0.0f
// decay rate for m1
#define GAM3 0.0f

template <class type>
int sign( type x )
{
	if( x > type( 0 )){
		return 1;
	}
	else if( x < type( 0 )){
		return -1;
	}
	return 0;
};

template <class type>
type sigmoid( type f, type f12, type slp )
{
	return type( 1 )/( type( 1 )+exp( -( f-f12 )/slp ));
}

///////////////////////////////////////////////////////////////////////////////
// class learning
class learnsyn{
	public:
		learnsyn( float fk_1 = 7.5f, float f12_1 = 44.f, float fk_2 = 15.f, float f12_2 = 100.f ) : Fk_1( fk_1 ), F12_1( f12_1 ), Fk_2( fk_2 ), F12_2( f12_2 )
		{
			make_synapse( 0.13f, 0.01f, 1.f, 1.f );
		};
		~learnsyn( void ){};
	public:
		void make_synapse( float lam, float gam, float ltp = 1.f, float ltd = 1.f )
		{
			Lam = lam; Gam = gam; LTP = ltp; LTD = ltd;
		};
		float learn( float w, float fpre, float fpost, float da = 1.f  )
		{
			da = ( da >= 0 )? da*LTP: da*LTD;
			return w+da*Lam*calc_fth( fpre )*calc_fth( fpost )-Gam*w;
//			return w+da*Lam*calc_f( fpre )*calc_f( fpost )-Gam*w;
		};
		float calc_fth( float f )
		{
			float out = calc_f( f )-calc_f( 0.f );
			return ( out > 0 )? out: 0.f;
		};
	protected:
		float calc_f( float f )
		{
			return 1.f/( 1.f+( exp( -( f-F12_1 )/Fk_1 )+exp(( f-F12_2 )/Fk_2 )));
		};
		float solve_exp( float x, float x0, float tau, float dt ) // tau*dx = x0-x -> x = exp(-dt/tau )*( x-x0 )+x0
		{
			return exp(-dt/tau )*( x-x0 )+x0;
		}
	public:
		float Lam;
		float Gam;
	protected:
		float LTP;
		float LTD;
		float Fk_1;
		float F12_1;
		float Fk_2;
		float F12_2;
};

///////////////////////////////////////////////////////////////////////////////
// class bgnet
class bgnet{
	public: // constructors/desrtuctor
		bgnet( void );
		~bgnet( void );
	public:	// file operation (open, close,v save results of simulation)
		bool close_sim( void );
		bool load_sim( const string &filename );
		bool save_data( const string &suffix, const string &ext );
	public: // initialize
		bool init( float rp = 0.f, float t = 4.0f );
		bool reinit( float rp = 0.f, float t = 4.0f );
		bool reset( float rp = 0.f, float t = 4.0f, bool total = false );
	public: // simulations
		size_t run_transition( size_t nsteps );
		size_t run_sim( void );
		size_t run_nsteps( size_t nsteps );
	public: // data operations
		void export_data( void );
		void import_data( void );
	public: // simulations
		void learning( float T = 1.f );
		void expl_stn( float d_expl, float d_crt );
		void expl_th( float d_expl, float d_crt );
		void expl_d1( float d_expl, float d_crt );
		void expl_d2( float d_expl, float d_crt );
virtual		void biomech( float target );
virtual		void dummymove( float target );
virtual		void storelog( int trial, ofstream &log );
virtual		void storelog1( int trial, ofstream &log );
virtual		void storelog_D12( int trial, ofstream &log );
virtual		void storelog_M1( int trial, ofstream &log );
virtual		void storelog_f( int trial, ofstream &log );  // dat
virtual		void storelog_w( int trial, ofstream &log );  // www
virtual		void storelog_xy( int trial, ofstream &log ); // qqq

	protected:
		void biomech_init( netpar *par );
virtual		void custom_init( netpar *par ){};
		void set_units( nns_project &pro, netpar *par );
		void set_connections( nns_project &pro, netpar *par );
	private: // methods (modification data in the device memory)

	protected: // methods (learnings)
virtual		void cbl_learning( float T ); // cerebellar learning
virtual		void ctx_learning( float T ); // cortex learning
virtual		void msn_learning( float T ); // spiny learning
	protected: // methods
virtual		float calc_da( void ){
			Rpre = Rpre-( Rpre-R )/T;
			DA = R-Rpre;
			return DA;
		}
		void msn1_learning( float da, float pfc, float d1, float d2, float &w1, float &w2 ); // learning for single spiny neuron
		void ctx1_learning( float pfc, float m1, float &w ); // learning for single cortex neuron
	public: 
	// data (output of key populations)
		parmap M1;
		parmap D1;
		parmap D2;
		parmap Gpe;
		parmap Gpi;
	// data (connectivity maps)
		wmap CRT2STN;
		wmap CRT2TH;
		wmap CRT2D1;
		wmap CRT2D2;
		wmap CRT2PFC;
		wmap PFC2D1;
		wmap PFC2D2;
		wmap PFC2M1;
	protected:
		bool IsLoaded;
		float DA;
		float R;
		float Rpre;
		float T;
		float D;
		float Y;
		vector<nns_project> Pros;
		netpar Netpar;
		fparser Fparser;
		learnsyn D1Syn;
		learnsyn D2Syn;
		learnsyn M1Syn;
		learnsyn OutSyn;
	protected:
		float Phi0[2];
		float Xini;
		float Yini;
		float Xt;
		float Yt;
		float Xc;
		float Yc;
};

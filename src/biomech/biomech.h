#ifndef __BIOMECH_H
#define __BIOMECH_H

#include <math.h>

#define NUM_MUSCLES 6

#ifndef M_PI
#define M_PI 3.14159265358979323846f
#endif

// limb parametes
const float g = 9.81f;                  // %m/sec^2, gravity constant
const float M1 = 0.955f;                // %kg, mass (upper segment)
const float M2 = 0.63f;                 // %kg, mass (lower segment)
const float L1 = 0.4035f;               // %m, length (upper segment)
const float L2 = 0.325f;                // %m, length (lower segment)
// geometry parametes
const float A1min = (180.f-145.f)*M_PI/180.f; // upper joint flexed (180-145)
const float A1max = (180.f+45.f)*M_PI/180.f;  // upper joint extended (180+45)
const float A2min = (180.f-155.f)*M_PI/180.f; // lower joint flexed (180-155)
const float A2max = (180.f+5.f)*M_PI/180.f;   // lower joint extended (180+5)
// joint viscocity & joint restriction (elastic and viscosity components)
const float BJ = 0.05f;
const float KJr = .3f;
const float BJr = 1.f;
// average moment arms (m)
const float R [NUM_MUSCLES+2]= { 0.015f, -0.008f, 0.035f, -0.021f, 0.020f, -0.005f, 0.036f, -0.021f };  

#endif /*__MUSCLES_H*/

#include "muscles.h"

// Muscles parametes
const float Fmax [NUM_MUSCLES]= { 420.0f, 570.0f, 1010.f, 1880.f, 460.0f, 630.0f };  // maximal forces (N)
const float Lopt  [NUM_MUSCLES]= { 0.185f, 0.170f, 0.180f,  0.055f, 0.130f,  0.150f };   // optimal lengths (m)
const float Beta = 2.3f, Omega = 1.26f, Ro = 1.62f;                 // length active component
const float Av1 = 0.17f, Bv1 = -0.69f, Bv2 = 0.18f;                 //   velocity active component

// Feedbacks parametes
// Ia feedback 
const float Lrest_I [NUM_MUSCLES] = { 0.037f, 0.0340f, 0.0360f, 0.0110f, 0.0260f, 0.0300f }; // 0.2*Lopt
// length component
const float kL_Ia [NUM_MUSCLES] = { 0.8f, 0.8f, 0.8f, 0.8f, 0.8f, 0.8f };
const float kEMG_Ia [NUM_MUSCLES] = { 0.05f, 0.05f, 0.05f, 0.05f, 0.05f, 0.05f };
const float C_Ia [NUM_MUSCLES] = { 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f };
// velocity components
const float Vmax_Ia [NUM_MUSCLES] = { 2.2f, 1.2f, 5.9f, 3.5f, 6.7f, 3.7f }; 
const float pV_Ia [NUM_MUSCLES] = { 0.6f, 0.6f, 0.6f, 0.6f, 0.6f, 0.6f };
const float kV_Ia [NUM_MUSCLES] = { 1.8f, 1.8f, 1.8f, 1.8f, 1.8f, 1.8f };
// II feedback (range between 0-0.95)
const float Lrest_II [NUM_MUSCLES] = { 0.0185f, 0.0170f, 0.0180f, 0.0055f, 0.0130f, 0.0150f };  // 0.1*Lopt;
const float kL_II  [NUM_MUSCLES] = { 0.8f, 0.8f, 0.8f, 0.8f, 0.8f, 0.8f };
const float kEMG_II [NUM_MUSCLES]  = { 0.05f, 0.05f, 0.05f, 0.05f, 0.05f, 0.05f };
const float C_II  [NUM_MUSCLES] = { 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f };
// Ib feedback (range between 0,0.9)
const float Fconst_Ib [NUM_MUSCLES] = { 21.0000f, 28.5000f, 50.5000f, 94.0000f, 23.0000f, 31.5000f }; // 0.05*Fmax; 

///////////////////////////////////////////////////////////////////////////////
// former muscles
void m_force( float *mn, float *l, float *v, float *f )
{
	for( int i = 0; i < NUM_MUSCLES; ++i ){
		float lnorm = l[i]/Lopt[i]; // normalized lenght
		float lmax = (( lnorm-1.f) > 0.f )? lnorm-1.f: 0.f;
		float f_act =  float( exp( -pow( fabs( pow( lnorm, Beta )-1.f )/Omega, Ro )));
		float fl_p = 3.5f*lmax*float( log( exp(( lnorm-1.4 )/0.05 )+1.0 )-0.02*( exp( -18.7*( lnorm-0.79 ))-1.0 ));
		float f_v = 0.f;
		if( v[i] <= 0.f ){
			f_v = ( Bv1-Av1*v[i])/(v[i]+Bv1);
		}
		else{
			f_v = ( Bv2-( -5.34f*lnorm*lnorm+8.41f*lnorm-4.7f )*v[i])/( v[i]+Bv2 );
		}
		f[i] = Fmax[i]*( mn[i]*f_act*f_v+fl_p );
	}
}

///////////////////////////////////////////////////////////////////////////////
// former geometry_block
void m_geometry( float *theta, float *theta_v, float l[NUM_MUSCLES], float v[NUM_MUSCLES], float h_up[NUM_MUSCLES], float h_dw[NUM_MUSCLES] )
{
	// specify joint restrictions
	float a1min = M_PI/2-A1max;
	float a1max = M_PI/2-A1min;
	float a2min = M_PI-A2max;
	float a2max = M_PI-A2min;
	// muscles map
	// [shoulder_flexor shoulder_extensor elbow_flexor elbow_extensor
	// bifunc_flexor bifunc_extensor] 
	double lm[2] = { 0.95*A1max-0.95*A1min, 0.95*A2max-0.95*A2min };              // %range
	double phi[2] = { M_PI/2-theta[0], M_PI-theta[1] };
	double alpha[4] = {phi[0]-A1min, A1max-phi[0], phi[1]-A2min, A2max-phi[1]};
	for( int i = 0; i < 4; ++i ){
		alpha[i] = ( alpha[i] > 0.)? alpha[i] : 0.;
	}
	// muscle lenghts
	l[0] = float(alpha[0]/lm[0])*Lopt[0];
	l[1] = float(alpha[1]/lm[0])*Lopt[1];
	l[2] = float(alpha[2]/lm[1])*Lopt[2];
	l[3] = float(alpha[3]/lm[1])*Lopt[3];
	l[4] = float((alpha[0]+alpha[2])/(lm[0]+lm[1]))*Lopt[4];
	l[5] = float((alpha[1]+alpha[3])/(lm[0]+lm[1]))*Lopt[5];
  	// vA = {-thetaV[0], -thetaV[1]};                   // %positive then extension, negative then flexion
	// v = {vA[0]*R[0], vA[0]*R[1], vA[1]*R[2], vA[1]*R[3], vA[1]*R[4]+vA[1]*R[6], vA[0]*R[5]+vA[1]*R[7]};
	// muscles velocities    
	v[0] = -theta_v[0]*R[0];
	v[1] = -theta_v[0]*R[1];
	v[2] = -theta_v[1]*R[2];
	v[3] = -theta_v[1]*R[3];
	v[4] = -( theta_v[0]*R[4]+theta_v[1]*R[6] );
	v[5] = -( theta_v[0]*R[5]+theta_v[1]*R[7] );
   
	h_up[0] = R[0]; h_up[1] = R[1]; h_up[2]= 0; h_up[3] = 0; h_up[4] = R[4]; h_up[5] = R[5];
	h_dw[0] = 0.f; h_dw[1] = 0.f; h_dw[2] = R[2]; h_dw[3] = R[3]; h_dw[4] = R[6]; h_dw[5]= R[7];
}

void m_feedback( float *l, float *v, float *mn, float *f, float *fb_Ia, float *fb_Ib, float *fb_II )
{
	for( int i = 0; i < NUM_MUSCLES; ++i ){
		float d_Ia = ( l[i]-Lrest_I[i])/Lopt[i]; // normalize d_Ia 
		float l_Ia = (( kL_Ia[i]*d_Ia ) > 0.f )? kL_Ia[i]*d_Ia: 0.f;
        	float sign_v = ( v[i] > 0.f ) ?1.f: (( v[i] < 0.f )? -1.f : 0.f );
		float v_Ia = sign_v*kV_Ia[i]*float( pow( fabs( v[i]/Vmax_Ia[i]), pV_Ia[i] ));
		float emg_Ia = kEMG_Ia[i]*mn[i];

		float d_II = (l[i]-Lrest_II[i])/Lopt[i]; // normalize d_II
		float l_II = (( kL_II[i]*d_II ) > 0.f )? kL_II[i]*d_II: 0.f;
		float emg_II = kEMG_II[i]*mn[i];

		fb_Ia[i] = (( v_Ia+l_Ia+emg_Ia+C_Ia[i] ) > 0.f )? v_Ia+l_Ia+emg_Ia+C_Ia[i]: 0.f;
		fb_Ib[i] = ((( f[i]-Fconst_Ib[i] )/Fmax[i]) > 0.f )? ( f[i]-Fconst_Ib[i] )/Fmax[i]: 0.f;  
		fb_II[i] = l_II+emg_II+C_II[i];
	}
}

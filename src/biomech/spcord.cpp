#include "spcord.h"
#include <math.h>
#include <stdlib.h>

const float B [NUM_MUSCLES] = {Eleak, Eleak, Eleak, Eleak, Eleak, Eleak};
const float Z [NUM_MUSCLES] = { 1.f, 1.f, 1.f, 1.f, 1.f, 1.f };

///////////////////////////////////////////////////////////////////////////////
// former ParWeights
static void init_connections( float wdr_mn[][NUM_MUSCLES], float wdr_Ia[][NUM_MUSCLES], float wdr_Ib[][NUM_MUSCLES], float wmn_rc[][NUM_MUSCLES],
		       float wrc_Ia [][NUM_MUSCLES], float wrc_mn[][NUM_MUSCLES], float wrc_rc[][NUM_MUSCLES], float wia_Ia[][NUM_MUSCLES], 
		       float wia_MN[][NUM_MUSCLES], float wib_MN[][NUM_MUSCLES], float wib_IaMN[][NUM_MUSCLES], float wib_IaIa [][NUM_MUSCLES],
                       float wib_IbIb[][NUM_MUSCLES], float wmn_mn[][NUM_MUSCLES] )
{
	// angonists map
	const float ang_map [NUM_MUSCLES][NUM_MUSCLES]= {
                           {0, 1, 0, 0, 0, 0},
                           {1, 0, 0, 0, 0, 0},
                           {0, 0, 0, 1, 0, 0},
                           {0, 0, 1, 0, 0, 0},
                           {0, 0, 0, 0, 0, 1},
                           {0, 0, 0, 0, 1, 0}
                          };
	// agonists map
	const float ag_map [NUM_MUSCLES][NUM_MUSCLES]= {
                           {0, 0, 1, 0, 1, 0},
                           {0, 0, 0, 1, 0, 1},
                           {1, 0, 0, 0, 1, 0},
                           {0, 1, 0, 0, 0, 1},
                           {1, 0, 1, 0, 0, 0},
                           {0, 1, 0, 1, 0, 0}
                          };
	// excitation from upper level
	for( int i = 0; i < NUM_MUSCLES; i++ ){
		for( int j = 0; j < NUM_MUSCLES; ++j ){
			wdr_mn[i][j]=(i==j)? 1.65f: 0.f;
			wdr_Ia[i][j]=(i==j)? 0.3f: 0.f;
			wdr_Ib[i][j]=(i==j)? 0.3f: 0.f;
		}
	}
	// motoneurons connections, excitation MN->RC
	for( int i = 0; i < NUM_MUSCLES; i++ ){
		for( int j = 0; j < NUM_MUSCLES; ++j ){
			wmn_rc[i][j]=(i==j)? 0.25f: 0.f;
		}
	}
	// Renshaw cells connections
	for( int i = 0; i < NUM_MUSCLES; i++ ){
		for( int j = 0; j < NUM_MUSCLES; ++j ){
			wrc_Ia[i][j] = ( i==j )? -0.25f: 0.f;    // inhibition RC->Ia
			wrc_mn[i][j] = ( i==j )? -0.25f: 0.f;    // inhibition RC->MN 
			wrc_mn[i][j] -= 0.125f*ag_map[i][j];
			wrc_rc[i][j] = -0.25f*ang_map[i][j]; //  inhibition RC->RC ("antogonists")
		}
	}
	// Ia reciprocal inhibition
	for( int i = 0; i < NUM_MUSCLES; i++ ){
		for( int j = 0; j < NUM_MUSCLES; ++j ){
			wia_Ia[i][j] =-0.25f*ang_map[i][j]; // inhibition on antogonists Ia->Ia
			wia_MN[i][j] =-0.25f*ang_map[i][j]; // inhibition on antogonists Ia->MN
		}
	}
	// Ib non-reciprocal inhibition
	for( int i = 0; i < NUM_MUSCLES; ++i ){
		for( int j = 0; j < NUM_MUSCLES; ++j ){
			wib_MN[i][j] = ( i==j )? -0.25f: 0.f; // inhibition RC->Ia
			wib_MN[i][j] -= 0.125f*ag_map[i][j];
		}
	}
	// feedbacks connections
	for( int i = 0; i < NUM_MUSCLES; ++i ){
		for( int j = 0; j < NUM_MUSCLES; ++j ){
			wib_IaMN[i][j] = (i==j)?0.15f: 0.f;
			wib_IaIa[i][j] = (i==j)?0.15f: 0.f;
			wib_IbIb[i][j] = (i==j)?0.15f: 0.f;
		}
	}
	// MN to MN
	for( int i = 0; i < NUM_MUSCLES; ++i ){
		for( int j = 0; j < NUM_MUSCLES; ++j ){
			wmn_mn[i][j] = 0.f; 
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// former Activationfunction
static float spn_activation( float y[NUM_MUSCLES], float out[NUM_MUSCLES] )
{
	float res = 0.f;
	for( int i = 0; i < NUM_MUSCLES; ++i ){
		float y_pre = out[i];
		out[i] = B[i]+Z[i]*y[i];
		out[i] = 1.f/( 1.f+float( exp( -(out[i]-SPN_V12 )/SPN_Slope )));
		res += ( fabs( out[i]-y_pre )) > EpsY? 1.f:0.f;
        }
	return 0.f; // temporal for backward compatibility
//	return res;
}

///////////////////////////////////////////////////////////////////////////////
// former WeightedInput (wsum should be initialized elsewhere)
static void spn_wsum( float w[NUM_MUSCLES][NUM_MUSCLES], float x[NUM_MUSCLES], float wsum[NUM_MUSCLES] )
{
	for( int i = 0; i < NUM_MUSCLES; ++i ){
		for( int k = 0; k < NUM_MUSCLES; ++k ){
			wsum[i] += (w[i][k]*x[k]);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// former sp_cord
void sp_cord( float fb_Ia[NUM_MUSCLES], float fb_Ib[NUM_MUSCLES], float drive[NUM_MUSCLES], float mn[NUM_MUSCLES] )
{
        //  Calculating Weighted input
	float wdr_MN[NUM_MUSCLES][NUM_MUSCLES] = {0.f}, wdr_Ia[NUM_MUSCLES][NUM_MUSCLES] = {0.f}, wdr_Ib[NUM_MUSCLES][NUM_MUSCLES] = {0.f};
	float wmn_RC[NUM_MUSCLES][NUM_MUSCLES] = {0.f}, wmn_MN[NUM_MUSCLES][NUM_MUSCLES] = {0.f};
	float wrc_Ia[NUM_MUSCLES][NUM_MUSCLES] = {0.f}, wrc_MN[NUM_MUSCLES][NUM_MUSCLES] = {0.f}, wrc_RC[NUM_MUSCLES][NUM_MUSCLES] = {0.f};
	float wia_Ia[NUM_MUSCLES][NUM_MUSCLES] = {0.f}, wia_MN[NUM_MUSCLES][NUM_MUSCLES] = {0.f}, wib_MN[NUM_MUSCLES][NUM_MUSCLES] = {0.f};
	float wfb_IaMN[NUM_MUSCLES][NUM_MUSCLES] = {0.f}, wfb_IaIa[NUM_MUSCLES][NUM_MUSCLES] = {0.f}, wfb_IbIb[NUM_MUSCLES][NUM_MUSCLES] = {0.f};
	init_connections( wdr_MN, wdr_Ia, wdr_Ib, wmn_RC, wrc_Ia, wrc_MN, wrc_RC, wia_Ia, wia_MN, wib_MN, wfb_IaMN, wfb_IaIa, wfb_IbIb, wmn_MN );
	float rc[NUM_MUSCLES] = {0.f}, ia[NUM_MUSCLES] = {0.f}, ib[NUM_MUSCLES] = {0.f};
	for( int j = 0; j < NUM_MUSCLES; j++ ){ 
		rc[j] = 0.001f*rand()/(float)RAND_MAX;  // randomize the outputs of the neurons (try to avoid bistability) 
		ia[j] = 0.001f*rand()/(float)RAND_MAX; 
		ib[j] = 0.001f*rand()/(float)RAND_MAX;  	
        }
	float y_drmn[NUM_MUSCLES] = {0.f}, y_drIa[NUM_MUSCLES] = {0.f}, y_drIb[NUM_MUSCLES] = {0.f}, y_mnrc[NUM_MUSCLES] = {0.f};
	float y_Iamn[NUM_MUSCLES] = {0.f}, y_Ibmn[NUM_MUSCLES] = {0.f}, y_fbIamn[NUM_MUSCLES]={0.f}, Y_fbIaIa[NUM_MUSCLES] = {0.f}, Y_fbIbIb[NUM_MUSCLES]={0.f};
	spn_wsum( wdr_MN, drive, y_drmn);
	spn_wsum( wdr_Ia, drive, y_drIa);
	spn_wsum( wdr_Ib, drive, y_drIb);
	spn_wsum( wfb_IaMN, fb_Ia, y_fbIamn);
	spn_wsum( wfb_IaIa, fb_Ia, Y_fbIaIa);
	spn_wsum( wfb_IbIb, fb_Ib, Y_fbIbIb);
	float y_rcIa[NUM_MUSCLES] = {0.f}, y_rcmn[NUM_MUSCLES] = {0.f}, y_rcrc[NUM_MUSCLES] = {0.f}, y_IaIa[NUM_MUSCLES] = {0.f};
	float y_mn[NUM_MUSCLES] = {0.f}, y_rc[NUM_MUSCLES] = {0.f}, y_Ia[NUM_MUSCLES] = {0.f}, y_Ib[NUM_MUSCLES] = {0.f};
	float res1 = 0.f, res2 = 0.f, res3 = 0.f, res4 = 0.f;
	do{
		spn_wsum( wmn_RC, mn, y_mnrc);
		spn_wsum( wrc_Ia, rc, y_rcIa);  
		spn_wsum( wrc_MN, rc, y_rcmn);  
		spn_wsum( wrc_RC, rc, y_rcrc);  
		spn_wsum( wia_Ia, ia, y_IaIa);
		spn_wsum( wia_MN, ia, y_Iamn);
		spn_wsum( wib_MN, ib, y_Ibmn);

		for( int j = 0; j < NUM_MUSCLES; j++ ){
			y_mn[j] = y_drmn[j]+y_rcmn[j]+y_Iamn[j]+y_Ibmn[j]+y_fbIamn[j];
			y_rc[j] = y_mnrc[j]+y_rcrc[j];
			y_Ia[j] = y_drIa[j]+y_rcIa[j]+y_IaIa[j]+Y_fbIaIa[j];
			y_Ib[j] = y_drIb[j]+Y_fbIbIb[j];
		}
	     
		res1 = spn_activation( y_mn, mn );
		res2 = spn_activation( y_rc, rc );
		res3 = spn_activation( y_Ia, ia );
		res4 = spn_activation( y_Ib, ib );
	} while(( res1+res2+res3+res4 ) > 0.f );
}

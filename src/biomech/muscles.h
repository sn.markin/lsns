#include "biomech.h"

#ifndef __MUSCLES_H
#define __MUSCLES_H

extern void m_force( float *mn, float *l, float *v, float *f );
extern void m_geometry( float *theta, float *theta_v, float l[NUM_MUSCLES], float v[NUM_MUSCLES], float h_up[NUM_MUSCLES], float h_dw[NUM_MUSCLES] );
extern void m_feedback( float *l, float *v, float *mn, float *f, float *fb_Ia, float *fb_Ib, float *fb_II );

#endif /*__MUSCLES_H*/

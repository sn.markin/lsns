#include "netasm.h"
#include "utilities.h"
#include <algorithm>

__NASM_NAMESPASE_BEGIN
///////////////////////////////////////////////////////////////////////////////
// calculate hash value for weighed sum
wsum::hashws get_hash( const wsum::srcmap &src )
{
	size_t hash = 0;
	for( size_t i = 0; i < src.size(); ++i ){
		hash_combine( hash, src[i].first );
		hash_combine( hash, src[i].second );
	}
	return hash;
}
///////////////////////////////////////////////////////////////////////////////
// class wsum
wsum::pos wsum::Src0 = 0;
wsum::pos wsum::Trg0 = 0;
///////////////////////////////////////////////////////////////////////////////
// create synapse for specific type of summation
bool wsum::create_syn( wstype t, wspar par )
{
	if( t < LSNS_MAX_SYNS && t > LSNS_CLONE_SYN){
		WSMap.clear();
		SrcMap.clear();
		Type = t;
		WSMap.push_back( par );
		return true;
	}
	return false;
}
///////////////////////////////////////////////////////////////////////////////
// add targets to the synapse
bool wsum::add_ws( const wspar &ws, bool major )
{
	if( Type < LSNS_MAX_SYNS && Type > LSNS_CLONE_SYN){
		if(!( major && !WSMap.empty() && ws == WSMap[0] )){ // insert target if necessary
			WSMap.push_back( ws );
		}
		return true;
	}
	return false;
}
///////////////////////////////////////////////////////////////////////////////
// add connections to the synapse
bool wsum::add_src( const srcmap &src )
{
	if( Type < LSNS_MAX_SYNS && Type > LSNS_CLONE_SYN ){
		SrcMap.insert( SrcMap.end(), src.begin(), src.end() );
		std::sort( SrcMap.begin(), SrcMap.end());
		SrcMap.erase( std::unique( SrcMap.begin(), SrcMap.end() ), SrcMap.end() ); // make sure to use unique connections
		return true;
	}
	return false;
}
///////////////////////////////////////////////////////////////////////////////
// map particular synapses and connections to network structure 
// (memory for weighted sum and synapses must be allocated before)
wsum::wstrg wsum::map2mem( netdat *data, synpar *par, std::vector<wstrg> &wsyn )
{
	wsyn.clear();
	pos srcnum4 = size4_src(); // specify number of sources
	for( pos src0 = wsum::Src0, wspos = wsum::Trg0, i = 0; i < ( pos )WSMap.size(); ++wspos, ++i ){
		wspar parpos = WSMap[i];						// specify parameters of summation
		// initialize Wsyn for current synapse
		_wsyn_total( data->Wsum.Wsyn[wspos] ) = 0.f;				// total sum
		_wsyn_ah( data->Wsum.Wsyn[wspos] ) = _synA( par[parpos] );		// rate of transmitter release
		_wsyn_edt( data->Wsum.Wsyn[wspos] ) = _synEdt( par[parpos] );		// parameter edt
		_wsyn_dt( data->Wsum.Wsyn[wspos] ) = _synDt( par[parpos] );		// parameter dt
		// initialize SynType for current synapse
		_syn_type( data->Wsum.SynType[wspos] ) = ( i )? LSNS_CLONE_SYN: Type;	// type of summation
		_syn_par( data->Wsum.SynType[wspos] ) = parpos;				// parameters of summation
		_syn_size( data->Wsum.SynType[wspos] ) = ( i )? 0: srcnum4;		// number of sources for major weighted sum or 0
		_syn_lut( data->Wsum.SynType[wspos] ) = ( i )? wsum::Trg0: src0;	// offset for Wall and SrcLUT arrays for major weighted sum or offset of major weighted sum for minor synapse
		// initialize SrcLUT and Wall for current target (only ones for major weighted sum)
		for( pos cur_pos = src0, index = 0; ( i == 0 )&&( cur_pos < src0+srcnum4 ); ++cur_pos, index += 4 ){
			map_ws2mem( data->Wsum.SrcLUT+cur_pos, data->Wsum.Wall+cur_pos, index );
		}
		wsyn.push_back( std::make_pair( wspos, parpos ));			// store offset in data->Wsum.Wsyn array
	}
	wsum::Src0 += srcnum4;
	wsum::Trg0 += pos( wsyn.size() );
	return ( wsyn.size() )? wsyn[0]: std::make_pair( pos( -1 ), wstype( -1 ));	// offset in data->Wsum.Wsyn array for major weighted sum
}
///////////////////////////////////////////////////////////////////////////////
// map 4 consequence weights and sources to correspondent w and src
void wsum::map_ws2mem( uint4 *src, float4 *w, pos index )
{
	float4 w_ = { 0.f, 0.f, 0.f, 0.f };
	uint4 src_= { 0, 0, 0, 0 };
	if( index < SrcMap.size()){
		src_.x = SrcMap[index].first;
		w_.x = SrcMap[index].second;
	}
	if( ++index < SrcMap.size()){
		src_.y = SrcMap[index].first;
		w_.y = SrcMap[index].second;
	} 
	if( ++index < SrcMap.size()){
		src_.z = SrcMap[index].first;
		w_.z = SrcMap[index].second;
	} 
	if( ++index < SrcMap.size()){
		src_.w = SrcMap[index].first;
		w_.w = SrcMap[index].second;
	} 
	*src = src_;
	*w = w_;
}
///////////////////////////////////////////////////////////////////////////////
// class mapsyn maps all synapses and connections to network structure 
// (memory for weighted sum and synapses must be allocated before)
///////////////////////////////////////////////////////////////////////////////
// add_synapse
void mapsyn::add_synapse( const syntype &syn, const syntrg &trg, wsum::srcmap &src, bool compress )
{
	if( !src.empty()){
		syntrg_ st( syn.Gmax, trg.Syn, trg.Vm, trg.Eds );
		std::sort( src.begin(), src.end() );
		src.erase( std::unique( src.begin(), src.end() ), src.end() ); // make sure to use unique connections
		wsum::hashws hash = wsum::get_hash( src );
		if( compress ){
			std::pair<mit, mit> range = SynMap.equal_range( hash );
			for( mit x = range.first; x != range.second; ++x ){
				if( x->second.WSum.Type == syn.Type && x->second.WSum == src ){
					x->second.TrgMap.insert( std::make_pair( syn.Par, st ));
					x->second.WSum.add_ws( syn.Par, true ); // TODO: try insert major type of synapses (change the logic for adaptation)
					return;
				}
			}
		}
		std::pair<wsum::wspar, syntrg_> _trg = std::make_pair( syn.Par, st );
		add_newsyn( syn.Type, _trg, src, hash );
	}
}
///////////////////////////////////////////////////////////////////////////////
// map2mem
void mapsyn::map2mem(  netdat *data, synpar *par )
{
	typedef trgmap_::map_::iterator trg_it;
	wsum::resetmap();
	std::vector<wsum::wstrg> ws_all;
	float4 *chang = data->Channels.ChanG;
	uint4 *chan_type = data->Channels.ChanType;
	uint4 *chan_lut = data->Channels.ChanLUT;
	uint4 *vchan_lut = data->Cells.VChanGLUT;
	for( mit _syn = SynMap.begin(); _syn != SynMap.end(); ++_syn ){
		ws_all.clear();
		wsum::wstrg ws0 = _syn->second.WSum.map2mem( data, par, ws_all );
		__lsns_assert( ws0.first != -1 && ws0.second != -1 );
		for( size_t j = 0; j < ws_all.size(); ++j ){
			std::pair<trg_it, trg_it> range = _syn->second.TrgMap.equal_range( ws_all[j].second );
			for( trg_it it = range.first; it != range.second; ++it ){
				wsum::pos chan_pos = it->second.Syn;			// position in ChanG
				wsum::pos cell_pos = it->second.Vm;			// position in CellV
				wsum::pos ion_pos = it->second.Eds;			// position in IonE
				_gate_typem( chan_type[chan_pos] ) = LSNS_SYNAPSE;	// channel activation - synapse
				_gate_parm( chan_type[chan_pos] ) = ws_all[j].second;	// activation parameters - parameters of synaptic summation
				_chan_lutv( chan_lut[chan_pos] ) = cell_pos;		// reference to membrane potential of target cell
				_chan_lute( chan_lut[chan_pos] ) = ion_pos;		// reference to reversal potential of correspondent ions
				_chan_lutm( chan_lut[chan_pos] ) = ws_all[j].first;	// reference to weighted sum in Wsyn array
				_chan_gmax( chang[chan_pos] ) = it->second.Gmax;	// maximal conductance
				map2lut( vchan_lut, cell_pos, chan_pos, MAX_CHAN_PER_CELL ); // build up channel's look-up-table for cells
			}
		}
	}
}
///////////////////////////////////////////////////////////////////////////////
// add new synapse
void mapsyn::add_newsyn( syntype::wstype type, std::pair<wsum::wspar, syntrg_> &trg, const wsum::srcmap &src, wsum::hashws hash )
{
	trgmap_ tm;
	tm.WSum.create_syn( type, trg.first );
	tm.TrgMap.insert( trg );
	synmap::iterator p = SynMap.insert( std::make_pair( hash, tm ));
	p->second.WSum.add_src( src );
}

///////////////////////////////////////////////////////////////////////////////
// class nasm 
// 'get_ionpos' get position of ion relative to iondat array
__lsns_pos32 nasm::get_ionpos( __lsns_uint32 ion_id, __lsns_pos32 unit_pos )
{
	if( unit_pos < Net.NetDim.MaxCells+Net.NetDim.MaxUnits ){
		uint4 *chang_lut = Net.Data->Cells.VChanGLUT+unit_pos*( MAX_CHAN_PER_CELL/4+1 );
		__lsns_uint32 *lut_ = ( __lsns_uint32 *)chang_lut;
		__lsns_uint32 counter = lut_[0];
		for( __lsns_pos32 i = 0, pos_ = lut_[1]; i < counter && counter != -1; ++i, pos_ = lut_[i+1] ){ // forloop through all channels
/*
			uint4 ctype_ = Net.Data->Channels.ChanType[pos_];
			uint4 clut_ = Net.Data->Channels.ChanLUT[pos_];
			__lsns_uint32 chan_id_ = make_chanid( _gate_typem( ctype_ ), _gate_parm( ctype_ ), _gate_typeh( ctype_ ), _gate_parh( ctype_ ));
			__lsns_pos32 ion_pos_ = _chan_lute(  clut_ );
			__lsns_pos32 unit_pos_ = _chan_lutv(  clut_ );
*/
		};
	}
	return -1;
}
/*
///////////////////////////////////////////////////////////////////////////////
// 'get_ionpos' get position of ions relative to iondat array
__lsns_pos32 get_ionpos( __lsns_uint32 ion_id, __lsns_pos32 unit_pos )
{
	uint4 *chang_lut = Data->Cells.VChanGLUT+unit_pos*( MAX_CHAN_PER_CELL/4+1 );
	__lsns_uint32 *lut_ = ( __lsns_uint32 *)chang_lut;
	__lsns_uint32 counter = lut_[0];
	for( __lsns_pos32 i = 0, pos_ = lut_[1]; i < counter && counter != -1; ++i, pos_ = lut_[i+1] ){
		uint4 clut_ = Data->Channels.ChanLUT[pos_];
		__lsns_pos32 unit_pos_ = _chan_lutv(  clut_ );
		if( unit_pos == unit_pos_ ){
			return pos_;
		}
	}
	return -1;
};
*/
///////////////////////////////////////////////////////////////////////////////
// 'get_chanpos' get position of channel relative to chandat array
__lsns_pos32 nasm::get_chanpos( __lsns_uint32 chan_id, __lsns_pos32 ion_pos, __lsns_pos32 unit_pos )
{
	if( unit_pos < Net.NetDim.MaxCells+Net.NetDim.MaxUnits ){
		uint4 *chang_lut = Net.Data->Cells.VChanGLUT+unit_pos*( MAX_CHAN_PER_CELL/4+1 );
		__lsns_uint32 *lut_ = ( __lsns_uint32 *)chang_lut;
		__lsns_uint32 counter = lut_[0];
		for( __lsns_pos32 i = 0, pos_ = lut_[1]; i < counter && counter != -1; ++i, pos_ = lut_[i+1] ){ // forloop through all channels
			uint4 ctype_ = Net.Data->Channels.ChanType[pos_];
			uint4 clut_ = Net.Data->Channels.ChanLUT[pos_];
			__lsns_uint32 chan_id_ = make_chanid( _gate_typem( ctype_ ), _gate_parm( ctype_ ), _gate_typeh( ctype_ ), _gate_parh( ctype_ ));
			__lsns_pos32 ion_pos_ = _chan_lute(  clut_ );
			__lsns_pos32 unit_pos_ = _chan_lutv(  clut_ );
			if( chan_id == chan_id_ && ion_pos == ion_pos_ && unit_pos == unit_pos_ ){
				return pos_;
			}
		}
	}
	return -1;
}
///////////////////////////////////////////////////////////////////////////////
// 'get_synpos' get position of synapse relative to chandat array
__lsns_pos32 nasm::get_synpos( __lsns_uint32 syn_id, __lsns_pos32 ion_pos, __lsns_pos32 unit_pos )
{
	return get_chanpos( syn_id, ion_pos, unit_pos );
}
///////////////////////////////////////////////////////////////////////////////
// 'getw_pos' provides access to any network variable allocated on host computer
__lsns_pos64 nasm::get_wpos( __lsns_uint32 syn_id, __lsns_pos32 ion_pos, __lsns_pos32 trg_pos, __lsns_pos32 src_pos )
{
	__lsns_pos32 syn_pos = get_synpos( syn_id, ion_pos, trg_pos );
	return get_wpos( syn_pos, trg_pos, src_pos );
}
///////////////////////////////////////////////////////////////////////////////
// 'getw_pos' provides access to any network variable allocated on host computer
__lsns_pos64 get_wpos( __lsns_uint32 syn_pos, __lsns_pos32 trg_pos, __lsns_pos32 src_pos )
{
	if( syn_pos != -1 ){
		__lsns_pos32 wsum_pos = _chan_lutm( Net.Data->Channels.ChanLUT[syn_pos] );
		__lsns_pos32 wsum_size = _syn_size( Net.Data->Wsum.SynType[wsum_pos] )
		if( _syn_type( Net.Data->Wsum.SynType[wsum_pos] ) == LSNS_CLONE_SYN && wsum_size == 0 ){
			wsum_pos = _syn_lut( Net.Data->Wsum.SynType[wsum_pos] );
			wsum_size = _syn_size( Net.Data->Wsum.SynType[wsum_pos] );
		}
                __lsns_pos32 *src_lut = Net.Data->Wsum.SrcLUT+wsum_pos;
		__lsns_pos32 offset = _syn_lut( Net.Data->Wsum.SynType[wsum_pos] );
		__lsns_pos32 bank = -1;
		for( __lsns_pos32 i = 0; i < wsum_size; ++i ){
			if( src_lut[i].x == src_pos ){
				bank = 0;
			}
			else if( src_lut[i].y == src_pos ){
				bank = 1;
			}
			else if( src_lut[i].z == src_pos ){
				bank = 2;
			}
			else if( src_lut[i].w == src_pos ){
				bank = 3;
			}
			if( bank != -1 ){
				return make_epos64( bank, offset+i );
			}
		}
	}
	return -1;
}
///////////////////////////////////////////////////////////////////////////////
// 'add_ion' get position of ion relative to iondat array
__lsns_pos32 nasm::add_ion( __lsns_uint32 ion_id, __lsns_pos32 unit_pos )
{
	__lsns_pos32 ion_pos_ = get_ionpos( ion_id, unit_pos );
	if( ion_pos_ != -1 ){
		return ion_pos_;
	}
	if( IonPos < Net.NetDim.MaxIons-1 && unit_pos < Net.NetDim.MaxCells+Net.NetDim.MaxUnits ){
		ion_pos_ = IonPos++;
		init_ion( ion_id, ion_pos_, unit_pos );
	}
	return -1;
}
///////////////////////////////////////////////////////////////////////////////
// 'add_syn'
__lsns_pos32 nasm::add_syn( __lsns_uint32 syn_id, __lsns_pos32 unit_pos, __lsns_pos32 ion_pos )
{
	return add_chan( syn_id, unit_pos, ion_pos, -1, -1 );
}
///////////////////////////////////////////////////////////////////////////////
// 'add_chan'
__lsns_pos32 nasm::add_chan( __lsns_uint32 chan_id, __lsns_pos32 unit_pos, __lsns_pos32 ion_pos, __lsns_pos32 ion_mpos = -1, __lsns_pos32 ion_hpos = -1 )
{
	__lsns_pos32 chan_pos_ = get_chanpos( chan_id, unit_pos, ion_pos );
	if( chan_pos_ != -1 ){
		return chan_pos_;
	}
	if( ChanPos < Net.NetDim.MaxChan-1 && unit_pos < Net.NetDim.MaxCells+Net.NetDim.MaxUnits ){
		chan_pos_ = ChanPos++;
		map2lut( Net.Data->Cells.VChanGLUT, unit_pos, chan_pos_, MAX_CHAN_PER_CELL );
		init_chan( chan_id, chan_pos_, unit_pos, ion_pos, ion_mpos, ion_hpos );
		return chan_pos_;
	}
	return -1; // not enough memory 
}
///////////////////////////////////////////////////////////////////////////////
// 'add_cell'
__lsns_pos32 nasm::add_cell( void )
{
	return -1; // not enough memory 
}

///////////////////////////////////////////////////////////////////////////////
// 'set_gmax'
bool nasm::set_changmax( __lsns_pos32 chan_pos, float gmax )
{
	if( chan_pos != -1 || chan_pos < Net.NetDim.MaxChan ){
		_chan_gmax( Net.Data->Channels.ChanG[chan_pos] ) = gmax;
		return true;
	}
	return false;
}
///////////////////////////////////////////////////////////////////////////////
// 'set_mh0'
bool nasm::set_chanmh0( __lsns_pos32 chan_pos, float m0, float h0, float pm, float ph  )
{
	if( chan_pos != -1 || chan_pos < Net.NetDim.MaxChan ){
		_gate_m( Net.Data->Channels.ChanMH[chan_pos] ) = m0;
		_gate_h( Net.Data->Channels.ChanMH[chan_pos] ) = h0;
		_gate_powm( Net.Data->Channels.ChanMH[chan_pos] ) = pm;
		_gate_powh( Net.Data->Channels.ChanMH[chan_pos] ) = ph;
		return true;
	}
	return false;
}
///////////////////////////////////////////////////////////////////////////////
// 'set_ione'
bool nasm::set_ione( __lsns_pos32 ion_pos, float eds, float in, float out, float rtfz )
{
	if( ion_pos != -1 || ion_pos < Net.NetDim.MaxIons ){
		_ions_eds( Net.Data->Ions.IonsE[ion_pos] ) = eds;
		_ions_in( Net.Data->Ions.IonsE[ion_pos] ) = in;
		_ions_out( Net.Data->Ions.IonsE[ion_pos] ) = out;
		_ions_rtfz( Net.Data->Ions.IonsE[ion_pos] ) = rtfz;
		return true;
	}
	return false;
}
///////////////////////////////////////////////////////////////////////////////
// 'set_ioni'
bool nasm::set_ioni( __lsns_pos32 ion_pos, float ipump, float ichan, float z = -1, float w = -1 )
{
	if( ion_pos != -1 || ion_pos < Net.NetDim.MaxIons ){
		_ions_ipump( Net.Data->Ions.IonsI[ion_pos] ) = ipump;
		_ions_ichan( Net.Data->Ions.IonsI[ion_pos] ) = ichan;
		_ions_iz( Net.Data->Ions.IonsI[ion_pos] ) = z;
		_ions_iw( Net.Data->Ions.IonsI[ion_pos] ) = w;
		return true;
	}
	return false;
}
bool nasm::set_iondyn( __lsns_pos32 ion_pos, __lsns_pos32 chan_pos )
{
	if( chan_pos != -1 || ion_pos != -1 || chan_pos < Net.NetDim.MaxChan || ion_pos < Net.NetDim.MaxIons ){
		map2lut( Net.Data->Ions.IChanGLUT, ion_pos, chan_pos, MAX_CHAN_PER_PUMP );
		return true;
	}
	return false;
}
///////////////////////////////////////////////////////////////////////////////
// 'map2lut' modify look-up-table of network elements (cells, drives, outputs etc)
// which refer to ions currents involved in dynamics of membrane potential
// 'lut' - array of look-up-tables for all network elements 
// 'lut_pos' - position of current network element
// 'lut_ref' - actual number @ lut[lut_pos+counter]
// 'size' - maximal size of look-up-table
void nasm::map2lut( uint4 *lut, __lsns_uint32 lut_pos, __lsns_uint32 lut_ref, __lsns_uint32 size )
{
	lut = lut+lut_pos*( size/4+1 );			// refer to look-up-table of current network element
	__lsns_uint32 *lut_ = ( __lsns_uint32 *)lut;
	__lsns_uint32 counter = lut_[0];
	counter = ( counter == -1 )? 0:counter;
	__lsns_assert( counter < size-1 );		// make sure the counter is not exceed its maximal number
	lut_[0] = ++counter;
	lut_[counter] = lut_ref;
}
///////////////////////////////////////////////////////////////////////////////
// 'init_ion'
void nasm::init_ion( __lsns_uint32 ion_id, __lsns_pos32 ion_pos, __lsns_pos32 unit_pos )
{
	memset( Net.Data->Ions.IonsE+ion_pos, 0, sizeof( float4 )); // IonsE must be initialized later
	memset( Net.Data->Ions.IonsI+ion_pos, 0, sizeof( float4 )); // IonsI must be initialized later
	_ions_lut_v( Net.Data->Ions.IonsLUT[ion_pos] ) = unit_pos;
	_ions_typeeds( Net.Data->Ions.IonsType[ion_pos] )  = get_hihi( ion_id );
	_ions_typepump( Net.Data->Ions.IonsType[ion_pos] ) = get_hilo( ion_id );
	_ions_typepar( Net.Data->Ions.IonsType[ion_pos] ) =  get_lohi( ion_id );
	_ions_typew( Net.Data->Ions.IonsType[ion_pos] ) =    get_lolo( ion_id );
}
///////////////////////////////////////////////////////////////////////////////
// 'init_chan'
void nasm::init_chan( __lsns_uint32 chan_id, __lsns_pos32 chan_pos, __lsns_pos32 unit_pos, __lsns_pos32 ion_pos, __lsns_pos32 ion_mpos, __lsns_pos32 ion_hpos )
{
	memset( Net.Data->Channels.ChanG+chan_pos, 0, sizeof( float4 ));  // ChanG must be initialized later
	memset( Net.Data->Channels.ChanMH+chan_pos, 0, sizeof( float4 )); // ChanMH must be initialized later
	_gate_typem( Net.Data->Channels.ChanType[chan_pos] ) = get_hihi( chan_id );
	_gate_parm( Net.Data->Channels.ChanType[chan_pos] ) =  get_hilo( chan_id );
	_gate_typeh( Net.Data->Channels.ChanType[chan_pos] ) = get_lohi( chan_id );
	_gate_parh( Net.Data->Channels.ChanType[chan_pos] ) =  get_lolo( chan_id );
	_chan_lutv( Net.Data->Channels.ChanLUT[chan_pos] ) = unit_pos;
	_chan_lute( Net.Data->Channels.ChanLUT[chan_pos] ) = ion_pos;
	_chan_lutm( Net.Data->Channels.ChanLUT[chan_pos] ) = ion_mpos;
	_chan_luth( Net.Data->Channels.ChanLUT[chan_pos] ) = ion_hpos;
}

__NASM_NAMESPASE_END

#pragma once

#ifndef __LSNS_ENGINE_H
#define __LSNS_ENGINE_H

#define GRN_XBLOCKS		4			/* granularity of x blocks in 2-D grid */
#define NUM_XGRN		4			/* number of granules in x blocks in 2-D grid */
#define MAX_XBLOCKS		GRN_XBLOCKS*NUM_XGRN	/* maximal number of x blocks in 2-D grid*/
#define NUM_THREADS		192			/* maximal number of threads per block*/
#define MAX_CONST_MEM		65355			/* size of constant memory */
#define MAX_DEV_MEM		0x3FFFFFFFF		/* RAM = 12 Gbytes on device*/

#include "netmap.h"
#include "iodata.h" //Added by Taegyo

///////////////////////////////////////////////////////////////////////////////
// kernel metrics for x86 architecture
class x86metric{
	public: // constructors/destructor
		x86metric( void ){};
		~x86metric( void ){};
	public: // operators
		size_t operator() ( size_t nthreads, size_t &nt, size_t &bx, size_t &by ){
			bx = by = 1;
			#if defined( __LSNS_DEBUG__ )
				nthreads = ( nthreads < NUM_THREADS )? NUM_THREADS: nthreads;
				nt = ( nthreads%NUM_THREADS == 0 )? nthreads: ( nthreads/NUM_THREADS+1 )*NUM_THREADS;
			#else
				nt = nthreads;
			#endif /* __LSNS_DEBUG__ */
			return nt*bx*by;
		};
};

///////////////////////////////////////////////////////////////////////////////
// kernel metrics for CUDA architecture
class cudametric{
	public: // constructors/destructor
		cudametric( void ){};
		~cudametric( void ){};
	public: // operators
		size_t operator() ( size_t nthreads, size_t &nt, size_t &bx, size_t &by ){
			bx = by = 1; nt = NUM_THREADS;
			nthreads = ( nthreads < NUM_THREADS )? NUM_THREADS: nthreads;
			nthreads = ( nthreads%NUM_THREADS == 0 )? nthreads: ( nthreads/NUM_THREADS+1 )*NUM_THREADS;
			size_t nblocks = ( nthreads%NUM_THREADS == 0 )? nthreads/NUM_THREADS: nthreads/NUM_THREADS+1;
			if( nblocks < MAX_XBLOCKS ){
				bx = nblocks;
			}
			else{
				nblocks = ( nblocks%GRN_XBLOCKS == 0 )? nblocks: ( nblocks/GRN_XBLOCKS+1 )*GRN_XBLOCKS;
				for( int i = MAX_XBLOCKS/GRN_XBLOCKS; i > 0; --i ){
					if( nblocks%(i*GRN_XBLOCKS) == 0 ){
						bx = i*GRN_XBLOCKS;
						by = nblocks/(i*GRN_XBLOCKS);
						break;
					}
				}
			}
			return nt*bx*by;
		}
};

///////////////////////////////////////////////////////////////////////////////
// kdim3 optimize kernel metrics according to device architecture
template <typename metric>
class kdim3{
	public: // constructors/destructor
		kdim3( void ) : NT( 1 ), BX( 1 ), BY( 1 ){};
		kdim3( size_t nthreads ){ metric m; m( nthreads, NT, BX, BY ); };
		kdim3( const kdim3 &dim ) : NT( dim.NT ), BX( dim.BX ), BY( dim.BY ){};
		~kdim3( void ){};
	public: // operators
		kdim3 &operator = ( size_t nthreads ){ metric m; m( nthreads, NT, BX, BY ); return *this; };
		kdim3 &operator = ( const kdim3 &dim ){ NT = dim.NT; BX = dim.BX; BY = dim.BY; return *this; };
		operator int(){ return int( NT*BX*BY ); }; /* return total number of threads */
		operator size_t(){ return NT*BX*BY; }; /* return total number of threads */
		operator dim3(){ return dim3( BX, BY, 1); }; /* return grid geometry */
	public: // data
		size_t NT;		// number of threads per one block
		size_t BX;		// number of x blocks in 2-D grid
		size_t BY;		// number of y blocks in 2-D grid
};

///////////////////////////////////////////////////////////////////////////////
// netpar structure
typedef struct __lsns_align( 16 ) __network_parameters{
	// data which have to be initialized before memory allocation by 'lsns_alloc' method
	float Threshold;					// threshold for spike discrimination
	float Step;						// integration step
	netdim NetDim;						// number  of network elements
	gatepar Gates[LSNS_MAX_GATEPARS];			// properties of ion channels
	ionspar Ions[LSNS_MAX_IONPARS];				// properties of ion dynamics
	synpar Synapses[LSNS_MAX_SYNPARS];			// properties of synaptic summation
	// data which will initialized after memory allocation by 'lsns_alloc' method
	netdat *Data;						// pointer to network data
} netpar;

///////////////////////////////////////////////////////////////////////////////
// interface methods
extern netdat *lsns_alloc( netpar &par );			// allocate memory on host
//extern void lsns_free( netdat *data );			// free memory both on host and device
extern bool lsns_free( netpar &par );				// !!! Return type and input type errors found by Taegyo 9.1.2015
//extern bool lsns_map2dev( netdat *data, netpar &parpar );	// allocate memory on device and copy the network configuration from host to device.
extern bool lsns_map2dev( netpar &par );			// Error correction: Inconsistency of input arguments between declare and definition. 7/22/2015 Taegyo 
//extern bool lsns_run( float step = -1. );			// run simulation
//extern bool lsns_run( netpar &par, int max_step );		// run simulation
extern bool lsns_run( netpar &par, size_t max_step , vector<nns_iodata>& iodata, float time = 0.f );	//Switch was added by Taegyo

#endif /*__LSNS_ENGINE_H*/

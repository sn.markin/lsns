#pragma once

#ifndef __VIEWS_H
#define __VIEWS_H

#include "config.h"

///////////////////////////////////////////////////////////////////////////////////
//=================================================================================
//View Type definitions
#define VIEW_IONSI_P 0		//IONSI.X PUMP CURRENT
#define VIEW_IONSI_CH 1		//IONSI.Y CHANNEL CURRENT

#define VIEW_IONSE_E 4		//IONSE.X REVERSAL POTENTIAL
#define VIEW_IONSE_CI 5		//IONSE.Y CONCENTRATION OF INSIDE ION
#define VIEW_IONSE_CO 6		//IONSE.Z CONCENTRATION OF OUTSIDE ION

#define VIEW_CHANG_G 9		//CHANG.Y CHANNEL CONDUCTANCE
#define VIEW_CHANG_I 10			//CHANG.Z CHANNEL CURRENT

#define VIEW_CHANMH_AC 12	//CHANMH.X CHANNEL ACTIVATION
#define VIEW_CHANMH_IAC 14	//CHANMH.Z CHANNEL INACTIVATION

#define VIEW_CELLV_V 16		//CELLV.X MEMBRANE POTENTIAL
#define VIEW_CELLV_SP 18	//CELLV.Z SPIKES ONSET
#define VIEW_CELLV_IJ 19	//CELLV.W INJECTED CURRENT

///////////////////////////////////////////////////////////////////////////////
// +'int4vw' is dirty hack that provides easy access to the fields of int4 
// structure
typedef union __lsns_align( 16 ) __int4_vw{
	int4 Dat;
	__lsns_int32 pDat[4];
} int4vw;
///////////////////////////////////////////////////////////////////////////////
// +'uint4vw' is dirty hack that provides easy access to the fields of uint4 
// structure
typedef union __lsns_align( 16 ) __uint4_vw{
	uint4 Dat;
	__lsns_uint32 pDat[4];
} uint4vw;
///////////////////////////////////////////////////////////////////////////////
// +'float4vw' is dirty hack that provides easy access to the fields of float4 
// structure
typedef union __lsns_align( 16 ) __float4_vw{
	float4 Dat;
	float pDat[4];
} float4vw;

#endif /*__VIEWS_H*/

#pragma once

#ifndef __LSNS_MATH_H
#define __LSNS_MATH_H

#include "config.h"
#include "debug.h"

///////////////////////////////////////////////////////////////////////////////
// some useful math macroses
#if defined( __CUDA__ )
	#if defined( __LSNS_DEBUG__ )
		#define lsns_div( x, y ) (( y != 0.f )? fdividef(( x ),( y )): float( __lsns_assert( 0 ) ))
		#define lsns_log( x ) (( x != 0.f )? logf( x ): float( __lsns_assert( 0 ) ))
	#else
		#define lsns_div( x, y ) __fdividef(( x ),( y ))
		#define lsns_log( x ) __logf( x )
	#endif
	#define lsns_pow( x, y ) __powf(( x ),( y ))
	#define lsns_exp( x ) __expf( x )
	#define lsns_cosh( x ) coshf( x )
#else
	#include <cmath>
	__lsns_inline float pow_fast( float x, int e )
	{
	    float y = 1.;
	    while( e ){
		if( e&1 ) y *= x;
		x *= x;
		e >>= 1;
	    }
	    return y;
	}
	#if defined( __LSNS_DEBUG__ )
		#define lsns_div( x, y ) (( y != 0.f )? ( x )/( y ): float( __lsns_assert( 0 ) ))
		#define lsns_log( x ) (( x != 0.f )? log( x ): float( __lsns_assert( 0 ) ))
	#else
		#define lsns_div( x, y ) ( x )/( y )
		#define lsns_log( x ) log( x )
	#endif
	#define lsns_pow( x, y ) pow_fast( x, int( y ))
	#define lsns_exp( x ) exp( x )
	#define lsns_cosh( x ) cosh( x )
#endif
///////////////////////////////////////////////////////////////////////////////
// +'lsns_fastsum' unrolls the calculation of total sum of series that consists 
// of up-to-24 elements and stores the results into 'res'. The series is located 
// in 'data' array. 'fn' is the method to extract the actual number from 'data[i]'. 
// The 'lut' keeps the actual size (<32) of the series and reference to actual 
// position (offset) of the particular element in the array 'data'.
#define lsns_fastsum( fn, data, lut, res, N )\
{\
	uint4 lut1 = __lsns_cached(( lut )[0]), lut2, lut3, lut4, lut5, lut6, lut7, lut8; \
	__lsns_uint32 n = lut1.x; \
	switch( n ){ \
		case 31: case 30: case 29: case 28: \
			lut8 = __lsns_cached(( lut )[7]); \
		case 27: case 26: case 25: case 24: \
			lut7 = __lsns_cached(( lut )[6]); \
		case 23: case 22: case 21: case 20: \
			lut6 = __lsns_cached(( lut )[5]); \
		case 19: case 18: case 17: case 16: \
			lut5 = __lsns_cached(( lut )[4]); \
		case 15: case 14: case 13: case 12: \
			lut4 = __lsns_cached(( lut )[3]); \
		case 11: case 10: case 9: case 8: \
			lut3 = __lsns_cached(( lut )[2]); \
		case 7: case 6: case 5: case 4: \
			lut2 = __lsns_cached(( lut )[1]); \
	} \
	( res  ) = 0; \
	int i = 0; \
	switch( n ){ \
		case 31: \
			i = lut8.w; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 30: \
			i = lut8.z; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 29: \
			i = lut8.y; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 28: \
			i = lut8.x; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 27: \
			i = lut7.w; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 26: \
			i = lut7.z; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 25: \
			i = lut7.y; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 24: \
			i = lut7.x; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 23: \
			i = lut6.w; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 22: \
			i = lut6.z; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 21: \
			i = lut6.y; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 20: \
			i = lut6.x; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 19: \
			i = lut5.w; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 18: \
			i = lut5.z; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 17: \
			i = lut5.y; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 16: \
			i = lut5.x; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 15: \
			i = lut4.w; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 14: \
			i = lut4.z;\
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 13: \
			i = lut4.y; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 12: \
			i = lut4.x; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 11: \
			i = lut3.w; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 10: \
			i = lut3.z;\
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 9: \
			i = lut3.y; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 8: \
			i = lut3.x; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 7: \
			i = lut2.w; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 6: \
			i = lut2.z; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 5: \
			i = lut2.y; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 4: \
			i = lut2.x; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 3: \
			i = lut1.w; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 2: \
			i = lut1.z; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		case 1: \
			i = lut1.y; \
			__lsns_assert( i >= 0 && i < N );\
			res += fn( __lsns_cached(( data )[i])); \
		default:; \
	} \
}
///////////////////////////////////////////////////////////////////////////////
// +'lsns_fastsum2' unrolls the calculation of total sum of series that consists 
// of up-to-24 elements and stores the results into 'res1' and 'res2'. The series 
// is located in 'data' array. 'fn1' and 'fn2' are the methods to extract the 
// actual numbers from 'data[i]'. The 'lut' keeps the actual size (<32) of the 
// series and reference to actual position (offset) of the particular element in 
// the array 'data'.
#define lsns_fastsum2( fn1, fn2, data, lut, res1, res2, N )\
{\
	uint4 lut1 = __lsns_cached(( lut )[0]), lut2, lut3, lut4, lut5, lut6, lut7, lut8; \
	__lsns_uint32 n = lut1.x; \
	switch( n ){ \
		case 31: case 30: case 29: case 28: \
			lut8 = __lsns_cached(( lut )[7]); \
		case 27: case 26: case 25: case 24: \
			lut7 = __lsns_cached(( lut )[6]); \
		case 23: case 22: case 21: case 20: \
			lut6 = __lsns_cached(( lut )[5]); \
		case 19: case 18: case 17: case 16: \
			lut5 = __lsns_cached(( lut )[4]); \
		case 15: case 14: case 13: case 12: \
			lut4 = __lsns_cached(( lut )[3]); \
		case 11: case 10: case 9: case 8: \
			lut3 = __lsns_cached(( lut )[2]); \
		case 7: case 6: case 5: case 4: \
			lut2 = __lsns_cached(( lut )[1]); \
	} \
	( res1  ) = 0; \
	( res2  ) = 0; \
	int i = 0; \
	float4 d; \
	switch( n ){ \
		case 31: \
			i = lut8.w; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 30: \
			i = lut8.z; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 29: \
			i = lut8.y; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 28: \
			i = lut8.x; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 27: \
			i = lut7.w; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 26: \
			i = lut7.z; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 25: \
			i = lut7.y; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 24: \
			i = lut7.x; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 23: \
			i = lut6.w; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 22: \
			i = lut6.z; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 21: \
			i = lut6.y; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 20: \
			i = lut6.x; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 19: \
			i = lut5.w; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 18: \
			i = lut5.z; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 17: \
			i = lut5.y; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 16: \
			i = lut5.x; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 15: \
			i = lut4.w; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 14: \
			i = lut4.z;\
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 13: \
			i = lut4.y; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 12: \
			i = lut4.x; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 11: \
			i = lut3.w; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 10: \
			i = lut3.z;\
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 9: \
			i = lut3.y; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 8: \
			i = lut3.x; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 7: \
			i = lut2.w; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 6: \
			i = lut2.z; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 5: \
			i = lut2.y; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 4: \
			i = lut2.x; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 3: \
			i = lut1.w; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 2: \
			i = lut1.z; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		case 1: \
			i = lut1.y; \
			__lsns_assert( i >= 0 && i < N );\
			d = __lsns_cached(( data )[i]); \
			res1 += fn1( d ); \
			res2 += fn2( d ); \
		default:; \
	} \
}
///////////////////////////////////////////////////////////////////////////////
// +'lsns_sigm' sigmoid function: 1/( 1+exp(-x) )
// + 'lsns_sigm' modified sigmoid function: 1/( 1+exp(-(v-v12 )/slp ))
// +'lsns_euler' 1-step Euler method: y = y+step*y_pre
// +'lsns_exp_euler' 1-step exponential Euler method: y = exp(-step/t )*( y-y_inf )+y_inf
#define lsns_sigm( x ) lsns_div( 1, 1+lsns_exp( -( x )))
#define lsns_msigm( v, v12, slp ) lsns_sigm( lsns_div(( v )-( v12 ), slp ))
#define lsns_euler( y, y_pre, step ) ( y )+( step )*( y_pre )
#define lsns_exp_euler( y, y_inf, step, t ) ( t > 0.f )? lsns_exp(-lsns_div( step, t ))*(( y )-( y_inf ))+( y_inf ): y_inf
#endif /*__LSNS_MATH_H*/

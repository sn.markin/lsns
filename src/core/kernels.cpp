///////////////////////////////////////////////////////////////////////////////
// kernels.cpp must be included directly to engine source file 
#if defined ( __LSNS_ENGINE_CPP ) /* kernels.cpp must be included directly to engine source file */
///////////////////////////////////////////////////////////////////////////////
// parameters of simulation data
//	x - current step of simulation
//	y - maximal steps of simulation
//	z - reserved
//	w - reserved
enum __int4_simdat{
	_sim_pos_nstep	= 0x00,				// current step of simulation
	_sim_pos_maxstep= 0x01,				// maximal steps of simulation
};
#define _sim_nstep( sim ) ( sim ).x
#define _sim_maxstep( sim ) ( sim ).y
///////////////////////////////////////////////////////////////////////////////
// parameters of simulation data
//	x - duration of simulations (msec)
//	y - integration step (msec)
//	z - spike threshold (mV)
//	w - reserved
enum __float4_simdat{
	_sim_pos_t	= 0x00,				// duration of simulations (msec)
	_sim_pos_step	= 0x01,				// integration step (msec)
	_sim_pos_thresh	= 0x02,				// spike threshold (mV)
};
#define _sim_t( sim ) ( sim ).x
#define _sim_step( sim ) ( sim ).y
#define _sim_thresh( sim ) ( sim ).z
///////////////////////////////////////////////////////////////////////////////
// parameters of network dimension
//	x - number of cells
//	y - number of non-spiking units (drives, outputs, feedbacks)
//	z - reserved
//	w - reserved
enum __uint4_netdim{
	_net_pos_ncells	= 0x00,				// number of cells
	_net_pos_nunits	= 0x01,				// number of non-spiking units (drives, outputs, feedbacks)
};
#define _net_ncells( dim ) ( dim ).x
#define _net_nunits( dim ) ( dim ).y
///////////////////////////////////////////////////////////////////////////////
// parameters of dimension of ion channels
//	x - number of channels
//	y - number of gates
//	z - number of gate parameters
//	w - reserved
enum __uint4_chandim{
	_net_pos_nchans	= 0x00,				// number of channels
	_net_pos_ngates	= 0x01,				// number of gates
	_net_pos_ngpars	= 0x02,				// number of gate parameters
};
#define _net_nchans( dim ) ( dim ).x
#define _net_ngates( dim ) ( dim ).y
#define _net_ngpars( dim ) ( dim ).z
///////////////////////////////////////////////////////////////////////////////
// parameters of ions dimension
//	x - number of ions
//	y - number of ion parameters
//	z - reserved
//	w - reserved
enum __uint4_ionsdim{
	_net_pos_nions	= 0x00,				// number of ions
	_net_pos_nipars	= 0x01,				// number of ion parameters
};
#define _net_nions( dim ) ( dim ).x
#define _net_nipars( dim ) ( dim ).y
///////////////////////////////////////////////////////////////////////////////
// parameters of views dimension
//	x - number of viewable parameters
//	y - number of parameters to be viewed
//	z - number of custom kernels
//	w - reserved
enum __uint4_viewsdim{
	_net_pos_maxvw	= 0x00,			// number of viewable parameters
	_net_pos_nviews	= 0x01,			// number of parameters to be viewed
	_net_pos_nkernels= 0x02,		// number of custom kernels
};
#define _net_maxvw( dim ) ( dim ).x
#define _net_nviews( dim ) ( dim ).y
#define _net_nkernels( dim ) ( dim ).z
///////////////////////////////////////////////////////////////////////////////
// parameters of synapses dimension
//	x - number of synapses
//	y - reserved
//	z - reserved
//	w - reserved
enum __uint4_synsdim{
	_net_pos_nsyn	= 0x00,			// number of synapses
	_net_pos_nws	= 0x01,			// number of connections
};
#define _net_nsyn( dim ) ( dim ).x
#define _net_nws( dim ) ( dim ).y
///////////////////////////////////////////////////////////////////////////////
// macroses which define the dimension the network data
#define MAX_IPARS int( _net_nipars( IonDim[0] ))
#define MAX_IONS int( _net_nions( IonDim[0] ))
#define MAX_NWSUM int( _net_nws( SynDim[0] ))    //SM
#define MAX_WSYNS int( _net_nsyn( SynDim[0] ))
#define MAX_CHANS int( _net_nchans( ChanDim[0] ))
#define MAX_GATES int( _net_ngates( ChanDim[0] ))
#define MAX_GPARS int( _net_ngpars( ChanDim[0] ))
#define MAX_CELLS int( _net_ncells( NetDim[0] ))
#define MAX_UNITS int( _net_nunits( NetDim[0] ))
#define MAX_GDATA int( _net_maxvw( ViewDim[0] ))
#define MAX_NVIEWS int( _net_nviews( ViewDim[0] ))
#define MAX_NKERNELS int( _net_nkernels( ViewDim[0] ))

///////////////////////////////////////////////////////////////////////////////
// common data for all networks elements (read-only)
__lsns_constant float4 SimDat[1] = { 0.f, -1.f, -10.f, 0.f };
__lsns_constant __lsns_align( 16 ) uint4 SynDim[1] = { 0, 0, 0, 0 };
__lsns_constant __lsns_align( 16 ) uint4 IonDim[1] = { 0, LSNS_MAX_IONPARS, 0, 0 };
__lsns_constant __lsns_align( 16 ) uint4 ChanDim[1] = { 0, LSNS_MAX_GATES, LSNS_MAX_GATEPARS+1, 0 };
__lsns_constant __lsns_align( 16 ) uint4 NetDim[1] = { 0, 0, 0, 0 };
__lsns_constant __lsns_align( 16 ) uint4 ViewDim[1] = { 0, 0, 0, 0 };
__lsns_constant __lsns_align( 16 ) gatepar Gates[LSNS_MAX_GATEPARS] = { 0 };
__lsns_constant __lsns_align( 16 ) ionspar Ions[LSNS_MAX_IONPARS] = { 0 };
__lsns_constant __lsns_align( 16 ) synpar Synapses[LSNS_MAX_SYNPARS] = { 0 };


///////////////////////////////////////////////////////////////////////////////
// control_kernel: 
// Input parameters:
// Output parameters:
#if defined( __CUDA__ )
__lsns_global void control_kernel( uint4 *counter )
#else
__lsns_global void control_kernel( int index, uint4 *counter )
#endif /* __CUDA__ */
{
	__lsns_ithread( index );				// get index of current thread
	if( index == 0 ){
		++( _sim_nstep( counter[0] ) );
	}
}

///////////////////////////////////////////////////////////////////////////////
// syn_kernel: 
// Input parameters:
// Output parameters:
// !!!!!!!!!!!!!!!must be optimized
#if defined( __CUDA__ )
__lsns_global void wsum_kernel( uint4 *syntype, uint4 *srclut, float4 *src, float4 *wall, float4 *wsyn ) //SM
#else
__lsns_global void wsum_kernel( int index, uint4 *syntype, uint4 *srclut, float4 *src, float4 *wall, float4 *wsyn ) //SM
#endif /* __CUDA__ */
{
	__lsns_ithread( index );				// get index of current thread
	__lsns_assert( index < MAX_NWSUM );			// DEBUG: check the range for 'index' variable
	// load type of synapse
	uint4 tp = syntype[index];
	// load synapse properties
	// process the synaptic summation if needed (size > 0)
	if( _syn_size( tp ) > 0 && _syn_size( tp ) != -1 ){ // TK
		// load parameters for simulation
	 	synpar par = Synapses[_syn_par( tp )];
		// load parameters synaptic weight
		float4 ws = wsyn[index];
		float w_total = 0.f;							// results of synaptic summation to be stored
		float ah = _wsyn_ah( ws ), edt = _synEdt( par ), dt = _synDt( par );	// parameters of synaptic summation
		switch( _syn_type( tp )){
			case LSNS_SUM_SYN:{
					for( __lsns_uint32 i = 0, j = _syn_lut( tp ); i < _syn_size( tp ); ++j, ++i ){
						float4 w = wall[j];	// load synaptic weights for 4 presynaptic neurons
						uint4 vlut = srclut[j]; // load look-up-table for 4 presynaptic units (drives/outputs/feedbacks etc)
						float4 v_raw[4] = { src[vlut.x], src[vlut.y], src[vlut.z], src[vlut.w] }; // load raw data from 4 presynaptic units
						float4 v = {_cell_v( v_raw[0] ), _cell_v( v_raw[1] ), _cell_v( v_raw[2] ),  _cell_v( v_raw[3] ) };
						w_total += proc_synsum( v, w );
					}
				}
				break;
			case LSNS_SIGMA_SYN:{
					float v12 = 0, slope = 1;
					for( __lsns_uint32 i = 0, j = _syn_lut( tp ); i < _syn_size( tp ); ++j, ++i ){
						float4 w = wall[j];	// load synaptic weights for 4 presynaptic neurons
						uint4 vlut = srclut[j]; // load look-up-table for 4 presynaptic units (drives/outputs/feedbacks etc)
						float4 v_raw[4] = { src[vlut.x], src[vlut.y], src[vlut.z], src[vlut.w] }; // load raw data from 4 presynaptic units
						float4 v = {lsns_msigm( _cell_v( v_raw[0] ), v12, slope ), lsns_msigm( _cell_v( v_raw[1] ), v12, slope ), lsns_msigm( _cell_v( v_raw[2] ), v12, slope ),  lsns_msigm( _cell_v( v_raw[3] ), v12, slope ) };
						w_total += proc_synsum( v, w );
					}
				}
				break;
			case LSNS_PULSE_SYN:{
					for( __lsns_uint32 i = 0, j = _syn_lut( tp ); i < _syn_size( tp ); ++j, ++i ){
						float4 w = wall[j];	// load synaptic weights for 4 presynaptic neurons
						uint4 vlut = srclut[j]; // load look-up-table for 4 presynaptic units (drives/outputs/feedbacks etc)
						float4 v_raw[4] = { src[vlut.x], src[vlut.y], src[vlut.z], src[vlut.w] }; // load raw data from 4 presynaptic units
						float4 v = {_cell_spike( v_raw[0] ), _cell_spike( v_raw[1] ), _cell_spike( v_raw[2] ),  _cell_spike( v_raw[3] ) };
						w_total += proc_synsum( v, w );
					}
				}
				break;
			default:
				ah = dt = edt = 0;		// type of synapse is not defined
		}
		_wsyn_total( ws ) = w_total;
		_wsyn_edt( ws ) = edt;
		_wsyn_dt( ws ) = dt;
		_wsyn_ah( ws ) = ah;
		// store parameters synaptic weight
		wsyn[index] = ws;
	}
}

///////////////////////////////////////////////////////////////////////////////
// +'ions_kernel': the engine kernel to calculate the properties of ions dynamics.
// such as pump current, concentration of ions inside the cell, etc.
// Input parameters:
// Output parameters:
#if defined( __CUDA__ )
__lsns_global void ions_kernel( uint4 *iontype, uint4 *ionlut, uint4 *ichanglut, float4 *chang, float4 *cellv, float4 *ione, float4 *ioni )
#else
__lsns_global void ions_kernel( int index, uint4 *iontype, uint4 *ionlut, uint4 *ichanglut, float4 *chang, float4 *cellv, float4 *ione, float4 *ioni )
#endif /* __CUDA__ */
{
	__lsns_ithread( index );				// get index of current thread
	__lsns_assert( index < MAX_IONS );			// DEBUG: check the range for 'index' variable
	// load parameters for simulation
	float step = _sim_step( SimDat[0] );
	// load type of ions (Na, K, Ca etc, and type of ion dynamics)
	uint4 tp = iontype[index];
	int type_dyn = _ions_typepump( tp ); // type_dyn and type eds are not initialized for drives
	int type_eds = _ions_typeeds( tp );
	if( type_dyn != -1 && type_dyn != LSNS_NO_DYN ){
		// load ions parameters, resting potential, ions concentrations if needed
		ionspar par = Ions[_ions_parpump( tp )];	// load pump properties
		uint4 lut = __lsns_cached( ionlut[index] );	// load references to external parameters (channel conductances etc)
		float4 e = __lsns_cached( ione[index] );	// load ions parameters
		float4 i = __lsns_cached( ioni[index] );	// load ions parameters
		float4 v = __lsns_cached( cellv[_ions_lut_v( lut )]);	// load cell parameters

		float vm = _cell_v( v );			// get membrane potential
		float tau = _pump_tau( par );			// get time constant for ions dynamics
		float out = _ions_out( e );			// concentation of ions outside cell
		float rtfz = _ions_rtfz( e );			// rtfz for specific neurons
		// start calculations
		float gchan = 0; 				// total conductances of all ion currents which are involved to ions dynamics
		lsns_fastsum( _chan_g, chang, ichanglut+index*( MAX_CHAN_PER_PUMP/4+1 ), gchan, MAX_CHANS ); // sum all gchan
		// 2-order Runge-Kutta integration method
		float ipump = 0, apump = 0, ichan = 0;
		// sub-step 1
		float in = _ions_in( e );			// concentation of ions inside the cell
		float eds = _ions_eds( e );			// resting potential
		float kca = KCA/_cell_area( v );
		lsns_ipump( type_dyn, par, in, apump, ipump, kca );// calculate pump current
		lsns_ichan( apump, vm, eds, gchan, ichan );	// calculate channels current
		// sub-step 2
		float in1 = in-step*( ichan+ipump )/( 2*tau );
		if( type_eds != LSNS_NOSPEC_EDS ){
			eds = lsns_eds( eds, rtfz, in1, out ); // calculate resting potential if needed
		}
		lsns_ipump( type_dyn, par, in1, apump, ipump, kca );// calculate pump current
		lsns_ichan( apump, vm, eds, gchan, ichan );	// calculate channels current
		// the final calculations for in, eds, ichan
		in = in-step*( ichan+ipump )/tau;
		if( type_eds != LSNS_NOSPEC_EDS ){
			eds = lsns_eds( eds, rtfz, in, out ); // calculate resting potential if needed
		}
		lsns_ipump( type_dyn, par, in, apump, ipump, kca );// calculate pump current
		lsns_ichan( apump, vm, eds, gchan, ichan );	// calculate channels current
		// store the results
		_ions_in( e ) = in;
		_ions_out( e ) = out;
		_ions_eds( e ) = eds;
		_ions_ipump( i ) = ipump;
		_ions_ichan( i ) = ichan;
		ione[index] = e;
		ioni[index] = i;
	}
	else if( type_eds != -1 && type_eds != LSNS_NOSPEC_EDS ){			// calculate reversal potential only (no ions dynamics)
		// load ions parameters
		float4 e = ione[index];				// load ions parameters
		float in = _ions_in( e );			// concentation of ions inside the cell
		float out = _ions_out( e );			// concentation of ions outside cell
		float rtfz = _ions_rtfz( e );			// rtfz for specific ions
		// calculate resting potential
		_ions_eds( e ) = lsns_eds( _ions_eds( e ), rtfz, in, out );
		// store the results
		ione[index] = e;
	}
}

///////////////////////////////////////////////////////////////////////////////
// +'chan_kernel': the engine kernel to calculate the dynamics of ion channels  
// and synaptic currents such as conductance, current etc.
#if defined( __CUDA__ )
__lsns_global void chan_kernel( uint4 *chantype, uint4 *chanlut, float4 *ione, float4 *wsyn, float4 *cellv, float4 *chanmh, float4 *chang )
#else
__lsns_global void chan_kernel( int index, uint4 *chantype, uint4 *chanlut, float4 *ione, float4 *wsyn, float4 *cellv, float4 *chanmh, float4 *chang )
#endif /* __CUDA__ */
{
	__lsns_ithread( index );				// get index of current thread
	__lsns_assert( index < MAX_CHANS );			// DEBUG: check the range for 'index' variable 
	float step = _sim_step( __lsns_cached( SimDat[0] ));
	// load type of ion channel (generic, a-b, etc) and its parameters (half-voltage, slope, etc)
	uint4 tp = chantype[index];
	// load references to external parameters (membrane potential, rest potential, etc)
	uint4 lut = chanlut[index];
	__lsns_assert( _gate_typem( tp ) < MAX_GATES && _gate_typeh( tp ) < MAX_GATES );// DEBUG: check the range for _gate_typem( tp ) && _gate_typeh( tp ) 
	__lsns_assert( _gate_parm( tp ) < MAX_GPARS && _gate_parh( tp ) < MAX_GPARS );	// DEBUG: check the range for  _gate_parm( tp ) && _gate_parh( tp ) 
	__lsns_assert( _chan_lutv( lut ) < MAX_CELLS+MAX_UNITS && _chan_lute( lut ) < MAX_IONS );	// DEBUG: check the range for _chan_lutv( lut ) && _chan_lute( lut )
//	__lsns_assert( _gate_typem( tp ) <= LSNS_PS_NMDA && _chan_lutm( lut ) < MAX_IONS );// DEBUG: check the range for _gate_typem( tp ) && _chan_lutm( lut )
//	__lsns_assert( _gate_typeh( tp ) <= LSNS_PS_NMDA && _chan_luth( lut ) < MAX_IONS );// DEBUG: check the range for _gate_typeh( tp ) && _chan_luth( lut )
//	__lsns_assert(_gate_typem(tp) > LSNS_PS_NMDA && _chan_lutm(lut) < MAX_WSYNS);	// DEBUG: check the range for _chan_lutm( lut )
//	__lsns_assert(_gate_typeh(tp) > LSNS_PS_NMDA && _chan_luth(lut) < MAX_WSYNS);	// DEBUG: check the range for _chan_luth( lut )
	// load properties of ions channel (conductance, current, etc)
	float4 g = __lsns_cached( chang[index] );
	// load properties of gate variables (activation, inactivation, etc) if needed
	float4 mh = ( _gate_typem( tp )+_gate_typeh( tp ) != LSNS_NOGATE ) ? __lsns_cached( chanmh[index] ): float4();
	float4 v = __lsns_cached( cellv[_chan_lutv( lut )] );
	float4 e = __lsns_cached( ione[_chan_lute( lut )] );
	// (use __lsns_cached for ione, cellv)
	float eds = _ions_eds( e );				// extract resting potential from 'IonsE'
	float vm = _cell_v( v );				// extract membrane potential from 'CellV'
	// perform calculations
	float mp = 1, hp = 1, _g = _chan_gmax( g );
	//todo: keep time constants instead power in 'mh' variable.  powers for activation and inactivateion should be stored in Gates structure
	proc_gate( _gate_typem(tp), Gates[_gate_parm( tp )], step, vm, _chan_lutm( lut ), ione, wsyn, _gate_powm( mh ), _gate_m( mh ), mp );
	proc_gate( _gate_typeh(tp), Gates[_gate_parh( tp )], step, vm, _chan_luth( lut ), ione, wsyn, _gate_powh( mh ), _gate_h( mh ), hp );
	_g *= mp*hp;
	_chan_g( g ) = _g;					// g
	_chan_ge( g ) = _g*eds;					// ge
	_chan_i( g ) = _g*( vm - eds );				// I
	// save properties of gate variables (activation, inactivation, etc) if needed
	if( _gate_typem( tp )+_gate_typeh( tp ) != LSNS_NOGATE ){
		chanmh[index] = mh;
	}
	// save properties of ions channel (conductance, current, etc)
	chang[index] = g;
}

///////////////////////////////////////////////////////////////////////////////
// +'cell_kernel': the engine kernel to calculate the dynamics of Hodgkin-Huxley
// neuron such as membrane potential, onset of the spike etc.
#if defined( __CUDA__ )
__lsns_global void cell_kernel( uint4 *changlut, uint4 *ionlut, float4 *chang, float4 *ioni, float4 *cellv, float4 *cellvstat )
#else
__lsns_global void cell_kernel( int index, uint4 *changlut, uint4 *ionlut, float4 *chang, float4 *ioni, float4 *cellv, float4 *cellvstat )
#endif /* __CUDA__ */
{
	__lsns_ithread( index );				// get index of current thread
	__lsns_assert( index < MAX_CELLS );			// DEBUG: check the range for 'index' variable 
	// load properties of the cell (membrane potential, etc)
	float threshold = _sim_thresh( __lsns_cached( SimDat[0] ));
	float step = _sim_step( __lsns_cached( SimDat[0] ));
	float4 v = __lsns_cached( cellv[index] );
	// preprocessing
	float g_sum = 0.f, ge_sum = 0.f, ipump = 0.f;
	lsns_fastsum( _ions_ipump, ioni, ionlut+index*( MAX_IPUMP_PER_CELL/4+1 ), ipump, MAX_IONS );		// sum all ipump
	lsns_fastsum2( _chan_g, _chan_ge, chang, changlut+index*( MAX_CHAN_PER_CELL/4+1 ), g_sum, ge_sum, MAX_CHANS );	// sum all g and ge
	if( g_sum > 0. ){ // check if any current involver to dynamics of membrane potential
		// perform calculations
		// ipump = 0.26f;
		float time = _cell_c( v )/g_sum;
		float v_inf = ( ge_sum+ipump )/g_sum;
		float vm = lsns_exp_euler( _cell_v( v ), v_inf, step, time );
		float spike = ( vm > threshold && _cell_v( v ) <= threshold ) ? 1.f : 0.f;
		// store the results of simulation
		_cell_v( v ) = vm;
		_cell_spike( v ) = spike;
		float4 vstat = cellvstat[index];
		_cell_v( vstat ) += vm;
		_cell_spike( vstat ) += spike;
		cellv[index] = v;
		cellvstat[index] = vstat;

	}
}
///////////////////////////////////////////////////////////////////////////////
// +'unit_kernel': the engine kernel to calculate the dynamics of non-spiking
// elements of a network such as drives, outputs etc.
#if defined( __CUDA__ )
__lsns_global void unit_kernel( uint4 *changlut, float4 *chang, float4 *unitv )
#else
__lsns_global void unit_kernel( int index, uint4 *changlut, float4 *chang, float4 *unitv )
#endif /* __CUDA__ */
{
	__lsns_ithread( index );				// get index of current thread
	__lsns_assert( index < MAX_UNITS );			// DEBUG: check the range for 'index' variable
	// load properties of the cell (membrane potential, etc)
	float step = _sim_step( __lsns_cached( SimDat[0] ));
	float4 out = __lsns_cached( unitv[index] );
	// processing
	float input = 0.f;
	float output = _unit_out( out );
	float tup = _unit_tup( out );
	float tdw = _unit_tdw( out );
	float thr = _unit_thr( out );
	lsns_fastsum( _chan_g, chang, changlut+index*( MAX_CHAN_PER_CELL/4+1 ), input, MAX_CHANS );	// sum of all inputs
	float preout = lsns_exp_euler( output, input, step, tup );
	preout = ( preout > output )? preout: lsns_exp_euler( output, input, step, tdw );
	_unit_out( out ) = ( preout > thr )? preout-thr: 0.f;
	unitv[index] = out;
}
///////////////////////////////////////////////////////////////////////////////
// +'store2dev_kernel': stores the simulation results to device memory.
// The results of the simulation store to device memory into 
// arrays 'float4 *DevViewData[MAX_STORED_STEPS]'. 
#if defined( __CUDA__ )
__lsns_global void store2dev_kernel( float4 *globaldata, uint4 *viewlut, float4 *viewdata )
#else
__lsns_global void store2dev_kernel( int index, float4 *globaldata, uint4 *viewlut, float4 *viewdata )
#endif /* __CUDA__ */
{
	__lsns_ithread( index );				// get index of current thread
	__lsns_assert( index < MAX_NVIEWS );			// DEBUG: check the range for 'index' & 'counter' variables
	uint4 *lut = viewlut+index;
	float4 res = get_data( globaldata, lut, MAX_GDATA );
	viewdata[index] = res;
}
///////////////////////////////////////////////////////////////////////////////
// +'resetstat_kernel': the engine kernel to calculate the dynamics of non-spiking
// elements of a network such as drives, outputs etc.
#if defined( __CUDA__ )
__lsns_global void resetstat_kernel( float4 *cellvstat )
#else
__lsns_global void resetstat_kernel( int index, float4 *cellvstat )
#endif /* __CUDA__ */
{
	__lsns_ithread( index );				// get index of current thread
	__lsns_assert( index < MAX_CELLS );			// DEBUG: check the range for 'index' variable 
	float4 vstat = {0.f, 0.f, 0.f, 0.f};
	cellvstat[index] = vstat;
}

#if defined( __CUDA__ )
__lsns_global void lsns_copy2dev_cuda( float4 *base, float4 *data, uint4 *lut ) //SM
{
	__lsns_ithread( i );				// get index of current thread
	uint4 lut_ = lut[i];
	float4 d = data[i];
	get_data( lut_.x, base ) = d.x;
	get_data( lut_.y, base ) = d.y;
	get_data( lut_.z, base ) = d.z;
	get_data( lut_.w, base ) = d.w;
/*
	__lsns_uint32 eposx = lut[i].x;
	__lsns_uint32 eposy = lut[i].y;
	__lsns_uint32 eposz = lut[i].z;
	__lsns_uint32 eposw = lut[i].w;
	if( eposx != 0xFFFFFFFF ){
		get_data( eposx, base ) = data[i].x;
	}
	if( eposy != 0xFFFFFFFF ){
		get_data( eposy, base ) = data[i].y;
	}
	if( eposz != 0xFFFFFFFF ){
		get_data( eposz, base ) = data[i].z;
	}
	if( eposw != 0xFFFFFFFF ){
		get_data( eposw, base ) = data[i].w;
	}
*/
}
__lsns_global void lsns_copy2host_cuda( float4 *base, float4 *data, uint4 *lut ) //SM
{
	__lsns_ithread( i );				// get index of current thread
	uint4 lut_ = lut[i];
	float4 d;
	d.x = get_data( lut_.x, base );
	d.y = get_data( lut_.y, base );
	d.z = get_data( lut_.z, base );
	d.w = get_data( lut_.w, base );
	data[i] = d;
}
#endif /* __CUDA__ */

#endif /* __LSNS_ENGINE_CPP */

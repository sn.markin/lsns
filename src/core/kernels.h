﻿#pragma once

#ifndef __LSNS_KERNELS_H
#define __LSNS_KERNELS_H

#include "config.h"
#include "gateproc.h"
#include "ionproc.h"
#include "synproc.h"
#include "viewproc.h"

#define MAX_CHAN_PER_PUMP	7				/*<31*/
#define MAX_CHAN_PER_CELL	15				/*<31*/
#define MAX_IPUMP_PER_CELL	3				/*<31*/
#define MAX_STORED_STEPS	100

//#define MAX_ION_PER_CELL	16

///////////////////////////////////////////////////////////////////////////////
// list of computational kernels
enum __lsns_kernels_list{
	LSNS_CTRLS_KERNEL,
	LSNS_IONS_KERNEL,
	LSNS_WSUM_KERNEL,
	LSNS_CHAN_KERNEL,
	LSNS_CELLS_KERNEL,
	LSNS_UNITS_KERNEL,
	LSNS_STORE2DEV_KERNEL,
	LSNS_MAX_KERNEL,
};
///////////////////////////////////////////////////////////////////////////////
// type of synapes (decodes SynType):
//	x - type of synapse;
//	y - parameters
//	z - number of connections /4 
//	w - initial index in Wall&CellLUT arrays
enum __uint4_syntype{
};
#define _syn_type( tp ) ( tp ).x
#define _syn_par( tp ) ( tp ).y
#define _syn_size( tp ) ( tp ).z
#define _syn_lut( tp ) ( tp ).w
///////////////////////////////////////////////////////////////////////////////
// parameters of the synaptic weight (decodes Wsyn):
//	x - total weight of all connections;
//	y - production: [rate of transmitter release]*[plasticity] (a*h)
//	z - 1 for pulse model or step/T other models;
//	w -  exp( step/T ) for pulse model or 1-step/T for othe models
enum __float4_wsyn{
	_wsyn_pos_total = 0x00,
	_wsyn_pos_ah  = 0x01,
	_wsyn_pos_dt  = 0x02,
	_wsyn_pos_edt = 0x03,
};
#define _wsyn_total( wsyn ) ( wsyn ).x
#define _wsyn_ah( wsyn ) ( wsyn ).y
#define _wsyn_dt( wsyn ) ( wsyn ).z
#define _wsyn_edt( wsyn ) ( wsyn ).w
///////////////////////////////////////////////////////////////////////////////
// syndat maps data which are related to synaptic properties onto global memory
typedef struct __lsns_align( 16 ) __weighedsum_data {
	// look-up-tables for shared variables (read-only)
	uint4 __lsns_align( 16 ) *SynType;		// x - type of synapse, y - parameters, z - size (0 for LSNS_CLONE_SYN), w - initial index in Wall&SrcLUT arrays or index in Wsyn for LSNS_CLONE_SYN
	uint4 __lsns_align( 16 ) *SrcLUT;		// look-up-table for all sorces which are converged onto particular synapse
	// shared variables (read-only)
	float4 __lsns_align( 16 ) *Wall;		// all weights for particular synapse
	float4 __lsns_align( 16 ) *Srcs;		// sources (cells and units): x - membrane potential, z - spike onset, y - membrane capacitance, w - surface area
	// shared variables (read/write)
	float4 __lsns_align( 16 ) *Wsyn;		// x - total sum; y - [rate of transmitter release]*[plasticity] (a*h); z - 1; w - exp( step/T ) for pulse model or 0 for other types
} wsumdat;
//=================== iondat macroses =========================================
///////////////////////////////////////////////////////////////////////////////
// type of ion dynamics (decodes IonsType):
//	x - type of ions pump (Na-pump, Ca-pump, Ca-pump_1 etc);
//	y - type of reversal potential (non-specific, specific etc);
//	z - parameters of ions pump;
//	w - reserved. 
enum __uint4_ionstype{
};
#define _ions_typepump( tp ) ( tp ).x 
#define _ions_typeeds( tp ) ( tp ).y
#define _ions_parpump( tp ) ( tp ).z
#define _ions_id( tp ) ( tp ).w
///////////////////////////////////////////////////////////////////////////////
// shared parameters for ion dynamics (decodes IonsLUT):
//	x - membrane potential;
//	y - reserved;
//	z - reserved;
//	w - reserved
enum __uint4_ionslut{
};
#define _ions_lut_v( sh ) ( sh ).x
///////////////////////////////////////////////////////////////////////////////
// parameters of ions dynamics (decodes IonsE):
//	x - reversal potential (Eds);
//	y - concentration of ions inside the cell;
//	z - concentration of ions outside the cell;
//	w - RT/Fz constant for specific ions
enum __float4_ionse{
	_ions_pos_eds  = 0x00,
	_ions_pos_in   = 0x01,
	_ions_pos_out  = 0x02,
	_ions_pos_rtfz = 0x03,
};
#define _ions_eds( e ) ( e ).x
#define _ions_in( e ) ( e ).y
#define _ions_out( e ) ( e ).z
#define _ions_rtfz( e ) ( e ).w
///////////////////////////////////////////////////////////////////////////////
// parameters of ion current (decodes IonsI):
//	x - pump current;
//	y - channels current;
//	z - reserved;
//	w - reserved.
enum __float4_ionsi{
	_ions_pos_ipump = 0x00,
	_ions_pos_ichan = 0x01,
};
#define _ions_ipump( i ) ( i ).x
#define _ions_ichan( i ) ( i ).y
///////////////////////////////////////////////////////////////////////////////
// iondat maps data which are related to ions dynamics onto global memory
typedef struct __lsns_align( 16 ) __ions_data{
	// look-up-tables for shared variables (read-only)
	uint4 __lsns_align( 16 ) *IonsType;		// type of ions: x - pump type, y - eds type, z - pump parameters, w - reserved
	uint4 __lsns_align( 16 ) *IonsLUT;		// indices of shared variables: x - cell properties, y, z, w - reserved
	uint4 __lsns_align( 16 ) *IChanGLUT;		// look-up-table of channel currents for specific type of ions: x - counter, the rest are actual indices of ChanG array
	// shared variables (read-only)
	float4 __lsns_align( 16 ) *ChanG;		// channel current: x - maximal conductance, y - conductance, z - current, w - G*Eds production
	float4 __lsns_align( 16 ) *CellV;		// cell properties: x - membrane potential, z - spike onset, y - membrane capacitance, w - surface area
	// shared variables (read/write)
	float4 __lsns_align( 16 ) *IonsE;		// ions properties: x - reversal potential (Eds), y - concentration of ions inside the cell, z - concentration of ions outside the cell, w - RT/Fz constant for specific ions
	float4 __lsns_align( 16 ) *IonsI;		// ion currents: x - pump current, y - channels current, z - time constant of ions dynamics, w - reserved.
} iondat;
///////////////////////////////////////////////////////////////////////////////
// gate types of ion channel (decodes ChanType):
//	x - type of activation
//	y - parameters of activation
//	z - type of inactivation
//	w - parameters of inactivation
enum __uint4_chantype{
	_gate_pos_typem = 0x00,
	_gate_pos_parm  = 0x01,
	_gate_pos_typeh = 0x02,
	_gate_pos_parh  = 0x03,
};
#define _gate_typem( t ) ( t ).x
#define _gate_parm( t ) ( t ).y
#define _gate_typeh( t ) ( t ).z
#define _gate_parh( t ) ( t ).w
///////////////////////////////////////////////////////////////////////////////
// indices of shared parameters for ion channels (decodes ChanLUT):
//	x - membrane potential
//	y - resting potential
//	z - concentration of ions inside the cell for activation for z-channels/or total weight of connections for the synapse (for m component)
//	w - concentration of ions inside the cell for inactivation for z-channels/or total weight of connections for the synapse (for h component)
enum __uint4_chanlut{
};
#define _chan_lutv( lut ) ( lut ).x
#define _chan_lute( lut ) ( lut ).y
#define _chan_lutm( lut ) ( lut ).z
#define _chan_luth( lut ) ( lut ).w
///////////////////////////////////////////////////////////////////////////////
// parameters of gate variables of ion channel (decodes ChanMH):
//	x - activation
//	y - inactivation
//	z - power of activation
//	w - power of inactivation
enum __float4_chanmh{
	_gate_pos_m	= 0x00,				// activation
	_gate_pos_h	= 0x01,				// inactivation
	_gate_pos_powm	= 0x02,				// power of activation
	_gate_pos_powh	= 0x03,				// power of inactivation
};
#define _gate_m( mh ) ( mh ).x
#define _gate_h( mh ) ( mh ).y
#define _gate_powm( mh ) ( mh ).z
#define _gate_powh( mh ) ( mh ).w
///////////////////////////////////////////////////////////////////////////////
// parameters of the ion channel (decodes ChanG):
//	x - maximal conductance
//	y - conductance
//	z - current
//	w - G*Eds production
enum __float4_chang{
	_chan_pos_gmax	= 0x00,				// maximal conductance
	_chan_pos_g	= 0x01,				// conductance
	_chan_pos_ge	= 0x02,				// current
	_chan_pos_i	= 0x03,				// G*Eds production
};
#define _chan_gmax( g ) ( g ).x
#define _chan_g( g ) ( g ).y
#define _chan_ge( g ) ( g ).z
#define _chan_i( g ) ( g ).w
///////////////////////////////////////////////////////////////////////////////
// chandat maps data which are related to ion channels onto global memory
typedef struct __lsns_align( 16 ) __channel_data{
	// local variables (read-only)
	uint4 __lsns_align( 16 ) *ChanType;		// type of channel: x - type of activation, y - parameters of activation, z - type of inactivation, w - parameters of inactivation.
	uint4 __lsns_align( 16 ) *ChanLUT;		// indices of shared variables: x - CellV, y - IonsE/eds, z - IonsE/in for M (z-channel)/or W total for synapse, w - IonsE/in for H (z-channel)/or W total for synapse
	// shared variables (read-only)
	float4 __lsns_align( 16 ) *CellV;		// cell properties: x - membrane potential, z - spike onset, y - membrane capacitance, w - surface area
	float4 __lsns_align( 16 ) *IonsE;		// ions properties: x - reversal potential (Eds), y - concentration of ions inside the cell, z - concentration of ions outside the cell, w - RT/Fz constant for specific ions
	float4 __lsns_align( 16 ) *Wsyn;		// synaptic weights: x - total sum, y - ( rate of transmitter release )*( plasticity ), z - 1 for pulse model or step/T other models, w - exp( step/T ) for pulse model or 1-step/T for othe models
	// shared variables (read-write)
	float4 __lsns_align( 16 ) *ChanMH;		// gate variables: x - activation, y - inactivation, z - power of activation, w - power of inactivation
	float4 __lsns_align( 16 ) *ChanG;		// channel current: x - maximal conductance, y - conductance, z - current, w - G*Eds production
} chandat;
///////////////////////////////////////////////////////////////////////////////
// parameters of cell (decodes CellV):
//	x - membrane potential
//	z - spike onset
//	y - membrane capacitance
//	w - surface area
// TODO: swap y<->z
enum __float4_cellv{
	_cell_pos_v	= 0x00,				// membrane potential
	_cell_pos_spike = 0x02,				// spike onset
	_cell_pos_c	= 0x01,				// membrane capacitance
	_cell_pos_area	= 0x03,				// surface area
};
#define _cell_v( v ) ( v ).x
#define _cell_spike( v ) ( v ).z
#define _cell_c( v ) ( v ).y
#define _cell_area( v ) ( v ).w
///////////////////////////////////////////////////////////////////////////////
// celldat maps data which are related to neurons onto global memory
typedef struct __lsns_align( 16 ) __cell_data{
	// local variables (read-only)
	uint4 __lsns_align( 16 ) *IonsILUT;		// look-up-table of pump current: x - counter, the rest are actual indices of IonsI array;
	uint4 __lsns_align( 16 ) *VChanGLUT;		// look-up-table of channel current for specific cell: x - counter, the rest are actual indices of ChanG array
	// shared variables (read-only)
	float4 __lsns_align( 16 ) *ChanG;		// channel current: x - maximal conductance, y - conductance, z - current, w - G*Eds production
	float4 __lsns_align( 16 ) *IonsI;		// ion currents: x - pump current, y - channels current, z - time constant of ions dynamics, w - reserved.
	// shared variables (read-write)
	float4 __lsns_align( 16 ) *CellV;		// cell properties: x - membrane potential, z - spike onset, y - membrane capacitance, w - surface area
	float4 __lsns_align( 16 ) *CellStat;		// cell statistics: x - average membrane potential, y - spike rate, z - reserved, w - reserved
} celldat;
///////////////////////////////////////////////////////////////////////////////
// parameters of unit (decodes UnitV):
//	x - output
//	z - time constant (up)
//	y - time constant (down)
//	w - threshold
enum __float4_unitv{
	_unit_pos_out	= 0x00,				// output
	_unit_pos_tup	= 0x01,				// time constant (up)
	_unit_pos_tdw	= 0x02,				// time constant (down)
	_unit_pos_thr	= 0x03,				// threshold
};
#define _unit_out( v ) ( v ).x
#define _unit_tup( v ) ( v ).y
#define _unit_tdw( v ) ( v ).z
#define _unit_thr( v ) ( v ).w
///////////////////////////////////////////////////////////////////////////////
// unitdat maps data which are related to network units (drives, outputs, 
// feedbacks) onto global memory
typedef struct __lsns_align( 16 ) __unit_data{
	// local variables (read-only)
	uint4 __lsns_align( 16 ) *UChanGLUT;		// look-up-table of channel current (input signals) for specific unit: x - counter, the rest are actual indices of ChanG array
	// shared variables (read-only)
	float4 __lsns_align( 16 ) *ChanG;		// channel current (input signals): x - maximal conductance, y - conductance (as input), z - current, w - G*Eds production
	// shared variables (read-write)
	float4 __lsns_align( 16 ) *UnitV;		// unit properties: x - membrane potential (as output), y - Tup, z - Tdown, w - threshold
	float4 __lsns_align( 16 ) *UnitStat;		// unit statistics: x - average membrane potential, y - spike rate, z - current step, w - reserved
} unitdat;
///////////////////////////////////////////////////////////////////////////////
// iodat contains the pointers to both host and device memory which are related 
// to data to be displayed
typedef struct __lsns_align( 16 ) __iobuf{
	// local variables (read-only). 
	// LUT format: bits 31..30 are coding the offset in each float4 variable (00 - x, 01 - y, 10 - z, 11 - w); 
	// bits 29..0 are coding the offset in the global array 'GlobalData'
	uint4 __lsns_align( 16 ) *ViewLUT;		// look-up-table for data needed to be stored (located both in device and host memory)
	// local variables (read-write)
	float4 __lsns_align( 16 ) *DevViewData[MAX_STORED_STEPS];// data to display (located in device memory)
	float4 __lsns_align( 16 ) *HostViewData;	// data to display (pinned memory located in host memory)
	// shared variables
	float4 __lsns_align( 16 ) *GlobalData;		// array of global data (located in device memory)
} iodat;
///////////////////////////////////////////////////////////////////////////////
// netdat contains the pointers to global memory which are related to network
// parameters and structure
typedef struct __lsns_align( 16 ) __network_data{
	// global memory
	uint4 __lsns_align( 16 ) *Counter;		// Counter: x - total number of steps, y - current step
	uint4 __lsns_align( 16 ) *GlobalLUT;		// look-up-tables (located in both device and host memory); size = MaxGlobalLUT
	float4 __lsns_align( 16 ) *GlobalData;		// array of global data (located in both device and host memory); size = MaxGlobalData
	// network elements (channels, ions, cells, etc) mapped on global memory
	wsumdat Wsum;					// weighted sum
	iondat Ions;					// ions data
	chandat Channels;				// channels data
	celldat Cells;					// cells (compartments) data
        unitdat Units;                                  // units data
	iodat IOData;					// data buffer to display the results of simulations
	// global memory on device
	struct __network_data *DevMap;			// pointer to device-specific memory (for non-cuda version this is a pointer to the same data structure)
} netdat;
///////////////////////////////////////////////////////////////////////////////
// netdim structure
typedef struct __lsns_align( 16 ) __network_dim{
	// data which have to be initialized before memory allocation by 'lsns_alloc' method
	int MaxIons;					// total number of ions parameters
	int MaxChan;					// total number of ion channels (including synapses)
	int MaxCells;					// total number of cells in all populations 
	int MaxUnits;					// total number of units (non-spiking elements in the network)
	int MaxWsyn;					// total number of synapses
	int MaxWS;					// total number of connections
	int MaxViewPars;				// total number of network parameters to be displayed
	// data which will initialized after memory allocation by 'lsns_alloc' method
	int DevViewPars;				// total number of network parameters to be displayed
	int MaxGlobalData;				// = 2*MaxIons+2*MaxChan+2*(MaxCells+MaxUnits)
	int MaxGlobalLUT;				// = MaxIons*( 3+MAX_CHAN_PER_PUMP/4 )+2*MaxChan+MaxCells*(2+MAX_CHAN_PER_CELL/4+MAX_IPUMP_PER_CELL/4)+MaxUnits*(1+MAX_CHAN_PER_CELL/4)
} netdim;
///////////////////////////////////////////////////////////////////////////////
// some useful operations with network structure
///////////////////////////////////////////////////////////////////////////////
// provides access to any network variable allocated on device
// epos is encoded position of requested variable relative to data array
__lsns_inline float &get_data( __lsns_uint32 epos, float4 *data )
{
	__lsns_uint32 bank = ( epos>>30 )&0x03;	// bank
	__lsns_uint32 offset = epos&0x3FFFFFFF;	// offset
	return (( float * )( data+offset ))[bank];
}
///////////////////////////////////////////////////////////////////////////////
// map2lut modify look-up-table of network elements (cells, drives, outputs etc)
// which refer to ions currents involved in dynamics of membrane potential
// 'lut' - array of look-up-tables for all network elements 
// 'lut_pos' - position of current network element
// 'lut_ref' - actual number @ lut[lut_pos+counter]
inline void map2lut( uint4 *lut, __lsns_uint32 lut_pos, __lsns_uint32 lut_ref, __lsns_uint32 size )
{
	lut = lut+lut_pos*( size/4+1 );			// refer to look-up-table of current network element
	__lsns_uint32 *lut_ = ( __lsns_uint32 *)lut;
	__lsns_uint32 counter = lut_[0];
	if( counter == -1 )  counter = 0;		// TK
	__lsns_assert( counter <= size-1 );		// make sure the counter is not exceed its maximal number
	lut_[0] = ++counter;
	lut_[counter] = lut_ref;
}
///////////////////////////////////////////////////////////////////////////////
// 'make_epos32' is based on the code provided by Taegyo Kim. 
// It returns encoded position of particular variable in 'float4' array 
// 'bank' - position of particular variable in float4 structure: 0-x, 1-y, 2-z, 3-w
// 'offset' - offset of particular float4 structure in flоt4 array
inline __lsns_uint32 make_epos32( __lsns_uint32 bank, __lsns_uint32 offset )
{
	if( offset < 0x3FFFFFFF ){
		bank &= 0x03;
		return ( bank << 30 )|offset; // address up to 4GB-1
	}
	return -1;	// out of range
};

///////////////////////////////////////////////////////////////////////////////
// 'get_ppos' provides access to any network variable allocated on host computer
inline __lsns_uint32 get_ppos( __lsns_uint32 syn_pos, __lsns_uint32 src_pos, const netdat *data )
{
	return -1;
}

///////////////////////////////////////////////////////////////////////////////
// 'get_wpos' provides access to any network variable allocated on host computer
inline __lsns_uint32 get_wpos( __lsns_uint32 syn_pos, __lsns_uint32 src_pos, const netdat *data )
{
	if( syn_pos != -1 ){
		__lsns_uint32 wsum_pos = _chan_lutm( data->Channels.ChanLUT[syn_pos] );
		__lsns_uint32 wsum_size = _syn_size( data->Wsum.SynType[wsum_pos] );
		if( _syn_type( data->Wsum.SynType[wsum_pos] ) == LSNS_CLONE_SYN && wsum_size == 0 ){
			wsum_pos = _syn_lut( data->Wsum.SynType[wsum_pos] );
			wsum_size = _syn_size( data->Wsum.SynType[wsum_pos] );
		}
		__lsns_uint32 offset = _syn_lut( data->Wsum.SynType[wsum_pos] );
		__lsns_uint32 bank = -1;
                uint4 *src_lut = data->Wsum.SrcLUT+offset;
		for( __lsns_uint32 i = 0; i < wsum_size; ++i ){
			if( src_lut[i].x == src_pos ){
				bank = 0;
			}
			else if( src_lut[i].y == src_pos ){
				bank = 1;
			}
			else if( src_lut[i].z == src_pos ){
				bank = 2;
			}
			else if( src_lut[i].w == src_pos ){
				bank = 3;
			}
			if( bank != -1 ){
				return make_epos32( bank, offset+i );
			}
		}
	}
	return -1;
}

#endif /* __LSNS_KERNELS_H */

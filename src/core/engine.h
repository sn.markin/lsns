#pragma once

#ifndef __LSNS_ENGINE_H
#define __LSNS_ENGINE_H

#define GRN_XBLOCKS		4			/* granularity of x blocks in 2-D grid */
#define NUM_XGRN		4			/* number of granules in x blocks in 2-D grid */
#define MAX_XBLOCKS		GRN_XBLOCKS*NUM_XGRN	/* maximal number of x blocks in 2-D grid*/
#define NUM_THREADS		192			/* maximal number of threads per block*/
#define MAX_CONST_MEM		65355			/* size of constant memory */

#include "netmap.h"
#include "iodata.h" //Added by Taegyo

///////////////////////////////////////////////////////////////////////////////
// netpar structure
typedef struct __lsns_align( 16 ) __network_parameters{
	// data which have to be initialized before memory allocation by 'lsns_alloc' method
	float Threshold;					// threshold for spike discrimination
	float Step;						// integration step
	netdim NetDim;						// number  of network elements
	gatepar Gates[LSNS_MAX_GATEPARS];			// properties of ion channels
	ionspar Ions[LSNS_MAX_IONPARS];				// properties of ion dynamics
	synpar Synapses[LSNS_MAX_SYNPARS];			// properties of synaptic summation
	// data which will initialized after memory allocation by 'lsns_alloc' method
	netdat *Data;						// pointer to network data
} netpar;

//=============================================================================
///////////////////////////////////////////////////////////////////////////////
// interface methods
extern netdat *lsns_alloc( netpar &par );			// allocate memory on host
extern bool lsns_free( netpar &par );				// free memory both on host and device !!! Return type and input type errors found by Taegyo 9.1.2015
extern bool lsns_map2dev( netpar &par );			// allocate memory on device and copy the network configuration from host to device. !!! Error correction: Inconsistency of input arguments between declare and definition. 7/22/2015 Taegyo 
extern bool lsns_reinit( netpar &par );				// call for each session
extern bool lsns_reset( netpar &par );				// call for each trial
extern bool lsns_refresh( netpar &par );			// keep all results, but reset sim counter and reset view buffers
extern size_t lsns_transition( netpar &par, size_t nsteps );
extern size_t lsns_run( netpar &par, vector<nns_iodata>& iodata, size_t Nsteps ); // modifyed by SM

extern void lsns_copy2dev( float4 *base, float4 *data, uint4 *lut, size_t sz4 ); // x86 implementation yet
extern void lsns_copy2host( float4 *base, float4 *data, uint4 *lut, size_t sz4 );// x86 implementation yet

#endif /*__LSNS_ENGINE_H*/

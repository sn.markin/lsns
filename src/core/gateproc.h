#pragma once

#ifndef __GATEPROC_H
#define __GATEPROC_H

#include "lsnsmath.h"
#include "gates.h"

///////////////////////////////////////////////////////////////////////////////
// +implementation of some usefull macroses to calculate the gate variables
//=============================================================================
// generic gate variable (lsns_ggate): 1/( 1+exp((v-v12 )/slp ))
// time constant for generic gate variable (lsns_ggate_t): t0+t/cosh(( v-v12 )/slp ))
// time constant for modified generic gate variable (lsns_ggate_tmod): t0+2*t/( exp(( v-v12 )/slp )+exp(-( v-v12_2 )/slp_2 ))
// alha/beta gate variable: ((a)*(v)+(b) )/( exp(( (v)+(c) )/(d) )+(e) )
#if defined( __CUDA__ )
		#define lsns_ggate( v, v12, slp ) \
			((( slp ) != 0.f )? lsns_div( 1.f, 1.f+lsns_exp( lsns_div(( v )-( v12 ), slp ))): 1.f )
		#define lsns_ggate_t( t0, t, v, v12, slp ) \
			(( t0 )+(( t > 0.f )? lsns_div( t, lsns_cosh( lsns_div(( v )-( v12 ), slp ))): 0.f ))
		#define lsns_ggate_tmod( res, t0, t, v, v12, slp, v12_2, slp_2 )\
			{\
				( res ) = ( t0 );\
				if(( t ) > 0.f && ( slp ) != 0  ){\
					if(( slp_2 ) == 0.f ){\
						( res ) += lsns_div(( t ), lsns_exp( lsns_div(( v )-( v12 ), slp )));\
					}\
					else{\
						( res ) += lsns_div( 2.f*( t ), lsns_exp( lsns_div(( v )-( v12 ), slp ))+lsns_exp( lsns_div(( v )-( v12_2 ), slp_2 )));\
					}\
				}\
			}
		#define lsns_abgate( res, v, a, b, c, d, e ) \
			{\
				float x = ( a )*( v )+( b );\
				float y = lsns_exp( lsns_div(( v )+( c ), d ))+( e );\
				( res ) = (( x ) == 0.f && ( y ) == 0.f )? 1.f: lsns_div( x, y );\
			}

#else
	#if defined( __LSNS_DEBUG__ )
		__lsns_inline float lsns_ggate( float v,  float v12,  float slp ){
			return ( slp != 0.f )? 1.f/( 1.f+lsns_exp(( v-v12 )/slp )): 1.f;
		};
		__lsns_inline float lsns_ggate_t( float t0, float t, float v,  float v12,  float slp ){
			return t0+(( t > 0.f )? t/lsns_cosh(( v-v12 )/slp ): 0.f );
		};
		__lsns_inline void lsns_ggate_tmod( float &res, float t0, float t, float v, float v12, float slp, float v12_2, float slp_2 ){
			res = t0;
			if( t > 0.f && slp != 0 ){
				if( slp_2 == 0.f ){
					res += t/lsns_exp(( v-v12 )/slp );
				}
				else{
					res += 2.f*t/( lsns_exp(( v-v12 )/slp )+lsns_exp(( v-v12_2 )/slp_2 ));
				}
			}
		};
		__lsns_inline void lsns_abgate( float &res, float v, float a, float b, float c, float d, float e ){
			float x = a*v+b;
			float y = lsns_exp( lsns_div(( v )+( c ), d ))+( e );
			res = (( x ) == 0.f && ( y ) == 0.f )? 1.f: x/y;
		};
	#else
		#define lsns_ggate( v, v12, slp ) \
			((( slp ) != 0.f )? lsns_div( 1.f, 1.f+lsns_exp( lsns_div(( v )-( v12 ), slp ))): 1.f )
		#define lsns_ggate_t( t0, t, v, v12, slp ) \
			(( t0 )+(( t > 0.f )? lsns_div( t, lsns_cosh( lsns_div(( v )-( v12 ), slp ))): 0.f ))
		#define lsns_ggate_tmod( res, t0, t, v, v12, slp, v12_2, slp_2 )\
			{\
				( res ) = ( t0 );\
				if(( t ) > 0.f && ( slp ) != 0  ){\
					if(( slp_2 ) == 0.f ){\
						( res ) += lsns_div(( t ), lsns_exp( lsns_div(( v )-( v12 ), slp )));\
					}\
					else{\
						( res ) += lsns_div( 2.f*( t ), lsns_exp( lsns_div(( v )-( v12 ), slp ))+lsns_exp( lsns_div(( v )-( v12_2 ), slp_2 )));\
					}\
				}\
			}
		#define lsns_abgate( res, v, a, b, c, d, e ) \
			{\
				float x = ( a )*( v )+( b );\
				float y = lsns_exp( lsns_div(( v )+( c ), d ))+( e );\
				( res ) = (( x ) == 0.f && ( y ) == 0.f )? 1.f: lsns_div( x, y );\
			}
	#endif
#endif

// alha/beta gate variable: ((a)*(v)+(b) )/( exp(( (v)+(c) )/(d) )+(e) )
///////////////////////////////////////////////////////////////////////////////
// +implementation of the gate variables of all types
//=============================================================================
///////////////////////////////////////////////////////////////////////////////
// +performes no calculation. Always returns 1.
#define proc_nogate() 1.f
///////////////////////////////////////////////////////////////////////////////
// +bypass the gate calculation. Always returns 'gate'
#define proc_passgate( gate ) ( gate )
///////////////////////////////////////////////////////////////////////////////
// +implements generic description of gate variable [M/H] for voltage-activated 
// ion channel. T*d[M/H]/dt = [M/H]inf-[M/H]; 
//-----------------------------------------------------------------------------
// four subtypes of generic gate variable for voltage activated ion channel are:
// 	1) 'ggate1' instant (time constant = 0):
//		[M/H]inf = 1/(1+exp(-(V-V12)/Slope))
//		[M/H] = [M/H]inf;
//	2) 'ggate2' generic description for time constant !=0:
//		[M/H]inf = 1/(1+exp(-(V-V12)/Slope))
//		T = T0+Tmax/cosh((V-V12T)/SlopeT)
//		T*d[M/H]/dt = [M/H]inf-[M/H];
//	3) 'ggate3' modified generic description for time constant !=0 :
//		[M/H]inf = 1/(1+exp(-(V-V12)/Slope))
//		T = T0+2*Tmax/(exp((V-V12T)/SlopeT)+exp(-(V-V12T2)/SlopeT2))
//		T*d[M/H]/dt = [M/H]inf-[M/H];
//	4) 'ggate4' modified generic description for time constant !=0 (A-current):
//		[M/H]inf = 1/(1+exp(-(V-V12)/Slope))
//		T = Tup;
//		if( v < Vthreshold ){
//			T = T0+2*Tmax/(exp((V-V12T)/SlopeT)+exp(-(V-V12T2)/SlopeT2))
//		}
//		T*d[M/H]/dt = [M/H]inf-[M/H];
//=============================================================================
#define proc_ggate_inf( par, v ) lsns_ggate( v, _ggatev12( par ), _ggateslp( par ))
#define proc_ggate_t( par, v ) lsns_ggate_t( _ggatet0( par ), _ggatetmax( par ), v, _ggatev12t( par ), _ggateslpt( par ))
#define proc_ggate_tmod( res, par, v ) lsns_ggate_tmod( res, _ggatet0( par ), _ggatetmax( par ), v, _ggatev12t( par ), _ggateslpt( par ), _ggatev12t2( par ), _ggateslpt2( par ))
#define proc_ggate_tamod( res, par, v ) \
	{\
		res = _ggatetup( par );\
		if(( v ) < _ggatevtr( par ))\
			lsns_ggate_tmod( res, _ggatet0( par ), _ggatetmax( par ), v, _ggatev12t( par ), _ggateslpt( par ), _ggatev12t2( par ), _ggateslpt2( par )); \
	}
///////////////////////////////////////////////////////////////////////////////
// +implements alpa/beta description of gate variable [M/H] for voltage-
// activated ion channel. T*d[M/H]/dt = Alpha*(1-[M/H])-Beta*[M/H];
//-----------------------------------------------------------------------------
// two subtypes of alpha/beta gate variable for voltage activated ion channel are:
// 	1) 'abgate1' instant (time constant is 0):
//		[M/H] = Alpha/(Alpha+Beta);
//	2) 'abgate2' alpha/beta description for time constant:
//		T = T0+Tmax/(Alpha+Beta);
//		[M/H]inf = Alpha/(Alpha+Beta);
//		T*d[M/H]/dt = [M/H]inf-[M/H];
//-----------------------------------------------------------------------------
//	Alpha/Beta = (A*v+B)/( exp(( v+C )/D )+E)
//=========================== abgate1 =========================================
#define proc_abgate_alpha( res, par, v ) lsns_abgate( res, v, _abgateAa( par ), _abgateBa( par ), _abgateCa( par ), _abgateDa( par ), _abgateEa( par ))
#define proc_abgate_beta( res, par, v ) lsns_abgate( res, v, _abgateAb( par ), _abgateBb( par ), _abgateCb( par ), _abgateDb( par ), _abgateEb( par ))
#define proc_abgate_time( alpha, beta ) lsns_div( 1.f, ( alpha )+( beta ))
///////////////////////////////////////////////////////////////////////////////
// +implements z-description of gate variable [M/H] for calcium-
// activated ion channel. T*d[M/H]/dt = [M/H]inf-[M/H];
//-----------------------------------------------------------------------------
// four subtypes of z-gate variable for voltage activated ion channel are:
// 	1) 'zgate1' instant generic description(time constant is 0):
//		k = Alpha*(Beta*[Ca_in])^Lymbda;
//		[M/H] = k/(1+k);
//	2) 'zgate2' z-gate generic description for time constant:
//		k = Alpha*(Beta*[Ca_in])^Lymbda;
//		[M/H]inf = 1/(1+k);
//		T = T0+Tmax/(1+Gamma*k);
//		T*d[M/H]/dt = [M/H]inf-[M/H];
// 	3) 'zgate3' instant alpha/beta description (time constant is 0):
//		alpha = [Ca_in]*Alpha*exp( -( v-V12 )/Slp );
//		beta = Beta*exp(( v-V12 )/Slp );
//		[M/H] = alpha/(alpha+beta);
//	4) 'zgate4' z-gate alpha/beta description for time constant:
//		alpha = [Ca_in]*Alpha*exp( -( v-V12 )/Slp );
//		beta = Beta*exp(( v-V12 )/Slp );
//		[M/H]inf = alpha/(alpha+beta);
//		T = T0+Tmax/(alpha+beta);
//		T*d[M/H]/dt = [M/H]inf-[M/H];
//=========================== zgate1 ==========================================
#define proc_zgate_k( par, in ) _zgateA( par )*lsns_pow( _zgateB( par )*( in ), _zgateL( par ))
#define proc_zgatek_ginf( k ) lsns_div(( k ), 1.f+( k ))
#define proc_zgatek_time( k )  _zgatet0( par )+lsns_div( _zgatetmax( par ), 1.f+_zgateG( par )*( k ));

#define proc_zgate_alpha( par, v, in ) ( in )*_zgateA( par )*lsns_exp( lsns_div( -(( v )-_zgatev12( par )), _zgateslp( par )))
#define proc_zgate_beta( par, v ) _zgateB( par )*lsns_exp( lsns_div(( v )-_zgatev12( par ), _zgateslp( par )))
#define proc_zgateab_ginf( alpha, beta ) lsns_div( alpha, ( alpha )+( beta ))
#define proc_zgateab_time( k ) lsns_div( alpha, ( alpha )+( beta ))
///////////////////////////////////////////////////////////////////////////////
// +implements synaptic plasticity on post-synaptic neuron as gate variable
// that modulates the synaptic conductance.
//-----------------------------------------------------------------------------
// one subtype of gate variable for synaptic plasticity on post-synaptic neuron:
// 	1) 'psgate1' nmda synapse:
//		[M/H] = 1/(1+exp(−0.062*V)*[Mg]/3.57)
//=========================== psgate1 =========================================
#define proc_psgate1( in, v ) lsns_div( 1.f, 1.f+lsns_exp( -0.062f*( v ))*lsns_div(( in ), 3.57f ))
///////////////////////////////////////////////////////////////////////////////
// implements synaptic summation of gate variable [M/H]  for synaptic current
// [M/H] = [M/H]*Edt+A*h*w*Dt, where:
//	A is rate of transmitter release,
//	h is synaptic plasticity,
//	w is total sum of all pre-synaptic neurons converged on the synapse
//-----------------------------------------------------------------------------
// 	1) 'proc_syngate1' pulse synapse:
//		Edt = exp( -step/T ) where T is time constant, Dt = 1 
//=========================== psgate1 =========================================
#define proc_syngate1( w, alpha, edt, dt, gate ) ( gate )*( edt )+( w )*( alpha )*( dt )
/*
inline float proc_syngate1(float w, float alpha, float edt, float dt, float gate)
{
	return (gate)*(edt)+(w)*(alpha)*(dt);
};
*/
///////////////////////////////////////////////////////////////////////////////
// +macros to calculate gate variable of any type
// BTW. It's funny but the current imlementation is faster then the one that
// uses the callback function
//-----------------------------------------------------------------------------
// 'type' is channel's type;
// 'par' are channels' parameters;
// 'step' is step of integration
// 'v' is membrane potential;
// 'lut' is either index for 'ions' aray or index for 'wsyn' array;
// 'ions' is array of ions' parameters of Ca- or Mg- ions which are used for NMDA synapse or Z-channels correspondingly;
// 'mod' is power of gate variable for channels
// 'gate' is gate variable;
// 'G' is 'gate'^'mod'  for channels or 'gate' for synapses
#define proc_gate( type, par, step, v, lut, ions, wsyn, mod, gate, G ) \
	switch( type ){ \
		case LSNS_NOGATE:{ \
			( gate ) = ( G ) = proc_nogate(); \
			} \
			break; \
		case LSNS_BYPASSGATE: \
			( G ) = lsns_pow( proc_passgate( gate ), mod ); \
			break; \
		case LSNS_GENERIC_INSTANT:{ \
			( gate ) = proc_ggate_inf( par, v ); \
			( G ) = lsns_pow( gate, mod ); \
			} \
			break; \
		case LSNS_GENERIC_T:{ \
			float gate_inf = proc_ggate_inf( par, v ); \
			float time = proc_ggate_t( par, v ); \
			( gate ) = lsns_exp_euler( gate, gate_inf, step, time ); \
			( G ) = lsns_pow( gate, mod ); \
			} \
			break; \
		case LSNS_GENERIC_TMOD:{ \
			float gate_inf = proc_ggate_inf( par, v ); \
			float time = 1; \
			proc_ggate_tmod( time, par, v ); \
			( gate ) = lsns_exp_euler( gate, gate_inf, step, time ); \
			( G ) = lsns_pow( gate, mod ); \
			} \
			break; \
		case LSNS_GENERIC_TAMOD:{ \
			float gate_inf = proc_ggate_inf( par, v ); \
			float time = 1; \
			proc_ggate_tamod( time, par, v ); \
			( gate ) = lsns_exp_euler( gate, gate_inf, step, time ); \
			( G ) = lsns_pow( gate, mod ); \
			} \
			break; \
		case LSNS_ALPHABETA_INSTANT:{ \
			float alpha = 1.; \
			proc_abgate_alpha( alpha, par, v ); \
			float beta = 1.; \
			proc_abgate_beta( beta, par, v ); \
			( gate ) = lsns_div( alpha, alpha+beta ); \
			( G ) = lsns_pow( gate, mod ); \
			} \
			break; \
		case LSNS_ALPHABETA_T:{ \
			float alpha = 1.; \
			proc_abgate_alpha( alpha, par, v ); \
			float beta = 1.; \
			proc_abgate_beta( beta, par, v ); \
			float time = proc_abgate_time( alpha, beta ); \
			float gate_inf = alpha*time; \
			time = _abgatet0( par )+_abgatetmax( par )*time; \
			( gate ) = lsns_exp_euler( gate, gate_inf, step, time ); \
			( G ) = lsns_pow( gate, mod ); \
			} \
			break; \
		case LSNS_ZGENERIC_INSTANT:{ \
			/* _ions_in( ions[lut] loads Ca- concentration inside the cell for Z-channels  float k = proc_zgate_k( par, in ); \*/\
			 float k = proc_zgate_k( par, _ions_in( ions[lut] )); \
			( gate ) = proc_zgatek_ginf( k ); \
			( G ) = lsns_pow( gate, mod ); \
			} \
			break; \
		case LSNS_ZGENERIC_T:{ \
			/* _ions_in( ions[lut] loads Ca- concentration inside the cell for Z-channels */\
			float k = proc_zgate_k( par, _ions_in( ions[lut] )); \
			float gate_inf = proc_zgatek_ginf( k ); \
			float time = _zgatet0( par )+lsns_div( _zgatetmax( par ), 1.f+_zgateG( par )*k ); \
			( gate ) = lsns_exp_euler( gate, gate_inf, step, time ); \
			( G ) = lsns_pow( gate, mod ); \
			} \
			break; \
		case LSNS_ZAPHABETA_INSTANT:{ \
			/* _ions_in( ions[lut] loads Ca- concentration inside the cell for Z-channels */\
			float alpha = proc_zgate_alpha( par, v, _ions_in( ions[lut] )); \
			float beta = proc_zgate_beta( par, v ); \
			( gate ) = proc_zgateab_ginf( alpha, beta ); \
			( G ) = lsns_pow( gate, mod ); \
			} \
			break; \
		case LSNS_ZAPHABETA_T:{ \
			/* _ions_in( ions[lut] loads Ca- concentration inside the cell for Z-channels */\
			float alpha = proc_zgate_alpha( par, v, _ions_in( ions[lut] )); \
			float beta = proc_zgate_beta( par, v ); \
			float time = lsns_div( 1.f, alpha+beta ); \
			float gate_inf = alpha*time; \
			time = _zgatet0( par )+_zgatetmax( par )*time; \
			( gate ) = lsns_exp_euler( gate, gate_inf, step, time ); \
			( G ) = lsns_pow( gate, mod ); \
			} \
			break; \
		case LSNS_PS_NMDA:{ \
			/* _ions_in( ions[lut] loads Mg- concentration inside the cell for NMDA synapse*/\
			( gate ) = proc_psgate1( _ions_in( ions[lut] ), v ); \
			( G ) = lsns_pow( gate, mod ); \
			} \
			break; \
		case LSNS_SYNAPSE:{ \
			/*loads total weight of connections converged on the synapse*/\
			float4 w = __lsns_cached( wsyn[lut] ); \
			( G ) = ( gate ) = proc_syngate1( _wsyn_total( w ), _wsyn_ah( w ), _wsyn_edt( w ), _wsyn_dt( w ), gate ); \
			}\
			break; \
		default: \
			G = gate = 0.;\
	} 
	
#endif /*__GATEPROC_H*/

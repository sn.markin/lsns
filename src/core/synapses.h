#pragma once

#ifndef __SYNAPSES_H
#define __SYNAPSES_H

#include "config.h"

#define LSNS_MAX_SYNPARS 64
///////////////////////////////////////////////////////////////////////////////
// +gates ids and brief descriptions
enum __lsns_synapse_types{
	LSNS_CLONE_SYN			= 0,						// copy weighted sum from specific synapse (see 'wsumdat' in kernels.h)
	LSNS_SUM_SYN			= 1,						// weighted-sum model of synapse (for network unit like drives, outputs etc): dt = 1; edt = 0;
	LSNS_SIGMA_SYN			= 2,						// sigma model of synapse (for non-spiking network units): dt = 1; edt = 0
	LSNS_PULSE_SYN			= 3,						// pulse model of synapse (for spiking network units): dt = 1; edt = exp( -step/tmax )
	LSNS_MAX_SYNS,
};
///////////////////////////////////////////////////////////////////////////////
// +extract parameters for all synaptic current from the structure 'synpar'
//=============================================================================
// get rate of transmitter release
#define _synA( par ) ( par ).Par1.x
// get parameter Edt: exp( -step/T ) for pulse synapse or 0 for other types of synapses
#define _synEdt( par ) ( par ).Par1.y
// get parameter Dt: 1 for all types of synapses
#define _synDt( par ) ( par ).Par1.z
// get time constant
#define _synTmax( par ) ( par ).Par1.w
// get type of synapse
#define _synType( par ) ( par ).Par2.x
///////////////////////////////////////////////////////////////////////////////
// structure 'synpar' to store all constant parameters for synapses
// of all types
typedef struct __lsns_align( 16 ) __synapres_par{
	float4 Par1;
	uint4 Par2;
} synpar;

#endif /*__SYNAPSES_H*/

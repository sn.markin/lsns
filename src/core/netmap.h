#include <map>
#include <vector>
#include <utility>
#include <unordered_map>

#include "kernels.h"
#include "engine.h"

#ifndef __LSNS_NETMAP_H
#define __LSNS_NETMAP_H

#define __ENG_NAMESPASE_BEGIN namespace engine{
#define	__ENG_NAMESPASE_END }

__ENG_NAMESPASE_BEGIN
///////////////////////////////////////////////////////////////////////////////
// kernel metrics for x86 architecture
class x86metric{
	public: // constructors/destructor
		x86metric( void ){};
		~x86metric( void ){};
	public: // operators
		size_t operator() ( size_t nthreads, size_t &nt, size_t &bx, size_t &by ){
			bx = by = 1;
			#if defined( __LSNS_DEBUG__ )
				nthreads = ( nthreads < NUM_THREADS )? NUM_THREADS: nthreads;
				nt = ( nthreads%NUM_THREADS == 0 )? nthreads: ( nthreads/NUM_THREADS+1 )*NUM_THREADS;
			#else
				nt = nthreads;
			#endif /* __LSNS_DEBUG__ */
			return nt*bx*by;
		};
};
///////////////////////////////////////////////////////////////////////////////
// kernel metrics for CUDA architecture
class cudametric{
	public: // constructors/destructor
		cudametric( void ){};
		~cudametric( void ){};
	public: // operators
		size_t operator() ( size_t nthreads, size_t &nt, size_t &bx, size_t &by ){
			bx = by = 1; nt = NUM_THREADS;
			nthreads = ( nthreads < NUM_THREADS )? NUM_THREADS: nthreads;
			nthreads = ( nthreads%NUM_THREADS == 0 )? nthreads: ( nthreads/NUM_THREADS+1 )*NUM_THREADS;
			size_t nblocks = ( nthreads%NUM_THREADS == 0 )? nthreads/NUM_THREADS: nthreads/NUM_THREADS+1;
			if( nblocks < MAX_XBLOCKS ){
				bx = nblocks;
			}
			else{
				nblocks = ( nblocks%GRN_XBLOCKS == 0 )? nblocks: ( nblocks/GRN_XBLOCKS+1 )*GRN_XBLOCKS;
				for( int i = MAX_XBLOCKS/GRN_XBLOCKS; i > 0; --i ){
					if( nblocks%(i*GRN_XBLOCKS) == 0 ){
						bx = i*GRN_XBLOCKS;
						by = nblocks/(i*GRN_XBLOCKS);
						break;
					}
				}
			}
			return nt*bx*by;
		}
};
///////////////////////////////////////////////////////////////////////////////
// kdim3 optimize kernel metrics according to device architecture
template <typename metric>
class kdim3{
	public: // constructors/destructor
		kdim3( void ) : NT( 1 ), BX( 1 ), BY( 1 ){};
		kdim3( size_t nthreads ){ metric m; m( nthreads, NT, BX, BY ); };
		kdim3( const kdim3 &dim ) : NT( dim.NT ), BX( dim.BX ), BY( dim.BY ){};
		~kdim3( void ){};
	public: // operators
		kdim3 &operator = ( size_t nthreads ){ metric m; m( nthreads, NT, BX, BY ); return *this; };
		kdim3 &operator = ( const kdim3 &dim ){ NT = dim.NT; BX = dim.BX; BY = dim.BY; return *this; };
		operator int(){ return int( NT*BX*BY ); }; /* return total number of threads */
		operator size_t(){ return NT*BX*BY; }; /* return total number of threads */
		operator dim3(){ return dim3( BX, BY, 1); }; /* return grid geometry */
	public: // data
		size_t NT;		// number of threads per one block
		size_t BX;		// number of x blocks in 2-D grid
		size_t BY;		// number of y blocks in 2-D grid
};
///////////////////////////////////////////////////////////////////////////////
// class wsum: auxiliary class that initialize network structures related to
// 'wsumdat'. It provides minimal functionality to describe the weighted sum 
// of input signals for particular synapse(s) and maps the description to 
// to correspondent network structure. First, to map actual weighted sum onto 
// correspondent memory of network structure the 'resetmap' should be called.
// Then the method 'map2mem' should be called for all synapses.
class wsum{
	public: // type definition
		typedef __lsns_uint32 wstype;			// type of summation
		typedef __lsns_uint32 wspar;			// parameters of summation
		typedef __lsns_uint32 pos;
		typedef std::pair<pos, wspar> wstrg;		// pair<position in the array of weighted sum (wsumdat), parameters of synaptic summation>
		typedef std::pair<pos, float> con;		// pair<position in the array of sources (cells and units),weight of connection>
		typedef std::vector<wspar> wsmap;		// type of synaptic summation
		typedef std::vector<con> srcmap;		// list of synaptic connections
		typedef size_t hashws;				// hash value of weghted sum 
	public: // constructors/destructor
		wsum( void ) : Type( wstype( -1 )){};
		wsum( const wsum &syn ) : Type( syn.Type ), WSMap( syn.WSMap ), SrcMap( syn.SrcMap ){};
		~wsum( void ){};
	public: // operators
		wsum& operator = ( const wsum &syn ){ 
			Type = syn.Type; WSMap = syn.WSMap; SrcMap = syn.SrcMap; return *this; 
		};
		bool operator == ( const wsum &syn ){
			return  ( Type == syn.Type )&&( SrcMap == syn.SrcMap );
		}
		bool operator == ( const srcmap &src ){
			return SrcMap == src;
		}
	public: // get dimention of specific synapses
		pos size_ws( void ){				// return total number of synapses related to particular weighted sum
			return pos( WSMap.size());
                };
		pos size_src( void ){				// return total number of connections for particular weighted sum
			return pos( SrcMap.size());
                };
		pos size4_src( void ){				// return total number of float4 and uint4 structures to store connections
			pos size = pos( SrcMap.size()); 
			return ( size%4 == 0 )? size/4: ( size/4+1 );
		};
	public: // operations with weighted sum and synapses
		bool create_syn( wstype t, wspar par );	// create synapse(type, parameters of summation)
		bool add_ws( const wspar &ws, bool major );	// add weighed wum
		bool add_src( const srcmap &src );		// add sources to the synapse
	public: // initialize network data related to the synapse
static		void resetmap( void ){ Trg0 = Src0 = 0; };	// reset all variales related to memory map
		wstrg map2mem( netdat *data, synpar *par, std::vector<wstrg> &wsyn ); // map particular synapses and connections to network structure
	private:
		void map_ws2mem( uint4 *src, float4 *w, pos index ); // map 4 consequence weights and sources to correspondent w and src
	public: // data
		wstype Type;			// type of synaptic summation<=>wsumdat.SynType.x (see: synapse.h)
		wsmap WSMap;			// list of similar synapses (the same Type and SrcMap)
		srcmap SrcMap;			// map of synaptic connections (pair of weight and source); SrcMap.size()<=>wsumdat.SynType.z; SrcMap[i]<=>wsumdat.Wall&SrcLUT[srcpos+i/4](x,y,z,w) (see synapse.h)
	private:
static		pos Src0;
static		pos Trg0;
};
///////////////////////////////////////////////////////////////////////////////
// class syntype
class syntype{
	public: // type definition
		typedef __lsns_uint32 wstype;			// type of summation
		typedef __lsns_uint32 wspar;			// parameters of summation
	public: // constructors/destructor
		syntype( void ){};
		syntype( float g, wstype t, wspar p ) : Gmax( g ), Type( t ), Par( p ){};
		syntype( const syntype &t ) : Gmax( t.Gmax ), Type( t.Type ), Par( t.Par ){};
		~syntype( void ){};
	public: // operators
		syntype &operator = ( const syntype &t ){ Gmax = t.Gmax; Type = t.Type; Par = t.Par; return *this; };
	public: // data
		float Gmax;			// maximal conductance
		wstype Type;			// type of synaptic summation
		wspar Par;			// parameters of synaptic summation

};
///////////////////////////////////////////////////////////////////////////////
// class syntrg
class syntrg{
	public: // type definition
		typedef __lsns_uint32 pos;
	public: // constructors/destructor
		syntrg( void ){};
		syntrg( pos s, pos v, pos e ) : Syn( s ), Vm( v ), Eds( e ){};
		syntrg( const syntrg &t ) : Syn( t.Syn ), Vm( t.Vm ), Eds( t.Eds ){};
		~syntrg( void ){};
	public: // operators
		syntrg &operator = ( const syntrg &t ){ Syn = t.Syn; Vm = t.Vm; Eds = t.Eds; return *this; };
	public: // data
		pos Syn;			// position in ChanG
		pos Vm;				// position in CellV/UnitV
		pos Eds;			// position in IonE
};
///////////////////////////////////////////////////////////////////////////////
// class mapsyn 
// chandat (target.ChanG)
//	ChanType.x <- LSNS_SYNAPSE; ChanType.y <- synpar (trg_.second in TrgMap); z,w -reserved (could be initialize elsewhere)
//	ChanLUT.x <- Vm (target.CellV/UnitV); ChanLUT.y <- Eds(target.IonsE); ChanLUT.z <- pos in Wsyn (trg_.first in TrgMap); w-reserved
// celldat/unitdat (target.CellV/UnitV)
//	VChanGLUT/UChanGLUT.x <- counter++, add target.ChanG to look-up-table
class mapsyn{
	public: // type definition
		typedef __lsns_uint32 pos;			// position of network element 
		typedef __lsns_uint32 wspar;			// parameters of synaptic summation
		///////////////////////////////////////////////////////////////////////////////
		// class syntrg_  parameters of synapse
		class syntrg_{
			public: // constructors/destructor
				syntrg_( void ){};
				syntrg_( float g, pos s, pos v, pos e ) : Gmax( g ), Syn( s ), Vm( v ), Eds( e ){};
				syntrg_( const syntrg_ &t ) : Gmax( t.Gmax ), Syn( t.Syn ), Vm( t.Vm ), Eds( t.Eds ){};
				~syntrg_( void ){};
			public: // operators
				syntrg_ &operator = ( const syntrg_ &t ){ Gmax = t.Gmax; Syn = t.Syn; Vm = t.Vm; Eds = t.Eds; return *this; };
				bool operator == ( const syntrg_ &t ){ return Syn == t.Syn; };
				bool operator < ( const syntrg_ &t ){ return Syn < t.Syn; };
			public: // data
				float Gmax;			// maximal conductance
				pos Syn;			// position in ChanG
				pos Vm;				// position in CellV/UnitV
				pos Eds;			// position in IonE
		};
		class trgmap_{
			public: // type definition
//				typedef std::unordered_multimap<wspar, syntrg_> map_;
				typedef std::multimap<wspar, syntrg_> map_;
			public: // constructors/destructor
				trgmap_( void ){};
				trgmap_( const trgmap_ &t ) : WSum( t.WSum ), TrgMap( t.TrgMap ){};
				~trgmap_( void ){};
			public: // operators
				trgmap_ &operator = ( const trgmap_ &t ){ WSum = t.WSum; TrgMap = t.TrgMap; return *this; };
			public: // data
				wsum WSum;
				map_ TrgMap;
		};
//		typedef std::unordered_multimap<wsum::hashws, trgmap_> synmap;// list of synapses for whole network
		typedef std::multimap<wsum::hashws, trgmap_> synmap;// list of synapses for whole network
		typedef synmap::iterator mit;
	public: // constructors/destructor
		mapsyn( void ){};
		mapsyn( const mapsyn &map ){};
		~mapsyn( void ){};
	public: // operators
		mapsyn& operator = ( const mapsyn &map ){
			return *this;
		};
	public: // methods
		pos size_wsum( void ){				// total number of all weighted sums
			pos size = 0;
			for( mit _syn = SynMap.begin(); _syn != SynMap.end(); size += _syn->second.WSum.size_ws(), ++_syn );
			return size;
		};
		pos size_src( void ){				// return total number of connections for all weighted sums
			pos size = 0;
			for( mit _syn = SynMap.begin(); _syn != SynMap.end(); size += _syn->second.WSum.size_src(), ++_syn );
			return size;
                };
		pos size4_src( void ){				// return total number of float4 and uint4 structures to store connections
			pos size = 0;
			for( mit _syn = SynMap.begin(); _syn != SynMap.end(); size += _syn->second.WSum.size4_src(), ++_syn );
			return size;
		};
		void add_synapse( const syntype &syn, const syntrg &trg, wsum::srcmap &src, bool compress = true );
		void map2mem(  netdat *data, synpar *par );
	private:
		void add_newsyn( syntype::wstype type, std::pair<wspar, syntrg_> &trg, const wsum::srcmap &src, wsum::hashws hash );
	public: // data
		synmap	SynMap;			// list of synapses for whole network
};

__ENG_NAMESPASE_END
#endif /*__LSNS_NETMAP_H*/

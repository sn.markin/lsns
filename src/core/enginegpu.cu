///////////////////////////////////////////////////////////////////////////////
// CUDA-based architecture version
#include "engine.h"
#include "debug.h"

#if defined ( __CUDA__ )

#define __LSNS_ENGINE_CPP

#include <cuda_runtime.h>
#include "helper_cuda.h"
#include "cuda_timer.h"

#include "util.h"

#include <stdlib.h> //Added by Taegyo
#include <string.h> //Added by Taegyo
#include <iostream> //Added by Taegyo

using namespace std;

#include "kernels.cpp"
#include "engine_common.cpp"

///////////////////////////////////////////////////////////////////////////////
// interface 
//=================== local methods ==========================================
///////////////////////////////////////////////////////////////////////////////
// +'lsns_map2dev': initialize the constant parameters, allocates memory on 
// device and copy the network configuration from host memory to device memory
bool lsns_map2dev( netpar &par )
{
	//constant parameters
	float4 simdat = { 0.f, -1.f, -10.f, 0.f };
	uint4 iondim  = { 0, LSNS_MAX_IONPARS, 0, 0 }; 
	uint4 syndim  = { 0, 0, 0, 0 };
	uint4 chandim = { 0, LSNS_MAX_GATES, LSNS_MAX_GATEPARS+1, 0 }; 
	uint4 netdim  = { 0, 0, 0, 0 };
	uint4 viewdim = { 0, 0, 0, 0 };
	// make sure that size of constant parameters is not exceed 64K limit
	__lsns_assert( sizeof( float4 )+5*sizeof( uint4 )+sizeof( gatepar )*LSNS_MAX_GATEPARS+sizeof( ionspar )*LSNS_MAX_IONPARS < MAX_CONST_MEM );
	cudaError_t e = cudaSetDevice( gpuGetMaxGflopsDeviceId()); __lsns_assert( e == cudaSuccess ); // initialize the device
	e = cudaSetDeviceFlags( cudaDeviceMapHost ); __lsns_assert( e == cudaSuccess ); // enable zero copy access
	//initialize the constant parameters
	_sim_step( simdat ) = par.Step;
	_sim_thresh( simdat ) = par.Threshold;
	_net_nions( iondim ) = par.NetDim.MaxIons;
	_net_nsyn( syndim ) = par.NetDim.MaxWsyn;
	_net_nws( syndim ) = par.NetDim.MaxWS;
	_net_nchans( chandim ) = par.NetDim.MaxChan;
	_net_ncells( netdim ) = par.NetDim.MaxCells;
	_net_nunits( netdim ) = par.NetDim.MaxUnits;
	_net_maxvw( viewdim ) = par.NetDim.MaxGlobalData; 
	_net_nviews( viewdim ) = par.NetDim.DevViewPars;
	// copy constant parameters to CUDA constant memory
	e = cudaMemcpyToSymbol( SimDat, &simdat, sizeof( float4 )); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpyToSymbol( IonDim, &iondim, sizeof( uint4 )); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpyToSymbol( SynDim, &syndim, sizeof( uint4 )); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpyToSymbol( ChanDim, &chandim, sizeof( uint4 )); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpyToSymbol( NetDim, &netdim, sizeof( uint4 )); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpyToSymbol( ViewDim, &viewdim, sizeof( uint4 )); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpyToSymbol( Gates, par.Gates, sizeof( gatepar )*LSNS_MAX_GATEPARS ); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpyToSymbol( Ions, par.Ions, sizeof( ionspar )*LSNS_MAX_IONPARS ); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpyToSymbol( Synapses, par.Synapses, sizeof( synpar )*LSNS_MAX_SYNPARS ); __lsns_assert( e == cudaSuccess );
	// allocate device-specific memory and copy initialized arrays from host memory to device memory
	lsns_alloc_dev( par.Data, &par.NetDim );
	return true;
}
///////////////////////////////////////////////////////////////////////////////
// +'lsns_alloc_dev' allocates device-specific memory and copy initialized arrays 
// from host memory to device memory (for non-cuda version this are pointers to the 
// same data structures)
bool lsns_alloc_dev( netdat *data, netdim *dim )
{
	int nviews = dim->DevViewPars;
	cudaError_t e;
	// allocate memory for DevMap structure
	data->DevMap = ( netdat *)malloc( sizeof( netdat )); __lsns_assert( data->DevMap != NULL );
	// allocate device memory for network data
	e = cudaMalloc(( void **)&( data->DevMap->Counter ), sizeof( float4 )); __lsns_assert( e == cudaSuccess );
	e = cudaMalloc(( void **)&( data->DevMap->GlobalData ), sizeof( float4 )*dim->MaxGlobalData ); __lsns_assert( e == cudaSuccess );
	e = cudaMalloc(( void **)&( data->DevMap->GlobalLUT ), sizeof( float4 )*dim->MaxGlobalLUT ); __lsns_assert( e == cudaSuccess );
	e = cudaMalloc(( void **)&( data->DevMap->IOData.ViewLUT ), sizeof( uint4 )*dim->DevViewPars ); __lsns_assert( e == cudaSuccess );
	// copy pointers
	e = cudaMemcpy( data->DevMap->Counter, data->Counter, sizeof( float4 ), cudaMemcpyHostToDevice ); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpy( data->DevMap->GlobalData, data->GlobalData, sizeof( float4 )*dim->MaxGlobalData, cudaMemcpyHostToDevice ); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpy( data->DevMap->GlobalLUT, data->GlobalLUT, sizeof( float4 )*dim->MaxGlobalLUT, cudaMemcpyHostToDevice ); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpy( data->DevMap->IOData.ViewLUT, data->IOData.ViewLUT, sizeof( uint4 )*dim->DevViewPars, cudaMemcpyHostToDevice );  __lsns_assert( e == cudaSuccess );
	e = cudaMalloc(( void **)&( data->DevMap->IOData.HostViewData ), sizeof( float4 )*nviews*MAX_STORED_STEPS ); __lsns_assert( e == cudaSuccess );
	for( int i = 0; i < MAX_STORED_STEPS; ++i ){
		data->DevMap->IOData.DevViewData[i] = data->DevMap->IOData.HostViewData+i*( nviews );
	}
	// 'HostViewData' is allocating as pinned-memory to improve the engine performance
	e = cudaHostAlloc(( void **)&data->IOData.HostViewData, sizeof( float4 )*nviews*MAX_STORED_STEPS, cudaHostAllocMapped ); __lsns_assert( e == cudaSuccess );
	// map data pointers to allocated memory
	lsns_data2mem( data->DevMap, dim );
	return true;
}

///////////////////////////////////////////////////////////////////////////////
// +'lsns_free_dev' free device specific memory and reset constant parameters of 
// the network
bool lsns_free_dev( netdat *data )
{
	// reset the constant parameters of the network
	float4 simdat = { 0.f, -1.f, -10.f, 0.f };
	uint4 iondim  = { 0, LSNS_MAX_IONPARS, 0, 0 };
	uint4 syndim  = { 0, 0, 0, 0 };
	uint4 chandim = { 0, LSNS_MAX_GATES, LSNS_MAX_GATEPARS+1, 0 };
	uint4 netdim  = { 0, 0, 0, 0 };
	uint4 viewdim = { 0, 0, 0, 0 };
	// copy constant parameters to CUDA constant memory
	cudaError_t e;
	e = cudaMemcpyToSymbol( SimDat, &simdat, sizeof( float4 )); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpyToSymbol( SynDim, &syndim, sizeof( uint4 )); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpyToSymbol( IonDim, &iondim, sizeof( uint4 )); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpyToSymbol( ChanDim, &chandim, sizeof( uint4 )); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpyToSymbol( NetDim, &netdim, sizeof( uint4 )); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpyToSymbol( ViewDim, &viewdim, sizeof( uint4 )); __lsns_assert( e == cudaSuccess );
	// free device-specific memory for actual parameters and look-up-tables (for non-cuda version simply free the memory allocated for DevMap structure)
	e = cudaFree( data->DevMap->IOData.HostViewData ); __lsns_assert( e == cudaSuccess );
	e = cudaFree( data->DevMap->IOData.ViewLUT ); __lsns_assert( e == cudaSuccess );
	e = cudaFree( data->DevMap->GlobalLUT ); __lsns_assert( e == cudaSuccess );
	e = cudaFree( data->DevMap->GlobalData ); __lsns_assert( e == cudaSuccess );
	e = cudaFree( data->DevMap->Counter ); __lsns_assert( e == cudaSuccess );
	// free pinned memory
	e = cudaFreeHost( data->IOData.HostViewData ); __lsns_assert( e == cudaSuccess );
	__lsns_assert( data->DevMap != NULL ); free( data->DevMap ); data->DevMap = NULL;
	e = cudaDeviceReset(); __lsns_assert( e == cudaSuccess );
	return true;
}
//=================== global methods ==========================================
///////////////////////////////////////////////////////////////////////////////
// +'lsns_run' initialize simulation
bool lsns_reinit( netpar &par ) // call for each session
{
	cudaError_t e;
	e = cudaMemcpy( par.Data->DevMap->GlobalData, par.Data->GlobalData, sizeof( float4 )*par.NetDim.MaxGlobalData, cudaMemcpyHostToDevice ); __lsns_assert( e == cudaSuccess );
	lsns_refresh( par );
	return true;
}

bool lsns_refresh( netpar &par ) // init before simulation
{
	cudaError_t e;
	_sim_nstep( par.Data->Counter[0] ) = 0;  // SM
	e = cudaMemcpy( par.Data->DevMap->Counter, par.Data->Counter, sizeof( float4 ), cudaMemcpyHostToDevice ); __lsns_assert( e == cudaSuccess );
	e = cudaMemset( par.Data->DevMap->IOData.HostViewData, 0, sizeof( float4 )*par.NetDim.DevViewPars*MAX_STORED_STEPS ); __lsns_assert( e == cudaSuccess );
	return true;
}

bool lsns_reset( netpar &par ) // call for each trial
{
	cudaError_t e;
	e = cudaMemcpy( par.Data->DevMap->Channels.ChanG, par.Data->Channels.ChanG, sizeof( float4 )*par.NetDim.MaxChan, cudaMemcpyHostToDevice ); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpy( par.Data->DevMap->Cells.CellV, par.Data->Cells.CellV, sizeof( float4 )*par.NetDim.MaxCells, cudaMemcpyHostToDevice ); __lsns_assert( e == cudaSuccess );
//<<<--- TODO <create and call kernel that modifies V>
/*
	for( int i = 0; i < par.NetDim.MaxCells; ++i ){
		float4 *v = par.Data->Cells.CellV+i;
		v->x = float( ::normrd( Rng, v->x, 0.3 ));
	}
*/
	return true;
}

void lsns_copy2dev( float4 *base, float4 *data, uint4 *lut, size_t sz4 )
{
	cudaError_t e;
	float4 __lsns_align( 16 ) *data_cuda = NULL;
	uint4 __lsns_align( 16 )  *lut_cuda = NULL;
	e = cudaMalloc(( void **)&( data_cuda ), sizeof( float4 )*sz4); __lsns_assert( e == cudaSuccess );
	e = cudaMalloc(( void **)&( lut_cuda ), sizeof( uint4 )*sz4); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpy( data_cuda, data, sizeof( float4 )*sz4, cudaMemcpyHostToDevice ); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpy( lut_cuda, lut, sizeof( float4 )*sz4, cudaMemcpyHostToDevice ); __lsns_assert( e == cudaSuccess );
	e = cudaDeviceSynchronize(); __lsns_assert( e == cudaSuccess );
	lsns_copy2dev_cuda<<<sz4, 1>>>( base, data_cuda, lut_cuda );
	e = cudaDeviceSynchronize(); __lsns_assert( e == cudaSuccess );
	e = cudaFree( lut_cuda ); __lsns_assert( e == cudaSuccess );
	e = cudaFree( data_cuda ); __lsns_assert( e == cudaSuccess );
}

void lsns_copy2host( float4 *base, float4 *data, uint4 *lut, size_t sz4 )
{
	cudaError_t e;
	float4 __lsns_align( 16 ) *data_cuda = NULL;
	uint4 __lsns_align( 16 )  *lut_cuda = NULL;
	e = cudaMalloc(( void **)&( data_cuda ), sizeof( float4 )*sz4); __lsns_assert( e == cudaSuccess );
	e = cudaMalloc(( void **)&( lut_cuda ), sizeof( uint4 )*sz4); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpy( lut_cuda, lut, sizeof( float4 )*sz4, cudaMemcpyHostToDevice ); __lsns_assert( e == cudaSuccess );
	e = cudaDeviceSynchronize(); __lsns_assert( e == cudaSuccess );
	lsns_copy2host_cuda<<<sz4, 1>>>( base, data_cuda, lut_cuda );
	e = cudaDeviceSynchronize(); __lsns_assert( e == cudaSuccess );
	e = cudaMemcpy( data, data_cuda, sizeof( float4 )*sz4, cudaMemcpyDeviceToHost ); __lsns_assert( e == cudaSuccess );
	e = cudaFree( lut_cuda ); __lsns_assert( e == cudaSuccess );
	e = cudaFree( data_cuda ); __lsns_assert( e == cudaSuccess );
}

///////////////////////////////////////////////////////////////////////////////
// +'lsns_transition' runs simulation
size_t lsns_transition( netpar &par, size_t nsteps )
{
	// ions
	uint4 *iontype = par.Data->DevMap->Ions.IonsType;
	uint4 *ionlut = par.Data->DevMap->Ions.IonsLUT;
	uint4 *ichanglut = par.Data->DevMap->Ions.IChanGLUT;
	float4 *ioni = par.Data->DevMap->Ions.IonsI;
	float4 *ione = par.Data->DevMap->Ions.IonsE;
	// weighted sums
	uint4 *syntype = par.Data->DevMap->Wsum.SynType;
	uint4 *srclut = par.Data->DevMap->Wsum.SrcLUT;
	float4 *src = par.Data->DevMap->Wsum.Srcs;
	float4 *wall = par.Data->DevMap->Wsum.Wall;
	float4 *wsyn  = par.Data->DevMap->Wsum.Wsyn;
	// channels
	uint4 *chantype = par.Data->DevMap->Channels.ChanType;
	uint4 *chanlut = par.Data->DevMap->Channels.ChanLUT;
	float4 *chanmh = par.Data->DevMap->Channels.ChanMH;
	float4 *chang = par.Data->DevMap->Channels.ChanG;
	// cells
	uint4 *changlut = par.Data->DevMap->Cells.VChanGLUT;
	uint4 *ionilut = par.Data->DevMap->Cells.IonsILUT;
	float4 *cellv = par.Data->DevMap->Cells.CellV;
	float4 *cellvstat = par.Data->DevMap->Cells.CellStat;
	// units
	uint4 *uchanglut = par.Data->DevMap->Units.UChanGLUT;
	float4 *unitv = par.Data->DevMap->Units.UnitV;
	dim3 nthreads( NUM_THREADS, 1 );
	dim3 nblocks[LSNS_MAX_KERNEL] = {
		kdim3<cudametric>(),			// LSNS_CTRLS_KERNEL
		kdim3<cudametric>(par.NetDim.MaxIons),	// LSNS_IONS_KERNEL
		kdim3<cudametric>(par.NetDim.MaxWS),	// LSNS_WSUM_KERNEL
		kdim3<cudametric>(par.NetDim.MaxChan),	// LSNS_CHAN_KERNEL
		kdim3<cudametric>(par.NetDim.MaxCells),	// LSNS_CELLS_KERNEL
		kdim3<cudametric>(par.NetDim.MaxUnits),	// LSNS_UNITS_KERNEL
	};
	cudaError_t e;
	for( size_t step = 0; step < nsteps; ++step ){
		ions_kernel<<<nblocks[LSNS_IONS_KERNEL], nthreads>>>( iontype, ionlut, ichanglut, chang, cellv, ione, ioni );
		e = cudaDeviceSynchronize(); __lsns_assert( e == cudaSuccess );
		wsum_kernel<<<nblocks[LSNS_WSUM_KERNEL], nthreads>>>( syntype, srclut, src, wall, wsyn );   // SM change the kernel
		e = cudaDeviceSynchronize(); __lsns_assert( e == cudaSuccess );
		chan_kernel<<<nblocks[LSNS_CHAN_KERNEL], nthreads>>>( chantype, chanlut, ione, wsyn, cellv, chanmh, chang );
		e = cudaDeviceSynchronize(); __lsns_assert( e == cudaSuccess );
		cell_kernel<<<nblocks[LSNS_CELLS_KERNEL], nthreads>>>( changlut, ionilut, chang, ioni, cellv, cellvstat );
		e = cudaDeviceSynchronize(); __lsns_assert( e == cudaSuccess );
		unit_kernel<<<nblocks[LSNS_UNITS_KERNEL], nthreads>>>( uchanglut, chang, unitv );
		e = cudaDeviceSynchronize(); __lsns_assert( e == cudaSuccess );
	}
	resetstat_kernel<<<nblocks[LSNS_CELLS_KERNEL], nthreads>>>( cellvstat );
	e = cudaDeviceSynchronize(); __lsns_assert( e == cudaSuccess );
	return nsteps;
}

///////////////////////////////////////////////////////////////////////////////
// +'lsns_run' runs simulation
size_t lsns_run( netpar &par, vector<nns_iodata>& iodata, size_t nsteps )
{
	// common info
	uint4 *counter = par.Data->DevMap->Counter;
	// ions
	uint4 *iontype = par.Data->DevMap->Ions.IonsType;
	uint4 *ionlut = par.Data->DevMap->Ions.IonsLUT;
	uint4 *ichanglut = par.Data->DevMap->Ions.IChanGLUT;
	float4 *ioni = par.Data->DevMap->Ions.IonsI;
	float4 *ione = par.Data->DevMap->Ions.IonsE;
	// weighted sums
	uint4 *syntype = par.Data->DevMap->Wsum.SynType;
	uint4 *srclut = par.Data->DevMap->Wsum.SrcLUT;
	float4 *src = par.Data->DevMap->Wsum.Srcs;
	float4 *wall = par.Data->DevMap->Wsum.Wall;
	float4 *wsyn  = par.Data->DevMap->Wsum.Wsyn;
	// channels
	uint4 *chantype = par.Data->DevMap->Channels.ChanType;
	uint4 *chanlut = par.Data->DevMap->Channels.ChanLUT;
	float4 *chanmh = par.Data->DevMap->Channels.ChanMH;
	float4 *chang = par.Data->DevMap->Channels.ChanG;
	// cells
	uint4 *changlut = par.Data->DevMap->Cells.VChanGLUT;
	uint4 *ionilut = par.Data->DevMap->Cells.IonsILUT;
	float4 *cellv = par.Data->DevMap->Cells.CellV;
	float4 *cellvstat = par.Data->DevMap->Cells.CellStat;
	// units
	uint4 *uchanglut = par.Data->DevMap->Units.UChanGLUT;
	float4 *unitv = par.Data->DevMap->Units.UnitV;
	// iodata
	uint4 *viewlut = par.Data->DevMap->IOData.ViewLUT;
	float4 **viewdata = par.Data->DevMap->IOData.DevViewData;
	float4 *dframedata = par.Data->DevMap->IOData.HostViewData;
	float4 *hframedata = par.Data->IOData.HostViewData;
	float4 *globaldata = par.Data->DevMap->IOData.GlobalData;
	dim3 nthreads( NUM_THREADS, 1 );
	dim3 nblocks[LSNS_MAX_KERNEL] = {
		kdim3<cudametric>(),			// LSNS_CTRLS_KERNEL
		kdim3<cudametric>(par.NetDim.MaxIons),	// LSNS_IONS_KERNEL
		kdim3<cudametric>(par.NetDim.MaxWS),	// LSNS_WSUM_KERNEL
		kdim3<cudametric>(par.NetDim.MaxChan),	// LSNS_CHAN_KERNEL
		kdim3<cudametric>(par.NetDim.MaxCells),	// LSNS_CELLS_KERNEL
		kdim3<cudametric>(par.NetDim.MaxUnits),	// LSNS_UNITS_KERNEL
		kdim3<cudametric>(par.NetDim.DevViewPars), // LSNS_STORE2DEV_KERNEL
	};
	cudaError_t e;
#if defined( __LSNS_DEBUG__ )
	float time = 0.;
	cuda_timer timer;
	timer.start();
#endif /*__LSNS_DEBUG__*/
	size_t counter_ = _sim_nstep( par.Data->Counter[0] );
	nsteps = ( counter_ < nsteps )? nsteps-counter_: 0;
	for( size_t step = 0; step < nsteps; ++step ){
		control_kernel<<<nblocks[LSNS_CTRLS_KERNEL], nthreads>>>( counter );
		e = cudaDeviceSynchronize(); __lsns_assert( e == cudaSuccess );
		ions_kernel<<<nblocks[LSNS_IONS_KERNEL], nthreads>>>( iontype, ionlut, ichanglut, chang, cellv, ione, ioni );
		e = cudaDeviceSynchronize(); __lsns_assert( e == cudaSuccess );
		wsum_kernel<<<nblocks[LSNS_WSUM_KERNEL], nthreads>>>( syntype, srclut, src, wall, wsyn );   // SM change the kernel
		e = cudaDeviceSynchronize(); __lsns_assert( e == cudaSuccess );
		chan_kernel<<<nblocks[LSNS_CHAN_KERNEL], nthreads>>>( chantype, chanlut, ione, wsyn, cellv, chanmh, chang );
		e = cudaDeviceSynchronize(); __lsns_assert( e == cudaSuccess );
		cell_kernel<<<nblocks[LSNS_CELLS_KERNEL], nthreads>>>( changlut, ionilut, chang, ioni, cellv, cellvstat );
		e = cudaDeviceSynchronize(); __lsns_assert( e == cudaSuccess );
		unit_kernel<<<nblocks[LSNS_UNITS_KERNEL], nthreads>>>( uchanglut, chang, unitv );
		e = cudaDeviceSynchronize(); __lsns_assert( e == cudaSuccess );
		store2dev_kernel<<<nblocks[LSNS_STORE2DEV_KERNEL], nthreads>>>( globaldata, viewlut, viewdata[step%MAX_STORED_STEPS] );
		e = cudaDeviceSynchronize(); __lsns_assert( e == cudaSuccess );
		if(( step+1 )%MAX_STORED_STEPS == 0 ){
			e = cudaMemcpy( hframedata, dframedata, sizeof( float4 )*par.NetDim.DevViewPars*MAX_STORED_STEPS, cudaMemcpyDeviceToHost ); __lsns_assert( e == cudaSuccess );
			size_t global_step = counter_+step;
			for (size_t is = 0; is < MAX_STORED_STEPS; is++) {
				int iT = 0, totaln;
				for (size_t id = 0; id < iodata.size(); id++) {
					if (iodata[id].type == 16) {
						totaln = iodata[id].ntneu + iodata[id].nnsu;
					}
					else {
						totaln = iodata[id].ntneu;
					}
					for (int itn = 0; itn < totaln; itn++){
						int tmp = itn / 4 + iT; //by TK 10/13/2015
#if !defined( __DATA_OUT__ )
						switch (itn % 4) {
							case 0:
								iodata[id].data[(global_step + 1 - MAX_STORED_STEPS + is)*totaln + itn] = hframedata[tmp + is*par.NetDim.DevViewPars].x;
								break;
							case 1:
								iodata[id].data[(global_step + 1 - MAX_STORED_STEPS + is)*totaln + itn] = hframedata[tmp + is*par.NetDim.DevViewPars].y;
								break;
							case 2:
								iodata[id].data[(global_step + 1 - MAX_STORED_STEPS + is)*totaln + itn] = hframedata[tmp + is*par.NetDim.DevViewPars].z;
								break;
							case 3:
								iodata[id].data[(global_step + 1 - MAX_STORED_STEPS + is)*totaln + itn] = hframedata[tmp + is*par.NetDim.DevViewPars].w;
								break;
							};
#else
							float temp;
 							switch( itn%4 ){
								case 0:
									temp = hframedata[tmp+is*par.NetDim.DevViewPars].x;
									break;
								case 1:
									temp = hframedata[tmp+is*par.NetDim.DevViewPars].y;
									break;
								case 2:
									temp = hframedata[tmp+is*par.NetDim.DevViewPars].z;
									break;
								case 3:
									temp = hframedata[tmp+is*par.NetDim.DevViewPars].w;
									break;
							};
							if (temp == 1) {
								std::cerr << itn << " " << temp+(step+1-MAX_STORED_STEPS+is)*simstep << endl;
							};
#endif //__DATA_OUT__
					};
					iT = iT + (int)ceil(totaln / 4.); //by TK 10/13/2015
				};
			};
		}
	}
	e = cudaMemcpy( par.Data->Counter, par.Data->DevMap->Counter, sizeof( float4 ), cudaMemcpyDeviceToHost ); __lsns_assert( e == cudaSuccess );
#if defined( __LSNS_DEBUG__ )
	timer.stop();
	time += timer.elapsed();
	cout << "time of simulation: " << time << " msec" << endl;
#endif /*__LSNS_DEBUG__*/
	counter_ = _sim_nstep( par.Data->Counter[0] )-counter_; 
	return counter_; // return actual number of iterations were done
}
#endif

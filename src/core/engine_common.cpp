#if defined ( __LSNS_ENGINE_CPP ) /* engine_common.cpp must be included directly to engine source file */
extern bool lsns_data2mem( netdat *data, netdim *dim );
extern bool lsns_alloc_dev( netdat *data, netdim *dim );
extern bool lsns_free_dev( netdat *data );

using namespace engine;

//=================== global methods ==========================================
///////////////////////////////////////////////////////////////////////////////
//+'lsns_alloc' allocates memory on host 
netdat *lsns_alloc( netpar &par )
{
#if defined ( __CUDA__ )
	par.NetDim.MaxIons = kdim3<cudametric>( par.NetDim.MaxIons );
	par.NetDim.MaxChan = kdim3<cudametric>( par.NetDim.MaxChan );
	par.NetDim.MaxCells = kdim3<cudametric>( par.NetDim.MaxCells );
	par.NetDim.MaxUnits = kdim3<cudametric>( par.NetDim.MaxUnits );
	par.NetDim.MaxWsyn = kdim3<cudametric>( par.NetDim.MaxWsyn );
	par.NetDim.MaxWS = kdim3<cudametric>( par.NetDim.MaxWS );
	par.NetDim.DevViewPars = kdim3<cudametric>( par.NetDim.MaxViewPars/4 );
#else
	par.NetDim.MaxIons = kdim3<x86metric>( par.NetDim.MaxIons );
	par.NetDim.MaxChan = kdim3<x86metric>( par.NetDim.MaxChan );
	par.NetDim.MaxCells = kdim3<x86metric>( par.NetDim.MaxCells );
	par.NetDim.MaxUnits = kdim3<x86metric>( par.NetDim.MaxUnits );
	par.NetDim.MaxWsyn = kdim3<x86metric>( par.NetDim.MaxWsyn );
	par.NetDim.MaxWS = kdim3<x86metric>( par.NetDim.MaxWS );
	par.NetDim.DevViewPars = kdim3<x86metric>( par.NetDim.MaxViewPars/4 );
#endif /* __CUDA__ */
	// allocate memory for 'netdat' structure
	par.Data = ( netdat *)malloc( sizeof( netdat )); __lsns_assert( par.Data != NULL );
	// calculate the dimension of arrays of actual parameters and look-up-tables
	par.NetDim.MaxGlobalData = 2*par.NetDim.MaxIons+2*par.NetDim.MaxChan+2*(par.NetDim.MaxCells+par.NetDim.MaxUnits)+par.NetDim.MaxWsyn+par.NetDim.MaxWS;
        par.NetDim.MaxGlobalLUT = ( 3+MAX_CHAN_PER_PUMP/4 )*par.NetDim.MaxIons+2*par.NetDim.MaxChan+(2+MAX_CHAN_PER_CELL/4+MAX_IPUMP_PER_CELL/4)*par.NetDim.MaxCells+(1+MAX_CHAN_PER_CELL/4)*par.NetDim.MaxUnits+par.NetDim.MaxWsyn+par.NetDim.MaxWS;
	// allocate memory for actual parameters and look-up-tables
	par.Data->Counter = ( uint4 * )malloc( sizeof( uint4 ) ); __lsns_assert( par.Data->Counter != NULL );
	par.Data->GlobalData = ( float4 *)malloc( sizeof( float4 )*par.NetDim.MaxGlobalData ); __lsns_assert( par.Data->GlobalData != NULL );
	par.Data->GlobalLUT = ( uint4 * )malloc( sizeof( uint4 )*par.NetDim.MaxGlobalLUT ); __lsns_assert( par.Data->GlobalLUT != NULL );
	par.Data->IOData.ViewLUT = ( uint4 * )malloc( sizeof( uint4 )*( par.NetDim.DevViewPars )); __lsns_assert( par.Data->IOData.ViewLUT != NULL );
	// initialization
	_sim_nstep( par.Data->Counter[0] ) = 0; _sim_maxstep( par.Data->Counter[0] ) = 0;
	// map data pointers to allocated memory
	lsns_data2mem( par.Data, &par.NetDim );
	// initialize allocated data&lut by default value 
#if defined( __LSNS_DEBUG__ )
	memset( par.Data->GlobalData, 0x00, sizeof( float4 )*par.NetDim.MaxGlobalData );
	memset( par.Data->GlobalLUT, 0x00, sizeof( uint4 )*par.NetDim.MaxGlobalLUT );
	memset( par.Data->IOData.ViewLUT, 0xFF, sizeof( uint4 )*( par.NetDim.DevViewPars ) ); // OK
#else
	memset( par.Data->GlobalData, 0x00, sizeof( float4 )*par.NetDim.MaxGlobalData );
	memset( par.Data->GlobalLUT, 0x00, sizeof( uint4 )*par.NetDim.MaxGlobalLUT );
	memset( par.Data->IOData.ViewLUT, 0x00, sizeof( uint4 )*( par.NetDim.DevViewPars ) );
#endif /*__LSNS_DEBUG__*/
	for( size_t i = 0; i < LSNS_MAX_SYNPARS; ++i ){	// initialize some parameters of synaptic summation
		switch( _synType( par.Synapses[i] )){
			case LSNS_SUM_SYN:{
				_synEdt( par.Synapses[i] ) = 0.f;
				_synDt( par.Synapses[i] ) = 1.f;
				}
				break;
			case LSNS_SIGMA_SYN:{
				_synEdt( par.Synapses[i] ) = 0.f;
				_synDt( par.Synapses[i] ) = 1.f;
				}
				break;
			case LSNS_PULSE_SYN:{
				_synEdt( par.Synapses[i] ) = exp( -par.Step/_synTmax( par.Synapses[i] ));
				_synDt( par.Synapses[i] ) = 1.f;
				}
				break;
		}
	}
	return par.Data;
}
///////////////////////////////////////////////////////////////////////////////
// +'lsns_free': frees both host and device memory
bool lsns_free( netpar &par )
{
	__lsns_assert( par.Data != NULL );
	// free device-specific memory
	lsns_free_dev( par.Data );
	// free host-specific memory for both actual parameters and look-up-tables
	__lsns_assert( par.Data->IOData.ViewLUT != NULL ); free( par.Data->IOData.ViewLUT );
	__lsns_assert( par.Data->GlobalLUT != NULL ); free( par.Data->GlobalLUT );
	__lsns_assert( par.Data->GlobalData != NULL ); free( par.Data->GlobalData ); 
	__lsns_assert( par.Data->Counter != NULL ); free( par.Data->Counter );
	// free memory for netdat structure
	free( par.Data ); par.Data = NULL;
	return true;
}
//=================== local methods ==========================================
///////////////////////////////////////////////////////////////////////////////
// +'lsns_data2mem' maps the specific pointer to allocated memory
bool lsns_data2mem( netdat *data, netdim *dim )
{
	// map the arrays of specific parameters onto the global memory
	data->Cells.IonsI	= 
	data->Ions.IonsI	= data->GlobalData;
	data->Channels.IonsE	= 
	data->Ions.IonsE	= data->Ions.IonsI+dim->MaxIons;
	data->Channels.ChanMH	= data->Ions.IonsE+dim->MaxIons;
        data->Units.ChanG       =
	data->Cells.ChanG	=
	data->Ions.ChanG	=
	data->Channels.ChanG	= data->Channels.ChanMH+dim->MaxChan;
	data->Ions.CellV	=
	data->Channels.CellV	=
	data->Cells.CellV	= data->Channels.ChanG+dim->MaxChan;
        data->Units.UnitV       = data->Cells.CellV+dim->MaxCells;
	data->Wsum.Srcs		= data->Cells.CellV;

	data->Cells.CellStat	= data->Units.UnitV+dim->MaxUnits;
	data->Units.UnitStat	= data->Cells.CellStat+dim->MaxCells;
	data->Wsum.Wsyn		= data->Units.UnitStat+dim->MaxUnits;

	data->Wsum.Wall		= data->Wsum.Wsyn+dim->MaxWsyn;
	// map the arrays of specific look-up-tables onto the global memory
	data->Ions.IonsType	= data->GlobalLUT;
	data->Ions.IonsLUT	= data->Ions.IonsType+dim->MaxIons;
	data->Ions.IChanGLUT	= data->Ions.IonsLUT+dim->MaxIons;
	data->Channels.ChanType	= data->Ions.IChanGLUT+dim->MaxIons*( MAX_CHAN_PER_PUMP/4+1 );
	data->Channels.ChanLUT	= data->Channels.ChanType+dim->MaxChan;
	data->Cells.IonsILUT	= data->Channels.ChanLUT+dim->MaxChan;
	data->Cells.VChanGLUT	= data->Cells.IonsILUT+dim->MaxCells*( MAX_IPUMP_PER_CELL/4+1 );
        data->Units.UChanGLUT	= data->Cells.VChanGLUT+dim->MaxCells*( MAX_CHAN_PER_CELL/4+1 );
	data->Wsum.SynType	= data->Units.UChanGLUT+dim->MaxUnits*( MAX_CHAN_PER_CELL/4+1 );
	data->Wsum.SrcLUT	= data->Wsum.SynType+dim->MaxWsyn;
        // initialize the pointer to all network parameters
	data->IOData.GlobalData	= data->GlobalData;
	// reset device-specific memory
	data->DevMap = NULL;
	return true;
}
#endif /* __LSNS_ENGINE_CPP */

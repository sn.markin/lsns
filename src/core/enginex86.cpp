///////////////////////////////////////////////////////////////////////////////
// x86-based architecture version
#include "engine.h"
#include "debug.h"
#include <chrono>

#if !defined ( __CUDA__ )

#define __LSNS_ENGINE_CPP

#include "util.h"

#include <stdlib.h> //Added by Taegyo
#include <string.h> //Added by Taegyo
#include <iostream> //Added by Taegyo

using namespace std;

#include "kernels.cpp"
#include "engine_common.cpp"
///////////////////////////////////////////////////////////////////////////////
// interface 
//=================== local methods ==========================================
///////////////////////////////////////////////////////////////////////////////
// +'lsns_map2dev': initialize the constant parameters, allocates memory on 
// device and copy the network configuration from host memory to device memory
bool lsns_map2dev( netpar &par )
{
	// make sure that size of constant parameters is not exceed 64K limit
	__lsns_assert( sizeof( float4 )+5*sizeof( uint4 )+sizeof( gatepar )*LSNS_MAX_GATEPARS+sizeof( ionspar )*LSNS_MAX_IONPARS < MAX_CONST_MEM );
	// setup the constant parameters of the network which are stored in constant memory for cuda version
	_sim_step( SimDat[0] ) = par.Step; 
	_sim_thresh( SimDat[0] ) = par.Threshold;
	_net_nions( IonDim[0] ) = par.NetDim.MaxIons;
	_net_nsyn( SynDim[0] ) = par.NetDim.MaxWsyn; 
	_net_nws( SynDim[0] ) = par.NetDim.MaxWS;
	_net_nchans( ChanDim[0] ) = par.NetDim.MaxChan;
	_net_ncells( NetDim[0] ) = par.NetDim.MaxCells; 
	_net_nunits( NetDim[0] ) = par.NetDim.MaxUnits;
	_net_maxvw( ViewDim[0] ) = par.NetDim.MaxGlobalData;  
	_net_nviews( ViewDim[0] ) = par.NetDim.DevViewPars;
	memcpy( Gates, par.Gates, sizeof( gatepar )*LSNS_MAX_GATEPARS ); //datatype error, not netpar but gatepar. 7.23 by Taegyo
	memcpy( Ions, par.Ions, sizeof( ionspar )*LSNS_MAX_IONPARS );
	memcpy( Synapses, par.Synapses, sizeof( synpar )*LSNS_MAX_SYNPARS );
	// allocate device-specific memory and copy initialized arrays from host memory to device memory (for non-cuda version this are pointers to the same arrays)
	lsns_alloc_dev( par.Data, &par.NetDim );
	return true;
}
///////////////////////////////////////////////////////////////////////////////
// +'lsns_alloc_dev' allocates device-specific memory and copy initialized arrays 
// from host memory to device memory (for non-cuda version this are pointers to the 
// same data structures)
bool lsns_alloc_dev( netdat *data, netdim *dim )
{
	int nviews = dim->DevViewPars;
	// allocate memory for DevMap structure
	data->DevMap = ( netdat *)malloc( sizeof( netdat )); __lsns_assert( data->DevMap != NULL );
	// copy pointers
	data->DevMap->Counter = data->Counter;		// cuda: allocate and copy
	// allocate and copy all network data (x86 implementation)
	data->DevMap->GlobalData = ( float4 *)malloc( sizeof( float4 )*dim->MaxGlobalData );
	memcpy( data->DevMap->GlobalData, data->GlobalData, sizeof( float4 )*dim->MaxGlobalData );
	data->DevMap->GlobalLUT = data->GlobalLUT;	// cuda: allocate and copy
	data->DevMap->IOData.ViewLUT = data->IOData.ViewLUT;    // cuda: allocate and copy
	data->DevMap->IOData.HostViewData = ( float4 *)malloc( sizeof( float4 )*nviews*MAX_STORED_STEPS ); __lsns_assert( data->DevMap->IOData.HostViewData  != NULL ); 
	for( int i = 0; i < MAX_STORED_STEPS; ++i ){
		data->DevMap->IOData.DevViewData[i] = data->DevMap->IOData.HostViewData+i*( nviews );
	}
	// 'HostViewData' should be pinned-memory for cuda version
	data->IOData.HostViewData = data->DevMap->IOData.HostViewData;
	// map data pointers to allocated memory
	lsns_data2mem( data->DevMap, dim );
	memset( data->DevMap->IOData.HostViewData, 0, sizeof( float4 )*nviews*MAX_STORED_STEPS );
	return true;
}
///////////////////////////////////////////////////////////////////////////////
// 'lsns_free_dev' free device specific memory and reset constant parameters of 
// the network
bool lsns_free_dev( netdat *data )
{
	// reset the constant parameters of the network
	_sim_step( SimDat[0] ) = -1.f;
	_sim_thresh( SimDat[0] ) = -10.f;
	_net_nions( IonDim[0] ) = 0;
	_net_nchans( ChanDim[0] ) = 0;
	_net_ncells( NetDim[0] ) = 0;
	_net_nunits( NetDim[0] ) = 0;
	_net_nsyn( SynDim[0] ) = 0;
	_net_nws( SynDim[0] ) = 0;
	_net_maxvw( ViewDim[0] ) = 0;
	_net_nviews( ViewDim[0] ) = 0;
	// free device-specific memory for actual parameters and look-up-tables (for non-cuda version simply free the memory allocated for DevMap structure)
	__lsns_assert( data->DevMap->GlobalData != NULL ); free( data->DevMap->GlobalData );
	__lsns_assert( data->DevMap->IOData.HostViewData != NULL ); free( data->DevMap->IOData.HostViewData );
	__lsns_assert( data != NULL && data->DevMap != NULL ); free( data->DevMap ); data->DevMap = NULL;
	return true;
}
//=================== global methods ==========================================
///////////////////////////////////////////////////////////////////////////////
// +'lsns_run' initialize simulation
bool lsns_reinit( netpar &par ) // call for each session
{
	memcpy( par.Data->DevMap->GlobalData, par.Data->GlobalData, sizeof( float4 )*par.NetDim.MaxGlobalData );
	lsns_refresh( par );
	return true;
}

bool lsns_refresh( netpar &par )
{
	// init before simulation
	_sim_nstep( par.Data->Counter[0] ) = _sim_nstep( par.Data->DevMap->Counter[0] ) = 0;  // SM
	memset( par.Data->DevMap->IOData.HostViewData, 0, sizeof( float4 )*MAX_NVIEWS*MAX_STORED_STEPS );
	return true;
}

bool lsns_reset( netpar &par ) // call for each trial
{
	memcpy( par.Data->DevMap->Channels.ChanG, par.Data->Channels.ChanG, sizeof( float4 )*MAX_CHANS );
	memcpy( par.Data->DevMap->Cells.CellV, par.Data->Cells.CellV, sizeof( float4 )*MAX_CELLS );
	for( int i = 0; i < MAX_CELLS; ++i ){
		float4 *v = par.Data->DevMap->Cells.CellV+i;
		v->x = float( ::normrd( Rng, v->x, 0.3 ));
	}
	return true;
}

void lsns_copy2dev( float4 *base, float4 *data, uint4 *lut, size_t sz4 )
{
	for( size_t i = 0; i < sz4; ++i ){
		__lsns_uint32 eposx = lut[i].x;
		__lsns_uint32 eposy = lut[i].y;
		__lsns_uint32 eposz = lut[i].z;
		__lsns_uint32 eposw = lut[i].w;
		if( eposx != 0xFFFFFFFF ){
			get_data( eposx, base ) = data[i].x;
		}
		if( eposy != 0xFFFFFFFF ){
			get_data( eposy, base ) = data[i].y;
		}
		if( eposz != 0xFFFFFFFF ){
			get_data( eposz, base ) = data[i].z;
		}
		if( eposw != 0xFFFFFFFF ){
			get_data( eposw, base ) = data[i].w;
		}
	}
}

void lsns_copy2host( float4 *base, float4 *data, uint4 *lut, size_t sz4 )
{
	for( size_t i = 0; i < sz4; ++i ){
		__lsns_uint32 eposx = lut[i].x;
		__lsns_uint32 eposy = lut[i].y;
		__lsns_uint32 eposz = lut[i].z;
		__lsns_uint32 eposw = lut[i].w;
		if( eposx != 0xFFFFFFFF ){
			data[i].x = get_data( eposx, base );
		}
		if( eposy != 0xFFFFFFFF ){
			data[i].y = get_data( eposy, base );
		}
		if( eposz != 0xFFFFFFFF ){
			data[i].z = get_data( eposz, base );
		}
		if( eposw != 0xFFFFFFFF ){
			data[i].w = get_data( eposw, base );
		}
//	printf("base = %x, offx = %u, offy = %u, offz = %u, offw = %u\n", base, eposx&0x3FFFFFFF, eposy&0x3FFFFFFF, eposz&0x3FFFFFFF, eposw&0x3FFFFFFF );
	printf("spike1 = %f, spike2 = %f, spike3 = %f, spike4 = %f\n", base[eposx&0x3FFFFFFF].z, base[eposy&0x3FFFFFFF].z, base[eposz&0x3FFFFFFF].z, base[eposw&0x3FFFFFFF].z );
	}
}

///////////////////////////////////////////////////////////////////////////////
// +'lsns_run' runs simulation
size_t lsns_transition( netpar &par, size_t nsteps )
{
	// common info
	#pragma omp parallel num_threads( 6 )
	{
		// ions
		uint4 *iontype = par.Data->DevMap->Ions.IonsType;
		uint4 *ionlut = par.Data->DevMap->Ions.IonsLUT;
		uint4 *ichanglut = par.Data->DevMap->Ions.IChanGLUT;
		float4 *ioni = par.Data->DevMap->Ions.IonsI;
		float4 *ione = par.Data->DevMap->Ions.IonsE;
		// weighted sums
		uint4 *syntype = par.Data->DevMap->Wsum.SynType;
		uint4 *srclut = par.Data->DevMap->Wsum.SrcLUT;
		float4 *src = par.Data->DevMap->Wsum.Srcs;
		float4 *wall = par.Data->DevMap->Wsum.Wall;
		float4 *wsyn  = par.Data->DevMap->Wsum.Wsyn;
		// channels
		uint4 *chantype = par.Data->DevMap->Channels.ChanType;
		uint4 *chanlut = par.Data->DevMap->Channels.ChanLUT;
		float4 *chanmh = par.Data->DevMap->Channels.ChanMH;
		float4 *chang = par.Data->DevMap->Channels.ChanG;
		// cells
		uint4 *changlut = par.Data->DevMap->Cells.VChanGLUT;
		uint4 *ionilut = par.Data->DevMap->Cells.IonsILUT;
		float4 *cellv = par.Data->DevMap->Cells.CellV;
		float4 *cellvstat = par.Data->DevMap->Cells.CellStat;
		// units
		uint4 *uchanglut = par.Data->DevMap->Units.UChanGLUT;
		float4 *unitv = par.Data->DevMap->Units.UnitV;
		// iodata
		for( size_t step = 0; step < nsteps; ++step ){
			#pragma omp for schedule( static )
				for( int ion = 0; ion < MAX_IONS; ++ion ){
					ions_kernel( ion, iontype, ionlut, ichanglut, chang, cellv, ione, ioni );
				}
			#pragma omp for schedule( static )
				for( int wsum = 0; wsum < MAX_NWSUM; ++wsum ){
					wsum_kernel( wsum, syntype, srclut, src, wall, wsyn );  // SM change kernel
				}
			#pragma omp for schedule( static )
				for( int chan = 0; chan < MAX_CHANS; ++chan ){
					chan_kernel( chan, chantype, chanlut, ione, wsyn, cellv, chanmh, chang );
				}
			#pragma omp for schedule( static )
				for( int cell = 0; cell < MAX_CELLS; ++cell ){
					cell_kernel( cell, changlut, ionilut, chang, ioni, cellv, cellvstat );
				}
			#pragma omp for schedule( static )
				for( int unit = 0; unit < MAX_UNITS; ++unit ){
					unit_kernel( unit, uchanglut, chang, unitv );
				}
		}
		#pragma omp for schedule( static )
			for( int cell = 0; cell < MAX_CELLS; ++cell ){
				resetstat_kernel( cell, cellvstat );
			}
	}
	return nsteps;
}

///////////////////////////////////////////////////////////////////////////////
// +'lsns_run' runs simulation
size_t lsns_run( netpar &par, vector<nns_iodata>& iodata, size_t nsteps )
{
#if defined( __LSNS_DEBUG__ )
	using msec = std::chrono::milliseconds;
	using get_time = std::chrono::steady_clock ;
	auto start = get_time::now();
#endif /*__LSNS_DEBUG__*/
	// common info
	float simstep = par.Step;
	uint4 *counter = par.Data->DevMap->Counter;
	size_t counter_ = _sim_nstep( counter[0] );
	nsteps = ( counter_ < nsteps )? nsteps-counter_: 0;
	#pragma omp parallel num_threads( 6 ) 
	{
		// ions
		uint4 *iontype = par.Data->DevMap->Ions.IonsType;
		uint4 *ionlut = par.Data->DevMap->Ions.IonsLUT;
		uint4 *ichanglut = par.Data->DevMap->Ions.IChanGLUT;
		float4 *ioni = par.Data->DevMap->Ions.IonsI;
		float4 *ione = par.Data->DevMap->Ions.IonsE;
		// weighted sums
		uint4 *syntype = par.Data->DevMap->Wsum.SynType;
		uint4 *srclut = par.Data->DevMap->Wsum.SrcLUT;
		float4 *src = par.Data->DevMap->Wsum.Srcs;
		float4 *wall = par.Data->DevMap->Wsum.Wall;
		float4 *wsyn  = par.Data->DevMap->Wsum.Wsyn;
		// channels
		uint4 *chantype = par.Data->DevMap->Channels.ChanType;
		uint4 *chanlut = par.Data->DevMap->Channels.ChanLUT;
		float4 *chanmh = par.Data->DevMap->Channels.ChanMH;
		float4 *chang = par.Data->DevMap->Channels.ChanG;
		// cells
		uint4 *changlut = par.Data->DevMap->Cells.VChanGLUT;
		uint4 *ionilut = par.Data->DevMap->Cells.IonsILUT;
		float4 *cellv = par.Data->DevMap->Cells.CellV;
		float4 *cellvstat = par.Data->DevMap->Cells.CellStat;
		// units
		uint4 *uchanglut = par.Data->DevMap->Units.UChanGLUT;
		float4 *unitv = par.Data->DevMap->Units.UnitV;
		// iodata
		uint4 *viewlut = par.Data->DevMap->IOData.ViewLUT;
		float4 **viewdata = par.Data->DevMap->IOData.DevViewData;
		float4 *hframedata = par.Data->IOData.HostViewData;
		float4 *globaldata = par.Data->DevMap->IOData.GlobalData;
		for( size_t step = 0; step < nsteps; ++step ){
			#pragma omp master
				control_kernel( 0, counter );
			#pragma omp for schedule( static )
				for( int ion = 0; ion < MAX_IONS; ++ion ){
					ions_kernel( ion, iontype, ionlut, ichanglut, chang, cellv, ione, ioni );
				}
			#pragma omp for schedule( static )
				for( int wsum = 0; wsum < MAX_NWSUM; ++wsum ){
					wsum_kernel( wsum, syntype, srclut, src, wall, wsyn );  // SM change kernel
				}
			#pragma omp for schedule( static )
				for( int chan = 0; chan < MAX_CHANS; ++chan ){
					chan_kernel( chan, chantype, chanlut, ione, wsyn, cellv, chanmh, chang );
				}
			#pragma omp for schedule( static )
				for( int cell = 0; cell < MAX_CELLS; ++cell ){
					cell_kernel( cell, changlut, ionilut, chang, ioni, cellv, cellvstat );
				}
			#pragma omp for schedule( static )
				for( int unit = 0; unit < MAX_UNITS; ++unit ){
					unit_kernel( unit, uchanglut, chang, unitv );
				}
			#pragma omp for schedule( static )
				for( int view = 0; view < MAX_NVIEWS; ++view ){
					int cnt = step%MAX_STORED_STEPS;
					store2dev_kernel( view, globaldata, viewlut, viewdata[cnt] );
				}
			#pragma omp master	// store date to global buffer
			{
				if ((step + 1) % MAX_STORED_STEPS == 0) {
					size_t global_step = counter_+step;
					for (size_t is = 0; is < MAX_STORED_STEPS; is++) {
						int iT = 0, totaln;
						for (size_t id = 0; id < iodata.size(); id++) {
							if (iodata[id].type == 16) {
								totaln = iodata[id].ntneu + iodata[id].nnsu;
							}
							else {
								totaln = iodata[id].ntneu;
							}
							for (int itn = 0; itn < totaln; itn++) {
								int tmp = itn / 4 + iT; //by TK 10/13/2015
#if !defined( __DATA_OUT__ )
								switch (itn % 4) {
								case 0:
									iodata[id].data[(global_step + 1 - MAX_STORED_STEPS + is)*totaln + itn] = hframedata[tmp + is*par.NetDim.DevViewPars].x;
									break;
								case 1:
									iodata[id].data[(global_step + 1 - MAX_STORED_STEPS + is)*totaln + itn] = hframedata[tmp + is*par.NetDim.DevViewPars].y;
									break;
								case 2:
									iodata[id].data[(global_step + 1 - MAX_STORED_STEPS + is)*totaln + itn] = hframedata[tmp + is*par.NetDim.DevViewPars].z;
									break;
								case 3:
									iodata[id].data[(global_step + 1 - MAX_STORED_STEPS + is)*totaln + itn] = hframedata[tmp + is*par.NetDim.DevViewPars].w;
									break;
								};
#else
								float temp;
 								switch( itn%4 ){
								case 0:
									temp = hframedata[tmp+is*par.NetDim.DevViewPars].x;
									break;
								case 1:
									temp = hframedata[tmp+is*par.NetDim.DevViewPars].y;
									break;
								case 2:
									temp = hframedata[tmp+is*par.NetDim.DevViewPars].z;
									break;
								case 3:
									temp = hframedata[tmp+is*par.NetDim.DevViewPars].w;
									break;
								};
								if (temp == 1) {
									std::cerr << itn << " " << temp+(step+1-MAX_STORED_STEPS+is)*simstep << endl;
								};
#endif //__DATA_OUT__
							};
							iT = iT + (int)ceil(totaln / 4.); //by TK 10/13/2015
						};
					};
				}
			}
		}
	}
#if defined( __LSNS_DEBUG__ )
	auto end = get_time::now();
	auto diff = end-start;
	cout<<"Elapsed time is :  "<< chrono::duration_cast<msec>(diff).count()<<" msec "<<endl;
#endif /*__LSNS_DEBUG__*/
	counter_ = _sim_nstep( counter[0] )-counter_; 
	return counter_; // return actual number of iterations were done
}

#endif

#include "netmap.h"
#include "utilities.h"
#include <algorithm>

__ENG_NAMESPASE_BEGIN
///////////////////////////////////////////////////////////////////////////////
// class tsyn: works with 'wsumdat'
wsum::pos wsum::Src0 = 0;
wsum::pos wsum::Trg0 = 0;
///////////////////////////////////////////////////////////////////////////////
// calculate hash value for weighed sum
wsum::hashws wsum::get_hash( const conmap &src )
{
	size_t hash = 0;
	for( size_t i = 0; i < src.size(); ++i ){
		hash_combine( hash, src[i].first );
		hash_combine( hash, src[i].second );
	}
	return hash;
}
///////////////////////////////////////////////////////////////////////////////
// create synapse for specific type of summation
bool wsum::create_syn( sumtype t, partype par )
{
	if( t < LSNS_MAX_SYNS && t > LSNS_CLONE_SYN){
		TrgMap.clear();
		WSMap.clear();
		Type = t;
		TrgMap.push_back( par );
		return true;
	}
	return false;
}
///////////////////////////////////////////////////////////////////////////////
// add targets to the synapse
bool wsum::add_trg( const partype trg, bool major )
{
	if( Type < LSNS_MAX_SYNS && Type > LSNS_CLONE_SYN){
		if(!( major && !TrgMap.empty() && trg == TrgMap[0] )){ // insert target if necessary
			TrgMap.push_back( trg );
			std::sort( TrgMap.begin(), TrgMap.end());
		}
		return true;
	}
	return false;
}
///////////////////////////////////////////////////////////////////////////////
// add connections to the synapse
bool wsum::add_src( const conmap &src )
{
	if( Type < LSNS_MAX_SYNS && Type > LSNS_CLONE_SYN ){
		WSMap.insert( WSMap.end(), src.begin(), src.end() );
		std::sort( WSMap.begin(), WSMap.end());
		WSMap.erase( std::unique( WSMap.begin(), WSMap.end() ), WSMap.end() ); // make sure to use unique connections
		return true;
	}
	return false;
}
///////////////////////////////////////////////////////////////////////////////
// map particular synapses and connections to network structure 
// (memory for weighted sum and synapses must be allocated before)
wsum::trg wsum::map2mem( netdat *data, synpar *par, std::vector<trg> &wsyn )
{
	wsyn.clear();
	pos srcnum4 = size4_src(); // specify number of sources
	for( pos src0 = wsum::Src0, wspos = wsum::Trg0, i = 0; i < ( pos )TrgMap.size(); ++wspos, ++i ){
		partype parpos = TrgMap[i];						// specify parameters of summation
		// initialize Wsyn for current synapse
		_wsyn_total( data->Wsum.Wsyn[wspos] ) = 0.f;				// total sum
		_wsyn_ah( data->Wsum.Wsyn[wspos] ) = _synA( par[parpos] );		// rate of transmitter release
		_wsyn_edt(data->Wsum.Wsyn[wspos]) = _synEdt(par[parpos]);		// parameter edt
		_wsyn_dt(data->Wsum.Wsyn[wspos]) = _synDt(par[parpos]);		// parameter dt
		// initialize SynType for current synapse
		_syn_type( data->Wsum.SynType[wspos] ) = ( i )? LSNS_CLONE_SYN: Type;	// type of summation
		_syn_par( data->Wsum.SynType[wspos] ) = parpos;				// parameters of summation
		_syn_size( data->Wsum.SynType[wspos] ) = ( i )? 0: srcnum4;		// number of sources for major weighted sum or 0
		_syn_lut( data->Wsum.SynType[wspos] ) = ( i )? wsum::Trg0: src0;	// offset for Wall and SrcLUT arrays for major weighted sum or offset of major weighted sum for minor synapse
		// initialize SrcLUT and Wall for current target (only ones for major weighted sum)
		for( pos cur_pos = src0, index = 0; ( i == 0 )&&( cur_pos < src0+srcnum4 ); ++cur_pos, index += 4 ){
			map_ws2mem( data->Wsum.SrcLUT+cur_pos, data->Wsum.Wall+cur_pos, index );
		}
		wsyn.push_back( std::make_pair( wspos, parpos ));			// store offset in data->Wsum.Wsyn array
	}
	wsum::Src0 += srcnum4;
	wsum::Trg0 += pos( wsyn.size() );
	return ( wsyn.size() )? wsyn[0]: std::make_pair( pos( -1 ), sumtype( -1 ));	// offset in data->Wsum.Wsyn array for major weighted sum
}
///////////////////////////////////////////////////////////////////////////////
// map 4 consequence weights and sources to correspondent w and src
void wsum::map_ws2mem( uint4 *src, float4 *w, pos index )
{
	float4 w_ = { 0.f, 0.f, 0.f, 0.f };
	uint4 src_= { 0, 0, 0, 0 };
	if( index < WSMap.size()){
		src_.x = WSMap[index].first;
		w_.x = WSMap[index].second;
	}
	if( ++index < WSMap.size()){
		src_.y = WSMap[index].first;
		w_.y = WSMap[index].second;
	} 
	if( ++index < WSMap.size()){
		src_.z = WSMap[index].first;
		w_.z = WSMap[index].second;
	} 
	if( ++index < WSMap.size()){
		src_.w = WSMap[index].first;
		w_.w = WSMap[index].second;
	} 
	*src = src_;
	*w = w_;
}
///////////////////////////////////////////////////////////////////////////////
// class mapsyn maps all synapses and connections to network structure 
// (memory for weighted sum and synapses must be allocated before)
///////////////////////////////////////////////////////////////////////////////
// add_synapse
void mapsyn::add_synapse( const syntype &syn, const syntrg &trg, wsum::conmap &src )
{
	syntrg_ st( syn.Gmax, trg.Syn, trg.Vm, trg.Eds );
	std::sort( src.begin(), src.end() );
	src.erase( std::unique( src.begin(), src.end() ), src.end() ); // make sure to use unique connections
	wsum::hashws hash = wsum::get_hash( src );
	std::pair<mit, mit> range = SynMap.equal_range( hash );
	for( mit x = range.first; x != range.second; ++x ){
		if( x->second.WSum.Type == syn.Type && x->second.WSum == src ){
			x->second.TrgMap.insert( std::make_pair( syn.Par, st ));
			x->second.WSum.add_trg( syn.Par, true ); // TODO: try insert major type of synapses (change the logic for adaptation)
			return;
		}
	}
	std::pair<partype, syntrg_> trg_new = std::make_pair( syn.Par, st );
	add_newsyn( syn.Type, trg_new, src, hash );
}
///////////////////////////////////////////////////////////////////////////////
// map2mem
void mapsyn::map2mem(  netdat *data, synpar *par )
{
	typedef trgmap_::map_::iterator trg_it;
	wsum::resetmap();
	std::vector<wsum::trg> ws_all;
	float4 *chang = data->Channels.ChanG;
	uint4 *chan_type = data->Channels.ChanType;
	uint4 *chan_lut = data->Channels.ChanLUT;
	uint4 *vchan_lut = data->Cells.VChanGLUT;
	for( mit _syn = SynMap.begin(); _syn != SynMap.end(); ++_syn ){
		ws_all.clear();
		wsum::trg ws0 = _syn->second.WSum.map2mem( data, par, ws_all );
		__lsns_assert( ws0.first != -1 && ws0.second != -1 );
		for( size_t j = 0; j < ws_all.size(); ++j ){
			std::pair<trg_it, trg_it> range = _syn->second.TrgMap.equal_range( ws_all[j].second );
			for( trg_it it = range.first; it != range.second; ++it ){
				pos chan_pos = it->second.Syn;				// position in ChanG
				pos cell_pos = it->second.Vm;				// position in CellV
				pos ion_pos = it->second.Eds;				// position in IonE
				_gate_typem( chan_type[chan_pos] ) = LSNS_SYNAPSE;	// channel activation - synapse
				_gate_parm( chan_type[chan_pos] ) = ws_all[j].second;	// activation parameters - parameters of synaptic summation
				_gate_typeh(chan_type[chan_pos]) = LSNS_NOGATE;		// channel activation - synapse
				_gate_parh(chan_type[chan_pos]) = LSNS_MAX_GATEPARS;	// activation parameters - parameters of synaptic summation
				_chan_lutv( chan_lut[chan_pos] ) = cell_pos;		// reference to membrane potential of target cell
				_chan_lute( chan_lut[chan_pos] ) = ion_pos;		// reference to reversal potential of correspondent ions
				_chan_lutm( chan_lut[chan_pos] ) = ws_all[j].first;	// reference to weighted sum in Wsyn array
				_chan_gmax( chang[chan_pos] ) = it->second.Gmax;	// maximal conductance
				_chan_g(chang[chan_pos]) = 0;						// conductance
				_chan_ge(chang[chan_pos]) = 0;						// G*Eds production
				_chan_i(chang[chan_pos]) = 0;						// current
				map2lut( vchan_lut, cell_pos, chan_pos, MAX_CHAN_PER_CELL ); // build up channel's look-up-table for cells
			}
		}
	}
}
///////////////////////////////////////////////////////////////////////////////
// add new synapse
void mapsyn::add_newsyn( syntype::sumtype type, std::pair<partype, syntrg_> &trg, const wsum::conmap &src, wsum::hashws hash )
{
	trgmap_ tm;
	tm.WSum.create_syn( type, trg.first );
	tm.TrgMap.insert( trg );
	synmap::iterator p = SynMap.insert( std::make_pair( hash, tm ));
	p->second.WSum.add_src( src );
}

__ENG_NAMESPASE_END

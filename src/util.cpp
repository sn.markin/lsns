#ifdef _WIN32 
#pragma warning (disable : 4996)
#endif 
#include "util.h"
#include <stdio.h>
#include <algorithm>

bool remove_char( const char *filename, char ch )
{
	using std::remove;
	FILE *file = fopen( filename, "r+b" );
	if( file ){
		fseek( file, 0, SEEK_END );
		long file_size = ftell( file );
		if( file_size > 0 ){
			fseek( file, 0, SEEK_SET );
			char *buffer = new char[file_size+1];
			size_t sz = fread( buffer, file_size, sizeof( char ), file );
			fclose( file );
			char *end_buffer = remove( buffer, buffer+file_size, 0x0D );
			size_t new_size = end_buffer-buffer;
			if( new_size != file_size ){
				file = fopen( filename, "w+b" );
				fwrite( buffer, new_size, sizeof( char ), file );
				fclose( file );
			}
			delete[] buffer;
			return true;
		}
	}
	return false;
}

#if !defined ( __CUDA__ )
///////////////////////////////////////////////////////////////////////////////
// see details in http://software.intel.com/en-us/articles/x87-and-sse-floating-point-assists-in-ia-32-flush-to-zero-ftz-and-denormals-are-zero-daz
#include <memory.h>

void mxcsr_on( int bit_num )
{
#if !defined( _WIN64 ) && defined ( __WINDOWS__ )
	__m128 state[32];
	__lsns_uint32 x;
	__asm fxsave state     
	memcpy(( void *)&x, ( char *)state+24, 4 );
	x |= ( 1 << bit_num );      
	__asm ldmxcsr   x  
#endif /* !_WIN64 */	
}

void mxcsr_off( int bit_num )
{
#if !defined( _WIN64 ) && defined ( __WINDOWS__ )
	__m128 state[32];
	__lsns_uint32 x;
	__asm fxsave state     
	memcpy(( void *)&x, ( char *)state+24, 4 );
	x &= ~( 1 << bit_num );
	__asm ldmxcsr x  
#endif /* !_WIN64 */	
}

void clear_flags( void )
{
#if !defined( _WIN64 ) && defined ( __WINDOWS__ )
	__m128 state[32];
	__lsns_uint32 x;
	__asm fxsave state     
	memcpy(( void *)&x, ( char *)state+24, 4 );
	x = x >> X87FLAGBITS;
	x = x << X87FLAGBITS;
	_asm ldmxcsr   x  
#endif /* !_WIN64 */	
}

void make_denormal()
{
#if !defined( _WIN64 ) && defined ( __WINDOWS__ )
	__m128 denormal;
	__lsns_uint32 den_vec[4] = { 1, 1, 1, 1 };
	memcpy( &denormal, den_vec, sizeof( __lsns_uint32 )*4 );
	denormal = _mm_add_ps( denormal, denormal );
#endif /* !_WIN64 */	
}
#endif /*! __CUDA__ */

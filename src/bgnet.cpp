#include "bgnet.h"

#include "myheader_all.h"
#include <functional> 
#include <algorithm> 

#define AMPL 0.87f

///////////////////////////////////////////////////////////////////////////////
// class bgnet
bgnet::bgnet( void ) :
	R( 0.f ), Rpre( 0.f ), DA( 0.f ), T( 4.0f ), IsLoaded( false ), 
	D1Syn( 9.0f, 44.f, 15.f, 95.f ), D2Syn( 9.f, 53.f, 15.f, 95.f ), M1Syn( 5.f, 55.f, 15.f, 75.f ), OutSyn( 10.f, 10.f, 80.f, 60.f )
{
	Phi0[0] = -0.832778f;
	Phi0[1] = 1.16426f;
	Xini = -float( L1*sin( Phi0[0] )+L2*sin( Phi0[1] ));
	Yini = float( L1*cos( Phi0[0] )+L2*cos( Phi0[1] ));
	D1Syn.make_synapse( 1.21f, 0.018f, 1.f, 0.5f );
	D2Syn.make_synapse( 1.2f, 0.04f, 1.f, 1.0f );
	M1Syn.make_synapse( 0.003f, 0.0005f );
	OutSyn.make_synapse( 0.0f, 0.0f );
}

bgnet::~bgnet( void )
{
}

bool bgnet::load_sim( const string &filename )
{
	if( !IsLoaded ){
		Fparser.parse( filename );
		// 1. create temporal file and work with it
		string _filename = filename;
	// TODO...
		// 2. remove 0x0d characters from temporal file (for compatibility reasons)
		#ifdef __LINUX__
			::remove_char( _filename.c_str(), 0x0D );
		#endif
		// 3. load model from temporal file
		create_proj( Pros, "new" );
		if( ::load_nns( _filename, Pros[0] ) == 1 ){
			::rename_proj( Pros, "new", Fparser.Basename );
			::prep_sim( Pros[0], Netpar );
			DA = R = Rpre = 0.f;
			IsLoaded = true;
		}
		// 4. remove temporal file
	// TODO...
		return IsLoaded;
	}
	return false;
}

bool bgnet::close_sim( void )
{
	if( IsLoaded ){
		::lsns_free( Netpar );
		::delete_iodata( Pros[0] );
		Fparser.clear();
		IsLoaded = false;
		return true;
	}
	return false;
}

bool bgnet::save_data( const string &suffix, const string &ext )
{
	if( IsLoaded ){
		string datfile = Fparser.Basename+string("-")+suffix+string( "." )+ext;
		::post_proc( Pros[0] );
		::save_as_dat( Pros[0], datfile, 0 );
		return true;
	}
	return false;
}

bool bgnet::init(  float rp, float t )
{
	if( IsLoaded ){
		if( Netpar.Data != NULL ){
			DA = 0.f; R = Rpre = rp; T =  t; D = 0.f; Y = 0.f;
			set_units( Pros[0], &Netpar );
			set_connections( Pros[0], &Netpar );
			biomech_init( &Netpar );
			custom_init( &Netpar );
			return true;
		}
	}
	return false;
}

bool bgnet::reinit( float rp, float t )
{
	if( IsLoaded ){
		if( Netpar.Data != NULL ){
			lsns_reinit( Netpar );
			return init( rp, t );
		}
	}
	return false;
}

bool bgnet::reset(  float rp, float t, bool total )
{
	if( IsLoaded ){
		if( Netpar.Data != NULL ){
			if( total ){
				DA = 0.f; R = Rpre = rp; T =  t; Y = 0.f;
				// TODO: reset all parameters of the model
			}
			lsns_reset( Netpar );
			// reset spike rate variables
			M1.reset_data( Netpar.Data );
			D1.reset_data( Netpar.Data );
			D2.reset_data( Netpar.Data );
			Gpe.reset_data( Netpar.Data );
			Gpi.reset_data( Netpar.Data );
			M1.export_data( Netpar.Data );
			D1.export_data( Netpar.Data );
			D2.export_data( Netpar.Data );
			Gpe.export_data( Netpar.Data );
			Gpi.export_data( Netpar.Data );
			// reset exploration
			CRT2STN.reset_data( Netpar.Data );
			CRT2TH.reset_data( Netpar.Data );
			CRT2D1.reset_data( Netpar.Data );
			CRT2D2.reset_data( Netpar.Data );
			CRT2STN.export_data( Netpar.Data );
			CRT2TH.export_data( Netpar.Data );
			CRT2D1.export_data( Netpar.Data );
			CRT2D2.export_data( Netpar.Data );
			return true;
		}
	}
	return false;
}

size_t bgnet::run_transition( size_t nsteps )
{
	if( IsLoaded ){
		size_t max_step = size_t( Pros[0].ctr_para.simduration/Pros[0].ctr_para.simstep );
		if( nsteps <= 0 || nsteps > max_step ){ 
			nsteps = max_step;
		}
		if( ::lsns_refresh( Netpar )){
			return ::lsns_transition( Netpar, nsteps );
		}
	}
	return 0;
}

size_t bgnet::run_sim( void )
{
	return run_nsteps( -1 );
}

size_t bgnet::run_nsteps( size_t nsteps )
{
	if( IsLoaded ){
		size_t max_step = size_t( Pros[0].ctr_para.simduration/Pros[0].ctr_para.simstep );
		if( nsteps <= 0 || nsteps > max_step ){ 
			nsteps = max_step;
		}
		if( ::lsns_refresh( Netpar )){
			return ::lsns_run( Netpar, Pros[0].iodata, nsteps );
		}
	}
	return 0;
}

void bgnet::export_data( void )
{
	PFC2M1.export_data( Netpar.Data );
	PFC2D1.export_data( Netpar.Data );
	PFC2D2.export_data( Netpar.Data );
}

void bgnet::import_data( void )
{
	M1.import_data( Netpar.Data );
	D1.import_data( Netpar.Data );
	D2.import_data( Netpar.Data );
	Gpe.import_data( Netpar.Data );
	Gpi.import_data( Netpar.Data );
}

void bgnet::learning( float T )
{
	msn_learning( T );
	ctx_learning( T );
	cbl_learning( T );
}

void bgnet::expl_stn( float d_expl, float d_crt )
{
	// change basic excitations
	Rng.seed( time( 0 ));
	for( size_t i = 0; i < CRT2STN.size(); ++i ){
		float w = CRT2STN[i]+d_crt+float( ::unird( Rng, d_expl ));
		w = ( w < W_M1MIN )? W_M1MIN: w;
		CRT2STN[i] = float( ::normrd( Rng, w, 0.01 ));
	}
	// update weights of connections 
	CRT2STN.export_data( Netpar.Data );
}

void bgnet::expl_th( float d_expl, float d_crt )
{
	// change basic excitations
	Rng.seed( time( 0 ));
	for( size_t i = 0; i < CRT2TH.size(); ++i ){
		float w = CRT2TH[i]+d_crt+float( ::unird( Rng, d_expl ));
		w = ( w < W_M1MIN )? W_M1MIN: w;
		CRT2TH[i] = float( ::normrd( Rng, w, 0.005 ));
	}
	// update weights of connections 
	CRT2TH.export_data( Netpar.Data );
}

void bgnet::expl_d1( float d_expl, float d_crt )
{
	// change basic excitations
	Rng.seed( time( 0 ));
	for( size_t i = 0; i < CRT2D1.size(); ++i ){
		float w = CRT2D1[i]+d_crt+float( ::unird( Rng, d_expl ));
		w = ( w < W_D1MIN )? W_D1MIN: ( w > W_D1MAX )? W_D1MAX: w;
		CRT2D1[i] = float( ::normrd( Rng, w, 0.01 ));
	}
	// update weights of connections 
	CRT2D1.export_data( Netpar.Data );
}

void bgnet::expl_d2( float d_expl, float d_crt )
{
	Rng.seed( time( 0 ));
	for( size_t i = 0; i < CRT2D2.size(); ++i ){
		float w = CRT2D2[i]+d_crt+float( ::unird( Rng, d_expl ));
		w = ( w < W_D2MIN )? W_D2MIN: ( w > W_D2MAX )? W_D2MAX: w;
		CRT2D2[i] = float( ::normrd( Rng, w, 0.01 ));
	}
	// update weights of connections 
	CRT2D2.export_data( Netpar.Data );
}

void bgnet::biomech( float target )
{
	size_t act_size = M1.size();
	size_t na = ( act_size > 100 )? 100: act_size;
	float phi[4] = { Phi0[0], Phi0[1], 0.f, 0.f };
	float y[100] = {0.0f}, th = 0.f,  S = AMPL;//0.8f
	for( size_t i = 0; i < na; ++i ){
		y[i] = OutSyn.calc_fth( M1[i] )-th;
		y[i] *= S;
	}
	reach( phi, y );
	float A = .2f;
	Xc = -float( L1*sin( phi[0] )+L2*sin( phi[1] ));
	Yc = float( L1*cos( phi[0] )+L2*cos( phi[1] ));
	Xt = Xini+A*cos( 2*M_PI*target/na );
	Yt = Yini+A*sin( 2*M_PI*target/na );
	D = hypot( Xc-Xt, Yc-Yt );
	int s = ( D > A )?1: -1;
	R = 0.0f;
	R = ( D < 0.2f )? 0.0f: 0.f;
	R = ( D < 0.13f )? 0.45f: R;
	R = ( D < 0.1f )? 0.99f: R;
	Y = s*R;
	calc_da();
}

void bgnet::dummymove( float target )
{
	size_t act_size = M1.size();
	size_t na = ( act_size > 100 )? 100: act_size;
	size_t na1 = 0;
	float y = 0, ymax = 0;
	for( size_t i = 0; i < na; ++i ){
		if( M1[i] > 0 ){
			y += i*M1[i];
			ymax += M1[i];
			na1++;
		}
	}
	if( ymax > 0.f ){
		y /= ymax;
		ymax /= na1;
		Y = y;
	}
	else{
		Y = -100;
		y = float( ::unird( Rng, target, 0.2 ));
	}
	float dy_i = fabs( y-target )/na;
	float dy_max = 0.4f-1.f/( 2.f+cosh(( ymax-30.f)/8.f ));
	dy_max = 0.f;
	R =  -1.0f*sqrt( fabs( dy_i*dy_i-dy_max*dy_max ));
	D = y-target;
	calc_da();
}

void bgnet::storelog( int trial, ofstream &log )
{
	log << "trial #" << trial << "; da = " << DA << "; D = " << D << "; Y = " << Y << std::endl;
}

void bgnet::storelog1( int trial, ofstream &log )
{
	log << trial << '\t' << Xc << '\t' << Yc << '\t' << Xt << '\t' << Yt << '\t' << 0 << '\t' << D << '\t' << D << '\t' << Xini << '\t' << Yini << '\t' << R-Rpre <<'\t'<< R << '\t' << Rpre << '\t' << DA << '\t' << endl;
}

void bgnet::storelog_D12( int trial, ofstream &log )
{
	size_t cue_size = 1;//PFC.size();
	size_t act_size = D1.size();
	log << "trial #" << trial << "; DA = " << DA << endl;
	for( size_t cue = 0; cue < cue_size; ++cue ){		// loop for active cues in PFC
		log << "cue #" << cue << endl;
		float FaD1 = 0.f, FaD2 = 0.f;
		float FeD1 = 0.f, FeD2 = 0.f;
		size_t n_d1 = 0, n_d2 = 0;
		for( size_t act = 0; act < act_size; ++act ){	// loop for all actions (all neurons in D1&D2)
			float f_d1 = D1[act];
			float f_d2 = D2[act];
			float w1 = PFC2D1[cue*act_size+act];	// PFC[cue]->D1[act] connection
			float w2 = PFC2D2[cue*act_size+act];	// PFC[cue]->D2[act] connection
			log << "\tact #\t" << act << ",\tw1 =\t" << w1 << ",\tf1 =\t" << f_d1 << ",\tw2 =\t" << w2 << ",\tf2 =\t" << f_d2 << endl;
			FaD1 += f_d1;
			FaD2 += f_d2;
			if( f_d1 > 0 ){
				FeD1 += f_d1;
				n_d1++;
			}
			if( f_d2 > 0 ){
				FeD2 += f_d2;
				n_d2++;
			}
		}
		FaD1 /= act_size;
		FaD2 /= act_size;
		FeD1 = ( n_d1 > 0 )? FeD1/n_d1: 0;
		FeD2 = ( n_d2 > 0 )? FeD2/n_d2: 0;
		log << "-------------------------------------------------------------------------------------------------"  << endl;
		log << "\taverage Fd1  = " << FaD1 << "\teffective Fd1 = " << FeD1 << "\taverage Fd2  = " << FaD2 << "\teffective Fd2 = " << FeD2 <<endl;
	}
}

void bgnet::storelog_M1( int trial, ofstream &log )
{
	size_t cue_size = 1;//PFC.size();
	size_t act_size = M1.size();
	log << "trial #" << trial << endl;
	for( size_t cue = 0; cue < cue_size; ++cue ){		// loop for active cues in PFC
		log << "cue #" << cue << endl;
		float Fa = 0.f;
		float Fe = 0.f;
		size_t n_effective = 0;
		for( size_t act = 0; act < act_size; ++act ){	// loop for all actions (all neurons in D1&D2)
			float f = M1[act];
			float w = PFC2M1[cue*act_size+act];	// PFC[cue]->M1[act] connection
			log << "\tact #\t" << act << ",\tw =\t" << w << ",\tf =\t" << f << endl;
			Fa += f;
			if( f > 0 ){
				Fe += f;
				n_effective++;
			}
		}
		Fa /= act_size;
		Fe = ( n_effective > 0 )? Fe/n_effective: 0;
		log << "-------------------------------------------------------------------------------------------------"  << endl;
		log << "\taverage F  = " << Fa << "\teffective F = " << Fe << endl;
	}
}

void bgnet::storelog_f( int trial, ofstream &log )
{
	for( size_t act_size = M1.size(), act = 0; act < act_size; ++act ){	// loop for all actions (all neurons in D1)
		log << M1[act] << '\t';
	}
	for( size_t act_size = D1.size(), act = 0; act < act_size; ++act ){	// loop for all actions (all neurons in D1)
		log << D1[act] << '\t';
	}
	for( size_t act_size = D2.size(), act = 0; act < act_size; ++act ){	// loop for all actions (all neurons in D1)
		log << D2[act] << '\t';
	}
	for( size_t act_size = Gpe.size(), act = 0; act < act_size; ++act ){	// loop for all actions (all neurons in D1)
		log << Gpe[act] << '\t';
	}
	for( size_t act_size = Gpi.size(), act = 0; act < act_size; ++act ){	// loop for all actions (all neurons in D1)
		log << Gpi[act] << '\t';
	}
	log << endl;
}

void bgnet::storelog_w( int trial, ofstream &log )
{
	for( size_t act_size = D1.size(), act = 0; act < act_size; ++act ){	// loop for all actions (all neurons in D1)
		log << PFC2D1[act] << '\t';
	}
	for( size_t act_size = D2.size(), act = 0; act < act_size; ++act ){	// loop for all actions (all neurons in D2)
		log << PFC2D2[act] << '\t';
	}
	for( size_t act_size = M1.size(), act = 0; act < act_size; ++act ){	// loop for all actions (all neurons in M1)
		log << PFC2M1[act] << '\t';
	}
	log << endl;
}

void bgnet::storelog_xy( int trial, ofstream &log )
{
	log << Xc << '\t' << Yc << '\t';	// store actual end-point
	log << Xt << '\t' << Yt << '\t';	// store desired end-point
	log << D << '\t';			// store distance
	log << Xini << '\t' << Yini << endl;	// store initial position
}

void bgnet::biomech_init( netpar *par )
{
	Phi0[0] = -0.832778f;
	Phi0[1] = 1.16426f;
	ifstream data( "ini\\phi0" );
	if( !data.is_open()){
		data.open( "phi0" );
	}
	if( data ){
		data >> Phi0[0] >> Phi0[1];
	}
	Xc = Xt = Xini = -float( L1*sin( Phi0[0] )+L2*sin( Phi0[1] ));
	Yc = Yt = Yini = float( L1*cos( Phi0[0] )+L2*cos( Phi0[1] ));
	reach_init();
}

void bgnet::set_units( nns_project &pro, netpar *par )
{
	M1.create( pro, "PMC", "SpikeStat" );
	D1.create( pro, "D1MSN", "SpikeStat" );
	D2.create( pro, "D2MSN", "SpikeStat" );
	Gpe.create( pro, "GPe", "SpikeStat" );
	Gpi.create( pro, "GPi", "SpikeStat" );

	M1.init_data( par->Data );
	D1.init_data( par->Data );
	D2.init_data( par->Data );
	Gpe.init_data( par->Data );
	Gpi.init_data( par->Data );
}

void bgnet::set_connections( nns_project &pro, netpar *par )
{
// create connectivity matrix
	CRT2PFC.create( pro, "PFC", "CRT", "SynpEx1" );  // create connectivity matrix between drive CRT and PFC population
	PFC2D1.create( pro, "D1MSN", "cuePFC", "SynpEx2" ); // create connectivity matrix between PFC and D1 population
	PFC2D2.create( pro, "D2MSN", "cuePFC", "SynpEx2" ); // create connectivity matrix between PFC and D2 population
	PFC2M1.create( pro, "PMC", "cuePFC", "SynpEx2" ); // create connectivity matrix between PFC and M1 population

	CRT2STN.create( pro, "STN", "CRT", "SynpEx1" );  // create connectivity matrix between drive CRT and STN population
	CRT2TH.create( pro, "TH", "CRT", "SynpEx1" );  // create connectivity matrix between drive CRT and TH population
	CRT2D1.create( pro, "D1MSN", "CRT", "SynpEx1" );  // create connectivity matrix between drive CRT and D1 population
	CRT2D2.create( pro, "D2MSN", "CRT", "SynpEx1" );  // create connectivity matrix between drive CRT and D2 population
// import actual data from the network to correspondent connectivity matrix 
	CRT2PFC.init_data( par->Data ); // import connectivity matrix
	PFC2D1.init_data( par->Data ); // import connectivity matrix
	PFC2D2.init_data( par->Data ); // import connectivity matrix
	PFC2M1.init_data( par->Data ); // import connectivity matrix

	CRT2STN.init_data( par->Data ); // import connectivity matrix
	CRT2TH.init_data( par->Data ); // import connectivity matrix
	CRT2D1.init_data( par->Data ); // import connectivity matrix
	CRT2D2.init_data( par->Data ); // import connectivity matrix
// activate specific #cue in PFC population
	delta_mod dlt_pfc( ACTIVE_CUE-1, 0.3f );
	CRT2PFC.modify( dlt_pfc );		// modify connectivity matrix
}

void bgnet::cbl_learning( float T ) // cerebellar learning
{
}

void bgnet::ctx_learning( float T ) // cortex learning (non-supervised)
{
	size_t cue_size = 1;//PFC.size();
	size_t act_size = M1.size();
	float f_pfc = 75.f/T;
	for( size_t cue = 0; cue < cue_size; ++cue ){	// loop for cues (all active cues of PFC)
		for( size_t act = 0; act < act_size; ++act ){		// loop for actions (all neurons of PMC)
			float f_pmc = M1[act]/T;
			float w = PFC2M1[cue*act_size+act];	// PFC[cue]->M1[act] connection
			ctx1_learning( f_pfc, f_pmc, w );
			w = float( ::normrd( Rng, w, 0.0001 ));
			PFC2M1[cue*act_size+act] = w;
		}
	}
}

void bgnet::msn_learning( float T ) // spiny learning (reinforcement)
{
	size_t cue_size = 1;//PFC.size();
	size_t act_size = D1.size();
	float f_pfc = 75.f/T;
	for( size_t cue = 0; cue < cue_size; ++cue ){		// loop for active cues in PFC
		for( size_t act = 0; act < act_size; ++act ){	// loop for all actions (all neurons in D1&D2)
			float f_d1 = D1[act]/T;
			float f_d2 = D2[act]/T;
			float w1 = PFC2D1[cue*act_size+act];	// PFC[cue]->D1[act] connection
			float w2 = PFC2D2[cue*act_size+act];	// PFC[cue]->D2[act] connection
			msn1_learning( DA, f_pfc, f_d1, f_d2, w1, w2 ); // reinforced learning for spiny neuron
			w1 = float( ::normrd( Rng, w1, 0.01 ));
			w2 = float( ::normrd( Rng, w2, 0.01 ));
			PFC2D1[cue*act_size+act] = w1;
			PFC2D2[cue*act_size+act] = w2;
		}
	}
} 

void bgnet::msn1_learning( float da, float pfc, float d1, float d2, float &w1, float &w2 )  // reinforced learning for single spiny neuron
{
	w1 = D1Syn.learn( w1, pfc, d1, da );
	w2 = D2Syn.learn( w2, pfc, d2, -da );
	if( w1 < W_D1MIN ){
		w1 = float( ::normrd( Rng, W_D1MIN, 0.01 ));
	}
	if( w2 < W_D2MIN ){
		w2 = float( ::normrd( Rng, W_D2MIN, 0.01 ));
	}
	if( w1 > W_D1MAX ){
		w1 = float( ::normrd( Rng, W_D1MAX, 0.01 ));
	}
	if( w2 > W_D2MAX ){
		w2 = float( ::normrd( Rng, W_D2MAX, 0.01 ));
	}
}

void bgnet::ctx1_learning( float pfc, float m1, float &w ) // learning for single cortex neuron
{
	w = M1Syn.learn( w, pfc, m1 );
	if( w < W_M1MIN ){
		w = W_M1MIN;
	}
	if( w > W_M1MAX ){
		w = W_M1MAX;
	}
}
